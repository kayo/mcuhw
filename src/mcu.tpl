[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (load "mcu.scm")
+][+ define h_body
+][+   for clk +][+ clk_decl +][+ endfor clk
+][+   for pio +][+ pio_decl +][+ endfor pio
+][+   for adc +][+ adc_decl +][+ endfor adc
+][+   for tim +][+ tim_decl +][+ endfor tim
+][+   for srt +][+ srt_decl +][+ endfor srt
+][+   for spi +][+ spi_decl +][+ endfor spi
+][+   for i2c +][+ i2c_decl +][+ endfor i2c
+][+   for iwd +][+ iwd_decl +][+ endfor iwd
+][+   irq_decl
+][+ enddef h_body
+][+ define c_body
+][+   for clk +][+ clk_impl +][+ endfor clk
+][+   for pio +][+ pio_impl +][+ endfor pio
+][+   for adc +][+ adc_impl +][+ endfor adc
+][+   for tim +][+ tim_impl +][+ endfor tim
+][+   for srt +][+ srt_impl +][+ endfor srt
+][+   for spi +][+ spi_impl +][+ endfor spi
+][+   for i2c +][+ i2c_impl +][+ endfor i2c
+][+   for iwd +][+ iwd_impl +][+ endfor iwd
+][+   irq_impl
+][+ enddef c_body
+][+ define h_head
+][+   (include-headers h-headers-list)
+][+   lds_load
+][+   (ld_declare_regions)
+][+ enddef h_head
+][+ define c_head
+][+   (include-headers c-headers-list)
+][+ enddef c_head
+][+ define h_tail
+][+   crc_decl
+][+   flash_decl
+][+   mcu_decl
+][+ enddef h_tail
+][+ define c_tail
+][+   crc_impl
+][+   flash_impl
+][+   mcu_impl
+][+ enddef c_tail
+][+ define ld_head
+][+   lds_load
+]/* Declare memory regions */
MEMORY {
[+      (ld_mapping_regions)
+]}
[+      (ld_provide_regions)
+][+ enddef ld_head
+]
