(define (target_tpl)
  (string-append
   (mcu_query mcu_targets
              mcu_targets_subdir)
   ".tpl"))

(define (mcu_make_query target table)
  (if (null? table)
      (error "Unsupported target")
      (if (~~ target (string-append
                      (string-downcase
                       (vector-ref
                        (car table)
                        0))
                      ".*"))
          (car table)
          (mcu_make_query target (cdr table)))))

(define (mcu_query table column)
  (vector-ref (mcu_make_query
               (get-down-name "target")
               table)
              column))

(define (has? key) ;; Key exist and non zero string
  (and (exist? key)
       (string? key)
       (not (string-null?
             (get key)))))

(define (oneof? val vals)
  (if (null? vals)
      #f
      (if (string=? val (string-downcase (car vals)))
          #t
          (oneof? val (cdr vals)))))

(define (is? key val . vals)
  (and (has? key)
       (oneof? (string-downcase (get key))
               (cons val vals))))

(define (is-true? key)
  (is? key "high" "on" "true" "yes" "1"))

(define (is-false? key)
  (is? key "low" "off" "false" "no" "0"))

(define (get-bool key)
  (cond ((is-true? key) #t)
        ((is-false? key) #f)))

(define (has-num? key)
  (and (has? key)
       (not (boolean?
             (string->number
              (get key))))))

(define (get-num key)
  (let* ([list (stack key)]
         [data (string->number (car list))]
         [unit (if (null? (cdr list)) "" (cadr list))]
         [mult (cond
                ((~~ unit "MHz") 1000000)
                ((~~ unit "KHz") 1000)
                ((~~ unit "mS") 0.001)
                ((~~ unit "uS") 0.000001)
                ((~~ unit "MBaud") 1000000)
                ((~~ unit "KBaud") 1000)
                ((~~ unit "Kb") (ash 1 10))
                ((~~ unit "Mb") (ash 1 20))
                (else 1))])
    (* data mult)))

(define (get-def-name key . pfx_sfx)
  (let ([pfx (if (null? pfx_sfx) "" (car pfx_sfx))]
        [sfx (if (or (null? pfx_sfx)
                     (null? (cdr pfx_sfx))) "" (cadr pfx_sfx))]
        [val (string-substitute
              (string-upcase (stack-join "." key))
              '("." "-" "*" "/") '("_" "_" "" "_DIV"))])
    (string-append pfx val sfx)))

(define (id-name name . maybe-id)
  (let ([id (if (null? maybe-id) "id" (car maybe-id))])
    (string-substitute
     (if (has? id)
         (string-append
          (get-down-name id)
          "_" name) name)
     '("." "-" "*" "/") '("_" "_" "" "_div"))))

(define (ID-NAME name . maybe-id)
  (string-upcase (apply id-name (cons name maybe-id))))

(define (get-default name default)
  (if (has? name)
      name
      default))

(define once-table '())

(define (once key)
  (if (assoc-ref once-table key)
      #f
      (let ()
        (set! once-table
          (assoc-set! once-table key #t))
        #t)))

(define (or-bits . args) ;; Join strings using binary or
  (let ([fields (filter (lambda (a)
                          (and (string? a) (not (string-null? a))))
                        args)])
    (if (null? fields) ""
        (if (null? (cdr fields))
            (car fields)
            (join " | " fields)))
    ))

(define (check-error lvl msg val)
  (cond ((= lvl 'error) (error msg))
        ((= lvl 'warn) (warn msg)))
  val)

(define (check-range lvl msg arg from to)
  (if (and (>= arg from) (<= arg to))
      arg (check-error lvl
                       (string-append msg " out of range "
                                      (number->string from) ".." (number->string to))
                       arg)))

(define (check-oneof lvl msg arg . args)
  (let ([vals (if (list? (car args)) (car args) args)])
    (if (in? arg vals)
        arg (check-error lvl (string-append msg " isn't one of "
                                            (join ", " (map number->string vals)))
                         arg))))

(define (ident) ;; Insert identation spaces
  (if (exist? "ident")
      (let* ([val (get "ident")]
             [num (string->number val)])
        (if num (make-string num #\space) val))
      ""))

(define (add-header headers path)
  (if (not (pair? (assoc path headers)))
      (assoc-set! headers path #t)))

(define (include-headers headers)
  (if (null? headers)
      ""
      (string-append
       (include-headers (cdr headers))
       "#include <" (caar headers) ">\n"
       )))

(define h-headers-list '())

(define (add-h-header path)
  (set! h-headers-list
    (add-header h-headers-list path)))

(define c-headers-list '())

(define (add-c-header path)
  (set! c-headers-list
    (add-header c-headers-list path)))

(define (max-uint bits)
  (- (ash 1 bits) 1))

(define (gen-sequence from to)
  (if (< from to)
      (cons from (gen-sequence (+ from 1) to))
      (if (< to from)
          (cons from (gen-sequence (- from 1) to))
          (list from))))

(define (slice-list lst from to)
  (let* ([len (length lst)]
         [num (- to from)]
         [from-lst (if (< len from)
                       '()
                       (list-tail lst from))]
         [from-len (- len from)])
    (if (< from-len num)
        from-lst
        (list-head from-lst num))))

(define (map-with-counters proc . args)
  (let ([idx 0]
        [num '()])
    (apply map-in-order
           (cons (lambda (elm)
                   (let* ([elm_idx idx]
                          [elm_has (assoc-ref num elm)]
                          [elm_num (if (number? elm_has)
                                       elm_has 0)])
                     (set! idx (+ 1 idx))
                     (set! num (assoc-set! num elm
                                           (+ 1 elm_num)))
                     (proc elm elm_idx elm_num)))
                 args)
           )))

(define (has-frequency? key)
  (let ([frequency-key
         (string-append key ".frequency")]
        [period-key
         (string-append key ".period")])
    (or (has-num? frequency-key)
        (has-num? period-key))))

(define (get-frequency key)
  (let ([frequency-key
         (string-append key ".frequency")]
        [period-key
         (string-append key ".period")])
    (cond
     ((has-num? frequency-key) (get-num frequency-key))
     ((has-num? period-key) (inexact->exact
                             (/ 1 (get-num period-key))))
     (else #f))
    ))

(define (select-divider req-divider . dividers)
  (apply min (filter
              (lambda (divider)
                (<= req-divider divider))
              dividers)))

(define (first-match-or-default function default maybe-list)
  (if (list? maybe-list)
      (let ([filtered (filter function maybe-list)])
        (if (null? filtered)
            default
            (car filtered)))
      default))

;; shared-element: (name condition)
;; vector: (name shared-list number fallback-handler)

(define (interrupts-enumerate-shared shared)
  (if (string? shared)
      (list shared "" "")
      (if (null? (cdr shared)) ;; no condition
          (list (car shared) "" "")
          (if (null? (cddr shared)) ;; no reset-condition
              (list (car shared) (cadr shared) "")
              shared))))

(define (interrupts-enumerate-vector vector default-num)
  (if (string? vector)
      (list vector '() default-num "blocking")
      (let* ([name (car vector)]
             [options (cdr vector)]
             [shared (first-match-or-default list? '() options)]
             [num (first-match-or-default number? default-num options)]
             [fallback (first-match-or-default string? "blocking" options)])
        (list name (map interrupts-enumerate-shared shared) num fallback))))

(define (interrupts-enumerate vectors . args)
  "Initializing vectors with numbers and set default handler to blocking"
  (let ([num (if (null? args) 0 (car args))])
    (if (null? vectors)
        '()
        (cons (interrupts-enumerate-vector (car vectors) num)
              (interrupts-enumerate (cdr vectors) (+ 1 num))))))

(define (get-interrupt-by-num vectors num)
  (let ([found-vectors (filter (lambda (vector)
                                 (= num (caddr vector))) vectors)])
    (if (not (null? found-vectors))
        (car found-vectors))))

(define (map-interrupts-sequence function vectors)
  (let ([num-list (map caddr vectors)])
    (map function (gen-sequence
                   (apply min num-list)
                   (apply max num-list)))))

(define interrupts-global-usage-table (make-hash-table))
(define interrupts-group-usage-tables (make-hash-table))

(define (add-interrupt name)
  "Add interrupt to usage table"
  (let* ([group (if (has? "id") (get "id") "")]
         [maybe-usage-table (hash-ref interrupts-group-usage-tables group)]
         [usage-table (if (hash-table? maybe-usage-table) maybe-usage-table
                          (let ([group-usage-table (make-hash-table)])
                            (hash-set! interrupts-group-usage-tables
                                       group group-usage-table)
                            group-usage-table))])
    (if (hash-ref interrupts-global-usage-table name)
        (error (string-append "Attempt to reuse already used interrupt: " name)))
    (hash-set! interrupts-global-usage-table name #t)
    (hash-set! usage-table name #t)
    ""))

(define (interrupt-global-used name-or-vector)
  (if (string? name-or-vector)
      (hash-ref interrupts-global-usage-table name-or-vector)
      (not (null?
            (filter interrupt-global-used
                    (cons (car name-or-vector)
                          (map car (cadr name-or-vector))))))))

(define (interrupt-group-used name-or-vector)
  (let* ([group (if (has? "id") (get "id") "")]
         [usage-table (hash-ref interrupts-group-usage-tables group)])
    (if (hash-table? usage-table)
        (if (string? name-or-vector)
            (hash-ref usage-table name-or-vector)
            (not (null?
                  (filter interrupt-group-used
                          (cons (car name-or-vector)
                                (map car (cadr name-or-vector)))))))
        #f)))

(define (number->uint8 num)
  (if (> 0 num)
      (+ 256 num)
      num))

(define callbacks-table '())

(define (callback-declaration name args cmt)
  (let* ([fn-rets (filter string? args)]
         [ret-type (if (null? fn-rets)
                       "void"
                       (car fn-rets))]
         [fn-args (filter pair? args)])
    (string-append
     (if (and (string? cmt) (not (string=? cmt "")))
         (string-append "/*\n * " cmt "\n */\n")
         "")
     "extern " ret-type " " name "("
     (if (null? args) "void"
         (join ", " (map (lambda (arg)
                           (string-append
                            (car arg) " "
                            (cdr arg))) args)))
     ");\n")))

(define (add-callback name . extra)
  ;; callback: name . '(return-type . ((argument-type argument-name) ...)) comment
  (let ([args (if (null? extra) '() (car extra))]
        [cmt (if (or (null? extra) (null? (cdr extra))) "" (cadr extra))]
        [cb-data (assoc-ref callbacks-table name)])
    (if (and (list? cb-data)
             (not (equal? (cdr cb-data) args)))
        (error (string-append
                "Attempt to add callback with same name: '"
                name
                "', but different signature: "
                (callback-declaration "" args "")
                ", opposing to: "
                (callback-declaration "" (cdr cb-data) ""))))
    (set! callbacks-table
      (assoc-set! callbacks-table
                  name
                  (cons cmt args)))))

(define (get-callback-name var name)
  (if (exist? var)
      (get-default var (id-name name))))

(define (get-cb-name var pfx)
  (get-callback-name var (string-append pfx "_" var)))

(define (add-callback-if-exist var name cmt . args)
  (if (exist? var)
      (add-callback
       (get-callback-name var name)
       args
       cmt)))

(define (add-cb-if-exist pfx var cmt . args)
  (apply add-callback-if-exist
         (cons var (cons (string-append pfx "_" var)
                         (cons cmt args)))))

(define (add-cbs-if-exist pfx . cbs)
  (if (not (null? cbs))
      (let ()
        (apply add-cb-if-exist
               (cons pfx (car cbs)))
        (apply add-cbs-if-exist
               (cons pfx (cdr cbs))))))

(define (run-callback name . args)
  (string-append "  " name "(" (join ", " args) ");\n"))

(define (run-callback-if-exist var name . args)
  (if (exist? var)
      (apply run-callback
             (cons (get-callback-name var name)
                   args))))

(define (run-cb-if-exist pfx var . args)
  (apply run-callback-if-exist
         (cons var (cons (string-append pfx "_" var) args))))

(define (callbacks-declarations)
  (define (declarations lst)
    (if (null? lst)
        ""
        (let* ([cb (car lst)]
               [name (car cb)]
               [cmt (cadr cb)]
               [args (cddr cb)])
          (string-append
           (declarations (cdr lst))
           "\n"
           (callback-declaration name args cmt)))))
  (let ([table callbacks-table])
    (set! callbacks-table '())
    (declarations table)))

(define (callback-invocation ident name args)
  (string-append ident name "(" (join ", " args) ");\n"))

(define (callback-reset-condition ident reset)
  (if (and (string? reset)
           (not (string=? "" reset)))
      (string-append ident reset ";\n")
      ""))

(define (callback-invocation-single reset name . args)
  (string-append
   (callback-reset-condition "  " reset)
   (callback-invocation "  " name args)))

(define (callback-invocation-multiple condition reset name . args)
  (if (and (string? condition)
            (not (string=? "" condition)))
      (string-append
       "  if (" condition ") {\n"
       (callback-reset-condition "    " reset)
       (callback-invocation "    " name args)
       "  }\n")
      (apply callback-invocation-single (cons reset (cons name args)))
      ))

;; callbacks: '((condition reset name . args))
(define (callbacks-invocation callbacks)
  (if (null? callbacks)
      "\n"
      (if (null? (cdr callbacks))
          ;; single callback (may skip condition)
          (apply callback-invocation-single (cdar callbacks))
          ;; multiple callbacks
          (apply string-append
                 (map (lambda (callback)
                        (apply callback-invocation-multiple callback))
                      callbacks))
          )))

;; bank entry: (id mode addr size)

;; area entry: (id mode bank size align)
(define ld_memory_table '())

(define (ld_add_area)
  (let* ([id (get "id")]
         [bank_id (get "bank")]
         [size (get-num "size")]
         [align (get "align")]
         [mode (get "mode")]
         [bank (assoc-ref mcu_memory_banks bank_id)])
    (if (not (list? bank))
        (error (string-append
                "Attempt to add memory area '" id
                "' to unknown bank '" bank_id "'"))
        (set! ld_memory_table
          (assoc-set! ld_memory_table id
                      (list (if (string? mode)
                                mode (car bank))
                            bank_id size
                            (if (string? align)
                                align "end")
                            ))))
    ""))

(define (ld_get_areas bank align)
  (filter
   (lambda (area)
     (and (or (not (string? bank))
              (string=? bank (caddr area)))
          (or (not (string? align))
              (string=? align (cadddr (cdr area))))))
   ld_memory_table))

(define (ld_bank_addr bank)
  (+ (caddr bank)
     (apply + (map (lambda (area) (cadddr area))
                   (ld_get_areas (car bank) "start")))))

(define (ld_bank_size bank)
  (- (cadddr bank)
     (apply + (map (lambda (area) (cadddr area))
                   (ld_get_areas (car bank) #f)))))

(define (ld_declare_region id cmt)
  (string-append
   "\n"
   (if (and (string? cmt) (not (string=? cmt "")))
       (string-append "/*\n * " cmt "\n */\n") "")
   "extern unsigned _" id "_start;\n"
   "extern unsigned _" id "_end;\n"
   ))

(define (ld_declare_regions)
  (define (declare_regions lst)
    (if (null? lst)
        ""
        (string-append
         (declare_regions (cdr lst))
         (let ([area (car lst)])
           (ld_declare_region
            (car area)
            (string-append
             "Declare memory region '"
             (car area) "'")))
         )))
  (declare_regions ld_memory_table))
