(define (irq_reg reg_name reg_num)
  (string-append "NVIC_" reg_name "(" (number->string reg_num) ")"))

(define (irq_reg_set reg vectors)
  (join "" (map (lambda (ireg)
                  (let ([bits (apply or-bits
                                     (map (lambda (vector)
                                            (if (and (= ireg (ash (number->uint8
                                                                   (caddr vector)) -5))
                                                     (interrupt-group-used vector))
                                                (string-append "(1 << ("
                                                               (number->string
                                                                (modulo
                                                                 (caddr vector)
                                                                 32)) "))")))
                                          vectors))])
                    (if (and (string? bits)
                             (not (string=? "" bits)))
                        (string-append "  " (irq_reg reg ireg) " |= " bits ";\n")
                        "")))
                '(0 1 2 3 4 5 6 7))))
