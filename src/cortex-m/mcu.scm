;; vectors: (vectors)
;; vector: "name" | ("name" (shared)? num? "fallback"?)
;; shared: "name" | ("name" "condition"? "reset-condition"?)

(define cortex-m_vectors
  '(("reset" -15 "reset")
    ("nmi" -14 "null")
    ("hard_fault" -13)
    ("sv_call" -5 "null")
    ("pend_sv" -2 "null")
    ("sys_tick" -1 "null")))

(define cortex-m3_m4_vectors
  '(("mem_manage" -12)
    ("bus_fault" -11)
    ("usage_fault" -10)
    ("debug_monitor" -4 "null")))

(define mcu_interrupts
  (apply append
         (map interrupts-enumerate
              (cond
               ((~~ mcu_core "cortex-m0")
                (list cortex-m_vectors
                      mcu_vectors))
               ((~~ mcu_core "cortex-m[34]")
                (list cortex-m_vectors
                      cortex-m3_m4_vectors
                      mcu_vectors))
               ))))

(define (isr_name name)
  (string-append (string-downcase name) "_handler"))

(define (isr_declaration name)
  (string-append "void " (isr_name name) "(void);\n"))

(define (isr_declarations vectors)
  (if (null? vectors)
      ""
      (string-append
       (let* ([vector (car vectors)]
              [name (car vector)])
         (if (interrupt-global-used vector)
             (isr_declaration name)
             ""))
       (isr_declarations (cdr vectors)))))

(define (isr_shared_handlers vectors)
  (if (null? vectors)
      ""
      (string-append
       (let* ([vector (car vectors)]
              [name (car vector)]
              [shared (cadr vector)])
         (if (and (not (null? shared))
                  (interrupt-global-used vector))
             (string-append
              "/*\n"
              " * Shared handler for '" name "'\n"
              " */\n"
              "void " (isr_name name) "(void);\n"
              "void " (isr_name name) "(void) {\n"
              (callbacks-invocation
               (map (lambda (vector)
                      (list (cadr vector)
                            (if (null? (cdr vector)) "" (caddr vector))
                            (isr_name (car vector))))
                    (filter (lambda (vector)
                              (interrupt-global-used
                               (car vector)))
                            shared)))
              "}\n")
             ""))
       (isr_shared_handlers (cdr vectors)))))

(define (isr_mapping vectors)
  (join ""
        (map-interrupts-sequence
         (lambda (num)
           (let ([vector (get-interrupt-by-num vectors num)])
             (if (list? vector)
                 (string-append
                  "  "
                  (isr_name (if (interrupt-global-used vector)
                                (car vector)
                                (cadddr vector)))
                  ", /* " (car vector) " (" (number->string num) ") */\n")
                 (string-append "  NULL, /* unused (" (number->string num) ") */\n"))
             )) vectors)))

(define (mcu_memory_prefill)
  (is-true? "memory.usage.prefill"))

(define (mcu_memory_pattern)
  (if (has? "memory.usage.pattern")
      (get "memory.usage.pattern") "0x55"))

(define (mcu_memory_freelen)
  (if (has-num? "memory.usage.freelen")
      (check-range
       'warn "memory.usage.freelen"
       (get-num "memory.usage.freelen")
       4 32) 8))
