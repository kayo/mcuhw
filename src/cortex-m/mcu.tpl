[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+  (load "mcu.scm")
+][+  (add-c-header "cortex-m/common.h")
+][+  (add-c-header "cortex-m/memorymap.h")
+][+  (add-c-header "cortex-m/scb.h")
+][+  (add-c-header "cortex-m/nvic.h")
+][+  define mcu_copying
+]/*
 * The content of this file derived from libopencm3 project.
 *
 * Copyright (C) 2010 Piotr Esden-Tempski <piotr@esden.net>,
 * Copyright (C) 2012 chrysn <chrysn@fsfe.org>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */
[+    enddef mcu_copying
+][+  define mcu_decl
+]
/*
 * No initialized data
 */
#define NOINIT_ATTR __attribute__((section(".noinit")))

[+    if (< 0 (car mcu_backup))
+]/*
 * Data at battery backup domain
 */
#define BACKUP_ATTR __attribute__((section(".backup"), aligned([+ (cadr mcu_backup) +])))
[+        bkp_decl
+][+    endif (< 0 (car mcu_backup))
+]/*
 * Data memory barrier
 */
static inline void mcu_data_mem_barrier(void) {
  asm volatile ("dmb");
}

/*
 * Data synchronization barrier
 */
static inline void mcu_data_sync_barrier(void) {
  asm volatile ("dsb");
}

/*
 * Instruction synchronization barrier
 */
static inline void mcu_instr_sync_barrier(void) {
  asm volatile ("isb");
}

/*
 * Stalls the CPU until the interrupt has happened
 */
static inline void mcu_wait_for_interrupt(void) {
  asm volatile ("wfi");
}

/*
 * Stalls the CPU until the event has happened
 */
static inline void mcu_wait_for_event(void) {
  asm volatile ("wfe");
}

/*
 * Get master stack pointer
 */
static inline void *mcu_get_master_stack(void) {
  register void *top;
  asm volatile ("mrs %0, msp" : "=r" (top));
  return top;
}

/*
 * Set master stack pointer
 */
static inline void mcu_set_master_stack(void *top) {
  asm volatile ("msr msp, %0" : : "r" (top) : );
}

/*
 * Get process stack pointer
 */
static inline void *mcu_get_process_stack(void) {
  register void *top;
  asm volatile ("mrs %0, psp" : "=r" (top));
  return top;
}

/*
 * Set process stack pointer
 */
static inline void mcu_set_process_stack(void *top) {
  asm volatile ("msr psp, %0" : : "r" (top) : );
}

/*
 * Disable the interrupt mask and enable interrupts globally
 */
static inline void mcu_enable_interrupts(void) {
  asm volatile ("CPSIE I\n");
}

/*
 * Mask all interrupts globally
 */
static inline void mcu_disable_interrupts(void) {
  asm volatile ("CPSID I\n");
}

/*
 * Disable the HardFault mask and enable fault interrupt globally
 */
static inline void mcu_enable_faults(void) {
  asm volatile ("CPSIE F\n");
}

/*
 * Mask the HardFault interrupt globally
 */
static inline void mcu_disable_faults(void) {
  asm volatile ("CPSID F\n");
}

/*
 * Checks, if interrupts are masked (disabled).
 *
 * Returns true when interrupts are disabled.
 */
__attribute__((always_inline))
static inline bool mcu_masked_interrupts(void) {
  register uint32_t res;
  asm volatile ("MRS %0, PRIMASK"  : "=r" (res));
  return res;
}

/*
 * Checks, if HardFault interrupt is masked (disabled).
 *
 * Returns true when HardFault interrupt is disabled.
 */
__attribute__((always_inline))
static inline bool mcu_masked_faults(void) {
  register uint32_t res;
  asm volatile ("MRS %0, FAULTMASK"  : "=r" (res));
  return res;
}

/*
 * This function switches the mask of the interrupts. If mask is true, the
 * interrupts will be disabled. The result of this function can be used for
 * restoring previous state of the mask.
 *
 * The mask is a new state of the interrupt mask
 * Returns the old state of the interrupt mask
 */
__attribute__((always_inline))
static inline uint32_t mcu_mask_interrupts(uint32_t msk) {
  register uint32_t old;
  asm volatile ("MRS %0, PRIMASK"  : "=r" (old));
  asm volatile (""  : : : "memory");
  asm volatile ("MSR PRIMASK, %0" : : "r" (msk));
  return old;
}

/*
 * This function switches the mask of the HardFault interrupt. If mask is true,
 * the HardFault interrupt will be disabled. The result of this function can be
 * used for restoring previous state of the mask.
 *
 * The mask is a new state of the HardFault interrupt mask
 * Returns the old state of the HardFault interrupt mask
 */
__attribute__((always_inline))
static inline uint32_t mcu_mask_faults(uint32_t msk) {
  register uint32_t old;
  asm volatile ("MRS %0, FAULTMASK"  : "=r" (old));
  asm volatile (""  : : : "memory");
  asm volatile ("MSR FAULTMASK, %0" : : "r" (msk));
  return old;
}

/*
 * Do not populate this definition outside
 */
static inline uint32_t __mcu_atomic_set__(uint32_t *val) {
  return mcu_mask_interrupts(*val);
}

#define __MCU_ATOMIC__                                    \
  uint32_t __mcu_atomic_val__ = true, __mcu_atomic_save__ \
  __attribute__((__cleanup__(__mcu_atomic_set__))) =      \
  __mcu_atomic_set__(&__mcu_atomic_val__)

/*
 * The context for atomic operations
 */
#define mcu_atomic_context() __MCU_ATOMIC__

/*
 * The block for atomic operations
 */
#define mcu_atomic_block()                               \
  for (__MCU_ATOMIC__, __mcu_atomic_once__ = true;       \
       __mcu_atomic_once__; __mcu_atomic_once__ = false)

/*
 * The memory usage
 */
typedef struct {
  /*
   * Max used heap (bytes)
   *
   * The maximum size of heap memory, which been used by application.
   */
  size_t heap_usage;
  /*
   * Max used stack (bytes)
   *
   * The maximum size of stack, which been required by application.
   */
  size_t stack_usage;
} mcu_memory_usage_t;

/*
 * Measure actual memory usage
 *
 * You may use this function to get real used memory in any reasonable place.
 * Usually you need call this after several full runs of mainloop of application.
 */
[+ if (mcu_memory_prefill)
+]void mcu_memory_usage(mcu_memory_usage_t *usage);
[+ else (mcu_memory_prefill)
+]#define mcu_memory_usage(usage) /* Enable 'memory.prefill' to use this */
[+ endif (mcu_memory_prefill)
+]
typedef enum {
  /*
   * Do not modify any modes
   */
  mcu_modes_notouch      = 0,
  /*
   * Wake up the CPU on pending interrupts from wake-for-event
   * This acts even if interrupts is disabled.
   */
  mcu_event_on_interrupt = 1 << 4,
  /*
   * Activate deep sleep mode
   */
  mcu_deep_sleep         = 1 << 2,
  /*
   * Automatic stall the CPU when exiting from interrupt service routine
   */
  mcu_sleep_on_exit      = 1 << 1,
} mcu_modes_t;

/*
 * Modify the MCU operation modes
 */
void mcu_modes_on_off(mcu_modes_t on,
                      mcu_modes_t off);

#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
/*
 * Reset CPU core
 */
void mcu_reset_core(void);
#endif

/*
 * Do system reset
 */
void mcu_reset_system(void);
[+  enddef mcu_decl
+][+  define mcu_impl
+]
void mcu_modes_on_off(mcu_modes_t on,
                      mcu_modes_t off) {
  SCB_SCR = (SCB_SCR & ~off) | on;
}

/* Those are defined only on CM3 or CM4 */
#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
void mcu_reset_core(void) {
  SCB_AIRCR = SCB_AIRCR_VECTKEY | SCB_AIRCR_VECTRESET;
  // mcu_data_sync_barrier();
  for (;;);
}
#endif

void mcu_reset_system(void) {
  SCB_AIRCR = SCB_AIRCR_VECTKEY | SCB_AIRCR_SYSRESETREQ;
  // mcu_data_sync_barrier();
  for (;;);
}

/* Symbols exported by the linker script(s): */
extern unsigned _data_loadaddr, _data, _edata, _ebss, _stack, end;
typedef void (*funcp_t) (void);
extern funcp_t __preinit_array_start, __preinit_array_end;
extern funcp_t __init_array_start, __init_array_end;
extern funcp_t __fini_array_start, __fini_array_end;
[+ if (mcu_memory_prefill)
+]
void mcu_memory_usage(mcu_memory_usage_t *usage) {
  int i;
  volatile char *ptr = mcu_get_master_stack();

  /* seek to end of pattern (max stack top) */
  for (i = 0; i != [+ (mcu_memory_freelen) +] && ptr >= (char*)&end; ptr--) {
    if (*ptr == [+ (mcu_memory_pattern) +]) {
      i++;
    } else {
      i = 0;
    }
  }
  usage->stack_usage = (char*)&_stack - ptr - [+ (mcu_memory_freelen) +] - 1;

  /* seek to beginning of pattern (max heap usage) */
  volatile char *stk = ptr;
  ptr = (char*)&end;
  for (i = 0; i != [+ (mcu_memory_freelen) +] && ptr < stk; ptr++) {
    if (*ptr == [+ (mcu_memory_pattern) +]) {
      i++;
    } else {
      i = 0;
    }
  }

  usage->heap_usage = ptr - (char*)&end - [+ (mcu_memory_freelen) +];
}
[+ endif (mcu_memory_prefill)
+]
void main(void);
void reset_handler(void);
void blocking_handler(void);
void null_handler(void);

void __attribute__ ((weak, naked))
reset_handler(void) {
[+ if (not (and (is-false? "system.startup.data")
                (is-false? "system.startup.bss")))
+]  {
    volatile unsigned *src = &_data_loadaddr;
    volatile unsigned *dst = &_data;
[+   if (not (is-false? "system.startup.data"))
+]    for (; dst < &_edata; *dst++ = *src++);
[+   endif
+][+ if (not (is-false? "system.startup.bss"))
+]    for (; dst < &_ebss; *dst++ = 0);
[+   endif
+]  }
[+ endif
+][+ if (eq? (car mcu_system) 'stack-align-8-byte-ensure)
+]
  /* Ensure 8-byte alignment of stack pointer on interrupts */
  /* Enabled by default on most Cortex-M parts, but not M3 r1 */
  SCB_CCR |= SCB_CCR_STKALIGN;
[+ endif
+][+ if (mcu_memory_prefill)
+]
  { /* Prefill free memory using pattern */
    static const union {
      unsigned val;
      unsigned char arr[sizeof(unsigned)];
    } pat = { .arr = { [0 ... sizeof(unsigned)-1] = [+ (mcu_memory_pattern) +] } };
    volatile unsigned *dst = (unsigned*)mcu_get_master_stack() - 1;
    for (; dst >= &end; *dst-- = pat.val);
  }
[+ endif (mcu_memory_prefill)
+]  /* might be provided by platform specific vector.c */
[+ mcu_pre_main
+]
[+ if (not (is-false? "system.startup.preinit"))
+]  { /* Call early constructors/initializers */
    funcp_t *fp = &__preinit_array_start;
    for (; fp < &__preinit_array_end; (*fp++)());
 }
[+ endif (not (is-false? "system.startup.preinit"))
+][+ if (not (is-false? "system.startup.init"))
+]  { /* Call constructors/initializers */
    funcp_t *fp = &__init_array_start;
    for (; fp < &__init_array_end; (*fp++)());
  }
[+ endif (not (is-false? "system.startup.init"))
+]
  /* Call the application's entry point */
  main();
[+ if (not (is-false? "system.startup.fini"))
+]  { /* Call destructors/finalizers */
    funcp_t *fp = &__fini_array_start;
    for (; fp < &__fini_array_end; (*fp++)());
  }
[+ endif (not (is-false? "system.startup.fini"))
+]}

void blocking_handler(void) {
  for (; ; );
}

void null_handler(void) {
  /* Do nothing. */
}
[+    (isr_shared_handlers mcu_interrupts)
+]
__attribute__ ((section(".vectors")))
void *vector_table[] = {
  &_stack,
[+      (isr_mapping mcu_interrupts)
+]};
[+    enddef mcu_impl
+]
