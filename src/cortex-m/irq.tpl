[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+  (load "irq.scm")
+][+  define irq_decl
+]
/*
 * Enable interrupt queries
 */
void [+ (id-name "irq_init") +](void);

/*
 * Disable interrupt queries
 */
void [+ (id-name "irq_done") +](void);

/*
 * User-defined callbacks
 */
[+      (callbacks-declarations)
+][+  enddef irq_decl
+][+  define irq_impl
+]
void [+ (id-name "irq_init") +](void) {
[+     (irq_reg_set "ISER" mcu_interrupts)
+]}

void [+ (id-name "irq_done") +](void) {
[+     (irq_reg_set "ICER" mcu_interrupts)
+]}
[+    enddef irq_impl
+][+  define irq_isr
+][+    (add-interrupt (get "name"))
+]
/*
 * Interrupt handler [+ (get "name") +]
 */
void [+ (isr_name (get "name")) +](void);
void [+ (isr_name (get "name"))
+](void) [+ enddef irq_isr
+][+  define irq_isr_cb
+][+    if (exist? (get "cb"))
+][+      irq_isr
+]{
[+          (run-cb-if-exist (get "pfx") (get "cb"))
+]}
[+      endif (exist? (get "cb"))
+][+  enddef irq_isr_cb
+]
