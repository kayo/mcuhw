(defun mcu-intr-fmt ()
  "Convert interrupt queries and routines table"
  (interactive)
  (save-excursion
    (goto-char (region-beginning))
    (replace-regexp ",[[:space:]]*$" ""))
  (save-excursion
    (goto-char (region-beginning))
    (insert "(define irq_vectors\n'("))
  (save-excursion
    (goto-char (region-end))
    (insert "))"))
  (indent-region))

(defun mcu-db-fmt ()
  "Convert space-delimited export to scheme."
  (interactive)
  (save-excursion
    (goto-char 0)
    (replace-regexp "^" "#(")
    (goto-char 0)
    (replace-regexp "$" ")")
    (goto-char 0)
    (replace-regexp "\"-\"" "#f")
    (goto-char 0)
    (replace-regexp "\"\\([-]?[.[:digit:]]+\\)\"" "\\1")))
