[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (load "f0/mcu.scm")
+][+ include "all/ld.tpl"
+][+ include "cortex-m/mcu.tpl"
+][+ include "cortex-m/irq.tpl"
+][+ (add-c-header "stm32/f0/memorymap.h")
+][+ include "all/mcu.tpl"
+][+ include "all/dma.tpl"
+][+ include "all/dma_v1.tpl"
+][+ include "all/tim.tpl"
+][+ include "f0/clk.tpl"
+][+ include "f0/pio.tpl"
+][+ include "f0/adc.tpl"
+][+ include "f0/srt.tpl"
+][+ include "all/spi_f03.tpl"
+][+ include "all/i2c_v2.tpl"
+][+ include "all/crc_v2.tpl"
+][+ include "all/iwd_f03.tpl"
+]
