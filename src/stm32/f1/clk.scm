(define (clk_APB1_frequency) (clk_APBx_frequency "1"))
(define (clk_APB2_frequency) (clk_APBx_frequency "2"))

(define (clk_RTC_frequency) (clk_RTC_frequency_HSE/x 128))

(define (clk_ADC_frequency)
  (if (exist? "ADC")
      (/ (clk_APB2_frequency)
         (if (has-num? "ADC.divider")
             (check-oneof 'error "ADC.divider"
                          (get-num "ADC.divider")
                          2 4 6 8)
             2))
      0))

(define clk_RCC_CFGR_ADCPRE #t)

(define (clk_RCC_CFGR2) (clk_RCC_CFGR2_PREDIVx_PLLy "1" ""))

(define clk_RCC_AHB_periph
  '("ETHMAC" "OTGFS" "SDIO" "FSMC" "CRC" "FLITF" "SRAM" "DMA2" "DMA1"))

(define clk_RCC_APB1_periph
  '("CEC" "DAC" "PWR" "BKP" "CAN2" "CAN1" "USB" "I2C2" "I2C1" "UART5" "UART4" "USART3" "USART2" "SPI3" "SPI2" "WWDG" "TIM14" "TIM13" "TIM12" "TIM7" "TIM6" "TIM5" "TIM4" "TIM3" "TIM2"))

(define clk_RCC_APB2_periph
  '("TIM17" "TIM16" "TIM15" "ADC3" "USART1" "TIM8" "SPI1" "TIM1" "ADC2" "ADC1" "GPIOG" "GPIOF" "GPIOE" "GPIOD" "GPIOC" "GPIOB" "GPIOA" "AFIO"))

(define (clk_SYSCFG_CFGR1_msk)
  (or-bits))

(define (clk_SYSCFG_CFGR1_set)
  (or-bits))

(define (clk_SYSCFG_CFGR1_clr)
  (or-bits))

(define (clk_AFIO_MAPR_msk)
  (or-bits
   (if (exist? "SWJ")
       "AFIO_MAPR_SWJ_CFG_MASK")

   (if (exist? "ADC2.remap.ext_trig.regular")
       "AFIO_MAPR_ADC2_ETRGREG_REMAP")
   (if (exist? "ADC2.remap.ext_trig.injected")
       "AFIO_MAPR_ADC2_ETRGINJ_REMAP")
   (if (exist? "ADC1.remap.ext_trig.regular")
       "AFIO_MAPR_ADC1_ETRGREG_REMAP")
   (if (exist? "ADC1.remap.ext_trig.injected")
       "AFIO_MAPR_ADC1_ETRGINJ_REMAP")
   
   (if (and (exist? "PD0.remap")
            (exist? "PD1.remap"))
       "AFIO_MAPR_PD01_REMAP")

   (if (exist? "TIM5.remap")
       "AFIO_MAPR_TIM5CH4_REMAP")
   (if (exist? "TIM4.remap")
       "AFIO_MAPR_TIM4_REMAP")
   (if (exist? "TIM3.remap")
       "AFIO_MAPR_TIM3_REMAP")
   (if (exist? "TIM2.remap")
       "AFIO_MAPR_TIM2_REMAP")
   (if (exist? "TIM1.remap")
       "AFIO_MAPR_TIM1_REMAP")
   
   (if (exist? "USART3.remap")
       "AFIO_MAPR_USART3_REMAP")
   (if (exist? "USART2.remap")
       "AFIO_MAPR_USART2_REMAP")
   (if (exist? "USART1.remap")
       "AFIO_MAPR_USART1_REMAP")

   (if (exist? "I2C1.remap")
       "AFIO_MAPR_I2C1_REMAP")

   (if (exist? "SPI1.remap")
       "AFIO_MAPR_SPI1_REMAP")

   (if (exist? "CAN1.remap")
       "AFIO_MAPR_CAN1_REMAP")
   ))

(define (clk_AFIO_MAPR_set)
  (or-bits
   (if (exist? "SWJ")
       (if (is-true? "SWJ.JTAG")
           (if (is-true? "SWJ.NJTRST")
               "AFIO_MAPR_SWJ_CFG_FULL_SWJ"
               "AFIO_MAPR_SWJ_CFG_FULL_SWJ_NO_JNTRST")
           (if (is-true? "SWJ.SW")
               "AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON"
               "AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_OFF")))

   (if (is-true? "ADC2.remap.ext_trig.regular")
       "AFIO_MAPR_ADC2_ETRGREG_REMAP")
   (if (is-true? "ADC2.remap.ext_trig.injected")
       "AFIO_MAPR_ADC2_ETRGINJ_REMAP")
   (if (is-true? "ADC1.remap.ext_trig.regular")
       "AFIO_MAPR_ADC1_ETRGREG_REMAP")
   (if (is-true? "ADC1.remap.ext_trig.injected")
       "AFIO_MAPR_ADC1_ETRGINJ_REMAP")
   
   (if (and (is? "PD0.remap" "OSC_IN")
            (is? "PD1.remap" "OSC_OUT"))
       "AFIO_MAPR_PD01_REMAP")

   (if (is? "TIM5.remap" "channel4")
       "AFIO_MAPR_TIM5CH4_REMAP")
   (if (is? "TIM4.remap" "full")
       "AFIO_MAPR_TIM4_REMAP")
   (if (is? "TIM3.remap" "partial")
       "AFIO_MAPR_TIM3_REMAP_PARTIAL")
   (if (is? "TIM3.remap" "full")
       "AFIO_MAPR_TIM3_REMAP_FULL")
   (if (is? "TIM2.remap" "partial1")
       "AFIO_MAPR_TIM2_REMAP_PARTIAL1")
   (if (is? "TIM2.remap" "partial2")
       "AFIO_MAPR_TIM2_REMAP_PARTIAL2")
   (if (is? "TIM2.remap" "full")
       "AFIO_MAPR_TIM2_REMAP_FULL")
   (if (is? "TIM1.remap" "partial")
       "AFIO_MAPR_TIM1_REMAP_PARTIAL")
   (if (is? "TIM1.remap" "full")
       "AFIO_MAPR_TIM1_REMAP_FULL")

   (if (is? "USART3.remap" "partial")
       "AFIO_MAPR_USART3_REMAP_PARTIAL")
   (if (is? "USART3.remap" "full")
       "AFIO_MAPR_USART3_REMAP_FULL")
   (if (is? "USART2.remap" "full")
       "AFIO_MAPR_USART2_REMAP")
   (if (is? "USART1.remap" "full")
       "AFIO_MAPR_USART1_REMAP")

   (if (is? "I2C1.remap" "full")
       "AFIO_MAPR_I2C1_REMAP")

   (if (is? "SPI1.remap" "full")
       "AFIO_MAPR_SPI1_REMAP")

   (if (is? "CAN1.remap" "PB")
       "AFIO_MAPR_CAN1_REMAP_PORTB")
   (if (is? "CAN1.remap" "PD")
       "AFIO_MAPR_CAN1_REMAP_PORTD")
   ))

(define (clk_AFIO_MAPR_clr)
  (if (not (string=? "" (clk_AFIO_MAPR_set)))
      "0"))

(define (clk_AFIO_MAPR2_msk)
  (or-bits
   (if (exist? "MISC.remap")
       "AFIO_MAPR2_MISC_REMAP")

   (if (exist? "TIM17.remap")
       "AFIO_MAPR2_TIM17_REMAP")
   (if (exist? "TIM16.remap")
       "AFIO_MAPR2_TIM16_REMAP")
   (if (exist? "TIM15.remap")
       "AFIO_MAPR2_TIM15_REMAP")
   (if (exist? "TIM14.remap")
       "AFIO_MAPR2_TIM14_REMAP")
   (if (exist? "TIM13.remap")
       "AFIO_MAPR2_TIM13_REMAP")
   (if (exist? "TIM12.remap")
       "AFIO_MAPR2_TIM12_REMAP")
   (if (exist? "TIM11.remap")
       "AFIO_MAPR2_TIM11_REMAP")
   (if (exist? "TIM10.remap")
       "AFIO_MAPR2_TIM10_REMAP")
   (if (exist? "TIM9.remap")
       "AFIO_MAPR2_TIM9_REMAP")
   (if (exist? "TIM67_DAC.remap.dma")
       "AFIO_MAPR2_TIM67_DAC_DMA_REMAP")
   (if (exist? "TIM1.remap.dma")
       "AFIO_MAPR2_TIM1_DMA_REMAP")

   (if (exist? "FSMC.connect")
       "AFIO_MAPR2_FSMC_NADV_DISCONNECT")

   (if (exist? "CEC.remap")
       "AFIO_MAPR2_CEC_REMAP")
   ))

(define (clk_AFIO_MAPR2_set)
  (or-bits
   (if (is-true? "MISC.remap")
       "AFIO_MAPR2_MISC_REMAP")

   (if (is? "TIM17.remap" "full")
       "AFIO_MAPR2_TIM17_REMAP")
   (if (is? "TIM16.remap" "full")
       "AFIO_MAPR2_TIM16_REMAP")
   (if (is? "TIM15.remap" "full")
       "AFIO_MAPR2_TIM15_REMAP")
   (if (is? "TIM14.remap" "full")
       "AFIO_MAPR2_TIM14_REMAP")
   (if (is? "TIM13.remap" "full")
       "AFIO_MAPR2_TIM13_REMAP")
   (if (is? "TIM12.remap" "full")
       "AFIO_MAPR2_TIM12_REMAP")
   (if (is? "TIM11.remap" "full")
       "AFIO_MAPR2_TIM11_REMAP")
   (if (is? "TIM10.remap" "full")
       "AFIO_MAPR2_TIM10_REMAP")
   (if (is? "TIM9.remap" "full")
       "AFIO_MAPR2_TIM9_REMAP")
   (if (is? "TIM67_DAC.remap.dma" "full")
       "AFIO_MAPR2_TIM67_DAC_DMA_REMAP")
   (if (is? "TIM1.remap.dma" "full")
       "AFIO_MAPR2_TIM1_REMAP")

   (if (is-false? "FSMC.connect")
       "AFIO_MAPR2_FSMC_NADV_DISCONNECT")

   (if (is-true? "CEC.remap")
       "AFIO_MAPR2_CEC_REMAP")
   ))

(define (clk_AFIO_MAPR2_clr)
  (if (not (string=? "" (clk_AFIO_MAPR2_set)))
      "0"))
