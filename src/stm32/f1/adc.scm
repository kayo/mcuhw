(define (adc_ch_number_regular)
  (length (adc_ch_list "sequence")))

(define (adc_ch_defines_regular)
  (adc_ch_defines "sequence"))

(define (adc_ch_fields_regular)
  (adc_ch_fields "sequence"))

(define (adc_ADC_CR1_msk)
  (or-bits
   "ADC_CR1_DISCNUM_MASK"
   "ADC_CR1_JDISCEN"
   "ADC_CR1_DISCEN"
   "ADC_CR1_JAUTO"
   "ADC_CR1_SCAN"
   ))

(define (adc_ADC_CR1_val)
  (or-bits
   (if (and (has-num? "group")
            (< 0 (check-range 'error "group (discontinuous channels count)"
                         (get-num "group")
                         0 8)))
       (get-def-name "group" "ADC_CR1_DISCNUM_" "CHANNELS"))
   (if (is? "injected.mode" "disc" "discont" "discontinuous")
       "ADC_CR1_JDISCEN")
   (if (is? "mode" "disc" "discont" "discontinuous")
       "ADC_CR1_DISCEN")
   (if (exist? "injected.auto")
       "ADC_CR1_JAUTO")
   (if (exist? "dma")
       "ADC_CR1_SCAN")
   ))

(define (adc_ADC_CR2_msk)
  (or-bits
   "ADC_CR2_TSVREFE"
   "ADC_CR2_EXTTRIG"
   "ADC_CR2_EXTSEL_MASK"
   "ADC_CR2_JEXTTRIG"
   "ADC_CR2_JEXTSEL_MASK"
   "ADC_CR2_ALIGN"
   "ADC_CR2_DMA"
   "ADC_CR2_CONT"
   ))

(define (adc_ADC_CR2_val)
  (or-bits
   (if (or (exist? "Temp")
           (exist? "Vref"))
       "ADC_CR2_TSVREFE")
   (if (exist? "trigger")
       "ADC_CR2_EXTTRIG")
   (if (has? "trigger.source")
       (if (has-num? "trigger.source")
           (get-def-name "trigger.source"
                         "" " << ADC_CR2_EXTSEL_SHIFT")
           (get-def-name "trigger.source"
                         "ADC_CR2_EXTSEL_"))
       "ADC_CR2_EXTSEL_SWSTART")
   (if (exist? "injected.trigger")
       "ADC_CR2_JEXTTRIG")
   (if (has? "injected.trigger.source")
       (if (has-num? "injected.trigger.source")
           (get-def-name "injected.trigger.source"
                         "" " << ADC_CR2_JEXTSEL_SHIFT")
           (get-def-name "injected.trigger.source"
                         "ADC_CR2_JEXTSEL_"))
       "ADC_CR2_JEXTSEL_JSWSTART")
   (if (is? "align" "left")
       "ADC_CR2_ALIGN_LEFT")
   (if (and (has? "dma.device")
            (has? "dma.channel"))
       "ADC_CR2_DMA")
   (if (is? "mode" "continuous" "cont")
       "ADC_CR2_CONT")
   ))

(define (adc_ADC_SMPR_val from to)
  (apply or-bits
   (map (lambda (ch)
          (let* ([ch_st_key (adc_ch_key ch "sample-time")]
                 [st_key (if (has? ch_st_key)
                             ch_st_key
                             (if (has? "sample-time")
                                 "sample-time"
                                 #f))])
            (if (and (adc_ch_exist? ch)
                     (string? st_key))
                (string-append
                 "(ADC_SMPR_SMP_"
                 (join "_" (stack st_key))
                 " << ADC_SMPR" (if (= 0 from) "2" "1")
                 "_SMP" (number->string ch)
                 "_LSB)"))))
        (gen-sequence from to))))

(define (adc_ADC_SQR_val from to)
  (let ([ch_seq (adc_ch_list "sequence")]
        [ch_idx from])
    (or-bits
     (if (= to 16)
         (string-append
          "("
          (number->string
           (- (length ch_seq) 1))
          " << ADC_SQR1_L_LSB)"))
     (apply or-bits
            (map-with-counters
             (lambda (ch idx rec)
               (string-append
                "(ADC_CHANNEL" (number->string ch)
                " << ADC_SQR" (if (= 0 from) "3" (if (= 6 from) "2" "1"))
                "_SQ" (number->string (+ 1 idx from))
                "_LSB)"))
             (slice-list ch_seq from to)))
     )))

(define (adc_ADC_JSQR_val)
  (or-bits))

(define (adc_ADC_CR2_START)
  (if (and (not (has? "trigger.source"))
           (exist? "start"))
      "ADC_CR2_ADON"))

(define adc_cal '())
