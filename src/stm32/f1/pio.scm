(define (pio_CRx x req)
  (let ([spec (cond ((= x 'lo) "L")
                    ((= x 'hi) "H"))])
    (apply or-bits
           (map (lambda (pin)
                  (let* ([out (pio_pin pin "output")]
                         [in (pio_pin pin "input")]
                         [spd (pio_pin pin "speed")]
                         [af (pio_pin pin "af")]
                         [mode (cond ((has? out) (string-append "OUTPUT_"
                                                                (if (has? spd)
                                                                    (string-substitute (get spd)
                                                                                       '("high" "medium" "low")
                                                                                       '("2" "10" "50"))
                                                                    "10") "_MHZ"))
                                     ((has? in) "INPUT"))]
                         [cnf (cond ((and (has? out) (not (is? out "analog")))
                                     (string-append "OUTPUT_"
                                                    (if (exist? af) "ALTFN_" "")
                                                    (cond ((is? out "pushpull") "PUSHPULL")
                                                          ((is? out "opendrain") "OPENDRAIN"))))
                                    ((has? in) (string-append "INPUT_" (cond ((is? in "analog") "ANALOG")
                                                                             ((is? in "float") "FLOAT")
                                                                             ((or (is? in "pullup")
                                                                                  (is? in "pulldown")) "PULL_UPDOWN")))))])
                    (or-bits
                     (if (string? mode)
                         (string-append "GPIO_CR" spec "_MODE"
                                        (cond ((= 'msk req) (string-append "_MASK(" pin ")"))
                                              ((= 'set req) (string-append "(" pin ", GPIO_MODE_" mode ")"))
                                              ((= 'clr req) (string-append "(" pin ", GPIO_MODE_INPUT)")))))
                     (if (string? cnf)
                         (string-append "GPIO_CR" spec "_CNF"
                                        (cond ((= 'msk req) (string-append "_MASK(" pin ")"))
                                              ((= 'set req) (string-append "(" pin ", GPIO_CNF_" cnf ")"))
                                              ((= 'clr req) (string-append "(" pin ", GPIO_CNF_INPUT_FLOAT)")))))
                     )))
                (pio_seq x)))))

(define (pio_CRL req) (pio_CRx 'lo req))
(define (pio_CRH req) (pio_CRx 'hi req))

(define (pio_BSRR_cfg) (pio_BSRR_cfg_gen #t))

(define pio_EXTICR_reg "AFIO_EXTICR")
