(define mcu_core
  "cortex-m3")

;; vectors: (vectors)
;; vector: "name" | ("name" (shared)? num? "fallback"?)
;; shared: "name" | ("name" "condition"? "reset-condition"?)
(define mcu_vectors
  '("wwdg"
    "pvd"
    "tamper"
    "rtc"
    "flash"
    "rcc"
    ("exti0_"
     (("exti0" "EXTI_PR & EXTI0"  "EXTI_PR &= EXTI0")))
    ("exti1_"
     (("exti1"   "EXTI_PR & EXTI1"  "EXTI_PR &= EXTI1")))
    ("exti2_"
     (("exti2"   "EXTI_PR & EXTI2"  "EXTI_PR &= EXTI2")))
    ("exti3_"
     (("exti3"   "EXTI_PR & EXTI3"  "EXTI_PR &= EXTI3")))
    ("exti4_"
     (("exti4"   "EXTI_PR & EXTI4"  "EXTI_PR &= EXTI4")))
    ("dma1_ch1"
     (("dma1_ch1_error"    "DMA_ISR(DMA1) & DMA_ISR_TEIF(1)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(1) | DMA_IFCR_CTEIF(1)")
      ("dma1_ch1_complete" "DMA_ISR(DMA1) & DMA_ISR_TCIF(1)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(1) | DMA_IFCR_CTCIF(1)")
      ("dma1_ch1_half"     "DMA_ISR(DMA1) & DMA_ISR_HTIF(1)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(1) | DMA_IFCR_CHTIF(1)")))
    ("dma1_ch2"
     (("dma1_ch2_error"    "DMA_ISR(DMA1) & DMA_ISR_TEIF(2)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(2) | DMA_IFCR_CTEIF(2)")
      ("dma1_ch2_complete" "DMA_ISR(DMA1) & DMA_ISR_TCIF(2)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(2) | DMA_IFCR_CTCIF(2)")
      ("dma1_ch2_half"     "DMA_ISR(DMA1) & DMA_ISR_HTIF(2)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(2) | DMA_IFCR_CHTIF(2)")))
    ("dma1_ch3"
     (("dma1_ch3_error"    "DMA_ISR(DMA1) & DMA_ISR_TEIF(3)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(3) | DMA_IFCR_CTEIF(3)")
      ("dma1_ch3_complete" "DMA_ISR(DMA1) & DMA_ISR_TCIF(3)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(3) | DMA_IFCR_CTCIF(3)")
      ("dma1_ch3_half"     "DMA_ISR(DMA1) & DMA_ISR_HTIF(3)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(3) | DMA_IFCR_CHTIF(3)")))
    ("dma1_ch4"
     (("dma1_ch4_error"    "DMA_ISR(DMA1) & DMA_ISR_TEIF(4)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(4) | DMA_IFCR_CTEIF(4)")
      ("dma1_ch4_complete" "DMA_ISR(DMA1) & DMA_ISR_TCIF(4)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(4) | DMA_IFCR_CTCIF(4)")
      ("dma1_ch4_half"     "DMA_ISR(DMA1) & DMA_ISR_HTIF(4)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(4) | DMA_IFCR_CHTIF(4)")))
    ("dma1_ch5"
     (("dma1_ch5_error"    "DMA_ISR(DMA1) & DMA_ISR_TEIF(5)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(5) | DMA_IFCR_CTEIF(5)")
      ("dma1_ch5_complete" "DMA_ISR(DMA1) & DMA_ISR_TCIF(5)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(5) | DMA_IFCR_CTCIF(5)")
      ("dma1_ch5_half"     "DMA_ISR(DMA1) & DMA_ISR_HTIF(5)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(5) | DMA_IFCR_CHTIF(5)")))
    ("dma1_ch6"
     (("dma1_ch6_error"    "DMA_ISR(DMA1) & DMA_ISR_TEIF(6)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(6) | DMA_IFCR_CTEIF(6)")
      ("dma1_ch6_complete" "DMA_ISR(DMA1) & DMA_ISR_TCIF(6)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(6) | DMA_IFCR_CTCIF(6)")
      ("dma1_ch6_half"     "DMA_ISR(DMA1) & DMA_ISR_HTIF(6)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(6) | DMA_IFCR_CHTIF(6)")))
    ("dma1_ch7"
     (("dma1_ch7_error"    "DMA_ISR(DMA1) & DMA_ISR_TEIF(7)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(7) | DMA_IFCR_CTEIF(7)")
      ("dma1_ch7_complete" "DMA_ISR(DMA1) & DMA_ISR_TCIF(7)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(7) | DMA_IFCR_CTCIF(7)")
      ("dma1_ch7_half"     "DMA_ISR(DMA1) & DMA_ISR_HTIF(7)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(7) | DMA_IFCR_CHTIF(7)")))
    "adc1_2"
    "usb_hp_can_tx"
    "usb_lp_can_rx0"
    "can_rx1"
    "can_sce"
    ("exti9_5"
     (("exti5"  "EXTI_PR & EXTI5"  "EXTI_PR &= EXTI5")
      ("exti6"  "EXTI_PR & EXTI6"  "EXTI_PR &= EXTI6")
      ("exti7"  "EXTI_PR & EXTI7"  "EXTI_PR &= EXTI7")
      ("exti8"  "EXTI_PR & EXTI8"  "EXTI_PR &= EXTI8")
      ("exti9"  "EXTI_PR & EXTI9"  "EXTI_PR &= EXTI9")))
    ("tim1_brk_"
     (("tim1_brk" "TIM_SR(TIM1) & TIM_SR_BIF"   "TIM_SR(TIM1) &= ~TIM_SR_BIF")))
    ("tim1_up_"
     (("tim1_up"   "TIM_SR(TIM1) & TIM_SR_UIF"   "TIM_SR(TIM1) &= ~TIM_SR_UIF")))
    ("tim1_trg_com"
     (("tim1_trg" "TIM_SR(TIM1) & TIM_SR_TIF"   "TIM_SR(TIM1) &= ~TIM_SR_TIF")
      ("tim1_com" "TIM_SR(TIM1) & TIM_SR_COMIF" "TIM_SR(TIM1) &= ~TIM_SR_COMIF")))
    ("tim1_cc"
     (("tim1_cc1" "TIM_SR(TIM1) & TIM_SR_CC1IF" "TIM_SR(TIM1) &= ~TIM_SR_CC1IF")
      ("tim1_cc2" "TIM_SR(TIM1) & TIM_SR_CC2IF" "TIM_SR(TIM1) &= ~TIM_SR_CC2IF")
      ("tim1_cc3" "TIM_SR(TIM1) & TIM_SR_CC3IF" "TIM_SR(TIM1) &= ~TIM_SR_CC3IF")
      ("tim1_cc4" "TIM_SR(TIM1) & TIM_SR_CC4IF" "TIM_SR(TIM1) &= ~TIM_SR_CC4IF")))
    ("tim2"
     (("tim2_up"  "TIM_SR(TIM2) & TIM_SR_UIF"   "TIM_SR(TIM2) &= ~TIM_SR_UIF")
      ("tim2_trg" "TIM_SR(TIM2) & TIM_SR_TIF"   "TIM_SR(TIM2) &= ~TIM_SR_TIF")
      ("tim2_cc1" "TIM_SR(TIM2) & TIM_SR_CC1IF" "TIM_SR(TIM2) &= ~TIM_SR_CC1IF")
      ("tim2_cc2" "TIM_SR(TIM2) & TIM_SR_CC2IF" "TIM_SR(TIM2) &= ~TIM_SR_CC2IF")
      ("tim2_cc3" "TIM_SR(TIM2) & TIM_SR_CC3IF" "TIM_SR(TIM2) &= ~TIM_SR_CC3IF")
      ("tim2_cc4" "TIM_SR(TIM2) & TIM_SR_CC4IF" "TIM_SR(TIM2) &= ~TIM_SR_CC4IF")))
    ("tim3"
     (("tim3_up"  "TIM_SR(TIM3) & TIM_SR_UIF"   "TIM_SR(TIM3) &= ~TIM_SR_UIF")
      ("tim3_trg" "TIM_SR(TIM3) & TIM_SR_TIF"   "TIM_SR(TIM3) &= ~TIM_SR_TIF")
      ("tim3_cc1" "TIM_SR(TIM3) & TIM_SR_CC1IF" "TIM_SR(TIM3) &= ~TIM_SR_CC1IF")
      ("tim3_cc2" "TIM_SR(TIM3) & TIM_SR_CC2IF" "TIM_SR(TIM3) &= ~TIM_SR_CC2IF")
      ("tim3_cc3" "TIM_SR(TIM3) & TIM_SR_CC3IF" "TIM_SR(TIM3) &= ~TIM_SR_CC3IF")
      ("tim3_cc4" "TIM_SR(TIM3) & TIM_SR_CC4IF" "TIM_SR(TIM3) &= ~TIM_SR_CC4IF")))
    ("tim4"
     (("tim4_up"  "TIM_SR(TIM4) & TIM_SR_UIF"   "TIM_SR(TIM4) &= ~TIM_SR_UIF")
      ("tim4_trg" "TIM_SR(TIM4) & TIM_SR_TIF"   "TIM_SR(TIM4) &= ~TIM_SR_TIF")
      ("tim4_cc1" "TIM_SR(TIM4) & TIM_SR_CC1IF" "TIM_SR(TIM4) &= ~TIM_SR_CC1IF")
      ("tim4_cc2" "TIM_SR(TIM4) & TIM_SR_CC2IF" "TIM_SR(TIM4) &= ~TIM_SR_CC2IF")
      ("tim4_cc3" "TIM_SR(TIM4) & TIM_SR_CC3IF" "TIM_SR(TIM4) &= ~TIM_SR_CC3IF")
      ("tim4_cc4" "TIM_SR(TIM4) & TIM_SR_CC4IF" "TIM_SR(TIM4) &= ~TIM_SR_CC4IF")))
    ("i2c1_ev"
     (("i2c1_tc"
       "(I2C_CR2(I2C1) & I2C_CR2_ITEVTEN) && (I2C_SR1(I2C1) & I2C_SR1_BTF)")
      ("i2c1_start"
       "(I2C_CR2(I2C1) & I2C_CR2_ITEVTEN) && (I2C_SR1(I2C1) & I2C_SR1_SB)")
      ("i2c1_stop"
       "(I2C_CR2(I2C1) & I2C_CR2_ITEVTEN) && (I2C_SR1(I2C1) & I2C_SR1_STOPF)"
       "I2C_CR1(I2C1) |= I2C_CR1_STOP")
      ("i2c1_addr"
       "(I2C_CR2(I2C1) & I2C_CR2_ITEVTEN) && (I2C_SR1(I2C1) & I2C_SR1_ADDR)"
       "(void)I2C_SR2(I2C1)")
      ("i2c1_rxne"
       "(I2C_CR2(I2C1) & I2C_CR2_ITBUFEN) && (I2C_SR1(I2C1) & I2C_SR1_RXNE)")
      ("i2c1_txe"
       "(I2C_CR2(I2C1) & I2C_CR2_ITBUFEN) && (I2C_SR1(I2C1) & I2C_SR1_TXE)")))
    ("i2c1_er"
     (("i2c1_nack"
       "(I2C_CR2(I2C1) & I2C_CR2_ITERREN) && (I2C_SR1(I2C1) & I2C_SR1_AF)"
       "I2C_SR1(I2C1) &= ~I2C_SR1_AF")
      ("i2c1_err"
       "(I2C_CR2(I2C1) & I2C_CR2_ITERREN) && (I2C_SR1(I2C1) & (I2C_SR1_SMBALERT | I2C_SR1_TIMEOUT | I2C_SR1_PECERR | I2C_SR1_OVR | I2C_SR1_ARLO | I2C_SR1_BERR))"
       "I2C_SR1(I2C1) &= ~(I2C_SR1_SMBALERT | I2C_SR1_TIMEOUT | I2C_SR1_PECERR | I2C_SR1_OVR | I2C_SR1_ARLO | I2C_SR1_BERR)")))
    ("i2c2_ev"
     (("i2c2_tc"
       "(I2C_CR2(I2C2) & I2C_CR2_ITEVTEN) && (I2C_SR1(I2C2) & I2C_SR1_BTF)")
      ("i2c2_start"
       "(I2C_CR2(I2C2) & I2C_CR2_ITEVTEN) && (I2C_SR1(I2C2) & I2C_SR1_SB)")
      ("i2c2_stop"
       "(I2C_CR2(I2C2) & I2C_CR2_ITEVTEN) && (I2C_SR1(I2C2) & I2C_SR1_STOPF)"
       "I2C_CR1(I2C2) |= I2C_CR1_STOP")
      ("i2c2_addr"
       "(I2C_CR2(I2C2) & I2C_CR2_ITEVTEN) && (I2C_SR1(I2C2) & I2C_SR1_ADDR)"
       "(void)I2C_SR2(I2C2)")
      ("i2c2_rxne"
       "(I2C_CR2(I2C2) & I2C_CR2_ITBUFEN) && (I2C_SR1(I2C2) & I2C_SR1_RXNE)")
      ("i2c2_txe"
       "(I2C_CR2(I2C2) & I2C_CR2_ITBUFEN) && (I2C_SR1(I2C2) & I2C_SR1_TXE)")))
    ("i2c2_er"
     (("i2c2_nack"
       "(I2C_CR2(I2C2) & I2C_CR2_ITERREN) && (I2C_SR1(I2C2) & I2C_SR1_AF)"
       "I2C_SR1(I2C2) &= ~I2C_SR1_AF")
      ("i2c2_err"
       "(I2C_CR2(I2C2) & I2C_CR2_ITERREN) && (I2C_SR1(I2C2) & (I2C_SR1_SMBALERT | I2C_SR1_TIMEOUT | I2C_SR1_PECERR | I2C_SR1_OVR | I2C_SR1_ARLO | I2C_SR1_BERR))"
       "I2C_SR1(I2C2) &= ~(I2C_SR1_SMBALERT | I2C_SR1_TIMEOUT | I2C_SR1_PECERR | I2C_SR1_OVR | I2C_SR1_ARLO | I2C_SR1_BERR)")))
    ("spi1"
     (("spi1_txe"
       "(SPI_CR2(SPI1) & SPI_CR2_TXEIE) && (SPI_SR(SPI1) & SPI_SR_TXE)")
      ("spi1_rxne"
       "(SPI_CR2(SPI1) & SPI_CR2_RXNEIE) && (SPI_SR(SPI1) & SPI_SR_RXNE)")
      ("spi1_err"
       "(SPI_CR2(SPI1) & SPI_CR2_ERRIE) && (SPI_SR(SPI1) & (SPI_SR_OVR | SPI_SR_MODF | SPI_SR_CRCERR | SPI_SR_UDR))")))
    ("spi2"
     (("spi2_txe"
       "(SPI_CR2(SPI2) & SPI_CR2_TXEIE) && (SPI_SR(SPI2) & SPI_SR_TXE)")
      ("spi2_rxne"
       "(SPI_CR2(SPI2) & SPI_CR2_RXNEIE) && (SPI_SR(SPI2) & SPI_SR_RXNE)")
      ("spi2_err"
       "(SPI_CR2(SPI2) & SPI_CR2_ERRIE) && (SPI_SR(SPI2) & (SPI_SR_OVR | SPI_SR_MODF | SPI_SR_CRCERR | SPI_SR_UDR))")))
    ("usart1"
     (("usart1_cts"
       "(USART_CR3(USART1) & USART_CR3_CTSIE) && (USART_SR(USART1) & USART_SR_CTS)"
       "USART_SR(USART1) &= ~USART_SR_CTS")
      ("usart1_brk"
       "(USART_CR2(USART1) & USART_CR2_LBDIE) && (USART_SR(USART1) & USART_SR_LBD)"
       "USART_SR(USART1) &= ~USART_SR_LBD")
      ("usart1_txe"
       "(USART_CR1(USART1) & USART_CR1_TXEIE) && (USART_SR(USART1) & USART_SR_TXE)")
      ("usart1_tc"
       "(USART_CR1(USART1) & USART_CR1_TCIE) && (USART_SR(USART1) & USART_SR_TC)"
       "USART_SR(USART1) &= ~USART_SR_TC")
      ("usart1_rxne"
       "(USART_CR1(USART1) & USART_CR1_RXNEIE) && (USART_SR(USART1) & USART_SR_RXNE)"
       "USART_SR(USART1) &= ~USART_SR_RXNE")
      ("usart1_idle"
       "(USART_CR1(USART1) & USART_CR1_IDLEIE) && (USART_SR(USART1) & USART_SR_IDLE)"
       "(void)USART_DR(USART1)")
      ("usart1_err"
       "(USART_CR3(USART1) & USART_CR3_EIE) && (USART_SR(USART1) & (USART_SR_ORE | USART_SR_NE | USART_SR_FE | USART_SR_PE))")))
    ("usart2"
     (("usart2_cts"
       "(USART_CR3(USART2) & USART_CR3_CTSIE) && (USART_SR(USART2) & USART_SR_CTS)"
       "USART_SR(USART2) &= ~USART_SR_CTS")
      ("usart2_brk"
       "(USART_CR2(USART2) & USART_CR2_LBDIE) && (USART_SR(USART2) & USART_SR_LBD)"
       "USART_SR(USART2) &= ~USART_SR_LBD")
      ("usart2_txe"
       "(USART_CR1(USART2) & USART_CR1_TXEIE) && (USART_SR(USART2) & USART_SR_TXE)")
      ("usart2_tc"
       "(USART_CR1(USART2) & USART_CR1_TCIE) && (USART_SR(USART2) & USART_SR_TC)"
       "USART_SR(USART2) &= ~USART_SR_TC")
      ("usart2_rxne"
       "(USART_CR1(USART2) & USART_CR1_RXNEIE) && (USART_SR(USART2) & USART_SR_RXNE)"
       "USART_SR(USART2) &= ~USART_SR_RXNE")
      ("usart2_idle"
       "(USART_CR1(USART2) & USART_CR1_IDLEIE) && (USART_SR(USART2) & USART_SR_IDLE)"
       "(void)USART_DR(USART2)")
      ("usart2_err"
       "(USART_CR3(USART2) & USART_CR3_EIE) && (USART_SR(USART2) & (USART_SR_ORE | USART_SR_NE | USART_SR_FE | USART_SR_PE))")))
    ("usart3"
     (("usart3_cts"
       "(USART_CR3(USART3) & USART_CR3_CTSIE) && (USART_SR(USART3) & USART_SR_CTS)"
       "USART_SR(USART3) &= ~USART_SR_CTS")
      ("usart3_brk"
       "(USART_CR2(USART3) & USART_CR2_LBDIE) && (USART_SR(USART3) & USART_SR_LBD)"
       "USART_SR(USART3) &= ~USART_SR_LBD")
      ("usart3_txe"
       "(USART_CR1(USART3) & USART_CR1_TXEIE) && (USART_SR(USART3) & USART_SR_TXE)")
      ("usart3_tc"
       "(USART_CR1(USART3) & USART_CR1_TCIE) && (USART_SR(USART3) & USART_SR_TC)"
       "USART_SR(USART3) &= ~USART_SR_TC")
      ("usart3_rxne"
       "(USART_CR1(USART3) & USART_CR1_RXNEIE) && (USART_SR(USART3) & USART_SR_RXNE)"
       "USART_SR(USART3) &= ~USART_SR_RXNE")
      ("usart3_idle"
       "(USART_CR1(USART3) & USART_CR1_IDLEIE) && (USART_SR(USART3) & USART_SR_IDLE)"
       "(void)USART_DR(USART3)")
      ("usart3_err"
       "(USART_CR3(USART3) & USART_CR3_EIE) && (USART_SR(USART3) & (USART_SR_ORE | USART_SR_NE | USART_SR_FE | USART_SR_PE))")))
    ("exti15_10"
     (("exti10" "EXTI_PR & EXTI10" "EXTI_PR &= EXTI10")
      ("exti11" "EXTI_PR & EXTI11" "EXTI_PR &= EXTI11")
      ("exti12" "EXTI_PR & EXTI12" "EXTI_PR &= EXTI12")
      ("exti13" "EXTI_PR & EXTI13" "EXTI_PR &= EXTI13")
      ("exti14" "EXTI_PR & EXTI14" "EXTI_PR &= EXTI14")
      ("exti15" "EXTI_PR & EXTI15" "EXTI_PR &= EXTI15")))
    "rtc_alarm"
    "usb_wakeup"
    ("tim8_brk_"
     (("tim8_brk"  "TIM_SR(TIM8) & TIM_SR_BIF"   "TIM_SR(TIM8) &= ~TIM_SR_BIF")))
    ("tim8_up_"
     (("tim8_up"   "TIM_SR(TIM8) & TIM_SR_UIF"   "TIM_SR(TIM8) &= ~TIM_SR_UIF")))
    ("tim8_trg_com"
     (("tim8_trg" "TIM_SR(TIM8) & TIM_SR_TIF"   "TIM_SR(TIM8) &= ~TIM_SR_TIF")
      ("tim8_com" "TIM_SR(TIM8) & TIM_SR_COMIF" "TIM_SR(TIM8) &= ~TIM_SR_COMIF")))
    ("tim8_cc"
     (("tim8_cc1" "TIM_SR(TIM8) & TIM_SR_CC1IF" "TIM_SR(TIM8) &= ~TIM_SR_CC1IF")
      ("tim8_cc2" "TIM_SR(TIM8) & TIM_SR_CC2IF" "TIM_SR(TIM8) &= ~TIM_SR_CC2IF")
      ("tim8_cc3" "TIM_SR(TIM8) & TIM_SR_CC3IF" "TIM_SR(TIM8) &= ~TIM_SR_CC3IF")
      ("tim8_cc4" "TIM_SR(TIM8) & TIM_SR_CC4IF" "TIM_SR(TIM8) &= ~TIM_SR_CC4IF")))
    "adc3"
    "fsmc"
    "sdio"
    ("tim5"
     (("tim5_up"  "TIM_SR(TIM5) & TIM_SR_UIF"   "TIM_SR(TIM5) &= ~TIM_SR_UIF")
      ("tim5_trg" "TIM_SR(TIM5) & TIM_SR_TIF"   "TIM_SR(TIM5) &= ~TIM_SR_TIF")
      ("tim5_cc1" "TIM_SR(TIM5) & TIM_SR_CC1IF" "TIM_SR(TIM5) &= ~TIM_SR_CC1IF")
      ("tim5_cc2" "TIM_SR(TIM5) & TIM_SR_CC2IF" "TIM_SR(TIM5) &= ~TIM_SR_CC2IF")
      ("tim5_cc3" "TIM_SR(TIM5) & TIM_SR_CC3IF" "TIM_SR(TIM5) &= ~TIM_SR_CC3IF")
      ("tim5_cc4" "TIM_SR(TIM5) & TIM_SR_CC4IF" "TIM_SR(TIM5) &= ~TIM_SR_CC4IF")))
    ("spi3"
     (("spi3_txe"
       "(SPI_CR2(SPI3) & SPI_CR2_TXEIE) && (SPI_SR(SPI3) & SPI_SR_TXE)")
      ("spi3_rxne"
       "(SPI_CR2(SPI3) & SPI_CR2_RXNEIE) && (SPI_SR(SPI3) & SPI_SR_RXNE)")
      ("spi3_err"
       "(SPI_CR2(SPI3) & SPI_CR2_ERRIE) && (SPI_SR(SPI3) & (SPI_SR_OVR | SPI_SR_MODF | SPI_SR_CRCERR | SPI_SR_UDR))")))
    ("uart4"
     (("uart4_brk"
       "(USART_CR2(UART4) & USART_CR2_LBDIE) && (USART_SR(UART4) & USART_SR_LBD)"
       "USART_SR(UART4) &= ~USART_SR_LBD")
      ("uart4_txe"
       "(USART_CR1(UART4) & USART_CR1_TXEIE) && (USART_SR(UART4) & USART_SR_TXE)")
      ("uart4_tc"
       "(USART_CR1(UART4) & USART_CR1_TCIE) && (USART_SR(UART4) & USART_SR_TC)"
       "USART_SR(UART4) &= ~USART_SR_TC")
      ("uart4_rxne"
       "(USART_CR1(UART4) & USART_CR1_RXNEIE) && (USART_SR(UART4) & USART_SR_RXNE)"
       "USART_SR(UART4) &= ~USART_SR_RXNE")
      ("uart4_idle"
       "(USART_CR1(UART4) & USART_CR1_IDLEIE) && (USART_SR(UART4) & USART_SR_IDLE)"
       "(void)USART_DR(UART4)")
      ("uart4_err"
       "(USART_CR3(UART4) & USART_CR3_EIE) && (USART_SR(UART4) & (USART_SR_ORE | USART_SR_NE | USART_SR_FE | USART_SR_PE))")))
    ("uart5"
     (("uart5_brk"
       "(USART_CR2(UART5) & USART_CR2_LBDIE) && (USART_SR(UART5) & USART_SR_LBD)"
       "USART_SR(UART5) &= ~USART_SR_LBD")
      ("uart5_txe"
       "(USART_CR1(UART5) & USART_CR1_TXEIE) && (USART_SR(UART5) & USART_SR_TXE)")
      ("uart5_tc"
       "(USART_CR1(UART5) & USART_CR1_TCIE) && (USART_SR(UART5) & USART_SR_TC)"
       "USART_SR(UART5) &= ~USART_SR_TC")
      ("uart5_rxne"
       "(USART_CR1(UART5) & USART_CR1_RXNEIE) && (USART_SR(UART5) & USART_SR_RXNE)"
       "USART_SR(UART5) &= ~USART_SR_RXNE")
      ("uart5_idle"
       "(USART_CR1(UART5) & USART_CR1_IDLEIE) && (USART_SR(UART5) & USART_SR_IDLE)"
       "(void)USART_DR(UART5)")
      ("uart5_err"
       "(USART_CR3(UART5) & USART_CR3_EIE) && (USART_SR(UART5) & (USART_SR_ORE | USART_SR_NE | USART_SR_FE | USART_SR_PE))")))
    ("tim6"
     (("tim6_up"  "TIM_SR(TIM6) & TIM_SR_UIF"  "TIM_SR(TIM6) &= ~TIM_SR_UIF")))
    ("tim7"
     (("tim7_up"  "TIM_SR(TIM7) & TIM_SR_UIF"  "TIM_SR(TIM7) &= ~TIM_SR_UIF")))
    ("dma2_ch1"
     (("dma2_ch1_error"    "DMA_ISR(DMA2) & DMA_ISR_TEIF(1)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(1) | DMA_IFCR_CTEIF(1)")
      ("dma2_ch1_complete" "DMA_ISR(DMA2) & DMA_ISR_TCIF(1)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(1) | DMA_IFCR_CTCIF(1)")
      ("dma2_ch1_half"     "DMA_ISR(DMA2) & DMA_ISR_HTIF(1)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(1) | DMA_IFCR_CHTIF(1)")))
    ("dma2_ch2"
     (("dma2_ch1_error"    "DMA_ISR(DMA2) & DMA_ISR_TEIF(2)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(2) | DMA_IFCR_CTEIF(2)")
      ("dma2_ch1_complete" "DMA_ISR(DMA2) & DMA_ISR_TCIF(2)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(2) | DMA_IFCR_CTCIF(2)")
      ("dma2_ch1_half"     "DMA_ISR(DMA2) & DMA_ISR_HTIF(2)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(2) | DMA_IFCR_CHTIF(2)")))
    ("dma2_ch3"
     (("dma2_ch3_error"    "DMA_ISR(DMA2) & DMA_ISR_TEIF(3)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(3) | DMA_IFCR_CTEIF(3)")
      ("dma2_ch3_complete" "DMA_ISR(DMA2) & DMA_ISR_TCIF(3)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(3) | DMA_IFCR_CTCIF(3)")
      ("dma2_ch3_half"     "DMA_ISR(DMA2) & DMA_ISR_HTIF(3)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(3) | DMA_IFCR_CHTIF(3)")))
    ("dma2_ch4_5"
     (("dma2_ch4_error"    "DMA_ISR(DMA2) & DMA_ISR_TEIF(4)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(4) | DMA_IFCR_CTEIF(4)")
      ("dma2_ch4_complete" "DMA_ISR(DMA2) & DMA_ISR_TCIF(4)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(4) | DMA_IFCR_CTCIF(4)")
      ("dma2_ch4_half"     "DMA_ISR(DMA2) & DMA_ISR_HTIF(4)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(4) | DMA_IFCR_CHTIF(4)")))
    ("dma2_ch5"
     (("dma2_ch5_error"    "DMA_ISR(DMA2) & DMA_ISR_TEIF(5)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(5) | DMA_IFCR_CTEIF(5)")
      ("dma2_ch5_complete" "DMA_ISR(DMA2) & DMA_ISR_TCIF(5)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(5) | DMA_IFCR_CTCIF(5)")
      ("dma2_ch5_half"     "DMA_ISR(DMA2) & DMA_ISR_HTIF(5)" "DMA_IFCR(DMA2) |= DMA_IFCR_CGIF(5) | DMA_IFCR_CHTIF(5)")))
    "eth"
    "eth_wkup"
    "can2_tx"
    "can2_rx0"
    "can2_rx1"
    "can2_sce"
    "otg_fs"))

(define mcu_timers
  '(("STM32F100"
     ;; advanced
     ((1)       CR1 CR2 SMCR DIER SR EGR CCMR1 CCMR2 CCER CNT PSC ARR RCR CCR1 CCR2 CCR3 CCR4 BDTR DCR DMAR)
     ;; general
     ((2 3 4 5) CR1 CR2 SMCR DIER SR EGR CCMR1 CCMR2 CCER CNT PSC ARR     CCR1 CCR2 CCR3 CCR4      DCR DMAR)
     ;; basic
     ((6 7)     CR1 CR2      DIER SR EGR                  CNT PSC ARR)
     ;; general
     ((12)      CR1 CR2 SMCR DIER SR EGR CCMR1       CCER CNT PSC ARR     CCR1 CCR2)
     ((13 14)   CR1          DIER SR EGR CCMR1       CCER CNT PSC ARR     CCR1)
     ((15)      CR1 CR2 SMCR DIER SR EGR CCMR1       CCER CNT PSC ARR RCR CCR1 CCR2           BDTR DCR DMAR)
     ((16 17)   CR1 CR2      DIER SR EGR CCMR1       CCER CNT PSC ARR RCR CCR1                BDTR DCR DMAR))
    ("STM32F10[1-357]"
     ;; advanced
     ((1)       CR1 CR2 SMCR DIER SR EGR CCMR1 CCMR2 CCER CNT PSC ARR RCR CCR1 CCR2 CCR3 CCR4 BDTR DCR DMAR)
     ;; general
     ((3)       CR1 CR2 SMCR DIER SR EGR CCMR1 CCMR2 CCER CNT PSC ARR     CCR1 CCR2 CCR3 CCR4      DCR DMAR)
     ;; basic
     ((6 7)     CR1          DIER SR EGR                  CNT PSC ARR)
     ;; general
     ((14)      CR1          DIER SR EGR CCMR1       CCER CNT PSC ARR     CCR1                              OR)
     ((15)      CR1 CR2 SMCR DIER SR EGR CCMR1       CCER CNT PSC ARR RCR CCR1 CCR2           BDTR DCR DMAR)
     ((16 17)   CR1 CR2 SMCR DIER SR EGR CCMR1       CCER CNT PSC ARR RCR CCR1                BDTR DCR DMAR))
    ))

(define mcu_database_rom_size 5)
(define mcu_database_ram_size 7)
(define mcu_database_speed_mhz 4)

(define mcu_database
  '(#("Part Number"
      "Marketing Status"
      "Package"
      "Core"
      "Operating Frequency (MHz) (Processor speed)"
      "FLASH Size (kB) (Prog)"
      "Data E2PROM (B) nom"
      "Internal RAM Size (kB)"
      "Timers (16 bit) typ"
      "Timers (32 bit) typ"
      "Other timer functions"
      "A/D Converters (12-bit channels)"
      "A/D Converters (16-bit channels)"
      "D/A Converters (12 bit) typ"
      "Comparator"
      "I/Os (High Current)"
      "Display controller"
      "Integrated op-amps"
      "I2C typ"
      "SPI typ"
      "I2S typ"
      "CAN typ"
      "USB typ"
      "USART typ"
      "UART typ"
      "Additional Serial Interfaces"
      "Parallel Interfaces"
      "Crypto-HASH"
      "TRNG typ"
      "Supply Voltage (V) min"
      "Supply Voltage (V) max"
      "Supply Current (µA) (Lowest power mode) typ"
      "Supply Current (µA) (Run mode (per Mhz)) typ"
      "Operating Temperature (°C) min"
      "Operating Temperature (°C) max")
    #("STM32F100C4" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M3" 24 16 #f 4 6 #f "2 x WDG,24-bit down counter,RTC" 10 #f 2 #f 37 #f #f 1 1 #f #f #f 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 358 -40 "105,85")
    #("STM32F100C6" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M3" 24 32 #f 4 6 #f "2 x WDG,24-bit down counter,RTC" 10 #f 2 0 37 #f #f 1 1 #f #f #f 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 358 -40 "105,85")
    #("STM32F100C8" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M3" 24 64 #f 8 7 #f "2 x WDG,24-bit down counter,RTC" 10 #f 2 0 37 #f #f 2 2 #f #f #f 3 #f "HDMI CEC" #f #f #f 2 3.6 1.7 358 -40 "105,85")
    #("STM32F100CB" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M3" 24 128 #f 8 7 #f "2 x WDG,24-bit down counter,RTC" 10 #f 2 0 37 #f #f 2 2 #f #f #f 3 #f "HDMI CEC" #f #f #f 2 3.6 1.7 358 -40 "105,85")
    #("STM32F100R4" "Active" "LQFP 64 10x10x1.4,TFBGA 64 5x5x1.2" "ARM Cortex-M3" 24 16 #f 4 6 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 1 1 #f #f #f 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 358 -40 85)
    #("STM32F100R6" "Active" "LQFP 64 10x10x1.4,TFBGA 64 5x5x1.2" "ARM Cortex-M3" 24 32 #f 4 6 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 1 1 #f #f #f 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 358 -40 85)
    #("STM32F100R8" "Active" "LQFP 64 10x10x1.4,TFBGA 64 5x5x1.2" "ARM Cortex-M3" 24 64 #f 8 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 2 #f #f #f 3 #f "HDMI CEC" #f #f #f 2 3.6 1.7 358 -40 "105,85")
    #("STM32F100RB" "Active" "LQFP 64 10x10x1.4,TFBGA 64 5x5x1.2" "ARM Cortex-M3" 24 128 #f 8 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 2 #f #f #f 3 #f "HDMI CEC" #f #f #f 2 3.6 1.7 358 -40 85)
    #("STM32F100RC" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 24 256 #f 24 11 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 #f 51 #f #f 2 3 #f #f #f 3 2 "HDMI CEC" #f #f #f 2 3.6 2.2 396 -40 85)
    #("STM32F100RD" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 24 384 #f 32 11 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 #f 51 #f #f 2 3 #f #f #f 5 #f "HDMI CEC" #f #f #f 2 3.6 2.2 396 -40 85)
    #("STM32F100RE" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 24 512 #f 32 11 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 #f 51 #f #f 2 3 #f #f #f 5 #f "HDMI CEC" #f #f #f 2 3.6 2.2 396 -40 85)
    #("STM32F100V8" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 24 64 #f 8 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 2 #f #f #f 3 #f "HDMI CEC" #f #f #f 2 3.6 1.7 358 -40 "105,85")
    #("STM32F100VB" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 24 128 #f 8 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 2 #f #f #f 3 #f "HDMI CEC" #f #f #f 2 3.6 1.7 358 -40 "105,85")
    #("STM32F100VC" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 24 256 #f 24 11 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 #f 80 #f #f 2 3 #f #f #f 5 #f "HDMI CEC" #f #f #f 2 3.6 2.2 396 -40 85)
    #("STM32F100VD" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 24 384 #f 32 11 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 #f 80 #f #f 2 3 #f #f #f 5 #f "HDMI CEC" #f #f #f 2 3.6 2.2 396 -40 "105,85")
    #("STM32F100VE" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 24 512 #f 32 11 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 #f 80 #f #f 2 3 #f #f #f 5 #f "HDMI CEC" #f #f #f 2 3.6 2.2 396 -40 "105,85")
    #("STM32F100ZC" "Active" "LQFP 144 20x20x1.4" "ARM Cortex-M3" 24 256 #f 24 11 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 #f 112 #f #f 2 3 #f #f #f 5 #f "HDMI CEC" #f #f #f 2 3.6 2.2 396 -40 85)
    #("STM32F100ZD" "Active" "LQFP 144 20x20x1.4" "ARM Cortex-M3" 24 384 #f 32 11 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 #f 112 #f #f 2 3 #f #f #f 5 #f "HDMI CEC" #f #f #f 2 3.6 2.2 396 -40 85)
    #("STM32F100ZE" "Active" "LQFP 144 20x20x1.4" "ARM Cortex-M3" 24 512 #f 32 11 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 #f 112 #f #f 2 3 #f #f #f 5 #f "HDMI CEC" #f #f #f 2 3.6 2.2 396 -40 "105,85")
    #("STM32F101C4" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M3" 36 16 #f 4 2 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 36 #f #f 1 1 #f #f #f 2 #f #f #f #f #f 2 3.6 1.7 363 -40 85)
    #("STM32F101C6" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M3" 36 32 #f 6 2 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 36 #f #f 1 1 #f #f #f 2 #f #f #f #f #f 2 3.6 1.7 363 -40 85)
    #("STM32F101C8" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55" "ARM Cortex-M3" 36 64 #f 10 3 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 36 #f #f 2 2 #f #f #f 3 #f #f #f #f #f 2 3.6 1.7 363 -40 85)
    #("STM32F101CB" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55" "ARM Cortex-M3" 36 128 #f 16 3 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 36 #f #f 2 2 #f #f #f 3 #f #f #f #f #f 2 3.6 1.7 363 -40 85)
    #("STM32F101R4" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 36 16 #f 4 2 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 51 #f #f 1 1 #f #f #f 2 #f #f #f #f #f 2 3.6 1.7 363 -40 85)
    #("STM32F101R6" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 36 32 #f 6 2 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 51 #f #f 1 1 #f #f #f 2 #f #f #f #f #f 2 3.6 1.7 363 -40 85)
    #("STM32F101R8" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 36 64 #f 10 3 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 51 #f #f 2 2 #f #f #f 3 #f #f #f #f #f 2 3.6 1.7 391 -40 85)
    #("STM32F101RB" "Active" "LQFP 64 10x10x1.4,TFBGA 64 5x5x1.2" "ARM Cortex-M3" 36 128 #f 16 3 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 51 #f #f 2 2 #f #f #f 3 #f #f #f #f #f 2 3.6 1.7 391 -40 85)
    #("STM32F101RC" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 36 256 #f 32 6 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101RD" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 36 384 #f 48 6 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101RE" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 36 512 #f 48 6 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101RF" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 36 768 #f 80 12 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101RG" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 36 1024 #f 80 12 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101T4" "Active" "VFQFPN 36 6x6x1-0" "ARM Cortex-M3" 36 16 #f 4 2 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 26 #f #f 1 1 #f #f #f 2 #f #f #f #f #f 2 3.6 1.7 363 -40 85)
    #("STM32F101T6" "Active" "VFQFPN 36 6x6x1-0" "ARM Cortex-M3" 36 32 #f 6 2 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 26 #f #f 1 1 #f #f #f 2 #f #f #f #f #f 2 3.6 1.7 363 -40 85)
    #("STM32F101T8" "Active" "VFQFPN 36 6x6x1-0" "ARM Cortex-M3" 36 64 #f 10 3 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 26 #f #f 1 1 #f #f #f 2 #f #f #f #f #f 2 3.6 1.7 391 -40 85)
    #("STM32F101TB" "Active" "VFQFPN 36 6x6x1-0" "ARM Cortex-M3" 36 128 #f 16 3 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 26 #f #f 1 1 #f #f #f 2 #f #f #f #f #f 2 3.6 1.7 391 -40 85)
    #("STM32F101V8" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 36 64 #f 10 3 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 80 #f #f 2 2 #f #f #f 3 #f #f #f #f #f 2 3.6 1.7 391 -40 85)
    #("STM32F101VB" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 36 128 #f 16 3 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 80 #f #f 2 2 #f #f #f 3 #f #f #f #f #f 2 3.6 1.7 391 -40 85)
    #("STM32F101VC" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 36 256 #f 32 6 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101VD" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 36 384 #f 48 6 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101VE" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 36 512 #f 48 6 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101VF" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 36 768 #f 80 12 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101VG" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 36 1024 #f 80 12 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101ZC" "Active" "LQFP 144 20x20x1.4" "ARM Cortex-M3" 36 256 #f 32 6 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 112 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101ZD" "Active" "LQFP 144 20x20x1.4" "ARM Cortex-M3" 36 384 #f 48 6 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 112 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101ZE" "Active" "LQFP 144 20x20x1.4" "ARM Cortex-M3" 36 512 #f 48 6 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 112 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101ZF" "Active" "LQFP 144 20x20x1.4" "ARM Cortex-M3" 36 768 #f 80 12 #f "24-bit downcounter,2xWDG,SysTick" 16 #f 2 0 112 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F101ZG" "Active" "LQFP 144 20x20x1.4" "ARM Cortex-M3" 36 1024 #f 80 12 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 112 #f #f 2 3 #f #f #f 3 2 #f #f #f #f 2 3.6 1.9 433 -40 85)
    #("STM32F102C4" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M3" 48 16 #f 4 2 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 36 #f #f 1 1 #f #f "USB Device" 2 #f #f #f #f #f 2 3.6 1.55 348 -40 85)
    #("STM32F102C6" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M3" 48 32 #f 6 2 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f #f 36 #f #f 1 1 #f #f "USB Device" 2 #f #f #f #f #f 2 3.6 1.55 348 -40 85)
    #("STM32F102C8" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M3" 48 64 #f 10 3 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 36 #f #f 2 2 #f #f "USB Device" 3 #f #f #f #f #f 2 3.6 1.7 373 -40 85)
    #("STM32F102CB" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M3" 48 128 #f 16 3 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 36 #f #f 2 2 #f #f "USB Device" 3 #f #f #f #f #f 2 3.6 1.7 373 -40 85)
    #("STM32F102R4" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 48 16 #f 4 2 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 51 #f #f 1 1 #f #f "USB Device" 2 #f #f #f #f #f 2 3.6 1.55 348 -40 85)
    #("STM32F102R6" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 48 32 #f 6 2 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 51 #f #f 1 1 #f #f "USB Device" 2 #f #f #f #f #f 2 3.6 1.55 348 -40 85)
    #("STM32F102R8" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 48 64 #f 10 3 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 51 #f #f 2 2 #f #f "USB Device" 3 #f #f #f #f #f 2 3.6 1.7 373 -40 85)
    #("STM32F102RB" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 48 128 #f 16 3 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 51 #f #f 2 2 #f #f "USB Device" 3 #f #f #f #f #f 2 3.6 1.7 373 -40 85)
    #("STM32F103C4" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M3" 72 16 #f 6 3 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 36 #f #f 1 1 #f 1 "USB Device" 2 #f #f #f #f #f 2 3.6 1.55 337 -40 "105,85")
    #("STM32F103C6" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55" "ARM Cortex-M3" 72 32 #f 10 3 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 36 #f #f 1 1 #f 1 "USB Device" 2 #f #f #f #f #f 2 3.6 1.55 337 -40 "105,85")
    #("STM32F103C8" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M3" 72 64 #f 20 4 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 36 #f #f 2 2 #f 1 "USB Device" 3 #f #f #f #f #f 2 3.6 1.7 373 -40 "105,85")
    #("STM32F103CB" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55" "ARM Cortex-M3" 72 128 #f 20 4 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 36 #f #f 2 2 #f 1 "USB Device" 3 #f #f #f #f #f 2 3.6 1.7 373 -40 "105,85")
    #("STM32F103R4" "Active" "LQFP 64 10x10x1.4,TFBGA 64 5x5x1.2" "ARM Cortex-M3" 72 16 #f 6 3 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 51 #f #f 1 1 #f 1 "USB Device" 2 #f #f #f #f #f 2 3.6 1.55 337 -40 "105,85")
    #("STM32F103R6" "Active" "LQFP 64 10x10x1.4,TFBGA 64 5x5x1.2" "ARM Cortex-M3" 72 32 #f 10 3 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 51 #f #f 1 1 #f 1 "USB Device" 2 #f #f #f #f #f 2 3.6 1.55 337 -40 "105,85")
    #("STM32F103R8" "Active" "LQFP 64 10x10x1.4,TFBGA 64 5x5x1.2" "ARM Cortex-M3" 72 64 #f 20 4 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 51 #f #f 2 2 #f 1 "USB Device" 3 #f #f #f #f #f 2 3.6 1.7 373 -40 "105,85")
    #("STM32F103RB" "Active" "LQFP 64 10x10x1.4,TFBGA 64 5x5x1.2" "ARM Cortex-M3" 72 128 #f 20 4 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 51 #f #f 2 2 #f 1 "USB Device" 3 #f #f #f #f #f 2 3.6 1.7 373 -40 85)
    #("STM32F103RC" "Active" "LQFP 64 10x10x1.4,WLCSP 64" "ARM Cortex-M3" 72 256 #f 64 8 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 3 2 1 "USB Device" 3 2 #f "SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F103RD" "Active" "LQFP 64 10x10x1.4,WLCSP 64" "ARM Cortex-M3" 72 384 #f 64 8 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 3 2 1 "USB Device" 3 2 #f "SDIO" #f #f 2 3.6 1.9 421 -40 85)
    #("STM32F103RE" "Active" "LQFP 64 10x10x1.4,WLCSP 64" "ARM Cortex-M3" 72 512 #f 64 8 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 3 2 1 "USB Device" 3 2 #f "SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F103RF" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 72 768 #f 96 14 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 3 2 1 "USB Device" 3 2 #f "SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F103RG" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 72 1024 #f 96 14 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 3 2 1 "USB Device" 3 2 #f "SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F103T4" "Active" "VFQFPN 36 6x6x1-0" "ARM Cortex-M3" 72 16 #f 6 3 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 26 #f #f 1 1 #f 1 "USB Device" 2 #f #f #f #f #f 2 3.6 1.55 337 -40 "105,85")
    #("STM32F103T6" "Active" "VFQFPN 36 6x6x1-0" "ARM Cortex-M3" 72 32 #f 10 3 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 26 #f #f 1 1 #f 1 "USB Device" 2 #f #f #f #f #f 2 3.6 1.55 373 -40 "105,85")
    #("STM32F103T8" "Active" "VFQFPN 36 6x6x1-0" "ARM Cortex-M3" 72 64 #f 20 4 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 26 #f #f 1 1 #f 1 "USB Device" 2 #f #f #f #f #f 2 3.6 1.7 373 -40 "105,85")
    #("STM32F103TB" "Active" "VFQFPN 36 6x6x1-0" "ARM Cortex-M3" 72 128 #f 20 4 #f "2 x WDG,24-bit down counter,RTC" 10 #f #f 0 26 #f #f 1 1 #f 1 "USB Device" 2 #f #f #f #f #f 2 3.6 1.7 373 -40 "105,85")
    #("STM32F103V8" "Active" "LFBGA 100 10x10x1.7,LQFP 100 14x14x1.4" "ARM Cortex-M3" 72 64 #f 20 4 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 80 #f #f 2 2 #f 1 "USB Device" 3 #f #f #f #f #f 2 3.6 1.7 373 -40 85)
    #("STM32F103VB" "Active" "LFBGA 100 10x10x1.7,LQFP 100 14x14x1.4,UFBGA 100 7x7x0.6" "ARM Cortex-M3" 72 128 #f 20 4 #f "2 x WDG,24-bit down counter,RTC" 16 #f #f 0 80 #f #f 2 2 #f 1 "USB Device" 3 #f #f #f #f #f 2 3.6 1.7 373 -40 "105,85")
    #("STM32F103VC" "Active" "LFBGA 100 10x10x1.7,LQFP 100 14x14x1.4" "ARM Cortex-M3" 72 256 #f 48 8 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 3 2 1 "USB Device" 3 2 #f "FSMC,SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F103VD" "Active" "LFBGA 100 10x10x1.7,LQFP 100 14x14x1.4" "ARM Cortex-M3" 72 384 #f 64 8 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 3 2 1 "USB Device" 3 2 #f "FSMC,SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F103VE" "Active" "LFBGA 100 10x10x1.7,LQFP 100 14x14x1.4" "ARM Cortex-M3" 72 512 #f 64 8 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 3 2 1 "USB Device" 3 2 #f "FSMC,SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F103VF" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 72 768 #f 96 14 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 3 2 1 "USB Device" 3 2 #f "SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F103VG" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 72 1024 #f 96 14 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 3 2 1 "USB Device" 3 2 #f "SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F103ZC" "Active" "LFBGA 144 10x10x1.7,LQFP 144 20x20x1.4" "ARM Cortex-M3" 72 256 #f 48 8 #f "2 x WDG,24-bit down counter,RTC" 21 #f 2 0 112 #f #f 2 3 2 1 "USB Device" 3 2 #f "SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F103ZD" "Active" "LFBGA 144 10x10x1.7,LQFP 144 20x20x1.4" "ARM Cortex-M3" 72 384 #f 64 8 #f "2 x WDG,24-bit down counter,RTC" 21 #f 2 0 112 #f #f 2 3 2 1 "USB Device" 3 2 #f "SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F103ZE" "Active" "LFBGA 144 10x10x1.7,LQFP 144 20x20x1.4" "ARM Cortex-M3" 72 512 #f 64 8 #f "2 x WDG,24-bit down counter,RTC" 21 #f 2 0 112 #f #f 2 3 2 1 "USB Device" 3 2 #f "SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F103ZF" "Active" "LFBGA 144 10x10x1.7,LQFP 144 20x20x1.4" "ARM Cortex-M3" 72 768 #f 96 14 #f "2 x WDG,24-bit down counter,RTC" 21 #f 2 0 112 #f #f 2 3 2 1 "USB Device" 3 2 #f "SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F103ZG" "Active" "LFBGA 144 10x10x1.7,LQFP 144 20x20x1.4" "ARM Cortex-M3" 72 1024 #f 96 14 #f "2 x WDG,24-bit down counter,RTC" 21 #f 2 0 112 #f #f 2 3 2 1 "USB Device" 3 2 #f "SDIO" #f #f 2 3.6 1.9 421 -40 "105,85")
    #("STM32F105R8" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 72 64 #f 64 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 3 2 2 "USB OTG FS" 3 2 #f #f #f #f 2 3.6 1.9 393 -40 "105,85")
    #("STM32F105RB" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 72 128 #f 64 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 3 2 2 "USB OTG FS" 3 2 #f #f #f #f 2 3.6 1.9 393 -40 "105,85")
    #("STM32F105RC" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 72 256 #f 64 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 2 3 2 2 "USB OTG FS" 3 2 #f #f #f #f 2 3.6 1.9 393 -40 "105,85")
    #("STM32F105V8" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 72 64 #f 64 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 #f 80 #f #f 2 3 2 2 "USB OTG FS" 3 2 #f #f #f #f 2 3.6 1.9 393 -40 "105,85")
    #("STM32F105VB" "Active" "LFBGA 100 10x10x1.7,LQFP 100 14x14x1.4" "ARM Cortex-M3" 72 128 #f 64 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 3 2 2 "USB OTG FS" 3 2 #f #f #f #f 2 3.6 1.9 393 -40 "105,85")
    #("STM32F105VC" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 72 256 #f 64 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 2 3 2 2 "USB OTG FS" 3 2 #f #f #f #f 2 3.6 1.9 393 -40 "105,85")
    #("STM32F107RB" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 72 128 #f 64 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 1 3 2 2 "USB OTG FS" 3 2 "Ethernet" #f #f #f 2 3.6 1.9 393 -40 "105,85")
    #("STM32F107RC" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M3" 72 256 #f 64 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 51 #f #f 1 3 2 2 "USB OTG FS" 3 2 "Ethernet" #f #f #f 2 3.6 1.9 393 -40 "105,85")
    #("STM32F107VB" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M3" 72 128 #f 64 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 1 3 2 2 "USB OTG FS" 3 2 "Ethernet" #f #f #f 2 3.6 1.9 393 -40 "105,85")
    #("STM32F107VC" "Active" "LFBGA 100 10x10x1.7,LQFP 100 14x14x1.4" "ARM Cortex-M3" 72 256 #f 64 7 #f "2 x WDG,24-bit down counter,RTC" 16 #f 2 0 80 #f #f 1 3 2 2 "USB OTG FS" 3 2 "Ethernet" #f #f #f 2 3.6 1.9 393 -40 85)))

(define mcu_class
  (let ([target (string-upcase (get "target"))]
        [flash (mcu_query
                mcu_database
                mcu_database_rom_size)])
    (cond ((~~ target "STM32F10[57].*")
           'connectivity-line)
          ((~~ target "STM32F10[0123].*")
           (cond ((<= flash 32) 'low-density)
                 ((<= flash 128) 'medium-density)
                 ((<= flash 512) 'high-density)
                 (else 'xl-density))))
    ))

;; page-size(KBites) sector-size(KBytes) program-word-size(Bytes)
(define mcu_flash
  (cond ((or (= mcu_class 'low-density)
             (= mcu_class 'medium-density))    '(1 4 2))
        ((or (= mcu_class 'high-density)
             (= mcu_class 'connectivity-line)) '(2 4 2))
        ))

;; pattern backup-domain-registers register-size(bytes)
(define mcu_backup
  (let ([target (string-upcase (get "target"))])
    (cond ((and (~~ target "STM32F100.*")
               (or (= mcu_class 'low-density)
                   (= mcu_class 'medium-density))) '(20 4))
          (else '(84 4)))))

;; 8-byte stack align
(define mcu_system
  (let ([target (string-upcase (get "target"))])
    (list (if (~~ target "STM32F100.*")
              'stack-align-8-byte-ensure
              'stack-align-8-byte-already)
          )))
