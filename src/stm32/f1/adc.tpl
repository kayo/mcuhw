[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+  (load "adc.scm")
+][+  include "../all/adc_f01.tpl"
+][+  (add-c-header "stm32/f1/adc.h")
+][+  define adc_init
+][+    # power on
+][+    reg_set ident=2 reg=(adc_reg "CR2") val=ADC_CR2_ADON
+][+    # wait for powering on
+][+    reg_wait ident=2 reg=(adc_reg "CR2") msk=ADC_CR2_ADON
+][+    # reset calibration
+][+    reg_set ident=2 reg=(adc_reg "CR2") val=ADC_CR2_RSTCAL
+][+    # wait for reseting calibration
+][+    reg_wait ident=2 reg=(adc_reg "CR2") unless msk=ADC_CR2_RSTCAL
+][+    # start calibration
+][+    reg_set ident=2 reg=(adc_reg "CR2") val=ADC_CR2_ADCAL
+][+    # wait for calibration
+][+    reg_wait ident=2 reg=(adc_reg "CR2") unless msk=ADC_CR2_ADCAL
+][+    # configure
+][+    reg_put ident=2 reg=(adc_reg "CR1") msk=(adc_ADC_CR1_msk) val=(adc_ADC_CR1_val)
+][+    reg_put ident=2 reg=(adc_reg "CR2") msk=(adc_ADC_CR2_msk) val=(adc_ADC_CR2_val)
+][+    reg_put ident=2 reg=(adc_reg "SMPR1") val=(adc_ADC_SMPR_val 10 17)
+][+    reg_put ident=2 reg=(adc_reg "SMPR2") val=(adc_ADC_SMPR_val 0 9)
+][+    reg_put ident=2 reg=(adc_reg "SQR1") val=(adc_ADC_SQR_val 12 16)
+][+    reg_put ident=2 reg=(adc_reg "SQR2") val=(adc_ADC_SQR_val 6 12)
+][+    reg_put ident=2 reg=(adc_reg "SQR3") val=(adc_ADC_SQR_val 0 6)
+][+    reg_put ident=2 reg=(adc_reg "JSQR") val=(adc_ADC_JSQR_val)
+][+    reg_set ident=2 reg=(adc_reg "CR2") val=(adc_ADC_CR2_START)
+][+  enddef adc_init
+][+  define adc_done
+][+    reg_clr ident=2 reg=(adc_reg "CR2") val=ADC_CR2_ADON
+][+  enddef adc_done
+][+  define adc_trig
+][+    # trigger conversion
+][+    reg_set ident=2 reg=(adc_reg "CR2") val=ADC_CR2_SWSTART
+][+  enddef adc_trig
+][+  define adc_start
+][+    # start conversion
+][+    reg_set ident=2 reg=(adc_reg "CR2") val=ADC_CR2_ADON
+][+  enddef adc_start
+][+  define adc_stop
+][+    # stop conversion
+][+    reg_clr ident=2 reg=(adc_reg "CR2") val=ADC_CR2_ADON
+][+    # enable device
+][+    reg_set ident=2 reg=(adc_reg "CR2") val=ADC_CR2_ADON
+][+  enddef adc_stop
+]
