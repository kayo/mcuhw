[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+  (load "pio.scm")
+][+  include "../all/pio.tpl"
+][+  (add-c-header "stm32/f1/gpio.h")
+][+  define pio_port_init
+][+    reg_put ident=2 reg=(pio_reg "CRL") msk=(pio_CRL 'msk) val=(pio_CRL 'set)
+][+    reg_put ident=2 reg=(pio_reg "CRH") msk=(pio_CRH 'msk) val=(pio_CRH 'set)
+][+    reg_set ident=2 reg=(pio_reg "BSRR") val=(pio_BSRR_cfg)
+][+  enddef pio_port_init
+][+  define pio_port_done
+][+    reg_put ident=2 reg=(pio_reg "CRL") msk=(pio_CRL 'msk) val=(pio_CRL 'clr)
+][+    reg_put ident=2 reg=(pio_reg "CRH") msk=(pio_CRL 'msk) val=(pio_CRH 'clr)
+][+  enddef pio_port_done
+]
