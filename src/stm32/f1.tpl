[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (load "f1/mcu.scm")
+][+ include "all/ld.tpl"
+][+ include "cortex-m/mcu.tpl"
+][+ include "cortex-m/irq.tpl"
+][+ (add-c-header "stm32/f1/memorymap.h")
+][+ include "all/mcu.tpl"
+][+ include "all/bkp.tpl"
+][+ include "all/dma.tpl"
+][+ include "all/dma_v1.tpl"
+][+ include "all/tim.tpl"
+][+ include "f1/clk.tpl"
+][+ include "f1/pio.tpl"
+][+ include "f1/adc.tpl"
+][+ include "all/srt_f124.tpl"
+][+ include "all/spi_l1f124.tpl"
+][+ include "all/i2c_v1.tpl"
+][+ include "all/crc_v1.tpl"
+][+ include "all/iwd_all.tpl"
+]
