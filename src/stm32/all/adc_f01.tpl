[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+  (load "adc_f01.scm")
+][+  define adc_decl
+][+    if (once "adc_def")
+]
/*
 * Calibration values
 */
[+        (adc_cal_decl adc_cal)
+][+    endif (once "adc_def")
+]
/*
 * ADC channel numbers
 */
[+   (adc_ch_defines_regular)
+]
/*
 * Number of ADC channels
 */
#define [+ (ID-NAME "adc_channels") +] [+ (adc_ch_number_regular) +]

/*
 * Number of ADC samples
 */
#define [+ (ID-NAME "adc_samples") +] [+ (adc_samples) +]

/*
 * ADC value type
 */
typedef [+ (adc_data_type) +] [+ (id-name "adc_value_t") +];

/*
 * ADC channels type
 */
typedef union {
  struct {
[+    (adc_ch_fields_regular)
+]  };
  [+ (id-name "adc_value_t") +] _[[+ (ID-NAME "adc_channels") +]];
} [+ (id-name "adc_channels_t") +];

/*
 * ADC samples buffer
 */
extern [+ (id-name "adc_channels_t")
+] [+ (id-name "adc_dma_data")
+][[+ (ID-NAME "adc_samples")
+]];

/*
 * Initialize ADC
 */
void [+ (id-name "adc_init") +](void);

/*
 * Finalize ADC
 */
void [+ (id-name "adc_done") +](void);

/*
 * Start conversion
 */
void [+ (id-name "adc_start") +](void);

/*
 * Stop conversion
 */
void [+ (id-name "adc_stop") +](void);

/*
 * Trigger conversion
 */
void [+ (id-name "adc_trig") +](void);
[+      (add-cbs-if-exist "adc"
          '("dma.on-half" "Half conversion handler")
          '("dma.on-full" "Complete conversion handler"))
+][+  enddef adc_decl
+][+  define adc_impl
+][+    if (once "adc_def")
+]
/*
 * Calibration values
 */
[+        (adc_cal_impl adc_cal)
+][+    endif (once "adc_def")
+]
/*
 * ADC samples buffer
 */
[+ (id-name "adc_channels_t")
+] [+ (id-name "adc_dma_data")
+][[+ (ID-NAME "adc_samples")
+]];

void [+ (id-name "adc_init") +](void) {
[+      dma_conf dn=(get "dma.device") cn=(get "dma.channel") src=periph dst=memory mbits=(adc_dma_bits) pbits=(adc_dma_bits) maddr=(id-name "adc_dma_data") paddr=(string-append "&" (adc_reg "DR")) count=(string-append (ID-NAME "adc_channels") " * " (ID-NAME "adc_samples")) half=(adc_dma_cb "half") complete=(adc_dma_cb "full") mincr circular state=on
+][+    adc_init
+]}

void [+ (id-name "adc_done") +](void) {
[+      adc_done
+][+    dma_set dn=(get "dma.device") cn=(get "dma.channel") state=off
+]}

void [+ (id-name "adc_start") +](void) {
[+      adc_start
+]}

void [+ (id-name "adc_stop") +](void) {
[+      adc_stop
+]}

void [+ (id-name "adc_trig") +](void) {
[+      adc_trig
+]}
[+      dma_isr dn=(get "dma.device") cn=(get "dma.channel") half=(adc_dma_cb "half") complete=(adc_dma_cb "full")
+][+  enddef adc_impl
+]
