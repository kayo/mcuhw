(define (i2c_I2C_CR1)
  (or-bits
   (i2c_I2C_CR1_all)
   ))

(define (i2c_I2C_CR2)
  (or-bits
   ;; DMA requests enable
   (if (or (exist? "rx.dma.enable")
           (exist? "tx.dma.enable"))
       "I2C_CR2_DMAEN")
   ;; Buffer interrupt enable
   (if (or (exist? "rx.on-data")
           (exist? "tx.on-data"))
       "I2C_CR2_ITBUFEN")
   ;; Event interrupt enable
   (if (or (exist? "on-addr")
           (exist? "on-start")
           (exist? "on-stop")
           (exist? "tx.on-done"))
       "I2C_CR2_ITEVTEN")
   ;; Error interrupt enable
   (if (or (exist? "on-error")
           (exist? "on-nack"))
       "I2C_CR2_ITERREN")
   ;; Peripheral clock frequency
   (string-append
    "I2C_CR2_FREQ_"
    (number->string
     (/ (i2c_freq) 1000000))
    "MHZ")
   ))

(define (i2c_I2C_CCR)
  (or-bits
   (if (eq? (i2c_mode) 'fast-mode)
       "I2C_CCR_FS")
   (if (not (is? "duty" "2"))
       "I2C_CCR_DUTY_16_DIV_9")
   (number->string
    (ceiling
    (if (eq? (i2c_mode) 'fast-mode)
        (if (is? "duty" "2")
            (/ (i2c_freq) (* 3 (i2c_rate)))
            (/ (i2c_freq) (* 25 (i2c_rate))))
        (/ (i2c_freq) (* 2 (i2c_rate)))
        )))
   ))

(define (i2c_I2C_TRISE)
  (number->string
   (inexact->exact
    (ceiling
     (if (has-num? "rise")
         (get-num "rise")
         (if (eq? (i2c_mode) 'fast-mode)
             (* (i2c_freq) 300e-9)
             (* (i2c_freq) 1000e-9)))
     ))))

(define (i2c_I2C_TIMINGR)
  (or-bits))
