[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+  define flash_decl
+]
/*
 * Flash page size
 */
#define FLASH_PAGE_SIZE ([+ (car mcu_flash) +] << 10)

/*
 * Flash sector size
 */
#define FLASH_SECT_SIZE ([+ (cadr mcu_flash) +] << 10)

/*
 * Minumum flash word size to write
 */
#define FLASH_WORD_SIZE [+ (caddr mcu_flash) +]

/*
 * Flash errors
 */
typedef enum {
  flash_success = 0,
  flash_not_empty = 1 << 2,
  flash_protected = 1 << 4,
} flash_res_t;

/*
 * Flash unlock sequece
 */
void flash_unlock(void *ptr);

/*
 * Flash lock sequece
 */
void flash_lock(void *ptr);

/*
 * Erase flash
 */
flash_res_t flash_erase(void *ptr, uint16_t len);

/*
 * Write flash
 */
flash_res_t flash_write(void *ptr, const void *src, uint16_t len);
[+    enddef flash_decl
+][+  define flash_bank
+][+    if (> mcu_flash_size 512)
+]const uint8_t bank = FLASH_BANK(ptr);[+
        else !xl-density
+](void)ptr;
  const uint8_t bank = FLASH_BANK1;[+
        endif xl-density
+][+  enddef flash_bank
+][+  define flash_wait
+][+    reg_wait reg='FLASH_SR(bank)' msk='FLASH_SR_EOP | FLASH_SR_PGERR | FLASH_SR_WRPRTERR'
+][+    reg_get var=(get "status") reg='FLASH_SR(bank)' msk='FLASH_SR_PGERR | FLASH_SR_WRPRTERR'
+][+    reg_set reg='FLASH_SR(bank)' val='FLASH_SR_EOP | FLASH_SR_PGERR | FLASH_SR_WRPRTERR'
+][+  enddef flash_wait
+][+  define flash_impl
+]
void flash_unlock(void *ptr) {
  [+ flash_bank +]
  FLASH_KEYR(bank) = FLASH_KEYR_KEY1;
  FLASH_KEYR(bank) = FLASH_KEYR_KEY2;
}

void flash_lock(void *ptr) {
  [+ flash_bank +]
  FLASH_CR(bank) |= FLASH_CR_LOCK;
}

flash_res_t flash_erase(void *ptr, uint16_t len) {
  [+ flash_bank +]

  /* enable flash erase */
  FLASH_CR(bank) |= FLASH_CR_PER;

  flash_res_t res = flash_success;
  uint8_t *d = ptr;
  uint16_t i = 0;

  for (; i < len && res == flash_success; i += FLASH_PAGE_SIZE) {
    /* set page address */
    FLASH_AR(bank) = (uint32_t)&d[i];

    /* perform erase operation */
    FLASH_CR(bank) |= FLASH_CR_STRT;

    /* wait for operation complete */
[+ flash_wait ident=4 status=res
+]  }

  /* disable flash erase */
  FLASH_CR(bank) &= ~FLASH_CR_PER;

  return res;
}

flash_res_t flash_write(void *ptr, const void *src, uint16_t len) {
  [+ flash_bank +]

  /* enable flash program */
  FLASH_CR(bank) |= FLASH_CR_PG;

  flash_res_t res = flash_success;
  uint8_t *d = ptr;
  const uint8_t *s = src;
  uint16_t i = 0;

  for (; i < len && res == flash_success; i += FLASH_WORD_SIZE) {
    /* write half word */
    MMIO16(&d[i]) = *(const uint16_t*)&s[i];

    /* wait for operation complete */
[+ flash_wait ident=4 status=res
+]  }

  /* disable flash program */
  FLASH_CR(bank) &= ~FLASH_CR_PG;

  return res;
}
[+    enddef flash_impl
+]
