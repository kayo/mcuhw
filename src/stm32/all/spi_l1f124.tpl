[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (load "spi_l1f124.scm")
+][+ define spi_set_bits_body
+]  if (bits == spi_bits_8) {
[+     reg_clr ident=4 reg=(spi_reg "CR1") val="SPI_CR1_DFF"
+]  } else {
[+     reg_set ident=4 reg=(spi_reg "CR1") val="SPI_CR1_DFF"
+]  }
[+   enddef spi_set_bits_body
+][+ include "spi.tpl"
+][+ (add-c-header "stm32/all/spi_l1f124.h")
+]
