(define (clk_HSI_frequency) ;; Get HSI frequency
  8000000)

(define (clk_HSE_frequency) ;; Get HSE frequency
  (if (has-num? "HSE.frequency")
      (check-range 'error "HSE.frequency"
                   (get-num "HSE.frequency")
                   4000000 32000000)))

(define (clk_PLL_frequency) ;; Get PLL frequency
  (let* ([src (if (has? "PLL.source")
                  (get "PLL.source") "HSI/2")]
         [osc (if (string=? src "HSE")
                  (clk_HSE_frequency)
                  (clk_HSI_frequency))]
         [div (if (string=? src "HSE")
                  (if (has-num? "PLL.divider")
                      (check-range 'error "PLL.divider"
                                   (get-num "PLL.divider")
                                   1 16) 1)
                  2)]
         [mul (if (has-num? "PLL.multiplier")
                  (check-range 'error "PLL.multiplier"
                               (get-num "PLL.multiplier")
                               2 16) 1)])
    (* (/ osc div) mul)))

(define (clk_SYS_frequency) ;; Get system clock frequency
  (let ([src (if (exist? "SYS.source")
                 (get "SYS.source") "HSI")])
    (check-range 'warn "calculated SYS.frequency"
                 (cond
                  ((string=? src "HSI") (clk_HSI_frequency))
                  ((string=? src "HSE") (clk_HSE_frequency))
                  ((string=? src "PLL") (clk_PLL_frequency)))
                 0 (* 1000000 (mcu_query mcu_database
                                         mcu_database_speed_mhz)))))

(define (clk_AHB_frequency) ;; Get AHB clock frequency
  (let ([osc (clk_SYS_frequency)]
        [div (if (has-num? "AHB.divider")
                 (check-oneof 'error "AHB.divider"
                              (get-num "AHB.divider")
                              1 2 4 8 16 64 128 256 512) 1)])
    (/ osc div)))

(define (clk_APBx_frequency x) ;; Get APB clock frequency
  (let* ([key (string-append "APB" x ".divider")]
         [osc (clk_AHB_frequency)]
         [div (if (has-num? key)
                  (check-oneof 'error key
                               (get-num key)
                               1 2 4 8 16) 1)])
    (/ osc div)))

(define (clk_LSI_frequency)
  (if (exist? "LSI")
      40000
      0))

(define (clk_LSE_frequency)
  (if (exist? "LSE")
      (if (has-num? "LSE.frequency")
          (get-num "LSE.frequency")
          32768)
      0))

(define (clk_RTC_frequency_HSE/x x)
  (if (exist? "RTC")
      (cond
       ((is? "RTC.source" "LSE") (clk_LSE_frequency))
       ((is? "RTC.source" "LSI") (clk_LSI_frequency))
       ((is? "RTC.source" (string-append
                           "HSE/" (number->string x)))
        (/ (clk_HSE_frequency) x))
       (else 0))
      0))

(define (clk_STK_source_frequency)
  (if (has? "STK.source")
      (cond
       ((is? "STK.source" "AHB") (clk_AHB_frequency))
       ((is? "STK.source" "AHB/8") (/ (clk_AHB_frequency) 8))
       ((is? "STK.source" "EXT") (get-num "STK.external_frequency")))
      (/ (clk_AHB_frequency) 8)))

(define (clk_STK_reload)
  (cond
   ((has-num? "STK.reload") ;; Get reload value directly
    (check-range 'error "STK.reload"
                 (get-num "STK.reload")
                 0 (max-uint 24)))
   ((has-num? "STK.divider") ;; Get reload value directly
    (check-range 'error "STK.divider"
                 (get-num "STK.divider")
                 1 (+ 1 (max-uint 24))))
   ((has-frequency? "STK") ;; Get reload value from frequency or period
    (- (/ (clk_STK_source_frequency)
          (get-frequency "STK")) 1))
   (else (max-uint 24))))

(define (clk_STK_frequency)
  (if (exist? "STK")
      (/ (clk_STK_source_frequency)
         (+ (clk_STK_reload) 1))
      0))

(define (clk_RCC_CR sfx . lst)
    (apply or-bits
           (map (lambda (key)
                  (if (exist? key)
                      (string-append "RCC_CR_" key sfx)))
                lst)))

(define (clk_RCC_CSR sfx . lst)
  (apply or-bits
         (map (lambda (key)
                (if (exist? key)
                    (string-append "RCC_CSR_" key sfx)))
              lst)))

(define (clk_RCC_CIR sfx . lst)
  (if (is? "wait" "sleep")
      (apply or-bits
       (map (lambda (dev)
              (if (exist? dev)
                  (string-append "RCC_CIR_" dev sfx)))
            lst))
      ""))

(define (clk_STK_CSR_msk)
  (or-bits
   (if (exist? "STK")
       "STK_CSR_ENABLE")
   (if (has? "STK.interrupt")
       "STK_CSR_TICKINT")
   (if (has? "STK.source")
       "STK_CSR_CLKSOURCE")))

(define (clk_STK_CSR_set)
  (or-bits
   (if (exist? "STK")
       "STK_CSR_ENABLE")
   (if (exist? "STK.on-tick")
       "STK_CSR_TICKINT")
   (if (has? "STK.source")
       (get-def-name "STK.source"
                     "STK_CSR_CLKSOURCE_"))))

(define (clk_STK_CSR_clr)
  (or-bits
   (if (exist? "STK")
       "STK_CSR_ENABLE")))

(define (clk_STK_RVR_val)
  (if (exist? "STK")
      (number->string
       (clk_STK_reload))))

(define (clk_RCC_CFGR_SW_msk)
  (if (has? "SYS.source") "RCC_CFGR_SW"))

(define (clk_RCC_CFGR_SW_set)
  (if (has? "SYS.source")
      (get-def-name "SYS.source" "RCC_CFGR_SW_")))

(define (clk_RCC_CFGR_SW_clr)
  (if (has? "SYS.source") "RCC_CFGR_SW_HSI"))

(define (clk_RCC_BDCR_msk)
  (or-bits
   (if (exist? "RTC")
       "RCC_BDCR_RTCEN")
   (if (has? "RTC.source")
       "RCC_BDCR_RTCSEL")
   (if (has? "LSE.drive")
       "RCC_BDCR_LSEDRV")
   (if (has? "LSE")
       "RCC_BDCR_LSEON")))

(define (clk_RCC_BDCR_set)
  (or-bits
   (if (exist? "RTC")
       "RCC_BDCR_RTCEN")
   (if (has? "RTC.source")
       (get-def-name "RTC.source"
                     "RCC_BDCR_RTCSEL_"))
      (if (has? "LSE.drive")
       (get-def-name "LSE.drive"
                     "RCC_BDCR_LSEDRV_"))
   (if (exist? "LSE")
       "RCC_BDCR_LSEON")
   ))

(define (clk_RCC_BDCR_clr)
  (or-bits
   (if (exist? "RTC")
       "RCC_BDCR_RTCEN")
   (if (exist? "LSE")
       "RCC_BDCR_LSEON")))

(define (clk_RCC_BDCR_rdy)
  (or-bits
   (if (exist? "LSE")
       "RCC_BDCR_LSERDY")))

(define (clk_RCC_CFGR_msk)
  (or-bits
   ;; Switch SYSCLK to HSE or HSI if configured
   (if (and (exist? "HSE")
            (not (is? "SYS.source" "HSE"))) "RCC_CFGR_SW")
   (if (has-num? "AHB.divider") "RCC_CFGR_HPRE")
   (if (has-num? "APB.divider") "RCC_CFGR_PPRE")
   (if (has-num? "APB1.divider") "RCC_CFGR_PPRE1")
   (if (has-num? "APB2.divider") "RCC_CFGR_PPRE2")
   (if (has-num? "PLL.multiplier") "RCC_CFGR_PLLMUL")
   ;; PLL divider bit0 (compatibility with old device revisions)
   (if (has-num? "PLL.divider") "RCC_CFGR_PLLXTPRE")
   (if (has? "PLL.source") "RCC_CFGR_PLLSRC")
   (if (and clk_RCC_CFGR_ADCPRE
            (has-num? "ADC.divider")) "RCC_CFGR_ADCPRE")
   ))

(define (clk_RCC_CFGR_val)
  (or-bits
   ;; Switch SYSCLK to HSE if configured
   (if (and (exist? "HSE")
            (not (is? "SYS.source" "HSE"))) "RCC_CFGR_SW_HSE")
   (if (has-num? "AHB.divider")
       (get-def-name "AHB.divider" "RCC_CFGR_HPRE_DIV"))
      (if (has-num? "APB.divider")
       (get-def-name "APB.divider" "RCC_CFGR_PPRE_DIV"))
   (if (has-num? "APB1.divider")
       (get-def-name "APB1.divider" "RCC_CFGR_PPRE1_DIV"))
   (if (has-num? "APB2.divider")
       (get-def-name "APB2.divider" "RCC_CFGR_PPRE2_DIV"))
   ;; Set PLL multiplier
   (if (has-num? "PLL.multiplier")
       (get-def-name "PLL.multiplier" "RCC_CFGR_PLLMUL_MUL"))
   ;; Set PLL divider bit0 (compatibility with old device revisions)
   (if (and (string=? (get "PLL.source") "HSE")
            (string=? (get "PLL.divider") "2"))
       "RCC_CFGR_PLLXTPRE_HSE_DIV2")
   (if (has? "PLL.source")
       (get-def-name "PLL.source" "RCC_CFGR_PLLSRC_"))
   (if (and clk_RCC_CFGR_ADCPRE (has-num? "ADC.divider"))
       (get-def-name "ADC.divider" "RCC_CFGR_ADCPRE_DIV"))
   ))

(define (clk_ADC_CFGR2_val)
  (if (and (not clk_RCC_CFGR_ADCPRE)
           (has? "ADC.source"))
      (get-def-name "ADC.source" "ADC_CFGR2_CKMODE_")
      ""))

(define (clk_RCC_CFGR2_PREDIVx_PLLy x y)
  (let ([key (string-append "PLL" y ".divider")]
        [pfx (string-append "RCC_CFGR2_PREDIV" x "_DIV")])
    (or-bits
     (if (has? key)
         (get-def-name key pfx)))))

(define (clk_FLASH_ACR)
  (let ([osc (clk_SYS_frequency)])
    (cond
     ((< 48000000 osc) "FLASH_ACR_LATENCY_2WS")
     ((< 24000000 osc) "FLASH_ACR_LATENCY_1WS")
     )))

(define (clk_RCC_AxBENR bus devices)
  (apply or-bits
         (map (lambda (dev) (string-append "RCC_" bus "ENR_" dev "EN"))
              (filter (lambda (dev)
                        (let ([pio (~~ dev "^GPIO([A-F])$")])
                          (exist? (if pio (string-substitute
                                           dev '("GPIO") '("PORT"))
                                      dev))))
                      devices))))

(define (clk_RCC_AHBENR)
  (clk_RCC_AxBENR "AHB" clk_RCC_AHB_periph))

(define (clk_RCC_APB1ENR)
  (clk_RCC_AxBENR "APB1" clk_RCC_APB1_periph))

(define (clk_RCC_APB2ENR)
  (clk_RCC_AxBENR "APB2" clk_RCC_APB2_periph))

(define (clk_add_cbs)
  (add-callback-if-exist "STK.on-tick" "clk_on_tick" "System tick handler"))

(define (clk_PWR_CR_msk)
  (or-bits
   (if (exist? "PWR.backup-access")
       "PWR_CR_DBP")
   (if (exist? "PWR.voltage-detector.enable")
       "PWR_CR_PVDE")
   (if (has-num? "PWR.voltage-detector.level")
       "PWR_CR_PLS_MASK")
   ))

(define (clk_PWR_CR_set)
  (or-bits
   (if (is-true? "PWR.backup-access")
       "PWR_CR_DBP")
   (if (exist? "PWR.voltage-detector.enable")
       "PWR_CR_PVDE")
   (if (has-num? "PWR.voltage-detector.level")
       (string-append "PWR_CR_PLS_"
                      (join "V" (stack
                                 "PWR.voltage-detector.level"))))
   ))

(define (clk_PWR_CR_clr)
  (or-bits
   "0"))

(define (clk_SCS_DEMCR)
  (or-bits
   (if (exist? "DWT")
       "SCS_DEMCR_TRCENA")
   ))

(define (clk_DWT_CTRL)
  (or-bits
   (if (exist? "DWT.cycles-counter")
       "DWT_CTRL_CYCCNTENA")
   ))
