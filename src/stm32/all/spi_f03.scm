(define spi_word_sizes
  '(4 5 6 7 8 9 10 11 12 13 14 15 16))

(define (spi_SPI_CR1)
  (or-bits
   (spi_SPI_CR1_all)
   ;; CRC length
   (if (is? "crc.bits" "16")
       "SPI_CR1_CRCL")
   ))

(define (spi_SPI_CR2)
  (or-bits
   (spi_SPI_CR2_all)
   ;; Last DMA transfer for transmission
   (if (is? "tx.dma.last" "odd")
       "SPI_CR2_LDMA_TX")
   ;; Last DMA transfer for reception
   (if (is? "rx.dma.last" "odd")
       "SPI_CR2_LDMA_RX")
   ;; FIFO reception threshold
   (if (is? "rx.threshold" "1/4" "8")
       "SPI_CR2_RFXTH")
   ;; Data size
   (if (has-num? "frame.bits")
       (get-def-name "frame.bits" "SPI_CR2_DS_" "BIT"))
   ;; Frame format
   (if (exist? "frame.ti-mode")
       "SPI_CR2_FRF")
   ;; NSS pulse management
   (if (exist? "master.select")
       "SPI_CR2_NSSP")
   ))
