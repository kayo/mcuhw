[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (load "i2c_v2.scm")
+][+ (add-c-header "stm32/all/i2c_v2.h")
+][+ include "i2c.tpl"
+][+ define i2c_start
+][+   reg_set ident=2 reg=(i2c_reg "CR2") val=I2C_CR2_START
+][+ enddef i2c_start
+][+ define i2c_stop
+][+   reg_set ident=2 reg=(i2c_reg "CR2") val=I2C_CR2_STOP
+][+ enddef i2c_stop
+][+ define i2c_get
+]  return [+ (i2c_reg "RXDR") +];
[+   enddef i2c_get
+][+ define i2c_put
+][+   reg_put ident=2 reg=(i2c_reg "TXDR") val=data
+][+ enddef i2c_put
+][+ define i2c_xfer_init
   +][+   reg_put reg=(i2c_reg "CR2") msk=(string-append (if (has? "addr") "I2C_CR2_SADD_7BIT_MASK" "0") (if (has? "dir") " | I2C_CR2_RD_WRN" "") (if (has? "len") " | I2C_CR2_NBYTES_MASK" "") (if (has? "autoend") " | I2C_CR2_AUTOEND" "") (if (exist? "start") " | I2C_CR2_START" "")) val=(string-append (if (has? "addr") (string-append "(((" (get "addr") ") & 0x7F) << I2C_CR2_SADD_7BIT_SHIFT)") "0") (if (is? "dir" "in" "read") " | I2C_CR2_RD_WRN" "") (if (has? "len") (string-append " | ((" (get "len") ") << I2C_CR2_NBYTES_SHIFT)") "") (if (has? "autoend") (string-append " | ((" (get "autoend") ") ? I2C_CR2_AUTOEND : 0)") "") (if (exist? "start") " | I2C_CR2_START" ""))
+][+ enddef i2c_xfer_init
+][+ define i2c_xfer
+]  if (wptr) {
[+   i2c_xfer_init ident=4 addr=addr dir=out len=wlen autoend='!rlen' start
+]    for (; wlen--; ) {
      bool wait = true;
      for (; wait; ) {
        if ([+ reg_has reg=(i2c_reg "ISR") msk=I2C_ISR_TXIS +]) wait = false;
        [+ reg_wait reg=(i2c_reg "ISR") msk=I2C_ISR_NACKF unless
+]      }
[+      reg_put ident=6 reg=(i2c_reg "TXDR") val='*wptr++'
+]    }
    if (rlen) [+ reg_wait reg=(i2c_reg "ISR") msk=I2C_ISR_TC
+]  }

  if (rptr) {
[+   i2c_xfer_init ident=4 addr=addr dir=in len=rlen start
+][+ i2c_xfer_init ident=4 autoend=1
+]    for (; rlen--;) {
[+      reg_wait ident=6 reg=(i2c_reg "ISR") msk=I2C_ISR_RXNE
+]      *rptr++ = [+ (i2c_reg "RXDR") +];
    }
  }
[+ enddef i2c_xfer
+]
