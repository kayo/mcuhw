[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+  # Timer for all
+][+  (load "tim.scm")
+][+  (add-c-header "stm32/all/tim.h")
+][+  define tim_init
+][+    (tim_checks)
+][+    reg_put ident=2 reg=(tim_reg "DCR") val=(tim_TIM_DCR)
+][+    reg_put ident=2 reg=(tim_reg "DIER") val=(tim_TIM_DIER)
+][+    reg_put ident=2 reg=(tim_reg "SMCR") val=(tim_TIM_SMCR)
+][+    reg_put ident=2 reg=(tim_reg "CCMR1") val=(tim_TIM_CCMR1)
+][+    reg_put ident=2 reg=(tim_reg "CCMR2") val=(tim_TIM_CCMR2)
+][+    reg_put ident=2 reg=(tim_reg "PSC") val=(tim_TIM_PSC)
+][+    reg_put ident=2 reg=(tim_reg "ARR") val=(tim_TIM_ARR)
+][+    reg_put ident=2 reg=(tim_reg "RCR") val=(tim_TIM_RCR)
+][+    # configure polarity and enable channels
+][+    reg_put ident=2 reg=(tim_reg "CCER") val=(tim_TIM_CCER)
+][+    reg_put ident=2 reg=(tim_reg "CR2") val=(tim_TIM_CR2)
+][+    reg_put ident=2 reg=(tim_reg "BDTR") val=(tim_TIM_BDTR)
+][+    # configure and enable counter
+][+    reg_put ident=2 reg=(tim_reg "CR1") val=(tim_TIM_CR1)
+][+  enddef tim_init
+][+  define tim_done
+][+    # disable counter
+][+    reg_clr ident=2 reg=(tim_reg "CR1") val=TIM_CR1_CEN
+][+    # disable channels
+][+    reg_clr ident=2 reg=(tim_reg "CCER") val=(tim_TIM_CCER #t)
+][+  enddef tim_done
+][+  # timer declaration
+][+  define tim_decl
+]
/*
 * Maximum value of timer counter
 */
#define [+ (ID-NAME "tim_top") +] [+ (tim_top_value) +]

/*
 * Modulation frequency of timer
 */
#define [+ (ID-NAME "tim_mod") +] [+ (tim_mod_frequency) +]

/*
 * Initialize timer
 */
void [+ (id-name "tim_init") +](void);

/*
 * Finalize timer
 */
void [+ (id-name "tim_done") +](void);

/*
 * Start timer counter
 */
void [+ (id-name "tim_start") +](void);

/*
 * Stop timer counter
 */
void [+ (id-name "tim_stop") +](void);

/*
 * Read timer counter
 */
uint16_t [+ (id-name "tim_get") +](void);

/*
 * Write timer counter
 */
void [+ (id-name "tim_put") +](uint16_t value);
[+      for cn in 1 2 3 4
+][+      if (exist? (tim_ch (get "cn")))
+][+        if (exist? (tim_ch (get "cn") "input"))
+]
/*
 * Read captured channel value
 */
uint16_t [+ (tim_ch_id-name (get "cn") "get") +](void);
[+          endif cn input
+][+        if (exist? (tim_ch (get "cn") "output"))
+]
/*
 * Write channel compare value
 */
void [+ (tim_ch_id-name (get "cn") "put") +](uint16_t value);
[+            (tim_ch_out_set_funcs (get "cn") #f)
+][+        endif ch input
+][+      endif ch exist
+][+    endfor ch
+][+    (tim_add_interrupts)
+][+  enddef tim_decl
+][+  # timer implementation
+][+  define tim_impl
+]
void [+ (id-name "tim_init") +](void) {
[+      tim_init
+]}

void [+ (id-name "tim_done") +](void) {
[+      tim_done
+]}

void [+ (id-name "tim_start") +](void) {
[+      reg_set ident=2 reg=(tim_reg "CR1") val=TIM_CR1_CEN
+]}

void [+ (id-name "tim_stop") +](void) {
[+      reg_clr ident=2 reg=(tim_reg "CR1") val=TIM_CR1_CEN
+]}

uint16_t [+ (id-name "tim_get") +](void) {
  return [+ (tim_reg "CNT") +];
}

void [+ (id-name "tim_put") +](uint16_t value) {
  [+ (tim_reg "CNT") +] = value;
}
[+      for cn in 1 2 3 4
+][+      if (exist? (tim_ch (get "cn")))
+][+        if (exist? (tim_ch (get "cn") "input"))
+]
uint16_t [+ (tim_ch_id-name (get "cn") "get") +](void) {
  return [+ (tim_reg "CCR" (get "cn")) +];
}
[+          endif ch input
+][+        if (exist? (tim_ch (get "cn") "output"))
+]
void [+ (tim_ch_id-name (get "cn") "put") +](uint16_t value) {
  [+ (tim_reg "CCR" (get "cn")) +] = value;
}
[+            (tim_ch_out_set_funcs (get "cn") #t)
+][+        endif ch output
+][+        if (or (exist? (tim_ch (get "cn") "on-capture")) (exist? (tim_ch (get "cn") "on-compare")))
+][+          irq_isr name=(tim_isr_name "compare" (get "cn"))
+]{
[+              (run-callback-if-exist (tim_ch (get "cn") "on-capture") (tim_cb_name "capture" (get "cn")))
+][+            (run-callback-if-exist (tim_ch (get "cn") "on-compare") (tim_cb_name "compare" (get "cn")))
+]}
[+          endif exist cb
+][+      endif ch exist
+][+    endfor ch
+][+    for intr in "break" "trigger" "update" "com"
+][+      if (exist? (tim_cb_var (get "intr")))
+][+        irq_isr name=(tim_isr_name (get "intr"))
+]{
[+            (run-callback-if-exist (tim_cb_var (get "intr")) (tim_cb_name (get "intr")))
+]}
[+        endif exist
+][+    endfor intr
+][+  enddef tim_impl
+]
