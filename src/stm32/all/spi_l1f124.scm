(define spi_word_sizes
  '(8 16))

(define (spi_SPI_CR1)
  (or-bits
   (spi_SPI_CR1_all)
   ;; Data frame format
   (if (is? "frame.bits" "16")
       "SPI_CR1_DFF")
   ))

(define (spi_SPI_CR2)
  (or-bits
   (spi_SPI_CR2_all)
   ))
