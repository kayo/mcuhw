(define (spi_dev)
  (let* ([dev (get "device")]
         [num (string->number dev)])
    (string-append "SPI" dev)))

(define (spi_reg reg)
  (string-append "SPI_" reg "(" (string-upcase (spi_dev)) ")"))

(define (spi_irq name)
  (string-append (string-downcase (spi_dev)) "_" name))

(define spi_dividers
  '(2 4 8 16 32 64 128 256))

(define (spi_word_size_options sizes)
  (join ""
   (map (lambda (bits)
          (string-append "  spi_bits_" (number->string bits) " = "
                         (number->string (- bits 1)) ",\n"))
        sizes)))

(define (spi_SPI_CR1_BR)
  (if (has-num? "divider")
      (string-append
       "SPI_CR1_BR_FPCLK_DIV_"
       (number->string
        (check-oneof 'error "SPI.divider"
                     (get-num "divider")
                     spi_dividers)))
      (cond
       ((and (has-num? "baud-rate")
             (has-num? "clock"))
        (string-append
         "SPI_CR1_BR_FPCLK_DIV_"
         (number->string
          (apply select-divider
                 (cons (/ (get-num "clock")
                          (get-num "baud-rate"))
                       spi_dividers)))))
       ((and (has? "baud-rate")
             (has? "clock"))
        (string-append
         "SPI_CR1_BR_FPCLK_DIV("
         (if (has-num? "clock")
             (number->string
              (get-num "clock"))
             (get "clock"))
         " / "
         (if (has-num? "baud-rate")
             (number->string
              (get-num "baud-rate"))
             (get "baud-rate"))
         ")"))
       (else (check-error 'error "SPI speed not configured." 0)))
      ))

(define (spi_data_type)
  (string-append
   "uint"
   (number->string (spi_dma_bits))
   "_t"))

(define (spi_dma_bits)
  (if (and (has-num? "frame.bits")
           (< 8 (get-num "frame.bits")))
      16 8))

(define (spi_SPI_CR1_all)
  (or-bits
   ;; Bidirectional data mode enable
   (if (not (exist? "rx.enable"))
       "SPI_CR1_BIDIMODE")
   ;; Output enable in bidirectional mode
   (if (and
        (not (exist? "rx.enable"))
        (exist? "tx.enable"))
       "SPI_CR1_BIDIOE")
   ;; Hardware CRC calculation enable
   (if (exist? "crc.enable")
       "SPI_CR1_CRCEN")
   ;; Transmit CRC next (SPI_CR1_CRCNEXT)
   ;; CRC length
   (if (is? "crc.bits" "16")
       "SPI_CR1_CRCL")
   ;; Receive only mode enabled
   (if (and (not (exist? "tx.enable"))
            (exist? "rx.enable"))
       "SPI_CR1_RXONLY")
   ;; Software slave management
   (if (exist? "slave.soft")
       "SPI_CR1_SSM")
   ;; Internal slave select
   (if (is-true? "slave.select")
       "SPI_CR1_SSI")
   ;; Frame format
   (if (exist? "frame.lsb-first")
       "SPI_CR1_LSBFIRST")
   ;; SPI enable
   (if (exist? "enable")
       "SPI_CR1_SPE")
   ;; Baud rate control
   (spi_SPI_CR1_BR)
   ;; Master selection
   (if (exist? "master.enable")
       "SPI_CR1_MSTR")
   ;; Clock polarity
   (if (is-false? "sck-line.active")
       "SPI_CR1_CPOL")
   ;; Clock phase
   (if (is? "sck-line.phase" "2")
       "SPI_CR1_CPHA")
   ))

(define (spi_SPI_CR2_all)
  (or-bits
   ;; Tx buffer empty interrupt enable
   (if (exist? "tx.on-data")
       "SPI_CR2_TXEIE")
   ;; Rx buffer not empty interrupt enable
   (if (exist? "rx.on-data")
       "SPI_CR2_RXNEIE")
   ;; Error interrupt enable
   (if (exist? "on-error")
       "SPI_CR2_ERRIE")
   ;; SS output enable
   (if (and (exist? "master")
            (not (exist? "master.multi")))
       "SPI_CR2_SSOE")
   ;; Tx buffer DMA enable
   (if (exist? "tx.dma")
       "SPI_CR2_TXDMAEN")
   ;; Rx buffer DMA enable
   (if (exist? "rx.dma")
       "SPI_CR2_RXDMAEN")
   ))

(define (spi_SPI_CRCPR)
  (if (has-num? "crc.poly")
      (get-num "crc.poly")))
