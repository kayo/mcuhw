[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ include "srt.tpl"
+][+ (add-c-header "stm32/all/usart_f124.h")
+][+ (load "srt_f124.scm")
+][+ define srt_get_databits
+]  return [+ reg_has reg=(srt_reg "CR1") msk=USART_CR1_M +] ? srt_databits_9 : srt_databits_8;
[+   enddef srt_get_databits
+][+ define srt_set_databits
+]  [+ reg_put reg=(srt_reg "CR1") msk="USART_CR1_M" val="databits == srt_databits_9 ? USART_CR1_M : 0"
+][+ enddef srt_set_databits
+][+ define srt_wait_send
+][+   reg_wait ident=2 reg=(srt_reg "SR") msk=USART_SR_TXE
+][+ enddef srt_wait_send
+][+ define srt_send_data
+][+   reg_put ident=2 reg=(srt_reg "DR") val="data & USART_DR_MASK"
+][+ enddef srt_send_data
+][+ define srt_wait_recv
+][+   reg_wait ident=2 reg=(srt_reg "SR") msk=USART_SR_RXNE
+][+ enddef srt_wait_recv
+][+ define srt_recv_data
   +]  return [+ (srt_reg "DR") +] & USART_DR_MASK;
[+   enddef srt_recv_data
+][+ define srt_error
+]  return ([+ reg_has reg=(srt_reg "SR") msk=USART_SR_ORE +] ? srt_err_overrun : srt_err_none)
    | ([+ reg_has reg=(srt_reg "SR") msk=USART_SR_NE +] ? srt_err_noise : srt_err_none)
    | ([+ reg_has reg=(srt_reg "SR") msk=USART_SR_FE +] ? srt_err_framing : srt_err_none)
    | ([+ reg_has reg=(srt_reg "SR") msk=USART_SR_PE +] ? srt_err_parity : srt_err_none);
[+   enddef srt_error
+]
