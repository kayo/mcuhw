(define (adc_reg reg)
  (string-append "ADC_" reg "(ADC" (if (has? "device") (get "device") "") ")"))

(define (adc_ch_key num . key)
  (string-append
   (cond
    ((= num 16) "Temp")
    ((= num 17) "Vref")
    ((= num 18) "Vbat")
    (else (string-append "CH[" (number->string num) "]")))
   (if (null? key) "" (string-append "." (car key)))))

(define (adc_ch_exist? num . key)
  (exist? (apply adc_ch_key (cons num key))))

(define (adc_ch_has? num . key)
  (has? (apply adc_ch_key (cons num key))))

(define (adc_ch_get num . key)
  (get (apply adc_ch_key (cons num key))))

(define (adc_ch_list . args) ;; list of existing channels
  (let ([lst (filter adc_ch_exist?
                     (if (exist? "reverse")
                         (gen-sequence 18 0)
                         (gen-sequence 0 18)))])
    (if (or (null? args)
            (not (has? (car args))))
        lst
        (let ([seq (stack (car args))])
          (map (lambda (id)
                 (cond ((string=? id "Temp") 16)
                       ((string=? id "Vref") 17)
                       ((string=? id "Vbat") 18)
                       (else (let ([nums (filter (lambda (num)
                                                   (is? (adc_ch_key num "id") id))
                                                 lst)])
                               (if (null? nums)
                                   (error (string-append "Unknown ADC channel: " id))
                                   (car nums)
                                   )))))
               (if (exist? "reverse") (reverse seq) seq)
               ))
        )))

(define (adc_data_type)
  (if (and (has-num? "bits")
           (>= 8 (get-num "bits")))
      "uint8_t"
      "uint16_t"))

(define (adc_ch_name num idx rep . pfx)
  (string-append
   (if (adc_ch_has? num "id")
       (adc_ch_get num "id")
       (cond
        ((= num 16) "Temp")
        ((= num 17) "Vref")
        ((= num 18) "Vbat")
        (else (string-append
               (if (and (not (null? pfx))
                        (string? (car pfx)))
                   (car pfx) "")
               (number->string idx)))))
   (if (= 0 rep) "" (number->string rep))))

(define (adc_ch_define num idx rep)
  (string-append
   "#define " (ID-NAME (string-append
                        "ADC_INDEX_"
                        (adc_ch_name num idx rep)))
   " " (number->string idx) "\n"))

(define (adc_ch_defines . args)
  (apply string-append
         (map-with-counters
          adc_ch_define
          (apply adc_ch_list args))))

(define (adc_ch_field num idx rep)
  (string-append
   "    " (id-name "adc_value_t")
   " " (id-name (adc_ch_name num idx rep "_"))
   ";\n"))

(define (adc_ch_fields . args)
  (apply string-append
         (map-with-counters
          adc_ch_field
          (apply adc_ch_list args))))

(define (adc_samples)
  (if (has-num? "samples")
      (get-num "samples")
      1))

(define (adc_dma_bits)
  (if (has-num? "bits")
      (if (< 8 (get-num "bits"))
          16 8)))

(define (adc_dma_cb name)
  (get-cb-name (string-append
                "dma.on-" name) "adc"))

(define (adc_cal_decl cals)
  (if (null? cals)
      ""
      (let ([cal (car cals)])
        (string-append
         "uint16_t adc_cal_" (car cal) "(void);\n"
         (adc_cal_decl (cdr cals))))))

(define (adc_cal_impl cals)
  (if (null? cals)
      ""
      (let ([cal (car cals)])
        (string-append
         "uint16_t adc_cal_" (car cal) "(void) {\n"
         "  return " (cdr cal) ";\n"
         "}\n"
         (adc_cal_impl (cdr cals))))))
