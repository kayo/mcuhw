(define (dma_iface)
  (get "iface"))

(define (dma_data_bits)
  (if (has-num? "bits")
      (get-num "bits")))

(define (dma_data_type)
  (if (has? "type")
      (get "type")
      (let ([bits (dma_data_bits)])
        (cond ((< 16 bits) "uint32_t")
              ((< 8 bits) "uint16_t")
              (else "uint8_t")))))

(define (dma_id-name name)
  (id-name (string-append
            (dma_iface)
            "_" name)))

(define (dma_ID-NAME name)
  (ID-NAME (string-append
            (dma_iface)
            "_" name)))

(define (dma_has_buf . dir)
  (if (null? dir)
      (or (has-num? "rx.dma.length")
          (has-num? "tx.dma.length"))
      (has-num? (string-append
                 (cond ((= (car dir) 'rx) "rx")
                       ((= (car dir) 'tx) "tx"))
                 ".dma.length"))))

(define (dma_uni_buf)
  (and (has-num? "rx.dma.length")
       (has-num? "tx.dma.length")
       (or (exist? "rx.dma.shared")
           (exist? "tx.dma.shared"))))

(define (dma_rx_buf)
  (if (has-num? "rx.dma.length")
      (string-append
       (dma_id-name "dma_data")
       ".rx")))

(define (dma_rx_cnt)
  (if (has-num? "rx.dma.length")
      (dma_ID-NAME "rx_size")))

(define (dma_tx_buf)
  (if (has-num? "tx.dma.length")
      (string-append
       (dma_id-name "dma_data")
       ".tx")))

(define (dma_tx_cnt)
  (if (has-num? "tx.dma.length")
      (dma_ID-NAME "tx_size")))
