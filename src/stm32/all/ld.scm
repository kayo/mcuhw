(define mcu_flash_size
  (mcu_query
   mcu_database
   mcu_database_rom_size))

(define mcu_memory_banks
  (list (list "rom" "rx" #x08000000 (* 1024 (mcu_query
                                             mcu_database
                                             mcu_database_rom_size)))
        (list "bkp" "rw" #x40006c04 (* (car mcu_backup)
                                       (cadr mcu_backup)))
        (list "ram" "rwx" #x20000000 (* 1024 (mcu_query
                                              mcu_database
                                              mcu_database_ram_size)))
        ))
