(define tim_mcu_timers
  (let* ([target (string-downcase (get "target"))]
         [infos (filter (lambda (info)
                          (~~ target (string-append (string-downcase (car info)) ".*")))
                        mcu_timers)])
    (if (null? infos)
        (let () (warn "No timers info for the specified target") '())
        (cdar infos))))

(define (tim_get_regs num)
  (let ([tims (filter (lambda (tim)
                        (in? num (car tim)))
                      tim_mcu_timers)])
    (if (null? tims)
        (let () (warn (string-append
                       "No registers info for the timer "
                       (number->string num)))
             '())
        (cdar tims))))

(define (tim_has_reg tim reg)
  (in? reg (tim_get_regs (if (string? tim)
                             (string->number tim) tim))))

(define (tim_reg? reg)
  (tim_has_reg (get "device") reg))

(define (tim_reg reg . ch)
  (string-append "TIM_" reg (if (null? ch) "" (car ch)) "(TIM" (get "device") ")"))

(define (tim_ch ch . key)
  (string-append
   "CH[" (if (number? ch)
             (number->string ch) ch)
   "]"
   (if (null? key)
       ""
       (string-append
        "." (car key)))
   ))

(define (tim_ch_id-name ch name)
  (id-name
   (string-append
    "tim_"
    (if (has? (tim_ch ch "id"))
        (get (tim_ch ch "id"))
        ch)
    "_" name)))

(define (tim_top_value) ;; maximum counts
  (if (has-num? "reload")
      (check-range 'error "TIM.reload"
                   (get-num "reload")
                   0 (- (ash 2 16) 1))))

(define (tim_divider) ;; prescaler/divider
  (if (has-num? "divider")
      (- (check-range 'error "TIM.divider"
                      (get-num "divider")
                      1 (ash 1 16)) 1)
      (if (has-num? "prescaler")
          (check-range 'error "TIM.prescaler"
                       (get-num "prescaler")
                       0 (- (ash 1 16) 1))
          1)))

(define (tim_mod_frequency) ;; modulation frequency
  0) ;; not implemented

(define (tim_checks)
  (if (has-num? "dts-multiplier")
      (check-oneof 'error "dts-multiplier"
                   (get-num "dts-multiplier")
                   1 2 4))
  )

(define (tim_TIM_CR1) ;; Control register 1
  (if (tim_reg? 'CR1)
      (or-bits
       (if (has-num? "dts-multiplier")
           (get-def-name "dts-multiplier"
                         "TIM_CR1_CKD_CK_INT_MUL_"))
       (if (exist? "buffered-reload")
           "TIM_CR1_ARPE")
       (if (has? "align") ;; edge | center, compare-down, compare-up
           (cond ((is? "align" "edge")
                  "TIM_CR1_CMS_EDGE")
                 ((is? "align" "center-1")
                  "TIM_CR1_CMS_CENTER_1")
                 ((is? "align" "center-2")
                  "TIM_CR1_CMS_CENTER_2")
                 ((is? "align" "center-3")
                  "TIM_CR1_CMS_CENTER_3")
                 ((is? "align" "center")
                  (or-bits
                   (if (in? "compare-down"
                            (stack "align"))
                       "TIM_CR1_CMS_CENTER_COMPARE_DOWN")
                   (if (in? "compare-up"
                            (stack "align"))
                       "TIM_CR1_CMS_CENTER_COMPARE_UP"))
                  )))
       (if (has? "direction")
           (cond ((is? "direction" "up")
                  "TIM_CR1_DIR_UP")
                 ((is? "direction" "down")
                  "TIM_CR1_DIR_DOWN")))
       (if (and (has? "mode")
                (is? "mode" "one-pulse"))
           "TIM_CR1_OPM")
       (if (exist? "one-pulse")
           "TIM_CR1_OPM")
       (if (exist? "update.edge-only")
           "TIM_CR1_URS")
       (if (exist? "update.disable")
           "TIM_CR1_UDIS")
       (if (exist? "start")
           "TIM_CR1_CEN")
       )))

(define (tim_TIM_CR2) ;; Control register 2
  (define (idle ch n)
    (let ([key (tim_ch 4 (if n "n-idle" "idle"))])
      (if (and (has? key) (is-true? key))
          (string-append "TIM_CR2_OIS"
                         (number->string ch)
                         (if n "N" "")))))
  (if (tim_reg? 'CR2)
      (or-bits
       (idle 4 #f)
       (idle 3 #t) (idle 3 #f)
       (idle 2 #t) (idle 2 #f)
       (idle 1 #t) (idle 1 #f)
       (if (exist? "capture-compare.ch123-to-ti1")
           "TIM_CR2_TI1S")
       (if (has? "master.mode") ;; reset|enable|update|compare-(pulse|ocXref)
           (get-def-name "master.mode"
                         "TIM_CR2_MMS_"))
       (if (exist? "capture-compare.request-on-update")
           "TIM_CR2_CCDS")
       (if (exist? "capture-compare.update-on-trgi")
           "TIM_CR2_CCUS")
       (if (exist? "capture-compare.preload-mode")
           "TIM_CR2_CCPC")
       )))

(define (tim_def_prescaler var field)
  (if (has-num? var)
      (let ([val (check-oneof
                  'error var
                  (get-num var)
                  1 2 4 8)])
        (get-def-name
         var
         (string-append field "(TIM_PS_DIV")
         ")"))))

(define (tim_def_filter var field)
  (if (has? var)
      (get-def-name
       var
       (string-append field "(TIM_IF_" (get var))
       ")")))

(define (tim_TIM_SMCR) ;; Slave-mode control register
  (if (tim_reg? 'SMCR)
      (or-bits
       (if (is-false? "external.trigger.active")
           "TIM_SMCR_ETP")
       (if (exist? "external.clock.enable")
           "TIM_SMCR_ECE")
       (tim_def_prescaler "external.trigger.divider"
                          "TIM_SMCR_ETPS")
       (tim_def_filter "external.trigger.filter"
                       "TIM_SMCR_ETF")
       (if (or (exist? "master.synchro")
               (exist? "slave.synchro"))
           "TIM_SMCR_MSM")
       (if (has? "slave.trigger.source")
           (get-def-name "slave.trigger.source"
                         "TIM_SMCR_TS_"))
       (if (has? "slave.mode")
           (get-def-name "slave.mode"
                         "TIM_SMCR_SMS_"))
       )))

(define (tim_TIM_DIER) ;; DMA/interrupt enable register
  (define (CCxDE ch)
    (if (exist? (tim_ch ch "request"))
        (string-append "TIM_DIER_CC" (number->string ch) "DE")))
  (define (CCxIE ch)
    (if (or (exist? (tim_ch ch "on-capture"))
            (exist? (tim_ch ch "on-compare")))
        (string-append "TIM_DIER_CC" (number->string ch) "IE")))
  (if (tim_reg? 'DIER)
      (let ([dma-request (stack "dma.request")])
        (or-bits
         ;; dma resuests
         (if (and (has? "dma.device")
                  (has? "dma.channel")) "TIM_DIER_TDE")
         (if (in? "com" dma-request) "TIM_DIER_COMDE")
         (CCxDE 4) (CCxDE 3) (CCxDE 2) (CCxDE 1)
         (if (in? "update" dma-request) "TIM_DIER_UDE")
         ;; interrupts
         (if (exist? "on-break") "TIM_DIER_BIE")
         (if (exist? "on-trigger") "TIM_DIER_TIE")
         (if (exist? "on-com") "TIM_DIER_COMIE")
         (CCxIE 4) (CCxIE 3) (CCxIE 2) (CCxIE 1)
         (if (exist? "on-update")
             "TIM_DIER_UIE")
         ))))

(define (tim_ch_out_opts ch)
  (define (string->mode m)
    (string-substitute
     (string-upcase m)
     '("-") '("_")))
  (let ([out (tim_ch ch "output")])
    (if (has? out)
        (map (lambda (opt)
               (let ([val (string-split opt #\:)])
                 (if (null? (cdr val))
                     (string->mode (car val))
                     (cons (car val)
                           (string->mode (cadr val))))))
             (stack out))
        '())))

(define (tim_ch_out_set_funcs ch impl)
  (define opt_reg
    (if (in? ch '("1" "2")) "CCMR1" "CCMR2"))
  (define (opt_set opts)
    (if (null? opts)
        ""
        (let ([name (caar opts)]
              [mode (cdar opts)])
          (string-append
           "/*\n"
           " * Timer output mode set\n"
           " */\n"
           "void " (tim_ch_id-name ch "out_")
           name "(void)"
           (if impl (string-append
                     "{\n  "
                     (tim_reg opt_reg)
                     " = ("
                     (tim_reg opt_reg)
                     " & ~" (string-append "TIM_" opt_reg "_OC" ch "M_MASK")
                     ") | " (string-append "TIM_" opt_reg "_OC" ch "M(TIM_OCM_" mode ")")
                     ";\n}\n"
                     ) ";\n")
           (opt_set (cdr opts))))))
  (opt_set (filter pair? (tim_ch_out_opts ch))))

(define (tim_ch_out_def ch)
  (let ([default_opts (filter string? (tim_ch_out_opts ch))])
    (if (not (null? default_opts)) (car default_opts))))

(define (tim_TIM_CCMRx x ch)
  (define (flag grp flg)
    (string-append
     "TIM_CCMR" (number->string x)
     "_" grp (number->string ch) flg))
  (if (tim_reg? (if (= x 1) 'CCMR1 'CCMR2))
      (if (exist? (tim_ch ch "output"))
          ;; output compare
          (or-bits (flag "CC" "S_OUT")
                   ;; output mode
                   (if (string? (tim_ch_out_def ch))
                       (flag "OC" (string-append
                                   "M(TIM_OCM_" (tim_ch_out_def ch) ")")))
                   ;; clear enable
                   (if (exist? (tim_ch ch "clear"))
                       (flag "OC" "CE"))
                   ;; preload enable
                   (if (exist? (tim_ch ch "preload"))
                       (flag "OC" "PE"))
                   ;; fast enable
                   (if (exist? (tim_ch ch "fast"))
                       (flag "OC" "FE")))
          (if (exist? (tim_ch ch "input"))
              ;; input capture
              (or-bits (get-def-name (tim_ch ch "input")
                                     (flag "CC" "S_IN_"))
                       ;; filter
                       (tim_def_filter (tim_ch ch "filter")
                                       (flag "IC" "F"))
                       ;; prescaler
                       (tim_def_prescaler (tim_ch ch "prescaler")
                                          (flag "IC" "PSC"))
                       ))
          )))

(define (tim_TIM_CCMR1) ;; Capture/compare mode register 1
  (or-bits (tim_TIM_CCMRx 1 2)
           (tim_TIM_CCMRx 1 1)))

(define (tim_TIM_CCMR2) ;; Capture/compare mode register 2
  (or-bits (tim_TIM_CCMRx 2 4)
           (tim_TIM_CCMRx 2 3)))

(define (tim_TIM_CCER . nopol) ;; Capture/compare enable register
  (define (flag ch flg)
    (string-append
     "TIM_CCER_CC" (number->string ch) flg))
  (define (CCx ch n)
    (or-bits
     ;; polarity
     (if (and (null? nopol)
              (is-false? (tim_ch ch (if n "n-active" "active"))))
         (flag ch (if n "NP" "P")))
     ;; enable
     (if (exist? (tim_ch ch (if n "n-enable" "enable")))
         (flag ch (if n "NE" "E")))
     ))
  (if (tim_reg? 'CCER)
      (or-bits (CCx 4 #f)
               (CCx 3 #t) (CCx 3 #f)
               (CCx 2 #t) (CCx 2 #f)
               (CCx 1 #t) (CCx 1 #f))))

(define (tim_TIM_PSC) ;; prescaler value
  (if (tim_reg? 'PSC)
      (if (has-num? "divider")
          (- (check-range 'error "TIM.divider"
                          (get-num "divider")
                          1 (ash 1 16)) 1)
          (if (has-num? "prescaler")
              (check-range 'error "TIM.prescaler"
                           (get-num "prescaler")
                           0 (- (ash 1 16) 1))))))

(define (tim_TIM_ARR) ;; auto-reload register
  (if (tim_reg? 'ARR)
      (if (has-num? "reload")
          (check-range 'error "TIM.reload"
                       (get-num "reload")
                       0 (- (ash 1 16) 1)))))

(define (tim_TIM_RCR) ;; repetition counter register
  (if (tim_reg? 'RCR)
      (if (has-num? "update.repeat")
          (check-range 'error "TIM.update.repeat"
                       (get-num "update.repeat")
                       0 (- (ash 1 8) 1)))))

(define (tim_TIM_BDTR) ;; Break and dead-time register
  (if (tim_reg? 'BDTR)
      (or-bits
       (if (or (not (exist? "output"))
               (exist? "output.enable"))
           "TIM_BDTR_MOE")
       (if (exist? "output.auto")
           "TIM_BDTR_AOE")
       (if (and (has? "break.active")
                (is-true? "break.active"))
           "TIM_BDTR_BKP")
       (if (exist? "break.enable")
           "TIM_BDTR_BKE")
       (if (in? "run" (stack "output.inactive"))
           "TIM_BDTR_OSSR")
       (if (in? "idle" (stack "output.inactive"))
           "TIM_BDTR_OSSI")
       (if (has? "dead-time") ;; TODO: proper dead-time setting
           (get-def-name "dead-time" "TIM_BDTR_DTG"))
       )))

(define (tim_TIM_DCR) ;; DMA control register
  (if (tim_reg? 'DCR)
      (or-bits
       (if (has-num? "dma.burst.size")
           (string-append
            "TIM_DCR_DBL("
            (number->string
             (- (get-num "dma.burst.size") 1)) ")"))
       (if (has? "dma.burst.base")
           (get-def-name "dma.burst.base"
                         "TIM_DCR_DBA_REG(" ")"))
       )))

(define (tim_isr_name name . ch)
  (string-append "tim" (get "device") "_"
                 (cond ((string=? name "break") "brk")
                       ((string=? name "update") "up")
                       ((string=? name "trigger") "trg")
                       ((string=? name "com") "com")
                       ((or (string=? name "capture")
                            (string=? name "compare"))
                        (string-append "cc" (car ch)))
                       )))

(define (tim_flag name . ch)
  (string-append "TIM_SR_"
                 (cond ((string=? name "break") "BIF")
                       ((string=? name "update") "UIF")
                       ((string=? name "trigger") "TIF")
                       ((string=? name "com") "COMIF")
                       ((or (string=? name "capture")
                            (string=? name "compare"))
                        (string-append "CC" (car ch) "IF"))
                       )))

(define (tim_ch_name ch)
  (let ([id (tim_ch ch "id")])
    (if (has? id) (get id)
        (string-append "ch" ch))))

(define (tim_cb_var name)
  (string-append "on-" name))

(define (tim_cb_name name . ch)
  (if (null? ch)
      (string-append "tim_on_" name)
      (string-append "tim_" (tim_ch_name (car ch)) "_on_" name)))

(define (tim_add_interrupts)
  (define (add_int key)
    (add-callback-if-exist
     (string-append "on-" key)
     (tim_cb_name key)
     (string-append
      "Timer '" key "' event handler"))
    (if (exist? (string-append "on-" key))
        (add-interrupt (tim_isr_name key))))
  
  (define (add_ch_int ch)
    (add-callback-if-exist
     (tim_ch ch "on-capture")
     (tim_cb_name "capture" ch)
     (string-append
      "Timer 'capture' event handler"))
    (add-callback-if-exist
     (tim_ch ch "on-compare")
     (tim_cb_name "compare" ch)
     (string-append
      "Timer 'compare' event handler"))
    (if (or (exist? (tim_ch ch "on-capture"))
            (exist? (tim_ch ch "on-compare")))
        (add-interrupt (tim_isr_name "compare" ch))))
  
  (add_int "break")
  (add_int "update")
  (add_int "trigger")
  (add_int "com")
  (add_ch_int "1")
  (add_ch_int "2")
  (add_ch_int "3")
  (add_ch_int "4"))
