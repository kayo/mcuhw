[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ include "crc.tpl"
+][+ (define (crc_CRC_INIT) (or-bits))
+][+ (define (crc_CRC_CR) (or-bits))
+][+ define crc_impl
+][+ crc_impl_all
+]
static inline uint32_t _rbit32(uint32_t val) {
#ifdef __thumb2__
  asm volatile("rbit %0,%0":"+r" (val):"r" (val));
#else
#error "thumb2 instruction set required!"
#endif
  return val;
}

uint32_t hw_crc32_update(const void *ptr, size_t len) {
  const uint32_t *wptr = (const uint32_t*)ptr;
  const uint32_t *wend = wptr + (len >> 2);
  
  for (; wptr < wend; CRC_DR = _rbit32(*wptr++));

  uint32_t crc = _rbit32(CRC_DR);
  len &= 0x3;

  if (len > 0) {
    uint8_t len8 = len << 3;
    
    /* reset crc data register to 0 */
    CRC_DR = CRC_DR;

    /* calculate crc for rest */
    CRC_DR = _rbit32((*wptr & ((1 << len8) - 1)) ^ crc) >> (32 - len8);

    /* update crc of rest */
    crc = (crc >> len8) ^ _rbit32(CRC_DR);
  }

  return ~crc;
}
[+ enddef crc_impl
+]
