(define srt_usarts '(1 2 3 6))
(define srt_uarts '(4 5 7 8))

(define (srt_rx_reg)
  (srt_reg "DR"))

(define (srt_tx_reg)
  (srt_reg "DR"))

;; Control register 1
(define (srt_USART_CR1_extra)
  (or-bits
   ;; Word length
   (if (is? "data-bits" "9")
       "USART_CR1_M")
   ;; Receiver wakeup
   (if (or (is? "wakeup" "idle-line")
           (is? "wakeup" "address-mark"))
       "USART_CR1_RWU")
   ))

;; Control register 2
(define (srt_USART_CR2_extra)
  (or-bits
   ;; LIN mode enable
   (if (exist? "tx.lin-break")
       "USART_CR2_LINEN")
   ;; LIN break detection interrupt enable
   (if (and (exist? "rx.on-break")
            (not (exist? "rx.off-break")))
       "USART_CR2_LBDIE")
   ;; LIN break detection length
   (if (exist? "rx.break")
       (get-def-name "break"
                     "USART_CR2_LBDL_"))
   ))

;; Control register 3
(define (srt_USART_CR3_extra)
  (or-bits
   ;; Smartcard mode enable
   (if (exist? "smartcard")
       "USART_CR3_SCEN")
   ;; Smartcard nack enable
   (if (exist? "smartcard.nack")
       "USART_CR3_NACK")
   ;; IrDA mode enable
   (if (exist? "irda")
       "USART_CR3_IREN")
   ;; IrDA low-power
   (if (exist? "irda.low-power")
       "USART_CR3_IRLP")
   ))

(define (srt_div_val baud)
  ;; div*16 = clock / baud
  (let ([clock (srt_clock)])
    (srt_round_wrap
     (string-append
      clock " / " baud
      ))))

(define (srt_baud_val div)
  ;; baud = clock / div*16
  (let ([clock (srt_clock)])
    (srt_round_wrap
     (string-append
      clock " / " div))))
