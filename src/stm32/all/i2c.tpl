[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (load "i2c.scm")
+][+ define i2c_decl
+][+   if (once "i2c_types")
+]
/*
 * I2C error type
 */
enum {
  i2c_err_none     = 0,
  i2c_err_arbiter  = 1 << 6,
  i2c_err_bus      = 1 << 5,
  i2c_err_xrun     = 1 << 4,
  i2c_err_timeout  = 1 << 3,
  i2c_err_pec      = 1 << 3,
  i2c_err_alert    = 1 << 3,
};
typedef uint8_t i2c_err_t;
[+     endif (once "i2c_types")
+][+   if (has-num? "device")
+]
/*
 * Initialize I2C
 */
void [+ (id-name "i2c_init") +](void);

/*
 * Finalize I2C
 */
void [+ (id-name "i2c_done") +](void);

/*
 * Send I2C start condition
 */
void [+ (id-name "i2c_start") +](void);

/*
 * Send I2C stop condition
 */
void [+ (id-name "i2c_stop") +](void);

/*
 * Read received I2C data
 */
uint8_t [+ (id-name "i2c_get") +](void);

/*
 * Write I2C data to transmit
 */
void [+ (id-name "i2c_put") +](uint8_t data);

/*
 * Read data from I2C slave
 */
static inline void [+ (id-name "i2c_read") +](uint8_t addr) {
  [+ (id-name "i2c_put") +]((addr << 1) & 0x1);
}

/*
 * Write data to I2C slave
 */
static inline void [+ (id-name "i2c_write") +](uint8_t addr) {
  [+ (id-name "i2c_put") +](addr << 1);
}

/*
 * Sync transfer
 */
void [+ (id-name "i2c_xfer") +](uint8_t addr, const uint8_t *wptr, size_t wlen, uint8_t *rptr, size_t rlen);

/*
 * Get I2C errors
 */
i2c_err_t [+ (id-name "i2c_errors") +](void);

[+
  (add-cbs-if-exist "i2c"
    '("on-error" "Bus error handler")
    '("on-nack" "Not acknowledge received handler")
    '("on-start" "Transfer start handler")
    '("on-stop" "Reception complete handler")
    '("on-addr" "Address sent/match handler")
    '("rx.on-data" "Receiption not empty handler")
    '("tx.on-data" "Transmission empty handler")
    '("tx.on-done" "Transfer complete handler"))
+][+   endif (has-num? "device")
+][+   if (and (has? "pio.scl") (has? "pio.sda"))
+]
/*
 * Flush out busy I2C bus
 */
void [+ (id-name "i2c_flushout") +](void);
[+     endif (and (has? "pio.scl") (has? "pio.sda"))
+][+ enddef i2c_decl
+][+ define i2c_impl
+][+   if (has-num? "device")
+]
void [+ (id-name "i2c_init") +](void) {
[+    reg_put ident=2 reg=(i2c_reg "CR2") val=(i2c_I2C_CR2)
+][+  reg_put ident=2 reg=(i2c_reg "CCR") val=(i2c_I2C_CCR)
+][+  reg_put ident=2 reg=(i2c_reg "TIMINGR") val=(i2c_I2C_TIMINGR)
+][+  reg_put ident=2 reg=(i2c_reg "TRISE") val=(i2c_I2C_TRISE)
+][+  reg_put ident=2 reg=(i2c_reg "CR1") val=(i2c_I2C_CR1)
+]}

void [+ (id-name "i2c_done") +](void) {
[+    reg_clr ident=2 reg=(i2c_reg "CR1") val=I2C_CR1_PE
+]}

void [+ (id-name "i2c_start") +](void) {
[+ i2c_start
+]}

void [+ (id-name "i2c_stop") +](void) {
[+ i2c_stop
+]}

uint8_t [+ (id-name "i2c_get") +](void) {
[+ i2c_get
+]}

void [+ (id-name "i2c_put") +](uint8_t data) {
[+ i2c_put
+]}

[+     # Handlers
+][+   irq_isr_cb pfx=i2c cb=on-error name=(i2c_irq "err")
+][+   irq_isr_cb pfx=i2c cb=on-nack name=(i2c_irq "nack")
+][+   irq_isr_cb pfx=i2c cb=on-start name=(i2c_irq "start")
+][+   irq_isr_cb pfx=i2c cb=on-stop name=(i2c_irq "stop")
+][+   irq_isr_cb pfx=i2c cb=on-addr name=(i2c_irq "addr")
+][+   irq_isr_cb pfx=i2c cb=tx.on-data name=(i2c_irq "txe")
+][+   irq_isr_cb pfx=i2c cb=rx.on-data name=(i2c_irq "rxne")
+][+   irq_isr_cb pfx=i2c cb=tx.on-done name=(i2c_irq "tc")
+]
void [+ (id-name "i2c_xfer") +](uint8_t addr, const uint8_t *wptr, size_t wlen, uint8_t *rptr, size_t rlen) {
[+  i2c_xfer
+]}
[+     endif (has-num? "device")
+][+   if (and (has? "pio.scl") (has? "pio.sda"))
+]
static void [+ (id-name "i2c_transwait") +](void) {
  [+ (id-name "clk_wait_us" "clk.id") +](1e3 / 2 / ([+ (get-num "baud-rate") +]));
}

void [+ (id-name "i2c_flushout") +](void) {
  uint8_t i = 0;
  for (; i < 10; i++) {
    /* Check SDA, it must be high as we set it above */
    if ([+ (i2c_pio_id-name "sda" "get") +]()) break;

    /* SCL 1->0 transition */
    [+ (i2c_pio_id-name "scl" "clr") +]();
    [+ (id-name "i2c_transwait") +]();

    /* SCL 0->1 transition */
    [+ (i2c_pio_id-name "scl" "set") +]();
    [+ (id-name "i2c_transwait") +]();
  }

  /* Hold SDA (set it low) */
  [+ (i2c_pio_id-name "sda" "clr") +]();
  [+ (id-name "i2c_transwait") +]();

  /* Release SDA (set it high)
   * This generates a STOP condition (a low-to-high SDA transition while SCL is high) */
  [+ (i2c_pio_id-name "sda" "set") +]();
  [+ (id-name "i2c_transwait") +]();
}
[+     endif (and (has? "pio.scl") (has? "pio.sda"))
+][+ enddef i2c_impl
+]
