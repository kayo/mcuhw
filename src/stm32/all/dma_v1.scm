(define (dma_reg reg)
  (string-append "DMA_" reg "(DMA" (get "dn")
                 ", DMA_CHANNEL" (get "cn") ")"))

(define (dma_irq)
  (string-append "dma" (get "dn") "_ch" (get "cn") "_" (get "ev")))

(define (dma_CCR)
  (or-bits
   (if (and (is? "src" "memory")
            (is? "dst" "memory"))
       "DMA_CCR_MEM2MEM")
   (if (has? "prio")
       (string-append "DMA_CCR_PL_"
                      (get-up-name "prio")))
   (if (has? "mbits")
       (string-append "DMA_CCR_MSIZE_"
                      (number->string
                       (check-oneof 'error "DMA.mbits"
                                    (string->number
                                     (get "mbits"))
                                    8 16 32)) "BIT"))
   (if (has? "pbits")
       (string-append "DMA_CCR_PSIZE_"
                      (number->string
                       (check-oneof 'error "DMA.pbits"
                                    (string->number
                                     (get "pbits"))
                                    8 16 32)) "BIT"))
   (if (or (exist? "mincr")
           (in? "mincr" (stack "modes"))) "DMA_CCR_MINC")
   (if (or (exist? "pincr")
           (in? "pincr" (stack "modes"))) "DMA_CCR_PINC")
   (if (or (exist? "circular")
           (in? "circular" (stack "modes"))) "DMA_CCR_CIRC")
   (if (not (is? "src" "periph")) "DMA_CCR_DIR")
   (if (has? "error") "DMA_CCR_TEIE")
   (if (has? "half") "DMA_CCR_HTIE")
   (if (has? "complete") "DMA_CCR_TCIE")
   (if (is-true? "state") "DMA_CCR_EN")))

(define (dma_CMAR)
  (if (has? "maddr")
      (string-append "(uint32_t)(" (get "maddr") ")")))

(define (dma_CPAR)
  (if (has? "paddr")
      (string-append "(uint32_t)(" (get "paddr") ")")))

(define (dma_CNDTR)
  (if (has? "count") (get "count")))
