[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (load "spi_f03.scm")
+][+ define spi_set_bits_body
+][+   reg_put ident=2 reg=(spi_reg "CR2") msk="SPI_CR2_DS_MASK" val="SPI_CR2_DS(bits)"
+][+ enddef spi_set_bits_body
+][+ include "spi.tpl"
+][+ (add-c-header "stm32/all/spi_f03.h")
+]
