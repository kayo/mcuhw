[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (load "i2c_v1.scm")
+][+ (add-c-header "stm32/all/i2c_v1.h")
+][+ include "i2c.tpl"
+][+ define i2c_start
+][+   reg_set ident=2 reg=(i2c_reg "CR1") val=I2C_CR1_START
+][+ enddef i2c_start
+][+ define i2c_stop
+][+   reg_set ident=2 reg=(i2c_reg "CR1") val=I2C_CR1_STOP
+][+ enddef i2c_stop
+][+ define i2c_get
+]  return [+ (i2c_reg "DR") +];
[+   enddef i2c_get
+][+ define i2c_put
+][+   reg_put ident=2 reg=(i2c_reg "DR") val=data
+][+ enddef i2c_put
+][+ define i2c_xfer
+]  if (wptr) {
[+ reg_wait ident=4 reg=(i2c_reg "SR2") msk=I2C_SR2_BUSY unless
+][+ reg_set ident=4 reg=(i2c_reg "CR1") val=I2C_CR1_START
+]    for (; !(([+ (i2c_reg "SR1") +] & I2C_SR1_SB) && ([+ (i2c_reg "SR2") +] & (I2C_SR2_MSL | I2C_SR2_BUSY))); );
[+   reg_put ident=4 reg=(i2c_reg "DR") val='(addr) << 1'
+][+ reg_wait ident=4 reg=(i2c_reg "SR1") msk=I2C_SR1_ADDR
+]    (void)[+ (i2c_reg "SR2") +];
    for (; wlen--; ) {
[+   reg_put ident=6 reg=(i2c_reg "DR") val='*wptr++'
+][+ reg_wait ident=6 reg=(i2c_reg "SR1") msk=I2C_SR1_BTF
+]    }
  }

  if (rptr) {
[+   reg_set ident=4 reg=(i2c_reg "CR1") val='I2C_CR1_ACK | I2C_CR1_START'
+]    for (; !(([+ (i2c_reg "SR1") +] & I2C_SR1_SB) && ([+ (i2c_reg "SR2") +] & (I2C_SR2_MSL | I2C_SR2_BUSY))); );
[+   reg_put ident=4 reg=(i2c_reg "DR") val='((addr) << 1) | 1'
+][+ reg_wait ident=4 reg=(i2c_reg "SR1") msk=I2C_SR1_ADDR
+]    (void)[+ (i2c_reg "SR2") +];
    for (; rlen--; ) {
      if (!rlen) [+ reg_clr reg=(i2c_reg "CR1") val='I2C_CR1_ACK'
+][+ reg_wait ident=6 reg=(i2c_reg "SR1") msk=I2C_SR1_RXNE
+]      *rptr++ = [+ (i2c_reg "DR") +];
    }
  }
[+ reg_set ident=2 reg=(i2c_reg "CR1") val=I2C_CR1_STOP
+][+ enddef i2c_xfer
+]
