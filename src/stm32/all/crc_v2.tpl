[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ include "crc.tpl"
+][+ (add-c-header "stm32/all/crc_v2.h")
+][+ (define (crc_CRC_INIT) "HW_CRC32_INIT")
+][+ (define (crc_CRC_CR) "CRC_CR_REV_IN_WORD | CRC_CR_REV_OUT")
+][+ define crc_impl
+][+ crc_impl_all
+]
uint32_t hw_crc32_update(const void *ptr, size_t len) {
  const uint32_t *wptr = (const uint32_t*)ptr;
  const uint32_t *wend = wptr + (len >> 2);

  for (; wptr < wend; CRC_DR = *wptr++);

  uint32_t crc = CRC_DR;
  len &= 0x3;

  if (len > 0) {
    uint8_t len8 = len << 3;
    
    /* reset crc data register to 0 */
    CRC_DR = CRC_DR;

    /* calculate crc for rest */
    CRC_DR = ((*wptr & ((1 << len8) - 1)) ^ crc) << (32 - len8);

    /* update crc of rest */
    crc = (crc >> len8) ^ CRC_DR;
  }

  return ~crc;
}
[+ enddef crc_impl
+]
