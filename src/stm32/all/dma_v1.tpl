[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+  # DMA for L1, F0, F1, F3
+][+  (load "dma_v1.scm")
+][+  (add-c-header "stm32/all/dma_v1.h")
+][+  # configure
+][+  define dma_conf
+][+    reg_put ident=2 reg=(dma_reg "CMAR") val=(dma_CMAR)
+][+    reg_put ident=2 reg=(dma_reg "CPAR") val=(dma_CPAR)
+][+    reg_put ident=2 reg=(dma_reg "CNDTR") val=(dma_CNDTR)
+][+    reg_put ident=2 reg=(dma_reg "CCR") val=(dma_CCR)
+][+  enddef dma_conf
+][+  define dma_isr
+][+    for ev in error complete half
+][+      if (has? (get "ev"))
+][+        irq_isr name=(dma_irq)
+]{
  [+          (get (get "ev")) +]();
}
[+        endif has ev
+][+    endfor ev
+][+  enddef dma_isr
+][+  define dma_get
+][+    if (exist? "count")
+][+      (dma_reg "CNDTR")
+][+    endif
+][+    if (exist? "maddr")
+](void*)[+ (dma_reg "CMAR")
+][+    endif
+][+    if (exist? "state")
+]([+ reg_has reg=(dma_reg "CCR") msk=DMA_CCR_EN
+] && [+ (dma_reg "CNDTR") +] > 0)[+
        endif
+][+  enddef dma_get
+][+  define dma_set
+][+    if (has? "count")
+][+      reg_put reg=(dma_reg "CNDTR") val=(get "count")
+][+    endif
+][+    if (has? "maddr")
+][+      reg_put reg=(dma_reg "CMAR") val=(string-append "(uint32_t)" (get "maddr"))
+][+    endif
+][+    if (is-true? "state")
+][+      reg_set reg=(dma_reg "CCR") val=DMA_CCR_EN
+][+    endif
+][+    if (is-false? "state")
+][+      reg_clr reg=(dma_reg "CCR") val=DMA_CCR_EN
+][+    endif
+][+  enddef dma_set
+]
