(define (srt_dev)
  (let* ([dev (get "device")]
         [num (string->number dev)]
         [name (cond ((in? num srt_usarts) "USART")
                     ((in? num srt_uarts) "UART"))])
    (string-append name dev)))

(define (srt_reg reg)
  (string-append "USART_" reg "(" (string-upcase (srt_dev)) ")"))

(define (srt_irq name)
  (string-append (string-downcase (srt_dev)) "_" name))

(define (srt_clock)
  (if (has-num? "clock")
      (number->string (get-num "clock"))
      (if (has? "clock")
          (get "clock")
          (error "Please set srt clock"))))

(define (srt_data_type)
  (string-append
   "uint"
   (number->string (srt_dma_bits))
   "_t"))

(define (srt_dma_bits)
  (if (is? "data-bits" "9") 16 8))

(define (srt_round_wrap val)
  ;; round(x) = int(x * 2 + 1) / 2
  (string-append "((2 * " val ") + 1) / 2"))

;; Control register 1
(define (srt_USART_CR1)
  (or-bits
   ;; Receiver wakeup method
   (if (is? "wakeup" "address-mark")
       "USART_CR1_WAKE")
   ;; Parity control enable
   (if (or (is? "parity" "even")
           (is? "parity" "odd"))
       "USART_CR1_PCE")
   ;; Parity selection
   (if (is? "parity" "odd")
       "USART_CR1_PS")
   ;; Parity error interrupt enable
   (if (and (or (is? "parity" "even")
                (is? "parity" "odd"))
            (exist? "rx.on-error")
            (not (exist? "rx.off-error")))
       "USART_CR1_PEIE")
   ;; Transmission error interrupt enable
   (if (exist? "tx.on-error")
       "USART_CR1_TXEIE")
   ;; Transmission complete interrupt enable
   (if (and (exist? "tx.on-done")
            (not (exist? "tx.off-done")))
       "USART_CR1_TCIE")
   ;; Receiver not empty interrupt enable
   (if (and (exist? "rx.on-data")
            (not (exist? "rx.off-data")))
       "USART_CR1_RXNEIE")
   ;; Idle interrupt enable
   (if (and (exist? "rx.on-idle")
            (not (exist? "rx.off-idle")))
       "USART_CR1_IDLEIE")
   ;; Transmitter enable
   (if (exist? "tx.enable")
       "USART_CR1_TE")
   ;; Receiver enable
   (if (exist? "rx.enable")
       "USART_CR1_RE")
   ;; USART enable
   (if (exist? "enable")
       "USART_CR1_UE")
   (srt_USART_CR1_extra)
   ))

;; Control register 2
(define (srt_USART_CR2)
  (or-bits
   ;; Address of the USART node
   (if (has? "address")
       (get-def-name "address"
                     "USART_CR2_ADD(" ")"))
   ;; Stop bits
   (if (exist? "stop-bits")
       (get-def-name "stop-bits"
                     "USART_CR2_STOP_"))
   ;; Clock line enable (SLCK pin)
   (if (exist? "clk-line")
       "USART_CR2_CLKEN")
   ;; Clock polarity (CPOL)
   (if (is? "clk-line.active" "low")
       "USART_CR2_CPOL")
   ;; Clock phase (CPHA)
   (if (is? "clk-line.phase" "2")
       "USART_CR2_CPHA")
   ;; Last bit clock pulse
   (if (exist? "clk-line.last-pulse")
       "USART_CR2_LBCL")
   (srt_USART_CR2_extra)
   ))

;; Control register 3
(define (srt_USART_CR3)
  (or-bits
   ;; CTS interrupt enable
   (if (and (exist? "on-cts")
            (not (exist? "off-cts")))
       "USART_CR3_CTSIE")
   ;; CTS enable
   (if (exist? "cts-line")
       "USART_CR3_CTSE")
   ;; RTS enable
   (if (exist? "rts-line")
       "USART_CR3_RTSE")
   ;; DMA enable transmitter
   (if (exist? "tx.dma")
       "USART_CR3_DMAT")
   ;; DMA enable receiver
   (if (exist? "rx.dma")
       "USART_CR3_DMAR")
   ;; Half-duplex selection
   (if (exist? "half-duplex")
       "USART_CR3_HDSEL")
   ;; Error interrupt enable
   (if (and (exist? "rx.on-error")
            (not (exist? "rx.off-error")))
       "USART_CR3_EIE")
   (srt_USART_CR3_extra)
   ))

;; Baud-rate register
(define (srt_USART_BRR)
  (if (has-num? "baud-rate")
      (srt_div_val
       (get "baud-rate"))))
