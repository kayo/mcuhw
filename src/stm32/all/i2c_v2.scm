(define (i2c_I2C_CR1)
  (or-bits
   (i2c_I2C_CR1_all)
   ;; SMBus Device Default address enable
   ;; SMBus Host address enable
   ;; Slave byte control
   (if (exist? "slave.byte-control")
       "I2C_CR1_SBC")
   ;; DMA reception requests enable
   (if (exist? "rx.dma.enable")
       "I2C_CR1_RXDMAEN")
   ;; DMA transmission requests enable
   (if (exist? "tx.dma.enable")
       "I2C_CR1_TXDMAEN")
   ;; Analog noise filter OFF
   (if (not (exist? "filter.analog"))
       "I2C_CR1_ANFOFF")
   ;; Digital noise filter
   (if (exist? "filter.digital")
       (string-append
        "I2C_CR1_DNF_UP_"
        (number->string
         (check-range 'error "I2C.filter.digital"
                      (get-num "filter.digital")
                      1 15))
        "_TI2CCLK"))
   ;; Error interrupts enable
   (if (exist? "on-error")
       "I2C_CR1_ERRIE")
   ;; Transfer Complete interrupt enable
   (if (exist? "tx.on-done")
       "I2C_CR1_TCIE")
   ;; STOP detection Interrupt enable
   (if (exist? "on-stop")
       "I2C_CR1_STOPIE")
   ;; Not acknowledge received Interrupt enable
   (if (exist? "on-nack")
       "I2C_CR1_NACKIE")
   ;; Address match Interrupt enable (slave only)
   (if (exist? "on-addr")
       "I2C_CR1_ADDRIE")
   ;; RX Interrupt enable
   (if (exist? "rx.on-data")
       "I2C_CR1_RXIE")
   ;; TX Interrupt enable
   (if (exist? "tx.on-data")
       "I2C_CR1_TXIE")
   ))

(define (i2c_I2C_CR2)
  (or-bits))

(define (i2c_I2C_CCR)
  (or-bits))

(define (i2c_I2C_TRISE)
  (or-bits))

(define (i2c_I2C_TIMINGR_val key num)
  (string-append
   "((I2C_TIMINGR_" key "_SHIFT << "
   (if (number? num)
       (number->string num)
       num)
   ") & I2C_TIMINGR_" key "_MASK)"))

(define (i2c_I2C_TIMINGR)
  (or-bits
   ;; prescaler
   (cond
    ((has-num? "prescaler")
     (i2c_I2C_TIMINGR_val
      "PRESC"
      (check-range 'error "I2C.prescaler"
                   (get-num "prescaler")
                   0 15)))
    ((has-num? "divider")
     (i2c_I2C_TIMINGR_val
      "PRESC"
      (- (check-range 'error "I2C.divider"
                      (get-num "divider")
                      1 16) 1)))
    ;; ((and (has-num? "baud-rate")
    ;;       (has-num? "clock"))
    ;;  (i2c_I2C_TIMINGR_val
    ;;   "PRESC"
    ;;   (number->string
    ;;    (apply select-divider
    ;;           (cons (/ (get-num "clock")
    ;;                    (get-num "baud-rate"))
    ;;                 i2c_dividers)))))
    ;; ((and (has? "baud-rate")
    ;;       (has? "clock"))
    ;;  (i2c_I2C_TIMINGR_val
    ;;   "PRESC"
    ;;   (string-append
    ;;    (if (has-num? "clock")
    ;;        (number->string
    ;;         (get-num "clock"))
    ;;        (get "clock"))
    ;;    " / "
    ;;    (if (has-num? "baud-rate")
    ;;        (number->string
    ;;         (get-num "baud-rate"))
    ;;        (get "baud-rate"))
    ;;    )))
    (else (error "I2C speed not configured.")))
   ;; Data setup time
   (if (has-num? "timing.data-setup")
       (i2c_I2C_TIMINGR_val
        "SCLDEL"
        (get-num "timing.data-setup")))
   ;; Data hold time
   (if (has-num? "timing.data-hold")
       (i2c_I2C_TIMINGR_val
        "SDADEL"
        (get-num "timing.data-hold")))
   ;; SCL high period (master mode)
   (if (has-num? "timing.clock-high")
       (i2c_I2C_TIMINGR_val
        "SCLH"
        (get-num "timing.clock-high")))
   ;; SCL low period (master mode)
   (if (has-num? "timing.clock-low")
       (i2c_I2C_TIMINGR_val
        "SCLL"
        (get-num "timing.clock-low")))
   ))
