[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (load "spi.scm")
+][+ (add-c-header "stm32/all/spi_all.h")
+][+ define spi_decl
+][+   if (once "spi_types")
+]
/*
 * SPI error type
 */
enum {
  spi_err_none     = 0,
  spi_err_overrun  = 1 << 6,
  spi_err_mode     = 1 << 5,
  spi_err_crc      = 1 << 4,
  spi_err_underrun = 1 << 3,
};

typedef uint8_t spi_err_t;

/*
 * SPI word size
 */
enum {
[+     (spi_word_size_options spi_word_sizes)
+]};

typedef uint8_t spi_bits_t;
[+   endif (once "spi_types")
+]
/*
 * Initialize SPI
 */
void [+ (id-name "spi_init") +](void);

/*
 * Finalize SPI
 */
void [+ (id-name "spi_done") +](void);

/*
 * Read from SPI data register
 */
[+ (spi_data_type) +] [+ (id-name "spi_get") +](void);

/*
 * Write to SPI data register
 */
void [+ (id-name "spi_put") +]([+ (spi_data_type) +] data);

/*
 * Send data using SPI
 */
void [+ (id-name "spi_send") +]([+ (spi_data_type) +] data);

/*
 * Receive data using SPI
 */
[+ (spi_data_type) +] [+ (id-name "spi_recv") +](void);

/*
 * Transfer data using SPI
 */
[+ (spi_data_type) +] [+ (id-name "spi_xfer") +]([+ (spi_data_type) +] data);

/*
 * Check busy status of SPI
 */
int [+ (id-name "spi_busy") +](void);

/*
 * Busy wait for SPI
 */
void [+ (id-name "spi_wait") +](void);

/*
 * Set word size in bits
 */
void [+ (id-name "spi_set_bits") +](spi_bits_t bits);

/*
 * Get SPI errors
 */
spi_err_t [+ (id-name "spi_errors") +](void);

/*
 * Enable handling tx on-data
 */
void [+ (id-name "spi_tx_data_on") +](void);

/*
 * Disable handling tx on-data
 */
void [+ (id-name "spi_tx_data_off") +](void);

/*
 * Enable handling rx on-data
 */
void [+ (id-name "spi_rx_data_on") +](void);

/*
 * Disable handling rx on-data
 */
void [+ (id-name "spi_rx_data_off") +](void);

/*
 * Enable handling on-error
 */
void [+ (id-name "spi_error_on") +](void);

/*
 * Disable handling on-error
 */
void [+ (id-name "spi_error_off") +](void);

[+
  (add-cbs-if-exist "spi"
    '("on-error" "Bus error handler")
    '("rx.on-data"  "Receiption not empty handler")
    '("tx.on-data"  "Transmission empty handler"))
+][+     dma_decl iface=spi bits=(spi_dma_bits)
+][+ enddef spi_decl
+][+ define spi_impl
+]
void [+ (id-name "spi_init") +](void) {
[+    dma_init iface=spi bits=(spi_dma_bits) rx-reg=(spi_reg "DR") tx-reg=(spi_reg "DR")
+][+  reg_put ident=2 reg=(spi_reg "CRCPR") val=(spi_SPI_CRCPR)
+][+  reg_put ident=2 reg=(spi_reg "CR2") val=(spi_SPI_CR2)
+][+  reg_put ident=2 reg=(spi_reg "CR1") val=(spi_SPI_CR1)
+]}

void [+ (id-name "spi_done") +](void) {
[+    # TODO: Correct shutdown sequence
+][+  reg_clr ident=2 reg=(spi_reg "CR1") val="SPI_CR1_SPE"
+][+  dma_done iface=srt
+]}

[+ (spi_data_type) +] [+ (id-name "spi_get") +](void) {
  return [+ (spi_reg "DR") +];
}

void [+ (id-name "spi_put") +]([+ (spi_data_type) +] data) {
[+   reg_put ident=2 reg=(spi_reg "DR") val=data
+]}

void [+ (id-name "spi_send") +]([+ (spi_data_type) +] data) {
[+   reg_wait ident=2 reg=(spi_reg "SR") msk=SPI_SR_TXE
+][+ reg_put ident=2 reg=(spi_reg "DR") val=data
+]}

[+ (spi_data_type) +] [+ (id-name "spi_recv") +](void) {
[+   reg_wait ident=2 reg=(spi_reg "SR") msk=SPI_SR_RXNE
+]  return [+ (spi_reg "DR") +];
}

[+ (spi_data_type) +] [+ (id-name "spi_xfer") +]([+ (spi_data_type) +] data) {
[+   reg_wait ident=2 reg=(spi_reg "SR") msk=SPI_SR_TXE
+][+ reg_put ident=2 reg=(spi_reg "DR") val=data
+][+ reg_wait ident=2 reg=(spi_reg "SR") msk=SPI_SR_RXNE
+]  return [+ (spi_reg "DR") +];
}

spi_err_t [+ (id-name "spi_errors") +](void) {
[+ reg_get ident=2 reg=(spi_reg "SR") msk="SPI_SR_OVR | SPI_SR_MODF | SPI_SR_CRCERR | SPI_SR_UDR" ret
+]}

int [+ (id-name "spi_busy") +](void) {
[+ reg_get ident=2 reg=(spi_reg "SR") msk=SPI_SR_BSY ret
+]}

void [+ (id-name "spi_wait") +](void) {
[+ reg_wait ident=2 reg=(spi_reg "SR") unless msk=SPI_SR_BSY
+]}

void [+ (id-name "spi_set_bits") +](spi_bits_t bits) {
[+ spi_set_bits_body
+]}

void [+ (id-name "spi_tx_data_on") +](void) {
[+ reg_set ident=2 reg=(spi_reg "CR2") val="SPI_CR2_TXEIE"
+]}

void [+ (id-name "spi_tx_data_off") +](void) {
[+ reg_clr ident=2 reg=(spi_reg "CR2") val="SPI_CR2_TXEIE"
+]}

void [+ (id-name "spi_rx_data_on") +](void) {
[+ reg_set ident=2 reg=(spi_reg "CR2") val="SPI_CR2_RXNEIE"
+]}

void [+ (id-name "spi_rx_data_off") +](void) {
[+ reg_clr ident=2 reg=(spi_reg "CR2") val="SPI_CR2_RXNEIE"
+]}

void [+ (id-name "spi_error_on") +](void) {
[+ reg_set ident=2 reg=(spi_reg "CR2") val="SPI_CR2_ERRIE"
+]}

void [+ (id-name "spi_error_off") +](void) {
[+ reg_clr ident=2 reg=(spi_reg "CR2") val="SPI_CR2_ERRIE"
+]}[+     # Bus error handler
+][+   irq_isr_cb pfx=spi cb=on-error name=(spi_irq "err")
+][+   # RX not empty handler
+][+   irq_isr_cb pfx=spi cb=rx.on-data name=(spi_irq "rxne")
+][+   # TX empty handler
+][+   irq_isr_cb pfx=spi cb=tx.on-data name=(spi_irq "txe")
+][+   # DMA
+][+   dma_impl iface=spi
+][+ enddef spi_impl
+]
