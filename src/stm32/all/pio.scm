(define (pio_reg reg)
  (string-append "GPIO_" reg "(GPIO" (get "port") ")"))

(define (pio_pin pin key . args)
  (let ([port (if (null? args)
                  (get "port")
                  (car args))])
    (string-append port "[" pin "]." key)))

(define (pio_key pin key . args)
  (let ([port (if (null? args)
                  (get "port")
                  (car args))])
    (string-append port "[" pin "]." key)))

(define (pio_seq . md)
  (map number->string
       (cond ((null? md) '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15))
             ((= 'hi (car md)) '(8 9 10 11 12 13 14 15))
             ((= 'lo (car md)) '(0 1 2 3 4 5 6 7)))))

(define (pio_active pin)
  (get-bool (pio_pin pin "active")))

(define (pio_initial pin)
  (let ([initial (pio_pin pin "initial")])
    (cond ((is? initial "active")
           (pio_active pin))
          ((is? initial "inactive")
           (not (pio_active pin)))
          (get-bool initial))))

(define (pio_BSRR_cfg_gen pull_set)
  (apply or-bits
         (map
          (lambda (pin)
            (let ([op (cond
                       ((or (and pull_set
                                 (is? (pio_pin pin "input") "pullup"))
                            (and (has? (pio_pin pin "output"))
                                 (eqv? #t (pio_initial pin)))) "SET")
                       ((or (and pull_set
                                 (is? (pio_pin pin "input") "pulldown"))
                            (and (has? (pio_pin pin "output"))
                                 (eqv? #f (pio_initial pin)))) "RESET"))])
              (if (string? op)
                  (string-append "GPIO_BSRR_" op "(" pin ")"))
              ))
          (pio_seq))))

(define (pio_input? pin)
  (and (has? (pio_pin pin "id"))
       (not (has? (pio_pin pin "af")))
       (has? (pio_pin pin "input"))
       (is? (pio_pin pin "input")
            "float" "pullup" "pulldown")))

(define (pio_output? pin)
  (and (has? (pio_pin pin "id"))
       (not (has? (pio_pin pin "af")))
       (has? (pio_pin pin "output"))
       (is? (pio_pin pin "output")
            "pushpull" "opendrain")))

(define (pio_id-name pin . sfx)
  (id-name
   (string-append
    "pio_"
    (string-substitute
     (if (has? (pio_pin pin "id"))
         (get (pio_pin pin "id"))
         (string-append
          (get "port") pin))
     '("~") '(""))
    (if (null? sfx) ""
        (string-append "_" (car sfx))))))

(define (pio_ext_regi_pins)
  (let* ([regi (get-num "regi")]
         [from (* 4 (- regi 1))])
    (gen-sequence
     from (+ 3 from))))

(define (pio_ext_get_port pin)
  (let* ([str_pin (if (string? pin)
                      pin
                      (number->string pin))]
         [ports (filter (lambda (port)
                          (has? (pio_pin str_pin "edge" port)))
                        '("A" "B" "C" "D" "E" "F" "G" "H"))])
    (if (null? ports)
        #f
        (if (null? (cdr ports))
            (car ports)
            (error (string-append
                    "Multiple edge detections on pin: "
                    str_pin
                    " and ports: "
                    (join ", " ports)))))
    ))

(define (pio_EXTICR msk)
  (apply or-bits
         (map-with-counters
          (lambda (pin idx rep)
            (let ([port (pio_ext_get_port pin)])
              (if (string? port)
                  (if msk
                      (string-append
                       pio_EXTICR_reg "_MSK("
                       (number->string idx)
                       ")")
                      (string-append
                       pio_EXTICR_reg "_VAL("
                       (number->string idx)
                       ", "
                       pio_EXTICR_reg "_GPIO" port
                       ")")))
              ))
          (pio_ext_regi_pins))))

(define (pio_EXTI_xTSR vals)
  (apply or-bits
         (map (lambda (pin)
                (let ([port (pio_ext_get_port pin)])
                  (if (string? port)
                      (if (in? (get (pio_pin pin "edge" port)) vals)
                          (string-append
                           "(1 << " pin ")"))
                      )))
              (pio_seq))))

(define (pio_EXTI_RTSR)
  (pio_EXTI_xTSR '("rise" "rising" "both")))

(define (pio_EXTI_FTSR)
  (pio_EXTI_xTSR '("fall" "falling" "both")))

(define (pio_EXTI_xMR intr)
  (apply or-bits
         (map (lambda (pin)
                (let ([port (pio_ext_get_port pin)])
                  (if (string? port)
                      (if (or (not intr)
                              (exist? (pio_pin pin "on-edge" port)))
                          (string-append "(1 << " pin ")"))
                      )))
              (pio_seq))))

(define (pio_EXTI_EMR)
  (pio_EXTI_xMR #f))

(define (pio_EXTI_IMR)
  (pio_EXTI_xMR #t))

(define (pio_ext_cb_add pin)
  (add-callback-if-exist
   (pio_key pin "on-edge")
   (pio_id-name pin "on_edge")
   "Edge detection callback"))

(define (pio_ext_cb_run pin)
  (run-callback-if-exist
   (pio_key pin "on-edge")
   (pio_id-name pin "on_edge")))
