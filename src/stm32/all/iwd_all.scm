(define iwd_dividers
  '(4 8 16 32 64 128 256))

(define (iwd_IWDG_PR_val)
  (or-bits
   (if (has-num? "divider")
       (string-append
        "IWDG_PR_DIV"
        (number->string
         (check-oneof 'error "iwd.divider"
                      (get-num "divider")
                      iwd_dividers)
         )))
   ))

(define (iwd_IWDG_RLR_val)
  (or-bits
   (if (has-num? "reload")
       (number->string
        (check-range 'error "iwd.reload"
                     (get-num "reload")
                     0 4096)
        ))
   ))

(define (iwd_IWDG_KR_val op)
  (or-bits
   (cond ((eq? op 'enable) "IWDG_KR_START")
         ((eq? op 'access) "IWDG_KR_UNLOCK")
         ((eq? op 'reload) "IWDG_KR_RESET")
         )))
