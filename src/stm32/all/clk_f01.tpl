[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+  (load "clk_f01.scm")
+][+  (add-c-header "cortex-m/scs.h")
+][+  (add-c-header "cortex-m/dwt.h")
+][+  (add-c-header "cortex-m/systick.h")
+][+  (add-c-header "stm32/all/pwr_v1.h")
+][+  define clk_def_clk
+][+    if (and (has-num? "val") (< 0 (get-num "val")))
+][+      if (has? "cmt")
+]
/*
 * [+ cmt +]
 */[+     endif cmt
+]
#define [+ (ID-NAME (string-append (get "name") "_CLK"))
+] [+ (get-num "val")
+][+    endif clk
+][+  enddef clk_def_clk
+][+  # Clock declarations
+][+  define clk_decl
+][+    clk_def_clk name=SYS val=(clk_SYS_frequency) cmt="System clock frequency"
+][+    clk_def_clk name=AHB val=(clk_AHB_frequency) cmt="AHB clock frequency"
+][+    clk_def_clk name=APB1 val=(clk_APB1_frequency) cmt="APB1 clock frequency"
+][+    clk_def_clk name=APB2 val=(clk_APB2_frequency) cmt="APB2 clock frequency"
+][+    clk_def_clk name=STK val=(clk_STK_frequency) cmt="SysTick clock frequency"
+][+    clk_def_clk name=RTC val=(clk_RTC_frequency) cmt="RealTime clock frequency"
+][+    clk_def_clk name=ADC val=(clk_ADC_frequency) cmt="ADC clock frequency"
+]
/*
 * Clock initialize
 */
void [+ (id-name "clk_init") +](void);

/*
 * Clock finalize
 */
void [+ (id-name "clk_done") +](void);

/*
 * Cycles-based delay loop
 */
void [+ (id-name "clk_wait") +](uint32_t cycles);

/*
 * Sync delay in uS
 */
#define [+ (id-name "clk_wait_us")
+](us) [+ (id-name "clk_wait")
+]((([+ (ID-NAME "SYS_CLK") +]) / 1000000) * (us))
[+      (clk_add_cbs)
+][+  enddef clk_decl
+][+  # Clock implementation
+][+  define clk_impl
+]
void [+ (id-name "clk_init") +](void) {
[+      # enable interrupts
+][+    reg_set ident=2 reg="RCC_CIR" val=(clk_RCC_CIR "RDYIE" "HSI14" "HSE" "HSI" "LSE" "LSI" "PLL")
+][+    reg_set ident=2 reg="RCC_CR" val=(clk_RCC_CR "ON" "HSI" "HSE" "CSS" "HSI14")
+][+    reg_wait ident=2 reg="RCC_CR" msk=(clk_RCC_CR "RDY" "HSI" "HSE" "HSI14")
+][+    reg_put ident=2 reg="RCC_CFGR" msk=(clk_RCC_CFGR_msk) val=(clk_RCC_CFGR_val)
+][+    reg_put ident=2 reg="RCC_CFGR2" val=(clk_RCC_CFGR2)
+][+    reg_set ident=2 reg="FLASH_ACR" val=(clk_FLASH_ACR)
+][+    reg_set ident=2 reg="RCC_CR" val=(clk_RCC_CR "ON" "PLL")
+][+    reg_wait ident=2 reg="RCC_CR" msk=(clk_RCC_CR "RDY" "PLL")
+][+    reg_put ident=2 reg="RCC_CFGR" msk=(clk_RCC_CFGR_SW_msk) val=(clk_RCC_CFGR_SW_set)
+][+    reg_set ident=2 reg="RCC_CSR" val=(clk_RCC_CSR "ON" "LSI")
+][+    reg_wait ident=2 reg="RCC_CSR" msk=(clk_RCC_CSR "RDY" "LSI")
+][+    reg_put ident=2 reg="RCC_BDCR" msk=(clk_RCC_BDCR_msk) val=(clk_RCC_BDCR_set)
+][+    reg_wait ident=2 reg="RCC_BDCR" msk=(clk_RCC_BDCR_rdy)
+][+    reg_put ident=2 reg="STK_RVR" val=(clk_STK_RVR_val)
+][+    reg_put ident=2 reg="STK_CSR" msk=(clk_STK_CSR_msk) val=(clk_STK_CSR_set)
+][+    # disable interrupts
+][+    reg_clr ident=2 reg="RCC_CIR" val=(clk_RCC_CIR "RDYIE" "HSI14" "HSE" "HSI" "LSE" "LSI" "PLL")
+][+    reg_set ident=2 reg="RCC_AHBENR" val=(clk_RCC_AHBENR)
+][+    reg_set ident=2 reg="RCC_APB1ENR" val=(clk_RCC_APB1ENR)
+][+    reg_set ident=2 reg="RCC_APB2ENR" val=(clk_RCC_APB2ENR)
+][+    reg_put ident=2 reg="ADC_CFGR2(ADC)" val=(clk_ADC_CFGR2_val)
+][+    reg_put ident=2 reg="AFIO_MAPR" msk=(clk_AFIO_MAPR_msk) val=(clk_AFIO_MAPR_set)
+][+    reg_put ident=2 reg="AFIO_MAPR2" msk=(clk_AFIO_MAPR2_msk) val=(clk_AFIO_MAPR2_set)
+][+    reg_put ident=2 reg="SYSCFG_CFGR1" msk=(clk_SYSCFG_CFGR1_msk) val=(clk_SYSCFG_CFGR1_set)
+][+    reg_put ident=2 reg="PWR_CR" msk=(clk_PWR_CR_msk) val=(clk_PWR_CR_set)
+][+    reg_put ident=2 reg="CRC_INIT" val=(crc_CRC_INIT)
+][+    reg_put ident=2 reg="CRC_CR" val=(crc_CRC_CR)
+][+    reg_set ident=2 reg="SCS_DEMCR" val=(clk_SCS_DEMCR)
+][+    reg_set ident=2 reg="DWT_CTRL" val=(clk_DWT_CTRL)
+]}

void [+ (id-name "clk_done") +](void) {
[+      reg_clr ident=2 reg="DWT_CTRL" val=(clk_DWT_CTRL)
+][+    reg_clr ident=2 reg="SCS_DEMCR" val=(clk_SCS_DEMCR)
+][+    reg_put ident=2 reg="PWR_CR" msk=(clk_PWR_CR_msk) val=(clk_PWR_CR_clr)
+][+    reg_put ident=2 reg="SYSCFG_CFGR1" msk=(clk_SYSCFG_CFGR1_msk) val=(clk_SYSCFG_CFGR1_clr)
+][+    reg_put ident=2 reg="AFIO_MAPR" msk=(clk_AFIO_MAPR_msk) val=(clk_AFIO_MAPR_clr)
+][+    reg_put ident=2 reg="AFIO_MAPR2" msk=(clk_AFIO_MAPR2_msk) val=(clk_AFIO_MAPR2_clr)
+][+    reg_clr ident=2 reg="RCC_APB2ENR" val=(clk_RCC_APB2ENR)
+][+    reg_clr ident=2 reg="RCC_APB1ENR" val=(clk_RCC_APB1ENR)
+][+    reg_clr ident=2 reg="RCC_AHBENR" val=(clk_RCC_AHBENR)
+][+    reg_put ident=2 reg="STK_CSR" msk=(clk_STK_CSR_msk) val=(clk_STK_CSR_clr)
+][+    reg_put ident=2 reg="RCC_BDCR" msk=(clk_RCC_BDCR_msk) val=(clk_RCC_BDCR_clr)
+][+    reg_put ident=2 reg="RCC_CFGR" msk=(clk_RCC_CFGR_SW_msk) val=(clk_RCC_CFGR_SW_clr)
+][+    reg_clr ident=2 reg="RCC_CR" val=(clk_RCC_CR "ON" "HSI" "HSE" "CSS" "PLL" "HSI14")
+][+    reg_clr ident=2 reg="FLASH_ACR" val=(clk_FLASH_ACR)
+]}

#pragma GCC push_options
#pragma GCC optimize ("O3")
void [+ (id-name "clk_wait") +](uint32_t cycles) {
[+ if (exist? "DWT.cycles-counter")
+]#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
  volatile uint32_t start = DWT_CYCCNT;
  for (; DWT_CYCCNT - start < cycles; );
#else /* !(defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)) */
  /* FIXME: TODO: proper delay loop implementation especially for M0/M0+ */
[+ endif (exist? "DWT.cycles-counter")
+]  for (; cycles--; ) asm volatile("nop");
[+ if (exist? "DWT.cycles-counter")
+]#endif /* <(defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)) */
[+ endif (exist? "DWT.cycles-counter")
+]}
#pragma GCC pop_options
[+      if (exist? "STK.on-tick")
+][+      irq_isr name="sys_tick"
+]{
[+          (run-callback-if-exist "STK.on-tick" "clk_on_tick")
+]}
[+      endif STK.on-tick
+][+  enddef clk_impl
+]
