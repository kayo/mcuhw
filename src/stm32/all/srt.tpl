[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (load "srt.scm")
+][+ (add-c-header "stm32/all/usart_all.h")
+][+ define srt_decl
+][+   if (once "srt_types")
+]
/*
 * USART error type
 */
enum {
  srt_err_none    = 0,
  srt_err_overrun = 1 << 0,
  srt_err_noise   = 1 << 1,
  srt_err_framing = 1 << 2,
  srt_err_parity  = 1 << 3,
};
typedef uint8_t srt_err_t;

/*
 * USART stop bits
 */
typedef enum {
  srt_stopbits_1   = 0x0,
  srt_stopbits_0p5 = 0x1, /* not for f0 */
  srt_stopbits_2   = 0x2,
  srt_stopbits_1p5 = 0x3,
} srt_stopbits_t;

/*
 * USART data bits
 */
typedef enum {
  srt_databits_8 = 0x0,
  srt_databits_9 = 0x1,
  srt_databits_7 = 0x2, /* not for f1 */
} srt_databits_t;

/*
 * USART parity
 */
typedef enum {
  srt_parity_none,
  srt_parity_even,
  srt_parity_odd,
} srt_parity_t;

/*
 * USART event flags
 */
enum {
  srt_no_touch = 0,      /* do not modify flags */
  srt_rx_idle  = 1 << 4, /* on/off idle handler */
  srt_rx_data  = 1 << 5, /* on/off receiver data not empty handler */
  srt_tx_done  = 1 << 6, /* on/off transmission complete handler */
  srt_tx_empty = 1 << 7, /* on/off transmitter data empty handler */
  srt_rx_error = 1 << 8, /* on/off receiver error handler */
};
typedef uint16_t srt_flags_t;
[+     endif (once "srt_types")
+]
/*
 * Initialize USART
 */
void [+ (id-name "srt_init") +](void);

/*
 * Finalize USART
 */
void [+ (id-name "srt_done") +](void);

/*
 * Configure USART
 */
srt_parity_t [+ (id-name "srt_get_parity") +](void);
void [+ (id-name "srt_set_parity") +](srt_parity_t parity);

srt_stopbits_t [+ (id-name "srt_get_stopbits") +](void);
void [+ (id-name "srt_set_stopbits") +](srt_stopbits_t stopbits);

srt_databits_t [+ (id-name "srt_get_databits") +](void);
void [+ (id-name "srt_set_databits") +](srt_databits_t databits);

uint32_t [+ (id-name "srt_get_baudrate") +](void);
void [+ (id-name "srt_set_baudrate") +](uint32_t baudrate);

/*
 * Send data using USART
 */
void [+ (id-name "srt_send") +]([+ (srt_data_type) +] data);

/*
 * Receive data using USART
 */
[+ (srt_data_type) +] [+ (id-name "srt_recv") +](void);

/*
 * Get USART errors
 */
srt_err_t [+ (id-name "srt_errors") +](void);

/*
 * Set USART flags
 */
void [+ (id-name "srt_on_off") +](srt_flags_t on, srt_flags_t off);
[+
  (add-cbs-if-exist "srt"
    '("on-cts" "CTS signal handler")
    '("rx.on-error" "Receiption error handler")
    '("rx.on-data"  "Receiption not empty handler")
    '("rx.on-idle"  "Idle receiption handler")
    '("rx.on-break" "Break receiption handler")
    '("tx.on-data"  "Transmission empty handler")
    '("tx.on-done"  "Complete transmission handler"))
+][+   dma_decl iface=srt bits=(srt_dma_bits)
+][+ enddef srt_decl
+][+ define srt_impl
+]
/*
 * Serial Receiver-Transmitter driver
 */
void [+ (id-name "srt_init") +](void) {
[+    dma_init iface=srt bits=(srt_dma_bits) rx-reg=(srt_rx_reg) tx-reg=(srt_tx_reg)
+][+  reg_put ident=2 reg=(srt_reg "CR3") val=(srt_USART_CR3)
+][+  reg_put ident=2 reg=(srt_reg "CR2") val=(srt_USART_CR2)
+][+  reg_put ident=2 reg=(srt_reg "BRR") val=(srt_USART_BRR)
+][+  reg_put ident=2 reg=(srt_reg "CR1") val=(srt_USART_CR1)
+]}

void [+ (id-name "srt_done") +](void) {
[+    reg_clr ident=2 reg=(srt_reg "CR1") val=USART_CR1_UE
+][+  dma_done iface=srt
+]}

srt_parity_t [+ (id-name "srt_get_parity") +](void) {
  return [+ reg_has reg=(srt_reg "CR1") msk=USART_CR1_PCE +] ? ([+ reg_has reg=(srt_reg "CR1") msk=USART_CR1_PS +] ? srt_parity_odd : srt_parity_even) : srt_parity_none;
}
void [+ (id-name "srt_set_parity") +](srt_parity_t parity) {
[+ reg_put ident=2 reg=(srt_reg "CR1") msk="USART_CR1_PCE | USART_CR1_PS" val="(parity == srt_parity_none ? 0 : USART_CR1_PCE) | (parity == srt_parity_even ? 0 : USART_CR1_PS)"
+]}

srt_stopbits_t [+ (id-name "srt_get_stopbits") +](void) {
  return [+ reg_has reg=(srt_reg "CR2") msk=USART_CR2_STOP_MASK +] >> USART_CR2_STOP_SHIFT;
}

void [+ (id-name "srt_set_stopbits") +](srt_stopbits_t stopbits) {
[+ reg_put ident=2 reg=(srt_reg "CR2") msk=USART_CR2_STOP_MASK val="stopbits << USART_CR2_STOP_SHIFT"
+]}

srt_databits_t [+ (id-name "srt_get_databits") +](void) {
[+ srt_get_databits
+]}

void [+ (id-name "srt_set_databits") +](srt_databits_t databits) {
[+ srt_set_databits
+]}

uint32_t [+ (id-name "srt_get_baudrate") +](void) {
  return [+ (srt_baud_val (srt_reg "BRR")) +];
}

void [+ (id-name "srt_set_baudrate") +](uint32_t baudrate) {
[+ reg_put ident=2 reg=(srt_reg "BRR") val=(srt_div_val "baudrate")
+]}

void [+ (id-name "srt_send") +]([+ (srt_data_type) +] data) {
[+   srt_wait_send
+][+ srt_send_data
+]}

[+ (srt_data_type) +] [+ (id-name "srt_recv") +](void) {
[+   srt_wait_recv
+][+ srt_recv_data
+]}

srt_err_t [+ (id-name "srt_errors") +](void) {
[+ srt_error
+]}

void [+ (id-name "srt_on_off") +](srt_flags_t on, srt_flags_t off) {
[+   reg_put ident=2 reg=(srt_reg "CR1") msk=off val=on
+][+ reg_put ident=2 reg=(srt_reg "CR3") msk="off >> 8" val="on >> 8"
+]}

[+     # CTS handler
+][+   irq_isr_cb pfx=srt cb=on-cts name=(srt_irq "cts")
+][+   # RX error handler
+][+   irq_isr_cb pfx=srt cb=rx.on-error name=(srt_irq "err")
+][+   # RX not empty handler
+][+   irq_isr_cb pfx=srt cb=rx.on-data name=(srt_irq "rxne")
+][+   # RX idle line handler
+][+   irq_isr_cb pfx=srt cb=rx.on-idle name=(srt_irq "idle")
+][+   # RX break handler
+][+   irq_isr_cb pfx=srt cb=rx.on-break name=(srt_irq "break")
+][+   # TX empty handler
+][+   irq_isr_cb pfx=srt cb=tx.on-data name=(srt_irq "txe")
+][+   # TX complete handler
+][+   irq_isr_cb pfx=srt cb=tx.on-done name=(srt_irq "tc")
+][+   # DMA
+][+   dma_impl iface=srt
+][+ enddef srt_impl
+]
