[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ define bkp_decl
+]/*
 * Backup data register
 */
typedef struct {
  uint16_t value;
  uint16_t unused;
} bkp_reg_t;

typedef bkp_reg_t bkp_reg16_t[1];
typedef bkp_reg_t bkp_reg32_t[2];
typedef bkp_reg_t bkp_reg64_t[4];

#define bkp_reg_put(reg, var)       \
  _bkp_reg_put((reg),               \
               (const void*)&(var), \
               sizeof(var)/2)

static inline void
_bkp_reg_put(bkp_reg_t *reg,
             const uint16_t *var,
             uint8_t len) {
  uint8_t i = 0;
  for (; i < len; i++) {
    reg[i].value = var[i];
  }
}

#define bkp_reg_get(reg, var) \
  _bkp_reg_get((reg),         \
               (void*)&(var), \
               sizeof(var)/2)

static inline void
_bkp_reg_get(const bkp_reg_t *reg,
             uint16_t *var,
             uint8_t len) {
  uint8_t i = 0;
  for (; i < len; i++) {
    var[i] = reg[i].value;
  }
}
[+   enddef bkp_decl
+]
