(define (i2c_dev)
  (let* ([dev (get "device")]
         [num (string->number dev)])
    (string-append "I2C" dev)))

(define (i2c_reg reg)
  (string-append "I2C_" reg "(" (string-upcase (i2c_dev)) ")"))

(define (i2c_irq name)
  (string-append (string-downcase (i2c_dev)) "_" name))

(define (i2c_rate)
  (cond ((has-num? "baud-rate")
         (get-num "baud-rate"))
        ((has-num? "rate")
         (get-num "rate"))
        (else (error "I2C baud rate isn't configured!"))))

(define (i2c_mode)
  (if (> (i2c_rate) 100000)
      'fast-mode
      'std-mode))

(define (i2c_freq)
  (check-range 'error "I2C.clock"
               (get-num "clock")
               2000000 50000000))

(define (i2c_I2C_CR1_all)
  (or-bits
   ;; PEC enable
   (if (exist? "pec")
       "I2C_CR1_PECEN")
   ;; SMBus alert enable
   ;; General call enable
   (if (exist? "slave.general-call")
       "I2C_CR1_GCEN")
   ;; Clock stretching disable
   (if (not (exist? "slave.clock-stretch"))
       "I2C_CR1_NOSTRETCH")
   ;; Peripheral enable
   (if (exist? "enable")
       "I2C_CR1_PE")
   ))

(define (i2c_pio_id-name pad fn)
  (id-name (string-append
            "pio_" (get (string-append
                         "pio." pad))
            "_" fn)
           "pio.id"))
