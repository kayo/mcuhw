[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+  (load "pio.scm")
+][+  (add-h-header "stdbool.h")
+][+  (add-c-header "stm32/all/gpio_all.h")
+][+  (add-c-header "stm32/all/exti.h")
+][+  define pio_decl
+]
/*
 * Ports initialize
 */
void [+ (id-name "pio_init") +](void);

/*
 * Ports finalize
 */
void [+ (id-name "pio_done") +](void);
[+      for port in A B C D E F G H
+][+      for pin in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
+][+        if (or (pio_input? (get "pin")) (pio_output? (get "pin")))
+]
/*
 * Pin definition
 */
#define [+ (pio_id-name (get "pin")) +]
[+          endif
+][+        if (pio_input? (get "pin"))
+]
/*
 * Get pin input state
 */
bool [+ (pio_id-name (get "pin") "get") +](void);
[+          endif
+][+        if (pio_output? (get "pin"))
+]
/*
 * Set pin output state
 */
void [+ (pio_id-name (get "pin") "set") +](void);
#define [+ (pio_id-name (get "pin") "on") +] [+ (pio_id-name (get "pin") "set") +]

/*
 * Clear pin output state
 */
void [+ (pio_id-name (get "pin") "clr") +](void);
#define [+ (pio_id-name (get "pin") "off") +] [+ (pio_id-name (get "pin") "clr") +]

/*
 * Put pin output state
 */
void [+ (pio_id-name (get "pin") "put") +](bool val);

/*
 * Get pin output state
 */
bool [+ (pio_id-name (get "pin") "val") +](void);
[+          endif
+][+        (pio_ext_cb_add (get "pin"))
+][+      endfor pin
+][+    endfor port
+][+  enddef pio_decl
+][+  define pio_impl
+]
void [+ (id-name "pio_init") +](void) {
[+      for port in A B C D E F G H
+][+      pio_port_init
+][+    endfor port
+][+    for regi in 1 2 3 4
+][+      reg_put ident=2 reg=(string-append pio_EXTICR_reg (get "regi")) msk=(pio_EXTICR #t) val=(pio_EXTICR #f)
+][+    endfor regi
+][+    reg_set ident=2 reg="EXTI_RTSR" val=(pio_EXTI_RTSR)
+][+    reg_set ident=2 reg="EXTI_FTSR" val=(pio_EXTI_FTSR)
+][+    reg_set ident=2 reg="EXTI_IMR" val=(pio_EXTI_IMR)
+][+    reg_set ident=2 reg="EXTI_EMR" val=(pio_EXTI_EMR)
+]}

void [+ (id-name "pio_done") +](void) {
[+      reg_clr ident=2 reg="EXTI_IMR" val=(pio_EXTI_IMR)
+][+    reg_clr ident=2 reg="EXTI_EMR" val=(pio_EXTI_EMR)
+][+    for port in A B C D E F G H
+][+      pio_port_done
+][+    endfor port
+]}
[+      for port in A B C D E F G H
+][+      for pin in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
+][+        if (pio_input? (get "pin"))
+]
bool [+ (pio_id-name (get "pin") "get") +](void) {
  return ([+ (pio_reg "IDR") +] & (1 << [+ (get "pin") +])) ? [+
              if (pio_active (get "pin")) +]true : false[+
              else +]false : true[+
              endif +];
}
[+          endif
+][+        if (pio_output? (get "pin"))
+]
void [+ (pio_id-name (get "pin") "set") +](void) {
  [+ (pio_reg (if (pio_active (get "pin")) "BSRR" "BRR")) +] |= (1 << [+ (get "pin") +]);
}

void [+ (pio_id-name (get "pin") "clr") +](void) {
  [+ (pio_reg (if (pio_active (get "pin")) "BRR" "BSRR")) +] |= (1 << [+ (get "pin") +]);
}

void [+ (pio_id-name (get "pin") "put") +](bool val) {
  if (val) {
    [+ (pio_id-name (get "pin") "set") +]();
  } else {
    [+ (pio_id-name (get "pin") "clr") +]();
  }
}

bool [+ (pio_id-name (get "pin") "val") +](void) {
  return ([+ (pio_reg "ODR") +] & (1 << [+ (get "pin") +])) ? [+
              if (pio_active (get "pin")) +]true : false[+
              else +]false : true[+
              endif +];
}
[+          endif
+][+        if (exist? (pio_pin (get "pin") "on-edge"))
+][+          irq_isr name=(string-append "exti" (get "pin"))
+]{
[+              (pio_ext_cb_run (get "pin"))
+]}
[+          endif exist on-edge
+][+      endfor pin
+][+    endfor port
+][+  enddef pio_impl
+]
