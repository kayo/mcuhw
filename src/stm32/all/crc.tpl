[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (add-c-header "stm32/all/crc_all.h")
+][+ define crc_decl
+]/*
 * CRC32 Initial value
 */
#define HW_CRC32_INIT 0xFFFFFFFF

/*
 * CRC32 Polynomial coefficients
 */
#define HW_CRC32_POLY 0x4C11DB7

/*
 * Reset CRC32 unit
 */
void hw_crc32_reset(void);

/*
 * Calculate CRC32 checksum
 */
uint32_t hw_crc32_update(const void *ptr, size_t len);
[+ enddef crc_decl
+][+ define crc_impl_all
+]
void hw_crc32_reset(void) {
  CRC_CR |= CRC_CR_RESET;
}
[+ enddef crc_impl_all
+]
