[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (load "iwd_all.scm")
+][+ (add-c-header "stm32/all/iwdg_all.h")
+][+ define iwd_decl
+]
/*
 * Independent watchdog start
 */
void [+ (id-name "iwd_start") +](void);

/*
 * Independent watchdog renew
 */
void [+ (id-name "iwd_renew") +](void);
[+   enddef iwd_decl
+][+ define iwd_impl
+]
void [+ (id-name "iwd_start") +](void) {
[+   reg_put ident=2 reg=IWDG_KR val=(iwd_IWDG_KR_val 'enable)
+][+ reg_put ident=2 reg=IWDG_KR val=(iwd_IWDG_KR_val 'access)
+][+ reg_put ident=2 reg=IWDG_PR val=(iwd_IWDG_PR_val)
+][+ reg_put ident=2 reg=IWDG_RLR val=(iwd_IWDG_RLR_val)
+][+ iwd_start_extra
+][+ reg_wait ident=2 reg=IWDG_SR val=0
+]}

void [+ (id-name "iwd_renew") +](void) {
[+   reg_put ident=2 reg=IWDG_KR val=(iwd_IWDG_KR_val 'reload)
+]}
[+   enddef iwd_impl
+]
