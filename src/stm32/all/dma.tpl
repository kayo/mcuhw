[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (load "dma.scm")
+][+ define dma_decl
+][+   if (dma_has_buf)
+][+     if (dma_has_buf 'rx)
+]
/*
 * Receiver buffer size
 */
#define [+ (dma_ID-NAME "rx_size") +] [+ (get-num "rx.dma.length")
+][+     endif (dma_has_buf 'rx)
+][+     if (dma_has_buf 'tx)
+]
/*
 * Transmitter buffer size
 */
#define [+ (dma_ID-NAME "tx_size") +] [+ (get-num "tx.dma.length")
+][+     endif (dma_has_buf 'tx)
+]
/*
 * Data buffer type
 */
typedef [+ if (dma_uni_buf) +]union[+ else +]struct[+ endif +] {
[+       if (dma_has_buf 'rx)
+]  [+ (dma_data_type) +] rx[[+ (dma_ID-NAME "rx_size") +]];
[+       endif (dma_has_buf 'rx)
+][+     if (dma_has_buf 'tx)
+]  [+ (dma_data_type) +] tx[[+ (dma_ID-NAME "tx_size") +]];
[+       endif (dma_has_buf 'tx)
+]} [+ (dma_id-name "buf_t") +];

/*
 * Data buffer
 */
extern [+ (dma_id-name "buf_t") +] [+ (dma_id-name "dma_data") +];
[+     endif (dma_has_buf)
+][+   if (exist? "rx.dma")
+]
/*
 * Get reception data count (the number of data to be received)
 */
uint16_t [+ (dma_id-name "rx_dma_get_len") +](void);

/*
 * Set reception data count (the number of data to be received)
 */
void [+ (dma_id-name "rx_dma_set_len") +](uint16_t len);

/*
 * Get pointer to reception buffer
 */
void *[+ (dma_id-name "rx_dma_get_ptr") +](void);

/*
 * Set pointer to reception buffer
 */
void [+ (dma_id-name "rx_dma_set_ptr") +](void *ptr);

/*
 * Get receiver state
 */
bool [+ (dma_id-name "rx_dma_state") +](void);

/*
 * Enable receiver
 */
void [+ (dma_id-name "rx_dma_on") +](void);

/*
 * Disable receiver
 */
void [+ (dma_id-name "rx_dma_off") +](void);
[+     endif (exist? "rx.dma")
+][+   if (exist? "tx.dma")
+]
/*
 * Get transmission data count (the number of data to be transmitted)
 */
uint16_t [+ (dma_id-name "tx_dma_get_len") +](void);

/*
 * Set transmission data count (the number of data to be transmitted)
 */
void [+ (dma_id-name "tx_dma_set_len") +](uint16_t len);

/*
 * Get pointer to data for transmission
 */
const void *[+ (dma_id-name "tx_dma_get_ptr") +](void);

/*
 * Set pointer to data for transmission
 */
void [+ (dma_id-name "tx_dma_set_ptr") +](const void *ptr);

/*
 * Get transmitter state
 */
bool [+ (dma_id-name "tx_dma_state") +](void);

/*
 * Enable transmitter
 */
void [+ (dma_id-name "tx_dma_on") +](void);

/*
 * Disable transmitter
 */
void [+ (dma_id-name "tx_dma_off") +](void);
[+     endif (exist? "tx.dma")
+][+
  (add-cbs-if-exist (dma_iface)
    '("rx.dma.on-half" "Half receiption handler")
    '("rx.dma.on-full" "Complete reception handler")
    '("rx.dma.on-fail" "Reception error handler")
    '("tx.dma.on-half" "Half transmission handler")
    '("tx.dma.on-full" "Complete transmission handler")
    '("tx.dma.on-fail" "Transmission error handler"))
+][+ enddef dma_decl
+][+ define dma_impl
+][+   if (dma_has_buf)
+]
/* DMA data buffer */
[+ (dma_id-name "buf_t") +] NOINIT_ATTR [+ (dma_id-name "dma_data") +];
[+     endif (dma_has_buf)
+][+   # DMA Receiver
+][+   if (exist? "rx.dma")
+]
void *[+ (dma_id-name "rx_dma_get_ptr") +](void) {
  return [+ dma_get dn=(get "rx.dma.device") cn=(get "rx.dma.channel") maddr +];
}

void [+ (dma_id-name "rx_dma_set_ptr") +](void *ptr) {
[+ dma_set ident=2 dn=(get "rx.dma.device") cn=(get "rx.dma.channel") maddr=ptr
+]}

uint16_t [+ (dma_id-name "rx_dma_get_len") +](void) {
  return [+ dma_get dn=(get "rx.dma.device") cn=(get "rx.dma.channel") count +];
}

void [+ (dma_id-name "rx_dma_set_len") +](uint16_t len) {
[+ dma_set ident=2 dn=(get "rx.dma.device") cn=(get "rx.dma.channel") count=len
+]}

bool [+ (dma_id-name "rx_dma_state") +](void) {
  return [+ dma_get dn=(get "rx.dma.device") cn=(get "rx.dma.channel") state +];
}

void [+ (dma_id-name "rx_dma_on") +](void) {
[+ dma_set ident=2 dn=(get "rx.dma.device") cn=(get "rx.dma.channel") state=on
+]}

void [+ (dma_id-name "rx_dma_off") +](void) {
[+ dma_set ident=2 dn=(get "rx.dma.device") cn=(get "rx.dma.channel") state=off
+]}
[+       dma_isr dn=(get "rx.dma.device") cn=(get "rx.dma.channel") half=(get-cb-name "rx.dma.on-half" (dma_iface)) complete=(get-cb-name "rx.dma.on-full" (dma_iface)) error=(get-cb-name "rx.dma.on-fail" (dma_iface))
+][+   endif (exist? "rx.dma")
+][+   # DMA Transmitter
+][+   if (exist? "tx.dma")
+]
const void *[+ (dma_id-name "tx_dma_get_ptr") +](void) {
  return [+ dma_get dn=(get "tx.dma.device") cn=(get "tx.dma.channel") maddr +];
}

void [+ (dma_id-name "tx_dma_set_ptr") +](const void *ptr) {
[+ dma_set ident=2 dn=(get "tx.dma.device") cn=(get "tx.dma.channel") maddr=ptr
+]}

uint16_t [+ (dma_id-name "tx_dma_get_len") +](void) {
  return [+ dma_get dn=(get "tx.dma.device") cn=(get "tx.dma.channel") count +];
}

void [+ (dma_id-name "tx_dma_set_len") +](uint16_t len) {
[+ dma_set ident=2 dn=(get "tx.dma.device") cn=(get "tx.dma.channel") count=len
+]}

bool [+ (dma_id-name "tx_dma_state") +](void) {
  return [+ dma_get dn=(get "tx.dma.device") cn=(get "tx.dma.channel") state +];
}

void [+ (dma_id-name "tx_dma_on") +](void) {
[+ dma_set ident=2 dn=(get "tx.dma.device") cn=(get "tx.dma.channel") state=on
+]}

void [+ (dma_id-name "tx_dma_off") +](void) {
[+ dma_set ident=2 dn=(get "tx.dma.device") cn=(get "tx.dma.channel") state=off
+]}
[+       dma_isr dn=(get "tx.dma.device") cn=(get "tx.dma.channel") half=(get-cb-name "tx.dma.on-half" (dma_iface)) complete=(get-cb-name "tx.dma.on-full" (dma_iface)) error=(get-cb-name "tx.dma.on-fail" (dma_iface))
+][+   endif (exist? "tx.dma")
+][+ enddef dma_impl
+][+ define dma_init
+][+   if (exist? "rx.dma")
+][+     dma_conf dn=(get "rx.dma.device") cn=(get "rx.dma.channel") src=periph dst=memory mbits=(dma_data_bits) pbits=(dma_data_bits) maddr=(dma_rx_buf) paddr=(string-append "&" (get "rx-reg")) count=(dma_rx_cnt) half=(get-cb-name "rx.dma.on-half" (dma_iface)) complete=(get-cb-name "rx.dma.on-full" (dma_iface)) error=(get-cb-name "rx.dma.on-fail" (dma_iface)) mincr modes=(if (exist? "rx.dma.circular") "circular") state=(if (exist? "rx.dma.enable") "on" "off")
+][+   endif (exist? "rx.dma")
+][+   if (exist? "tx.dma")
+][+     dma_conf dn=(get "tx.dma.device") cn=(get "tx.dma.channel") src=memory dst=periph mbits=(dma_data_bits) pbits=(dma_data_bits) maddr=(dma_tx_buf) paddr=(string-append "&" (get "tx-reg")) count=(dma_tx_cnt) half=(get-cb-name "tx.dma.on-half" (dma_iface)) complete=(get-cb-name "tx.dma.on-full" (dma_iface)) error=(get-cb-name "tx.dma.on-fail" (dma_iface)) mincr modes=(if (exist? "tx.dma.circular") "circular") state=(if (exist? "tx.dma.enable") "on" "off")
+][+   endif (exist? "tx.dma")
+][+ enddef dma_init
+][+ define dma_done
+][+   if (exist? "tx.dma")
+][+     dma_set ident=2 dn=(get "tx.dma.device") cn=(get "tx.dma.channel") state=off
+][+   endif (exist? "tx.dma")
+][+   if (exist? "rx.dma")
+][+     dma_set ident=2 dn=(get "rx.dma.device") cn=(get "rx.dma.channel") state=off
+][+   endif (exist? "rx.dma")
+][+ enddef dma_done
+]
