(define (adc_ch_number_regular)
  (length (adc_ch_list)))

(define (adc_ch_defines_regular)
  (adc_ch_defines))

(define (adc_ch_fields_regular)
  (adc_ch_fields))

(define (adc_ADC_CFGR1_val)
  (or-bits
   (if (is? "mode" "discontinuous" "discont" "disc")
       "ADC_CFGR1_DISCEN")
   (if (has? "auto-off")
       "ADC_CFGR1_AUTOOFF")
   (if (has? "wait-conv")
       "ADC_CFGR1_WAIT")
   (if (is? "mode" "continuous" "cont")
       "ADC_CFGR1_CONT")
   (if (and (has? "trigger.source")
            (has? "trigger.edge"))
       (get-def-name "trigger.edge"
                     "ADC_CFGR1_EXTEN_"
                     "_EDGE"))
   (if (has? "trigger.source")
       (if (has-num? "trigger.source")
           (get-def-name "trigger.source"
                         "ADC_CFGR1_EXTSEL_VAL(" ")")
           (get-def-name "trigger.source"
                         "ADC_CFGR1_EXTSEL_")))
   (if (is? "align" "left")
       "ADC_CFGR1_ALIGN")
   (if (has? "bits")
       (string-append
        "ADC_CFGR1_RES_"
        (number->string
         (check-oneof 'error "adc.bits"
                      (get-num "bits")
                      6 8 10 12))
        "_BIT"))
   (if (exist? "reverse")
       "ADC_CFGR1_SCANDIR")
   (if (and (has? "dma.device")
            (has? "dma.channel"))
       "ADC_CFGR1_DMAEN")
   (if (is? "dma.mode" "circular")
       "ADC_CFGR1_DMACFG")
   ))

(define (adc_ADC_SMPR_val)
  (if (has? "sample-time")
      (get-def-name "sample-time" "ADC_SMPR_SMP_")))

(define (adc_ADC_CHSELR_val)
  (or-bits
   (if (exist? "Vref")
       "ADC_CHSELR_CHSEL(ADC_CHANNEL_VREF)")
   (if (exist? "Temp")
       "ADC_CHSELR_CHSEL(ADC_CHANNEL_TEMP)")
   (apply or-bits
          (map
           (lambda (num)
             (string-append
              "ADC_CHSELR_CHSEL("
              (number->string num)
              ")"))
           (adc_ch_list)))
   ))

(define (adc_ADC_CCR_val)
  (or-bits
   (if (exist? "Vref") "ADC_CCR_VREFEN")
   (if (exist? "Temp") "ADC_CCR_TSEN")
   ))

(define (adc_ADC_CR_set)
  (if (or (exist? "start")
          (has? "trigger.source"))
      "ADC_CR_ADSTART" ""))

(define adc_cal
  '(("temp_30C_at_3V3" . "ADC_CALR(ADC_CAL_TEMP_30C_AT_3V3)")
    ("temp_110C_at_3V3" . "ADC_CALR(ADC_CAL_TEMP_110C_AT_3V3)")))
