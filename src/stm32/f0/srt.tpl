[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ include "../all/srt.tpl"
+][+ (add-c-header "stm32/all/usart_v2.h")
+][+ (add-c-header "stm32/f0/usart.h")
+][+ (load "srt.scm")
+][+ define srt_get_databits
+]  return [+ reg_has reg=(srt_reg "CR1") msk=USART_CR1_M1 +] ? srt_databits_7 : [+ reg_has reg=(srt_reg "CR1") msk=USART_CR1_M0 +] ? srt_databits_9 : srt_databits_8;
[+   enddef srt_get_databits
+][+ define srt_set_databits
+]  [+ reg_put reg=(srt_reg "CR1") msk="USART_CR1_M1 | USART_CR1_M0" val="databits == srt_databits_7 ? USART_CR1_M1 : databits == srt_databits_9 ? USART_CR1_M0 : 0"
+][+ enddef srt_set_databits
+][+ define srt_wait_send
+][+   reg_wait ident=2 reg=(srt_reg "ISR") msk=USART_ISR_TXE
+][+ enddef srt_wait_send
+][+ define srt_send_data
+][+   reg_put ident=2 reg=(srt_reg "TDR") val=data
+][+ enddef srt_send_data
+][+ define srt_wait_recv
+][+   reg_wait ident=2 reg=(srt_reg "ISR") msk=USART_ISR_RXNE
+][+ enddef srt_wait_recv
+][+ define srt_recv_data
   +]  return [+ (srt_reg "RDR") +];
[+   enddef srt_recv_data
+][+ define srt_error
+]  return ([+ reg_has reg=(srt_reg "ISR") msk=USART_ISR_ORE +] ? srt_err_overrun : srt_err_none)
    | ([+ reg_has reg=(srt_reg "ISR") msk=USART_ISR_NF +] ? srt_err_noise : srt_err_none)
    | ([+ reg_has reg=(srt_reg "ISR") msk=USART_ISR_FE +] ? srt_err_framing : srt_err_none)
    | ([+ reg_has reg=(srt_reg "ISR") msk=USART_ISR_PE +] ? srt_err_parity : srt_err_none);
[+   enddef srt_error
+]
