[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+  (load "pio.scm")
+][+  include "../all/pio.tpl"
+][+  (add-c-header "stm32/all/gpio_f234.h")
+][+  (add-c-header "stm32/all/gpio_f24.h")
+][+  (add-c-header "stm32/f0/gpio.h")
+][+  (add-c-header "stm32/f0/syscfg.h")
+][+  define pio_port_init
+][+    reg_put ident=2 reg=(pio_reg "MODER") msk=(pio_MODER #t) val=(pio_MODER #f)
+][+    reg_put ident=2 reg=(pio_reg "OTYPER") msk=(pio_OTYPER #t) val=(pio_OTYPER #f)
+][+    reg_put ident=2 reg=(pio_reg "OSPEEDR") msk=(pio_OSPEEDR #t) val=(pio_OSPEEDR #f)
+][+    reg_put ident=2 reg=(pio_reg "PUPDR") msk=(pio_PUPDR #t) val=(pio_PUPDR #f)
+][+    reg_put ident=2 reg=(pio_reg "AFRL") msk=(pio_AFRL #t) val=(pio_AFRL #f)
+][+    reg_put ident=2 reg=(pio_reg "AFRH") msk=(pio_AFRH #t) val=(pio_AFRH #f)
+][+    reg_set ident=2 reg=(pio_reg "BSRR") val=(pio_BSRR_cfg)
+][+  enddef pio_port_init
+][+  define pio_port_done
+][+    reg_clr ident=2 reg=(pio_reg "MODER") val=(pio_MODER #t)
+][+  enddef pio_port_done
+]
