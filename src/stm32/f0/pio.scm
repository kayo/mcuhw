(define (pio_MODER msk)
  (apply or-bits
         (map (lambda (pin)
                (let* ([af (pio_pin pin "af")]
                       [in (pio_pin pin "input")]
                       [out (pio_pin pin "output")]
                       [val (cond ((has? af) "AF")
                                  ((and (has? in) (is? in "analog")) "ANALOG")
                                  ((has? out) "OUTPUT")
                                  ((has? in) "INPUT")
                                  (else #f))])
                  (if (string? val)
                      (if msk (string-append "GPIO_MODE_MASK(" pin ")")
                          (string-append "GPIO_MODE(" pin ", GPIO_MODE_" val ")")))
                  ))
              (pio_seq))))

(define (pio_OTYPER msk)
  (apply or-bits
         (map (lambda (pin)
                (let ([out (pio_pin pin "output")])
                  (if (has? out)
                      (let ([val (cond ((is? out "pushpull") "PP")
                                       ((is? out "opendrain") "OD"))])
                        (if (string? val)
                            (if msk (string-append "GPIO_OTYPE_MASK(" pin ")")
                                (string-append "GPIO_OTYPE(" pin ", GPIO_OTYPE_" val ")")))
                        ))))
              (pio_seq))))

(define (pio_OSPEEDR msk)
  (apply or-bits
         (map (lambda (pin)
                (let ([out (pio_pin pin "output")]
                      [spd (pio_pin pin "speed")])
                  (if (and (has? out) (has? spd))
                      (let ([val (cond ((is? spd "low") "LOW")
                                       ((is? spd "medium") "MED")
                                       ((is? spd "high") "HIGH")
                                       ((is? spd "2") "2MHZ")
                                       ((is? spd "25") "25MHZ")
                                       ((is? spd "50") "50MHZ"))])
                        (if (string? val)
                            (if msk (string-append "GPIO_OSPEED_MASK(" pin ")")
                                (string-append "GPIO_OSPEED(" pin ", GPIO_OSPEED_" val ")")))
                        ))))
              (pio_seq))))

(define (pio_PUPDR msk)
  (apply or-bits
         (map (lambda (pin)
                (let ([in (pio_pin pin "input")])
                  (if (has? in)
                      (let ([val (cond ((is? in "pullup") "PULLUP")
                                       ((is? in "pulldown") "PULLDOWN"))])
                        (if (string? val)
                            (if msk (string-append "GPIO_PUPD_MASK(" pin ")")
                                (string-append "GPIO_PUPD(" pin ", GPIO_PUPD_" val ")")))
                        ))))
              (pio_seq))))

(define (pio_AFRx x msk)
  (let ([spec (cond
               ((= x 'lo) "L")
               ((= x 'hi) "H"))])
  (apply or-bits
         (map (lambda (pin)
                (let ([af (pio_pin pin "af")])
                  (if (has? af)
                      (let* ([cfg (get af)]
                             [val (check-range 'error "af" (string->number cfg) 0 15)])
                        (if (string? cfg)
                            (if msk (string-append "GPIO_AFR" spec "_AF_MASK(" pin ")")
                                (string-append "GPIO_AFR" spec "_AF(" pin ", GPIO_AF" cfg ")")))
                        ))))
                (pio_seq x)))))

(define (pio_AFRL msk) (pio_AFRx 'lo msk))
(define (pio_AFRH msk) (pio_AFRx 'hi msk))

(define (pio_BSRR_cfg) (pio_BSRR_cfg_gen #f))

(define pio_EXTICR_reg "SYSCFG_EXTICR")
