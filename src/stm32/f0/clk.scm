(define (clk_APB_frequency) (clk_APBx_frequency ""))

(define clk_APB1_frequency clk_APB_frequency)
(define clk_APB2_frequency clk_APB_frequency)

(define (clk_RTC_frequency) (clk_RTC_frequency_HSE/x 32))

(define (clk_ADC_frequency)
  (cond
   ((is? "ADC.source" "HSI14") 14000000)
   ((is? "ADC.source" "APB/2") (/ (clk_APB_frequency) 2))
   ((is? "ADC.source" "APB/4") (/ (clk_APB_frequency) 4))
   (else 0)))

(define clk_RCC_CFGR_ADCPRE #f)

(define (clk_RCC_CFGR2) (clk_RCC_CFGR2_PREDIVx_PLLy "" ""))

(define clk_RCC_AHB_periph
  '("TSC" "GPIOF" "GPIOE" "GPIOD" "GPIOC" "GPIOB" "GPIOA" "CRC" "FLTF" "SRAM" "DMA"))

(define clk_RCC_APB1_periph
  '("CEC" "DAC" "PWR" "CRS" "CAN" "USB" "I2C2" "I2C1" "USART4" "USART3" "USART2" "SPI2" "WWDG" "TIM14" "TIM7" "TIM6" "TIM3" "TIM2"))

(define clk_RCC_APB2_periph
  '("DBGMCU" "TIM17" "TIM16" "TIM15" "USART1" "SPI1" "TIM1" "ADC" "SYSCFG"))

(define (clk_SYSCFG_CFGR1_msk)
  (or-bits
   (if (exist? "USART3.remap.dma")
       "SYSCFG_CFGR1_USART3_DMA_RMP")
   (if (exist? "USART2.remap.dma")
       "SYSCFG_CFGR1_USART2_DMA_RMP")
   (if (exist? "USART1.remap.dma.rx")
       "SYSCFG_CFGR1_USART1_RX_DMA_RMP")
   (if (exist? "USART1.remap.dma.tx")
       "SYSCFG_CFGR1_USART1_TX_DMA_RMP")

   (if (exist? "ADC.remap.dma")
       "SYSCFG_CFGR1_ADC_DMA_RMP")

   (if (exist? "SPI2.remap.dma")
       "SYSCFG_CFGR1_SPI2_DMA_RMP")
   
   (if (exist? "I2C.fast_mode_plus")
       "SYSCFG_CFGR1_I2C_PA10_FMP | SYSCFG_CFGR1_I2C_PA9_FMP | SYSCFG_CFGR1_I2C_PB9_FMP | SYSCFG_CFGR1_I2C_PB8_FMP | SYSCFG_CFGR1_I2C_PB7_FMP")
   (if (exist? "I2C1.fast_mode_plus")
       "SYSCFG_CFGR1_I2C1_FMP")
   (if (exist? "I2C2.fast_mode_plus")
       "SYSCFG_CFGR1_I2C2_FMP")
   (if (exist? "I2C1.remap.dma")
       "SYSCFG_CFGR1_I2C1_DMA_RMP")
   
   (if (exist? "TIM1.remap.dma")
       "SYSCFG_CFGR1_TIM1_DMA_RMP")
   (if (exist? "TIM2.remap.dma")
       "SYSCFG_CFGR1_TIM2_DMA_RMP")
   (if (exist? "TIM3.remap.dma")
       "SYSCFG_CFGR1_TIM3_DMA_RMP")
   (if (exist? "TIM16.remap.dma")
       "SYSCFG_CFGR1_TIM16_DMA_RMP | SYSCFG_CFGR1_TIM16_DMA_RMP2")
   (if (exist? "TIM17.remap.dma")
       "SYSCFG_CFGR1_TIM17_DMA_RMP | SYSCFG_CFGR1_TIM17_DMA_RMP2")

   (if (and (exist? "PA11.remap")
            (exist? "PA12.remap"))
       "SYSCFG_CFGR1_PA11_PA12_RMP")

   (if (exist? "MEM.mode")
       "SYSCFG_CFGR1_MEM_MODE")
   ))

(define (clk_SYSCFG_CFGR1_set)
  (or-bits
   (if (and (is? "USART3.remap.dma.rx" "3")
            (is? "USART3.remap.dma.tx" "2"))
       "SYSCFG_CFGR1_USART3_DMA_RMP")
   (if (and (is? "USART2.remap.dma.rx" "6")
            (is? "USART2.remap.dma.rx" "7"))
       "SYSCFG_CFGR1_USART2_DMA_RMP")
   (if (is? "USART1.remap.dma.rx" "5")
       "SYSCFG_CFGR1_USART1_RX_DMA_RMP")
   (if (is? "USART1.remap.dma.tx" "4")
       "SYSCFG_CFGR1_USART1_TX_DMA_RMP")

   (if (is? "ADC.remap.dma" "2")
       "SYSCFG_CFGR1_ADC_DMA_RMP")

   (if (and (is? "SPI2.remap.dma.rx" "6")
            (is? "SPI2.remap.dma.tx" "7"))
       "SYSCFG_CFGR1_SPI2_DMA_RMP")
   
   (if (exist? "I2C.fast_mode_plus.A[10]")
       "SYSCFG_CFGR1_I2C_PA10_FMP")
   (if (exist? "I2C.fast_mode_plus.A[10]")
       "SYSCFG_CFGR1_I2C_PA9_FMP")
   (if (exist? "I2C.fast_mode_plus.B[9]")
       "SYSCFG_CFGR1_I2C_PB9_FMP")
   (if (exist? "I2C.fast_mode_plus.B[8]")
       "SYSCFG_CFGR1_I2C_PB8_FMP")
   (if (exist? "I2C.fast_mode_plus.B[7]")
       "SYSCFG_CFGR1_I2C_PB7_FMP")
   (if (exist? "I2C1.fast_mode_plus")
       "SYSCFG_CFGR1_I2C1_FMP")
   (if (exist? "I2C2.fast_mode_plus")
       "SYSCFG_CFGR1_I2C2_FMP")
   (if (and (is? "I2C1.remap.dma.rx" "7")
            (is? "I2C1.remap.dma.tx" "6"))
       "SYSCFG_CFGR1_I2C1_DMA_RMP")
   
   (if (is? "TIM1.remap.dma" "6")
       "SYSCFG_CFGR1_TIM1_DMA_RMP")
   (if (is? "TIM2.remap.dma" "7")
       "SYSCFG_CFGR1_TIM2_DMA_RMP")
   (if (is? "TIM3.remap.dma" "6")
       "SYSCFG_CFGR1_TIM3_DMA_RMP")
   (if (is? "TIM16.remap.dma" "4")
       "SYSCFG_CFGR1_TIM16_DMA_RMP")
   (if (is? "TIM16.remap.dma" "6")
       "SYSCFG_CFGR1_TIM16_DMA_RMP2")
   (if (is? "TIM17.remap.dma" "2")
       "SYSCFG_CFGR1_TIM17_DMA_RMP")
   (if (is? "TIM17.remap.dma" "7")
       "SYSCFG_CFGR1_TIM17_DMA_RMP2")

   (if (and (is? "PA11.remap" "PA9")
            (is? "PA12.remap" "PA10"))
       "SYSCFG_CFGR1_PA11_PA12_RMP")

   (if (is? "MEM.mode" "flash")
       "SYSCFG_CFGR1_MEM_MODE_FLASH")
   (if (is? "MEM.mode" "system")
       "SYSCFG_CFGR1_MEM_MODE_SYSTEM")
   (if (is? "MEM.mode" "sram")
       "SYSCFG_CFGR1_MEM_MODE_SRAM")
   ))

(define (clk_SYSCFG_CFGR1_clr)
  (if (not (string=? "" (clk_SYSCFG_CFGR1_set)))
      "0"))

(define (clk_AFIO_MAPR_msk)
  (or-bits))

(define (clk_AFIO_MAPR_set)
  (or-bits))

(define (clk_AFIO_MAPR_clr)
  (or-bits))

(define (clk_AFIO_MAPR2_msk)
  (or-bits))

(define (clk_AFIO_MAPR2_set)
  (or-bits))

(define (clk_AFIO_MAPR2_clr)
  (or-bits))
