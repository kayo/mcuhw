(define mcu_core
  "cortex-m0")

;; vectors: (vectors)
;; vector: "name" | ("name" (shared)? num? "fallback"?)
;; shared: "name" | ("name" "condition"? "reset-condition"?)
(define mcu_vectors
  '("wwdg"
    "pvd"
    "rtc"
    "flash"
    "rcc"
    ("exti0_1"
     (("exti0"  "EXTI_PR & EXTI0"  "EXTI_PR &= EXTI0")
      ("exti1"  "EXTI_PR & EXTI1"  "EXTI_PR &= EXTI1")))
    ("exti2_3"
     (("exti2"  "EXTI_PR & EXTI2"  "EXTI_PR &= EXTI2")
      ("exti3"  "EXTI_PR & EXTI3"  "EXTI_PR &= EXTI3")))
    ("exti4_15"
     (("exti4"  "EXTI_PR & EXTI4"  "EXTI_PR &= EXTI4")
      ("exti5"  "EXTI_PR & EXTI5"  "EXTI_PR &= EXTI5")
      ("exti6"  "EXTI_PR & EXTI6"  "EXTI_PR &= EXTI6")
      ("exti7"  "EXTI_PR & EXTI7"  "EXTI_PR &= EXTI7")
      ("exti8"  "EXTI_PR & EXTI8"  "EXTI_PR &= EXTI8")
      ("exti9"  "EXTI_PR & EXTI9"  "EXTI_PR &= EXTI9")
      ("exti10" "EXTI_PR & EXTI10" "EXTI_PR &= EXTI10")
      ("exti11" "EXTI_PR & EXTI11" "EXTI_PR &= EXTI11")
      ("exti12" "EXTI_PR & EXTI12" "EXTI_PR &= EXTI12")
      ("exti13" "EXTI_PR & EXTI13" "EXTI_PR &= EXTI13")
      ("exti14" "EXTI_PR & EXTI14" "EXTI_PR &= EXTI14")
      ("exti15" "EXTI_PR & EXTI15" "EXTI_PR &= EXTI15")))
    "tsc"
    ("dma1_ch1"
     (("dma1_ch1_error"    "DMA_ISR(DMA1) & DMA_ISR_TEIF(1)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(1) | DMA_IFCR_CTEIF(1)")
      ("dma1_ch1_complete" "DMA_ISR(DMA1) & DMA_ISR_TCIF(1)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(1) | DMA_IFCR_CTCIF(1)")
      ("dma1_ch1_half"     "DMA_ISR(DMA1) & DMA_ISR_HTIF(1)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(1) | DMA_IFCR_CHTIF(1)")))
    ("dma1_ch2_3"
     (("dma1_ch2_error"    "DMA_ISR(DMA1) & DMA_ISR_TEIF(2)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(2) | DMA_IFCR_CTEIF(2)")
      ("dma1_ch2_complete" "DMA_ISR(DMA1) & DMA_ISR_TCIF(2)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(2) | DMA_IFCR_CTCIF(2)")
      ("dma1_ch2_half"     "DMA_ISR(DMA1) & DMA_ISR_HTIF(2)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(2) | DMA_IFCR_CHTIF(2)")
      ("dma1_ch3_error"    "DMA_ISR(DMA1) & DMA_ISR_TEIF(3)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(3) | DMA_IFCR_CTEIF(3)")
      ("dma1_ch3_complete" "DMA_ISR(DMA1) & DMA_ISR_TCIF(3)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(3) | DMA_IFCR_CTCIF(3)")
      ("dma1_ch3_half"     "DMA_ISR(DMA1) & DMA_ISR_HTIF(3)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(3) | DMA_IFCR_CHTIF(3)")))
    ("dma1_ch4_5"
     (("dma1_ch4_error"    "DMA_ISR(DMA1) & DMA_ISR_TEIF(4)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(4) | DMA_IFCR_CTEIF(4)")
      ("dma1_ch4_complete" "DMA_ISR(DMA1) & DMA_ISR_TCIF(4)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(4) | DMA_IFCR_CTCIF(4)")
      ("dma1_ch4_half"     "DMA_ISR(DMA1) & DMA_ISR_HTIF(4)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(4) | DMA_IFCR_CHTIF(4)")
      ("dma1_ch5_error"    "DMA_ISR(DMA1) & DMA_ISR_TEIF(5)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(5) | DMA_IFCR_CTEIF(5)")
      ("dma1_ch5_complete" "DMA_ISR(DMA1) & DMA_ISR_TCIF(5)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(5) | DMA_IFCR_CTCIF(5)")
      ("dma1_ch5_half"     "DMA_ISR(DMA1) & DMA_ISR_HTIF(5)" "DMA_IFCR(DMA1) |= DMA_IFCR_CGIF(5) | DMA_IFCR_CHTIF(5)")))
    ("adc_comp"
     (("adc_awd1"  "ADC_ISR(ADC) & ADC_ISR_AWD1"  "ADC_ISR(ADC) |= ADC_ISR_AWD1")
      ("adc_ovr"   "ADC_ISR(ADC) & ADC_ISR_OVR"   "ADC_ISR(ADC) |= ADC_ISR_OVR")
      ("adc_eoseq" "ADC_ISR(ADC) & ADC_ISR_EOSEQ" "ADC_ISR(ADC) |= ADC_ISR_EOSEQ")
      ("adc_eoc"   "ADC_ISR(ADC) & ADC_ISR_EOC"   "ADC_ISR(ADC) |= ADC_ISR_EOC")
      ("adc_eosmp" "ADC_ISR(ADC) & ADC_ISR_EOSMP" "ADC_ISR(ADC) |= ADC_ISR_EOSMP")
      ("adc_adrdy" "ADC_ISR(ADC) & ADC_ISR_ADRDY" "ADC_ISR(ADC) |= ADC_ISR_ADRDY"))
      ("comp"))
    ("tim1_brk_up_trg_com"
     (("tim1_brk" "TIM_SR(TIM1) & TIM_SR_BIF"   "TIM_SR(TIM1) &= ~TIM_SR_BIF")
      ("tim1_up"  "TIM_SR(TIM1) & TIM_SR_UIF"   "TIM_SR(TIM1) &= ~TIM_SR_UIF")
      ("tim1_trg" "TIM_SR(TIM1) & TIM_SR_TIF"   "TIM_SR(TIM1) &= ~TIM_SR_TIF")
      ("tim1_com" "TIM_SR(TIM1) & TIM_SR_COMIF" "TIM_SR(TIM1) &= ~TIM_SR_COMIF")))
    ("tim1_cc"
     (("tim1_cc1" "TIM_SR(TIM1) & TIM_SR_CC1IF" "TIM_SR(TIM1) &= ~TIM_SR_CC1IF")
      ("tim1_cc2" "TIM_SR(TIM1) & TIM_SR_CC2IF" "TIM_SR(TIM1) &= ~TIM_SR_CC2IF")
      ("tim1_cc3" "TIM_SR(TIM1) & TIM_SR_CC3IF" "TIM_SR(TIM1) &= ~TIM_SR_CC3IF")
      ("tim1_cc4" "TIM_SR(TIM1) & TIM_SR_CC4IF" "TIM_SR(TIM1) &= ~TIM_SR_CC4IF")))
    ("tim2"
     (("tim2_up"  "TIM_SR(TIM2) & TIM_SR_UIF"   "TIM_SR(TIM2) &= ~TIM_SR_UIF")
      ("tim2_trg" "TIM_SR(TIM2) & TIM_SR_TIF"   "TIM_SR(TIM2) &= ~TIM_SR_TIF")
      ("tim2_cc1" "TIM_SR(TIM2) & TIM_SR_CC1IF" "TIM_SR(TIM2) &= ~TIM_SR_CC1IF")
      ("tim2_cc2" "TIM_SR(TIM2) & TIM_SR_CC2IF" "TIM_SR(TIM2) &= ~TIM_SR_CC2IF")
      ("tim2_cc3" "TIM_SR(TIM2) & TIM_SR_CC3IF" "TIM_SR(TIM2) &= ~TIM_SR_CC3IF")
      ("tim2_cc4" "TIM_SR(TIM2) & TIM_SR_CC4IF" "TIM_SR(TIM2) &= ~TIM_SR_CC4IF")))
    ("tim3"
     (("tim3_up"  "TIM_SR(TIM3) & TIM_SR_UIF"   "TIM_SR(TIM3) &= ~TIM_SR_UIF")
      ("tim3_trg" "TIM_SR(TIM3) & TIM_SR_TIF"   "TIM_SR(TIM3) &= ~TIM_SR_TIF")
      ("tim3_cc1" "TIM_SR(TIM3) & TIM_SR_CC1IF" "TIM_SR(TIM3) &= ~TIM_SR_CC1IF")
      ("tim3_cc2" "TIM_SR(TIM3) & TIM_SR_CC2IF" "TIM_SR(TIM3) &= ~TIM_SR_CC2IF")
      ("tim3_cc3" "TIM_SR(TIM3) & TIM_SR_CC3IF" "TIM_SR(TIM3) &= ~TIM_SR_CC3IF")
      ("tim3_cc4" "TIM_SR(TIM3) & TIM_SR_CC4IF" "TIM_SR(TIM3) &= ~TIM_SR_CC4IF")))
    ("tim6_dac"
     (("tim6_up"  "TIM_SR(TIM6) & TIM_SR_UIF"   "TIM_SR(TIM6) &= ~TIM_SR_UIF")
      "dac"))
    ("tim7"
     (("tim7_up"  "TIM_SR(TIM7) & TIM_SR_UIF"   "TIM_SR(TIM7) &= ~TIM_SR_UIF")))
    ("tim14"
     (("tim14_up"  "TIM_SR(TIM14) & TIM_SR_UIF"   "TIM_SR(TIM14) &= ~TIM_SR_UIF")
      ("tim14_cc1" "TIM_SR(TIM14) & TIM_SR_CC1IF" "TIM_SR(TIM14) &= ~TIM_SR_CC1IF")))
    ("tim15"
     (("tim15_brk" "TIM_SR(TIM15) & TIM_SR_BIF"   "TIM_SR(TIM15) &= ~TIM_SR_BIF")
      ("tim15_up"  "TIM_SR(TIM15) & TIM_SR_UIF"   "TIM_SR(TIM15) &= ~TIM_SR_UIF")
      ("tim15_trg" "TIM_SR(TIM15) & TIM_SR_TIF"   "TIM_SR(TIM15) &= ~TIM_SR_TIF")
      ("tim15_com" "TIM_SR(TIM15) & TIM_SR_COMIF" "TIM_SR(TIM15) &= ~TIM_SR_COMIF")
      ("tim15_cc1" "TIM_SR(TIM15) & TIM_SR_CC1IF" "TIM_SR(TIM15) &= ~TIM_SR_CC1IF")
      ("tim15_cc2" "TIM_SR(TIM15) & TIM_SR_CC2IF" "TIM_SR(TIM15) &= ~TIM_SR_CC2IF")))
    ("tim16"
     (("tim16_brk" "TIM_SR(TIM16) & TIM_SR_BIF"   "TIM_SR(TIM16) &= ~TIM_SR_BIF")
      ("tim16_up"  "TIM_SR(TIM16) & TIM_SR_UIF"   "TIM_SR(TIM16) &= ~TIM_SR_UIF")
      ("tim16_com" "TIM_SR(TIM16) & TIM_SR_COMIF" "TIM_SR(TIM16) &= ~TIM_SR_COMIF")
      ("tim16_cc1" "TIM_SR(TIM16) & TIM_SR_CC1IF" "TIM_SR(TIM16) &= ~TIM_SR_CC1IF")))
    ("tim17"
     (("tim17_brk" "TIM_SR(TIM17) & TIM_SR_BIF"   "TIM_SR(TIM17) &= ~TIM_SR_BIF")
      ("tim17_up"  "TIM_SR(TIM17) & TIM_SR_UIF"   "TIM_SR(TIM17) &= ~TIM_SR_UIF")
      ("tim17_com" "TIM_SR(TIM17) & TIM_SR_COMIF" "TIM_SR(TIM17) &= ~TIM_SR_COMIF")
      ("tim17_cc1" "TIM_SR(TIM17) & TIM_SR_CC1IF" "TIM_SR(TIM17) &= ~TIM_SR_CC1IF")))
    ("i2c1"
     (("i2c1_err"
       "(I2C_CR1(I2C1) & I2C_CR1_ERRIE) && (I2C_ISR(I2C1) & (I2C_ISR_ARLO | I2C_ISR_BERR | I2C_ISR_OVR | I2C_ISR_TIMEOUT | I2C_ISR_PECERR | I2C_ISR_ALERT))"
       "I2C_ICR(I2C1) |= I2C_ICR_ARLOCF | I2C_ICR_BERRCF | I2C_ICR_OVRCF | I2C_ICR_TIMEOUTCF | I2C_ICR_PECCF | I2C_ICR_ALERTCF")
      ("i2c1_tc"
       "(I2C_CR1(I2C1) & I2C_CR1_TCIE) && (I2C_ISR(I2C1) & (I2C_ISR_TC | I2C_ISR_TCR))")
      ("i2c1_start"
       "")
      ("i2c1_stop"
       "(I2C_CR1(I2C1) & I2C_CR1_STOPIE) && (I2C_ISR(I2C1) & I2C_ISR_STOPF)"
       "I2C_ICR(I2C1) |= I2C_ICR_STOPCF")
      ("i2c1_nack"
       "(I2C_CR1(I2C1) & I2C_CR1_NACKIE) && (I2C_ISR(I2C1) & I2C_ISR_NACKF)"
       "I2C_ICR(I2C1) |= I2C_ICR_NACKCF")
      ("i2c1_addr"
       "(I2C_CR1(I2C1) & I2C_CR1_ADDRIE) && (I2C_ISR(I2C1) & I2C_ISR_ADDR)"
       "I2C_ICR(I2C1) |= I2C_ICR_ADDRCF")
      ("i2c1_rxne"
       "(I2C_CR1(I2C1) & I2C_CR1_RXIE) && (I2C_ISR(I2C1) & I2C_ISR_RXNE)")
      ("i2c1_txe"
       "(I2C_CR1(I2C1) & I2C_CR1_TXIE) && (I2C_ISR(I2C1) & I2C_ISR_TXIS)")))
    ("i2c2"
     (("i2c2_err"
       "(I2C_CR1(I2C2) & I2C_CR1_ERRIE) && (I2C_ISR(I2C2) & (I2C_ISR_ARLO | I2C_ISR_BERR | I2C_ISR_OVR | I2C_ISR_TIMEOUT | I2C_ISR_PECERR | I2C_ISR_ALERT))"
       "I2C_ICR(I2C2) |= I2C_ICR_ARLOCF | I2C_ICR_BERRCF | I2C_ICR_OVRCF | I2C_ICR_TIMEOUTCF | I2C_ICR_PECCF | I2C_ICR_ALERTCF")
      ("i2c2_tc"
       "(I2C_CR1(I2C2) & I2C_CR1_TCIE) && (I2C_ISR(I2C2) & (I2C_ISR_TC | I2C_ISR_TCR))")
      ("i2c2_stop"
       "(I2C_CR1(I2C2) & I2C_CR1_STOPIE) && (I2C_ISR(I2C2) & I2C_ISR_STOPF)"
       "I2C_ICR(I2C2) |= I2C_ICR_STOPCF")
      ("i2c_nack"
       "(I2C_CR1(I2C2) & I2C_CR1_NACKIE) && (I2C_ISR(I2C2) & I2C_ISR_NACKF)"
       "I2C_ICR(I2C2) |= I2C_ICR_NACKCF")
      ("i2c2_addr"
       "(I2C_CR1(I2C2) & I2C_CR1_ADDRIE) && (I2C_ISR(I2C2) & I2C_ISR_ADDR)"
       "I2C_ICR(I2C2) |= I2C_ICR_ADDRCF")
      ("i2c2_rxne"
       "(I2C_CR1(I2C2) & I2C_CR1_RXIE) && (I2C_ISR(I2C2) & I2C_ISR_RXNE)")
      ("i2c2_txe"
       "(I2C_CR1(I2C2) & I2C_CR1_TXIE) && (I2C_ISR(I2C2) & I2C_ISR_TXIS)")))
    ("spi1"
     (("spi1_txe"
       "(SPI_CR2(SPI1) & SPI_CR2_TXEIE) && (SPI_SR(SPI1) & SPI_SR_TXE)")
      ("spi1_rxne"
       "(SPI_CR2(SPI1) & SPI_CR2_RXNEIE) && (SPI_SR(SPI1) & SPI_SR_RXNE)")
      ("spi1_err"
       "(SPI_CR2(SPI1) & SPI_CR2_ERRIE) && (SPI_SR(SPI1) & (SPI_SR_OVR | SPI_SR_MODF | SPI_SR_CRCERR | SPI_SR_UDR))")))
    ("spi2"
     (("spi2_txe"
       "(SPI_CR2(SPI2) & SPI_CR2_TXEIE) && (SPI_SR(SPI2) & SPI_SR_TXE)")
      ("spi2_rxne"
       "(SPI_CR2(SPI2) & SPI_CR2_RXNEIE) && (SPI_SR(SPI2) & SPI_SR_RXNE)")
      ("spi2_err"
       "(SPI_CR2(SPI2) & SPI_CR2_ERRIE) && (SPI_SR(SPI2) & (SPI_SR_OVR | SPI_SR_MODF | SPI_SR_CRCERR | SPI_SR_UDR))")))
    ("usart1"
     (("usart1_cm"
       "(USART_CR1(USART1) & USART_CR1_CMIE) && (USART_ISR(USART1) & USART_ISR_CMF)"
       "USART_ICR(USART1) |= USART_ICR_CMCF")
      ("usart1_rto"
       "(USART_CR2(USART1) & USART_CR2_RTOEN) && (USART_ISR(USART1) & USART_ISR_RTO)"
       "USART_ICR(USART1) |= USART_ICR_RTOCF")
      ("usart1_cts"
       "(USART_CR3(USART1) & USART_CR3_CTSIE) && (USART_ISR(USART1) & USART_ISR_CTSIF)"
       "USART_ICR(USART1) |= USART_ICR_CTSCF")
      ("usart1_txe"
       "(USART_CR1(USART1) & USART_CR1_TXEIE) && (USART_ISR(USART1) & USART_ISR_TXE)")
      ("usart1_tc"
       "(USART_CR1(USART1) & USART_CR1_TCIE) && (USART_ISR(USART1) & USART_ISR_TC)"
       "USART_ICR(USART1) |= USART_ICR_TCCF")
      ("usart1_rxne"
       "(USART_CR1(USART1) & USART_CR1_RXNEIE) && (USART_ISR(USART1) & USART_ISR_RXNE)"
       "USART_RQR(USART1) |= USART_RQR_RXFRQ")
      ("usart1_idle"
       "(USART_CR1(USART1) & USART_CR1_IDLEIE) && (USART_ISR(USART1) & USART_ISR_IDLE)"
       "USART_ICR(USART1) |= USART_ICR_IDLECF")
      ("usart1_err"
       "(USART_CR3(USART1) & USART_CR3_EIE) && (USART_ISR(USART1) & (USART_ISR_ORE | USART_ISR_NF | USART_ISR_FE | USART_ISR_PE))")))
    ("usart2"
     (("usart2_cm"
       "(USART_CR1(USART2) & USART_CR1_CMIE) && (USART_ISR(USART2) & USART_ISR_CMF)"
       "USART_ICR(USART2) |= USART_ICR_CMCF")
      ("usart2_rto"
       "(USART_CR2(USART2) & USART_CR2_RTOEN) && (USART_ISR(USART2) & USART_ISR_RTO)"
       "USART_ICR(USART2) |= USART_ICR_RTOCF")
      ("usart2_cts"
       "(USART_CR3(USART2) & USART_CR3_CTSIE) && (USART_ISR(USART2) & USART_ISR_CTSIF)"
       "USART_ICR(USART2) |= USART_ICR_CTSCF")
      ("usart2_txe"
       "(USART_CR1(USART2) & USART_CR1_TXEIE) && (USART_ISR(USART2) & USART_ISR_TXE)")
      ("usart2_tc"
       "(USART_CR1(USART2) & USART_CR1_TCIE) && (USART_ISR(USART2) & USART_ISR_TC)"
       "USART_ICR(USART2) |= USART_ICR_TCCF")
      ("usart2_rxne"
       "(USART_CR1(USART2) & USART_CR1_RXNEIE) && (USART_ISR(USART2) & USART_ISR_RXNE)"
       "USART_RQR(USART2) |= USART_RQR_RXFRQ")
      ("usart2_idle"
       "(USART_CR1(USART2) & USART_CR1_IDLEIE) && (USART_ISR(USART2) & USART_ISR_IDLE)"
       "USART_ICR(USART2) |= USART_ICR_IDLECF")
      ("usart2_err"
       "(USART_CR3(USART2) & USART_CR3_EIE) && (USART_ISR(USART2) & (USART_ISR_ORE | USART_ISR_NF | USART_ISR_FE | USART_ISR_PE))")))
    ("usart3_4"
     (("usart3_cm"
       "(USART_CR1(USART3) & USART_CR1_CMIE) && (USART_ISR(USART3) & USART_ISR_CMF)"
       "USART_ICR(USART3) |= USART_ICR_CMCF")
      ("usart3_rto"
       "(USART_CR2(USART3) & USART_CR2_RTOEN) && (USART_ISR(USART3) & USART_ISR_RTO)"
       "USART_ICR(USART3) |= USART_ICR_RTOCF")
      ("usart3_cts"
       "(USART_CR3(USART3) & USART_CR3_CTSIE) && (USART_ISR(USART3) & USART_ISR_CTSIF)"
       "USART_ICR(USART3) |= USART_ICR_CTSCF")
      ("usart3_txe"
       "(USART_CR1(USART3) & USART_CR1_TXEIE) && (USART_ISR(USART3) & USART_ISR_TXE)")
      ("usart3_tc"
       "(USART_CR1(USART3) & USART_CR1_TCIE) && (USART_ISR(USART3) & USART_ISR_TC)"
       "USART_ICR(USART3) |= USART_ICR_TCCF")
      ("usart3_rxne"
       "(USART_CR1(USART3) & USART_CR1_RXNEIE) && (USART_ISR(USART3) & USART_ISR_RXNE)"
       "USART_RQR(USART3) |= USART_RQR_RXFRQ")
      ("usart3_idle"
       "(USART_CR1(USART3) & USART_CR1_IDLEIE) && (USART_ISR(USART3) & USART_ISR_IDLE)"
       "USART_ICR(USART3) |= USART_ICR_IDLECF")
      ("usart3_err"
       "(USART_CR3(USART3) & USART_CR3_EIE) && (USART_ISR(USART3) & (USART_ISR_ORE | USART_ISR_NF | USART_ISR_FE | USART_ISR_PE))")
      ("usart4_cm"
       "(USART_CR1(USART4) & USART_CR1_CMIE) && (USART_ISR(USART4) & USART_ISR_CMF)"
       "USART_ICR(USART4) |= USART_ICR_CMCF")
      ("usart4_rto"
       "(USART_CR2(USART4) & USART_CR2_RTOEN) && (USART_ISR(USART4) & USART_ISR_RTO)"
       "USART_ICR(USART4) |= USART_ICR_RTOCF")
      ("usart4_cts"
       "(USART_CR3(USART4) & USART_CR3_CTSIE) && (USART_ISR(USART4) & USART_ISR_CTSIF)"
       "USART_ICR(USART4) |= USART_ICR_CTSCF")
      ("usart4_txe"
       "(USART_CR1(USART4) & USART_CR1_TXEIE) && (USART_ISR(USART4) & USART_ISR_TXE)")
      ("usart4_tc"
       "(USART_CR1(USART4) & USART_CR1_TCIE) && (USART_ISR(USART4) & USART_ISR_TC)"
       "USART_ICR(USART4) |= USART_ICR_TCCF")
      ("usart4_rxne"
       "(USART_CR1(USART4) & USART_CR1_RXNEIE) && (USART_ISR(USART4) & USART_ISR_RXNE)"
       "USART_RQR(USART4) |= USART_RQR_RXFRQ")
      ("usart4_idle"
       "(USART_CR1(USART4) & USART_CR1_IDLEIE) && (USART_ISR(USART4) & USART_ISR_IDLE)"
       "USART_ICR(USART4) |= USART_ICR_IDLECF")
      ("usart4_err"
       "(USART_CR3(USART4) & USART_CR3_EIE) && (USART_ISR(USART4) & (USART_ISR_ORE | USART_ISR_NF | USART_ISR_FE | USART_ISR_PE))")))
    ("cec_can"
     ("cec"
      "can"))
    "usb"))

(define mcu_timers
  '(("STM32F030"
     ;; advanced
     ((1)       CR1 CR2 SMCR DIER SR EGR CCMR1 CCMR2 CCER CNT PSC ARR RCR CCR1 CCR2 CCR3 CCR4 BDTR DCR DMAR)
     ;; general
     ((3)       CR1 CR2 SMCR DIER SR EGR CCMR1 CCMR2 CCER CNT PSC ARR     CCR1 CCR2 CCR3 CCR4      DCR DMAR)
     ;; basic
     ((6 7)     CR1          DIER SR EGR                  CNT PSC ARR)
     ;; general
     ((14)      CR1          DIER SR EGR CCMR1       CCER CNT PSC ARR     CCR1                              OR)
     ((15)      CR1 CR2 SMCR DIER SR EGR CCMR1       CCER CNT PSC ARR RCR CCR1 CCR2           BDTR DCR DMAR)
     ((16 17)   CR1 CR2      DIER SR EGR CCMR1       CCER CNT PSC ARR RCR CCR1                BDTR DCR DMAR))
    ))

(define mcu_database_rom_size 5)
(define mcu_database_ram_size 7)
(define mcu_database_speed_mhz 4)

(define mcu_database
  '(#("Part Number"
      "Marketing Status"
      "Package"
      "Core"
      "Operating Frequency (MHz) (Processor speed)"
      "FLASH Size (kB) (Prog)"
      "Data E2PROM (B) nom"
      "Internal RAM Size (kB)"
      "Timers (16 bit) typ"
      "Timers (32 bit) typ"
      "Other timer functions"
      "A/D Converters (12-bit channels)"
      "A/D Converters (16-bit channels)"
      "D/A Converters (12 bit) typ"
      "Comparator"
      "I/Os (High Current)"
      "Display controller"
      "Integrated op-amps"
      "I2C typ"
      "SPI typ"
      "I2S typ"
      "CAN typ"
      "USB typ"
      "USART typ"
      "UART typ"
      "Additional Serial Interfaces"
      "Parallel Interfaces"
      "Crypto-HASH"
      "TRNG typ"
      "Supply Voltage (V) min"
      "Supply Voltage (V) max"
      "Supply Current (µA) (Lowest power mode) typ"
      "Supply Current (µA) (Run mode (per Mhz)) typ"
      "Operating Temperature (°C) min"
      "Operating Temperature (°C) max")
    #("STM32F030C6" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M0" 48 32 #f 4 5 #f "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 39 #f #f 1 1 #f #f #f 1 #f #f #f #f #f 2.4 3.6 3.4 250 -40 85)
    #("STM32F030C8" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M0" 48 64 #f 8 7 #f "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 39 #f #f 2 2 #f #f #f 2 #f #f #f #f #f 2.4 3.6 3.4 250 -40 85)
    #("STM32F030CC" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M0" 48 256 #f 32 8 #f "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 38 #f #f 2 2 #f #f #f 6 #f #f #f #f #f 2.4 3.6 1.8 306 -40 85)
    #("STM32F030F4" "Active" "TSSOP 20" "ARM Cortex-M0" 48 16 #f 4 5 #f "24-bit downcounter,2xWDG,RTC" 9 #f #f #f 15 #f #f 1 1 #f #f #f 1 #f #f #f #f #f 2.4 3.6 3.4 250 -40 85)
    #("STM32F030K6" "Active" "LQFP 32 7x7x1.4" "ARM Cortex-M0" 48 32 #f 4 5 #f "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 26 #f #f 1 1 #f #f #f 1 #f #f #f #f #f 2.4 3.6 3.4 250 -40 85)
    #("STM32F030R8" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M0" 48 64 #f 8 7 #f "24-bit downcounter,2xWDG,RTC" 16 #f #f #f 55 #f #f 2 2 #f #f #f 2 #f #f #f #f #f 2.4 3.6 3.4 250 -40 85)
    #("STM32F030RC" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M0" 48 256 #f 32 8 #f "24-bit downcounter,2xWDG,RTC" 16 #f #f #f 52 #f #f 2 2 #f #f #f 6 #f #f #f #f #f 2.4 3.6 1.8 306 -40 85)
    #("STM32F031C4" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M0" 48 16 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 39 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 2 3.6 1.7 250 -40 85)
    #("STM32F031C6" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M0" 48 32 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 39 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F031E6" "Active" "WLCSP 25L P 0.4 MM DIE 444" "ARM Cortex-M0" 48 32 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 20 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 2 3.6 1.7 250 -40 85)
    #("STM32F031F4" "Active" "TSSOP 20" "ARM Cortex-M0" 48 16 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 9 #f #f #f 15 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F031F6" "Active" "TSSOP 20" "ARM Cortex-M0" 48 32 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 9 #f #f #f 15 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F031G4" "Active" "UFQFPN 28 4x4x0.55" "ARM Cortex-M0" 48 16 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 23 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F031G6" "Active" "UFQFPN 28 4x4x0.55" "ARM Cortex-M0" 48 32 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 23 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F031K4" "Active" "UFQFPN 32 5x5x0.55" "ARM Cortex-M0" 48 16 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 27 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 2 3.6 1.7 250 -40 85)
    #("STM32F031K6" "Active" "LQFP 32 7x7x1.4,UFQFPN 32 5x5x0.55" "ARM Cortex-M0" 48 32 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 27 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F038C6" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M0" 48 32 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 38 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 1.65 1.95 1.7 250 -40 "105,85")
    #("STM32F038E6" "Active" "WLCSP 25L P 0.4 MM DIE 444" "ARM Cortex-M0" 48 32 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 9 #f #f #f 20 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 1.65 1.95 1.7 250 -40 85)
    #("STM32F038F6" "Active" "TSSOP 20" "ARM Cortex-M0" 48 32 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 8 #f #f #f 14 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 1.65 1.95 1.7 250 -40 85)
    #("STM32F038G6" "Active" "UFQFPN 28 4x4x0.55" "ARM Cortex-M0" 48 32 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 9 #f #f #f 22 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 1.65 1.95 1.7 250 -40 85)
    #("STM32F038K6" "Active" "UFQFPN 32 5x5x0.55" "ARM Cortex-M0" 48 32 #f 4 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 26 #f #f 1 1 1 #f #f 1 #f #f #f #f #f 1.65 1.95 1.7 250 -40 85)
    #("STM32F042C4" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M0" 48 16 #f 6 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 38 #f #f 1 2 1 1 "USB Device" 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 85)
    #("STM32F042C6" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55" "ARM Cortex-M0" 48 32 #f 6 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 38 #f #f 1 2 1 1 "USB Device" 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F042F4" "Active" "TSSOP 20" "ARM Cortex-M0" 48 16 #f 6 5 1 "24-bit downcounter,2xWDG,RTC" 9 #f #f #f 16 #f #f 1 1 1 1 "USB Device" 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 85)
    #("STM32F042F6" "Active" "TSSOP 20" "ARM Cortex-M0" 48 32 #f 6 5 1 "24-bit downcounter,2xWDG,RTC" 9 #f #f #f 16 #f #f 1 1 1 1 "USB Device" 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F042G4" "Active" "UFQFPN 28 4x4x0.55" "ARM Cortex-M0" 48 16 #f 6 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 24 #f #f 1 1 1 1 "USB Device" 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 85)
    #("STM32F042G6" "Active" "UFQFPN 28 4x4x0.55" "ARM Cortex-M0" 48 32 #f 6 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 24 #f #f 1 1 1 1 "USB Device" 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 85)
    #("STM32F042K4" "Active" "UFQFPN 32 5x5x0.55" "ARM Cortex-M0" 48 16 #f 6 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 28 #f #f 1 1 1 1 "USB Device" 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 85)
    #("STM32F042K6" "Active" "LQFP 32 7x7x1.4,UFQFPN 32 5x5x0.55" "ARM Cortex-M0" 48 32 #f 6 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 28 #f #f 1 1 1 1 "USB Device" 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F042T6" "Active" "WLCSP 36L DIE 445 P 0.4 MM" "ARM Cortex-M0" 48 32 #f 6 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 30 #f #f 1 1 1 1 "USB Device" 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 85)
    #("STM32F048C6" "Active" "UFQFPN 48 7x7x0.55" "ARM Cortex-M0" 48 32 #f 6 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 38 #f #f 1 2 1 1 "USB Device" 2 #f "HDMI CEC" #f #f #f 1.65 1.95 1.7 250 -40 85)
    #("STM32F048G6" "Active" "UFQFPN 28 4x4x0.55" "ARM Cortex-M0" 48 32 #f 6 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 24 #f #f 1 1 1 1 "USB Device" 2 #f "HDMI CEC" #f #f #f 1.65 1.95 1.7 250 -40 85)
    #("STM32F048T6" "Active" "WLCSP 36L DIE 445 P 0.4 MM" "ARM Cortex-M0" 48 32 #f 6 5 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 30 #f #f 1 1 1 1 "USB Device" 2 #f "HDMI CEC" #f #f #f 1.65 1.95 1.7 250 -40 85)
    #("STM32F051C4" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55" "ARM Cortex-M0" 48 16 #f 8 7 1 "24-bit downcounter,2xWDG,RTC" 10 #f 1 2 39 #f #f 1 1 1 #f #f 1 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 85)
    #("STM32F051C6" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55" "ARM Cortex-M0" 48 32 #f 8 7 1 "24-bit downcounter,2xWDG,RTC" 10 #f 1 2 39 #f #f 1 1 1 #f #f 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F051C8" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55" "ARM Cortex-M0" 48 64 #f 8 7 1 "24-bit downcounter,2xWDG,RTC" 10 #f 1 2 39 #f #f 2 2 1 #f #f 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F051K4" "Active" "LQFP 32 7x7x1.4,UFQFPN 32 5x5x0.55" "ARM Cortex-M0" 48 16 #f 8 7 1 "24-bit downcounter,2xWDG,RTC" 10 #f 1 2 27 #f #f 1 1 1 #f #f 1 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F051K6" "Active" "LQFP 32 7x7x1.4,UFQFPN 32 5x5x0.55" "ARM Cortex-M0" 48 32 #f 8 7 1 "24-bit downcounter,2xWDG,RTC" 10 #f 1 2 27 #f #f 1 1 1 #f #f 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F051K8" "Active" "LQFP 32 7x7x1.4,UFQFPN 32 5x5x0.55" "ARM Cortex-M0" 48 64 #f 8 7 1 "24-bit downcounter,2xWDG,RTC" 10 #f 1 2 27 #f #f 1 1 1 #f #f 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F051R4" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M0" 48 16 #f 8 7 1 "24-bit downcounter,2xWDG,RTC" 16 #f 1 2 55 #f #f 1 1 1 #f #f 1 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 85)
    #("STM32F051R6" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M0" 48 32 #f 8 7 1 "24-bit downcounter,2xWDG,RTC" 16 #f 1 2 55 #f #f 1 1 1 #f #f 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F051R8" "Active" "LQFP 64 10x10x1.4,UFBGA 5X5X0.6 64L P 0.5 MM" "ARM Cortex-M0" 48 64 #f 8 7 1 "24-bit downcounter,2xWDG,RTC" 16 #f 1 2 55 #f #f 2 2 1 #f #f 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 "105,85")
    #("STM32F051T8" "Active" "WLCSP 36L P 0.4 MM DIE 440" "ARM Cortex-M0" 48 64 #f 8 7 1 "24-bit downcounter,2xWDG,RTC" 10 #f 1 2 29 #f #f 1 1 1 #f #f 2 #f "HDMI CEC" #f #f #f 2 3.6 1.7 250 -40 85)
    #("STM32F058C8" "Active" "UFQFPN 48 7x7x0.55" "ARM Cortex-M0" 48 64 #f 8 7 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f 2 39 #f #f 2 2 1 #f #f 2 #f "HDMI CEC" #f #f #f 1.65 1.95 1.7 250 -40 85)
    #("STM32F058R8" "Active" "LQFP 64 10x10x1.4,UFBGA 5X5X0.6 64L P 0.5 MM" "ARM Cortex-M0" 48 64 #f 8 7 1 "24-bit downcounter,2xWDG,RTC" 16 #f #f 2 55 #f #f 2 2 1 #f #f 2 #f "HDMI CEC" #f #f #f 1.65 1.95 1.7 250 -40 "105,85")
    #("STM32F058T8" "Active" "WLCSP 36L P 0.4 MM DIE 440" "ARM Cortex-M0" 48 64 #f 8 7 1 "24-bit downcounter,2xWDG,RTC" 10 #f #f 2 28 #f #f 1 1 1 #f #f 2 #f "HDMI CEC" #f #f #f 1.65 1.95 1.7 250 -40 85)
    #("STM32F070C6" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M0" 48 32 #f 6 5 #f "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 37 #f #f 1 1 #f #f "USB Device" 2 #f #f #f #f #f 2.4 3.6 1.7 264 -40 85)
    #("STM32F070CB" "Active" "LQFP 48 7x7x1.4" "ARM Cortex-M0" 48 128 #f 16 8 #f "24-bit downcounter,2xWDG,RTC" 10 #f #f #f 37 #f #f 2 2 #f #f "USB Device" 4 #f #f #f #f #f 2.4 3.6 1.8 273 -40 85)
    #("STM32F070F6" "Active" "TSSOP 20" "ARM Cortex-M0" 48 32 #f 6 5 #f "24-bit downcounter,2xWDG,RTC" 9 #f #f #f 15 #f #f 1 1 #f #f "USB Device" 2 #f #f #f #f #f 2.4 3.6 1.7 264 -40 85)
    #("STM32F070RB" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M0" 48 128 #f 16 8 #f "24-bit downcounter,2xWDG,RTC" 16 #f #f #f 51 #f #f 2 2 #f #f "USB Device" 4 #f #f #f #f #f 2.4 3.6 1.8 273 -40 85)
    #("STM32F071C8" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55" "ARM Cortex-M0" 48 64 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 10 #f 2 2 37 #f #f 2 2 2 #f #f 4 #f "HDMI CEC" #f #f #f 2 3.6 1.8 260 -40 85)
    #("STM32F071CB" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55,WLCSP 49L DIE 448 P 0.4 MM" "ARM Cortex-M0" 48 128 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 10 #f 2 2 37 #f #f 2 2 2 #f #f 4 #f "HDMI CEC" #f #f #f 2 3.6 1.8 260 -40 "105,85")
    #("STM32F071RB" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M0" 48 128 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 51 #f #f 2 2 2 #f #f 4 #f "HDMI CEC" #f #f #f 2 3.6 1.8 260 -40 "105,85")
    #("STM32F071V8" "Active" "LQFP 100 14x14x1.4,UFBGA 100 7x7x0.6" "ARM Cortex-M0" 48 64 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 87 #f #f 2 2 2 #f #f 4 #f "HDMI CEC" #f #f #f 2 3.6 1.8 260 -40 "105,85")
    #("STM32F071VB" "Active" "LQFP 100 14x14x1.4,UFBGA 100 7x7x0.6" "ARM Cortex-M0" 48 128 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 87 #f #f 2 2 2 #f #f 4 #f "HDMI CEC" #f #f #f 2 3.6 1.8 260 -40 85)
    #("STM32F072C8" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55" "ARM Cortex-M0" 48 64 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 10 #f 2 2 37 #f #f 2 2 2 1 "USB Device" 4 #f "HDMI CEC" #f #f #f 2 3.6 1.8 260 -40 "105,85")
    #("STM32F072CB" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55,WLCSP 49L DIE 448 P 0.4 MM" "ARM Cortex-M0" 48 128 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 10 #f 2 2 37 #f #f 2 2 2 1 "USB Device" 4 #f "HDMI CEC" #f #f #f 2 3.6 1.8 260 -40 "105,85")
    #("STM32F072R8" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M0" 48 64 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 51 #f #f 2 2 2 1 "USB Device" 4 #f "HDMI CEC" #f #f #f 2 3.6 1.8 260 -40 85)
    #("STM32F072RB" "Active" "LQFP 64 10x10x1.4,UFBGA 5X5X0.6 64L P 0.5 MM" "ARM Cortex-M0" 48 128 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 51 #f #f 2 2 2 1 "USB Device" 4 #f "HDMI CEC" #f #f #f 2 3.6 1.8 260 -40 "105,85")
    #("STM32F072V8" "Active" "LQFP 100 14x14x1.4,UFBGA 100 7x7x0.6" "ARM Cortex-M0" 48 64 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 87 #f #f 2 2 2 1 "USB Device" 4 #f "HDMI CEC" #f #f #f 2 3.6 1.8 260 -40 85)
    #("STM32F072VB" "Active" "LQFP 100 14x14x1.4,UFBGA 100 7x7x0.6" "ARM Cortex-M0" 48 128 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 87 #f #f 2 2 2 1 "USB Device" 4 #f "HDMI CEC" #f #f #f 2 3.6 1.8 260 -40 85)
    #("STM32F078CB" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55,WLCSP 49L DIE 448 P 0.4 MM" "ARM Cortex-M0" 48 128 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 10 #f 2 2 36 #f #f 2 2 2 1 "USB Device" 4 #f "HDMI CEC" #f #f #f 1.65 1.95 1.8 260 -40 85)
    #("STM32F078RB" "Active" "LQFP 64 10x10x1.4,UFBGA 5X5X0.6 64L P 0.5 MM" "ARM Cortex-M0" 48 128 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 50 #f #f 2 2 2 1 "USB Device" 4 #f "HDMI CEC" #f #f #f 1.65 1.95 1.8 260 -40 "105,85")
    #("STM32F078VB" "Active" "LQFP 100 14x14x1.4,UFBGA 100 7x7x0.6" "ARM Cortex-M0" 48 128 #f 16 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 86 #f #f 2 2 2 1 "USB Device" 4 #f "HDMI CEC" #f #f #f 1.65 1.95 1.8 260 -40 85)
    #("STM32F091CB" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55" "ARM Cortex-M0" 48 128 #f 32 8 1 "24-bit downcounter,2xWDG,RTC" 10 #f 2 2 38 #f #f 2 2 2 1 #f 6 #f "HDMI CEC" #f #f #f 2 3.6 1.8 306 -40 85)
    #("STM32F091CC" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55" "ARM Cortex-M0" 48 256 #f 32 8 1 "24-bit downcounter,2xWDG,IR Timer,RTC" 10 #f 2 2 38 #f #f 2 2 2 1 #f 6 #f "HDMI CEC" #f #f #f 2 3.6 1.8 306 -40 "105,85")
    #("STM32F091RB" "Active" "LQFP 64 10x10x1.4" "ARM Cortex-M0" 48 128 #f 32 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 52 #f #f 2 2 2 1 #f 8 #f "HDMI CEC" #f #f #f 2 3.6 1.8 306 -40 85)
    #("STM32F091RC" "Active" "LQFP 64 10x10x1.4,UFBGA 5X5X0.6 64L P 0.5 MM,WLCSP 64L DIE 442 P 0.4 MM" "ARM Cortex-M0" 48 256 #f 32 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 52 #f #f 2 2 2 1 #f 8 #f "HDMI CEC" #f #f #f 2 3.6 1.8 306 -40 "105,85")
    #("STM32F091VB" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M0" 48 128 #f 32 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 88 #f #f 2 2 2 1 #f 8 #f "HDMI CEC" #f #f #f 2 3.6 1.8 306 -40 "105,85")
    #("STM32F091VC" "Active" "LQFP 100 14x14x1.4,UFBGA 100 7x7x0.6" "ARM Cortex-M0" 48 256 #f 32 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 88 #f #f 2 2 2 1 #f 8 #f "HDMI CEC" #f #f #f 2 3.6 1.8 306 -40 "105,85")
    #("STM32F098CC" "Active" "LQFP 48 7x7x1.4,UFQFPN 48 7x7x0.55" "ARM Cortex-M0" 48 256 #f 32 8 1 "24-bit downcounter,2xWDG,RTC" 10 #f 2 2 37 #f #f 2 2 2 1 #f 8 #f "HDMI CEC" #f #f #f 1.65 1.95 1.8 306 -40 "105,85")
    #("STM32F098RC" "Active" "LQFP 64 10x10x1.4,UFBGA 5X5X0.6 64L P 0.5 MM,WLCSP 64L DIE 442 P 0.4 MM" "ARM Cortex-M0" 48 256 #f 32 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 51 #f #f 2 2 2 1 #f 8 #f "HDMI CEC" #f #f #f 1.65 1.95 1.8 306 -40 85)
    #("STM32F098VC" "Active" "LQFP 100 14x14x1.4" "ARM Cortex-M0" 48 256 #f 32 8 1 "24-bit downcounter,2xWDG,RTC" 16 #f 2 2 87 #f #f 2 2 2 1 #f 8 #f "HDMI CEC" #f #f #f 1.65 1.95 1.8 306 -40 85)))

;; pattern page-size(KBites) sector-size(KBytes) program-word-size(bytes)
(define mcu_flash
  (let ([target (string-upcase (get "target"))])
    (cond ((~~ target "STM32F0(30.[468]|70.6).*") '(1 4 2))
          ((~~ target "STM32F0(30.C|70.B).*")     '(2 4 2)))))

;; pattern backup-domain-registers register-size(bytes)
(define mcu_backup
  '(0 0))

;; 8-byte stack align
(define mcu_system
  '(stack-align-8-byte-already))
