[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+  (load "adc.scm")
+][+  include "../all/adc_f01.tpl"
+][+  (add-c-header "stm32/f0/adc.h")
+][+  define adc_init
+][+    # start calibration
+][+    reg_set ident=2 reg=(adc_reg "CR") val=ADC_CR_ADCAL
+][+    # wait for calibration end
+][+    reg_wait ident=2 reg=(adc_reg "CR") unless msk=ADC_CR_ADCAL
+][+    # enable
+][+    reg_set ident=2 reg=(adc_reg "CR") val=ADC_CR_ADEN
+][+    # wait for ready
+][+    reg_wait ident=2 reg=(adc_reg "ISR") msk=ADC_ISR_ADRDY
+][+    # configure
+][+    reg_put ident=2 reg=(adc_reg "CFGR1") val=(adc_ADC_CFGR1_val)
+][+    reg_put ident=2 reg=(adc_reg "SMPR1") val=(adc_ADC_SMPR_val)
+][+    reg_put ident=2 reg=(adc_reg "CHSELR") val=(adc_ADC_CHSELR_val)
+][+    reg_put ident=2 reg=(adc_reg "CCR") val=(adc_ADC_CCR_val)
+][+    reg_set ident=2 reg=(adc_reg "CR") val=(adc_ADC_CR_set)
+][+  enddef adc_init
+][+  define adc_done
+][+    # stop and disable
+][+    reg_set ident=2 reg=(adc_reg "CR") val="ADC_CR_ADSTP | ADC_CR_ADDIS"
+][+  enddef adc_done
+][+  define adc_trig
+][+    # trigger conversion
+][+    reg_put ident=2 reg=(adc_reg "CR") val=ADC_CR_ADSTART
+][+  enddef adc_trig
+]
