(define srt_usarts '(1 2 3 4))
(define srt_uarts '(5 6))

(define (srt_rx_reg)
  (srt_reg "RDR"))

(define (srt_tx_reg)
  (srt_reg "TDR"))

;; Control register 1
(define (srt_USART_CR1_extra)
  (or-bits
   ;; Word length 1
   (if (is? "data-bits" "7")
       "USART_CR1_M1")
   ;; End of block interrupt enable
   ;; Receiver timeout interrupt enable
   ;; Driver enable assertion time
   ;; Driver enable de-assertion time
   ;; Oversampling mode
   (if (is? "sampling" "8")
       "USART_CR1_OVER8")
   ;; Character match interrupt enable
   ;; Mute mode enable
   (if (or (is? "wakeup" "idle-line")
           (is? "wakeup" "address-mark"))
       "USART_CR1_MME")
   ;; Word length 0
   (if (is? "data-bits" "9")
       "USART_CR1_M0")
   ))

;; Control register 2
(define (srt_USART_CR2_extra)
  (or-bits
   ;; Receiver timeout enable
   ;; Auto baudrate mode
   (if (has? "auto-baud-rate")
       (get-def-name "auto-baud-rate" ;; startbit|falltofall
                     "USART_CR2_ABRMOD_"))
   ;; Auto baudrate enable
   (if (has? "auto-baud-rate")
       "USART_CR2_ABREN")
   ;; Most significant bit first
   (if (is? "bits-order" "msb")
       "USART_CR2_MSBFIRST")
   ;; Binary data inversion
   (if (is-false? "active")
       "USART_CR2_DATAINV")
   ;; Transmitter pin active level inversion
   (if (is-false? "tx.active")
       "USART_CR2_TXINV")
   ;; Receiver pin active level inversion
   (if (is-false? "rx.active")
       "USART_CR2_RXINV")
   ;; Swap transmitter/receiver pins
   (if (exist? "swap-lines")
       "USART_CR2_SWAP")
   ;; 7/4-bits address detection
   (if (is? "address-bits" "7")
       "USART_CR2_ADDM7")
   ))

;; Control register 3
(define (srt_USART_CR3_extra)
  (or-bits
   ;; Driver enable polarity
   ;; Driver enable mode
   ;; Disable DMA on reception error
   (if (exist? "rx.dma.disable-on-error")
       "USART_CR3_DDRE")
   ;; Overrun disable
   (if (exist? "no-overrun")
       "USART_CR3_OVRDIS")
   ;; One/three sample bit method
   (if (is? "sample-bits" "1")
       "USART_CR3_ONEBIT")
   ))

(define (srt_div_val baud)
  ;; div = round(clock / baud),     when sampling = 16
  ;; div = round(2 * clock / baud), when sampling = 8
  (let ([mul (if (is? "sampling" "8") "2 * " "")]
        [clock (srt_clock)])
    (srt_div_wrap
     (srt_round_wrap
      (string-append
       mul clock " / " baud
       )))))

(define (srt_baud_val div)
  ;; baud = round(clock / div),     when sampling = 16
  ;; baud = round(2 * clock / div), when sampling = 8
  (let ([mul (if (is? "sampling" "8") "2 * " "")]
        [clock (srt_clock)])
    (srt_round_wrap
     (string-append
      mul clock " / "
      (srt_div_unwrap div)))))

(define (srt_div_wrap div)
  (if (is? "sampling" "8")
      (string-append "({ uint16_t div = " div "; ((div) & 0xfff0) | (((div) & 0x000f) >> 1); })")
      div))

(define (srt_div_unwrap div)
  (if (is? "sampling" "8")
      (string-append "({ uint16_t div = " div "; ((div) & 0xfff0) | (((div) & 0x000f) << 1); })")
      div))
