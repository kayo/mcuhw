(define (ld_mapping_region name mode addr size cmt)
  (string-append
   (if (and (string? cmt) (not (string=? cmt "")))
       (string-append "  /* " cmt " */\n") "")
   "  " name " (" mode ") : "
   "ORIGIN = 0x" (number->string addr 16) ", "
   "LENGTH = 0x" (number->string size 16) "\n"))

(define (ld_mapping_areas bank addr align)
  (define (mapping_area lst)
    (if (null? lst)
        ""
        (string-append
         (mapping_area (cdr lst))
         (let* ([area (car lst)]
                [area_id (car area)]
                [area_mode (cadr area)]
                [area_size (cadddr area)]
                [area_addr (apply
                            + (cons addr
                                    (map (lambda (area)
                                           (cadddr area))
                                         (cdr lst))))])
           (ld_mapping_region
            area_id area_mode
            area_addr area_size
            (string-append
             "Memory area '" area_id "' region")))
         )))
  (mapping_area (ld_get_areas bank align)))

(define (ld_mapping_regions)
  (apply string-append
         (map (lambda (bank)
                (let ([bank_id (car bank)]
                      [bank_mode (cadr bank)]
                      [bank_addr (caddr bank)]
                      [calc_addr (ld_bank_addr bank)]
                      [calc_size (ld_bank_size bank)])
                  (string-append
                   (ld_mapping_areas
                    bank_id bank_addr "start")
                   (ld_mapping_region ;; bank region
                    bank_id bank_mode
                    calc_addr calc_size
                    (string-append
                     "Memory bank '" bank_id "' region"))
                   (ld_mapping_areas
                    bank_id (+ calc_addr calc_size) "end")
                   )))
              mcu_memory_banks)))

(define (ld_provide_region id name cmt)
  (string-append
   "\n"
   (if (and (string? cmt) (not (string=? cmt "")))
       (string-append "/* " cmt " */\n") "")
   "PROVIDE(_" id "_start = ORIGIN(" name "));\n"
   "PROVIDE(_" id "_end = ORIGIN(" name ") + LENGTH(" name "));\n"
   ))

(define (ld_provide_regions)
  (define (provide_regions lst)
    (if (null? lst)
        ""
        (string-append
         (provide_regions (cdr lst))
         (let ([area (car lst)])
           (ld_provide_region
            (car area) (car area)
            (string-append
             "Provide symbols for region '"
             (car area) "'")))
         )))
  (provide_regions ld_memory_table))
