[+ autogen5 template -*- mode: poly-c+autogen; -*-
+][+ (load "lib.scm")
+][+  (add-h-header "stddef.h")
+][+  (add-h-header "stdint.h")
+][+  # clear register bits
+][+  define reg_clr
+][+    if (has? "val")
+][+      (ident)
+][+      reg
+] &= ~([+ val
+]);
[+      endif
+][+  enddef reg_clr
+][+  # set register bits
+][+  define reg_set
+][+    if (has? "val")
+][+      (ident)
+][+      reg
+] |= ([+ val
+]);
[+      endif
+][+  enddef reg_set
+][+  # put register fields
+][+  define reg_put
+][+    if (has? "val")
+][+      (ident)
+][+      reg
+] = [+   if (has? "msk")
+]([+       reg
+] & ~([+   msk
+]))
[+ (ident) +]  | [+ endif
+]([+   val
+]);
[+      endif
+][+  enddef reg_put
+][+  # get and check register fields
+][+  define reg_has
+][+    if (or (has? "msk") (has? "val"))
+]([+     reg
+][+      if (has? "msk")
+] & ([+     msk
+])[+     endif
+])[+     if (has? "val")
+] == ([+   val
+])[+     endif
+][+    endif
+][+  enddef reg_has
+][+  define reg_get
+][+    if (or (has? "var") (exist? "ret"))
+][+      (ident)
+][+      if (has? "var")
+][+        (get "var") +] = [+
          else (exist? "ret")
            +]return [+
          endif (has? "var")
+][+      reg_has
+];
[+      endif (or (has? "var") (exist? "ret"))
+][+  enddef reg_get
+][+  # busy wait
+][+  define reg_wait
+][+    if (or (has? "msk") (has? "val"))
+][+      (ident)
+]for (; [+ (if (exist? "unless") "" "!")
+]([+ reg_has
+]); )[+ if (is? "wait" "sleep") +] mcu_wait_for_event()[+ endif +];
[+      endif
+][+  enddef reg_wait
+][+  define lds_load
+][+    for memory
+][+      for region
+][+        (ld_add_area)
+][+      endfor region
+][+    endfor memory
+][+  enddef lds_load
+]
