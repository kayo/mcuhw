#!/usr/bin/env python
"""
This file is part of PyCortexMDebug

PyCortexMDebug is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyCortexMDebug is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyCortexMDebug.  If not, see <http://www.gnu.org/licenses/>.
"""

import gdb
import gdb_util

DEMCR = 0xE000EDFC

DWT_CTRL     = 0xE0001000
DWT_CYCCNT   = 0xE0001004
DWT_CPICNT   = 0xE0001008
DWT_EXTCNT   = 0xE000100C
DWT_SLEEPCNT = 0xE0001010
DWT_LSUCNT   = 0xE0001014
DWT_FOLDCNT  = 0xE0001018
DWT_PCSR     = 0xE000101C

DWT_CTRL_CYCCNTENA = (1 << 0)

DEMCR_TRCENA = (1 << 24)

prefix = "dwt: "

usage = """
dwt:
        List available peripherals
dwt configclk [Hz]:
        Set clock for rendering time values in seconds
dwt reset:
        Reset everything in DWT
dwt reset counters:
        Reset all DWT counters
dwt cyccnt:
        Display the cycle count
        d(default): decimal, x: hex, o: octal, b: binary
"""

def show_help():
    gdb.write(usage)

def cyccnt_enable():
    gdb_util.reg_modify(DWT_CTRL, 0, DWT_CTRL_CYCCNTENA)

def cyccnt_disable():
    gdb_util.reg_modify(DWT_CTRL, DWT_CTRL_CYCCNTENA, 0)

def cyccnt_active():
    return gdb_util.reg_read(DWT_CTRL) & DWT_CTRL_CYCCNTENA

def cyccnt_read():
    return gdb_util.reg_read(DWT_CYCCNT)

def cyccnt_reset(value = 0):
    gdb_util.reg_write(DWT_CYCCNT, value)

def cpicnt_reset(value = 0):
    gdb_util.reg_write(DWT_CPICNT, value & 0xFF)

class DWT(gdb.Command):
    clk = None
    inited = False
    
    def __init__(self):
        gdb.Command.__init__(self, "dwt", gdb.COMMAND_DATA)
    
    def invoke(self, args, from_tty):
        if not self.inited:
            # enable use of the trace and debug blocks
            gdb_util.reg_modify(DEMCR, 0, DEMCR_TRCENA)
            gdb_util.reg_write(DWT_CTRL, 0)
            self.inited = True

        form, s = gdb_util.get_format(gdb_util.lower_args(gdb_util.parse_args(args)))
        
        # Check for empty command
        if s[0] in ['', 'help']:
            show_help()
            return
        
        if s[0] == "cyccnt":
            if len(s) > 1:
                if s[1][:2] == "en":
                    cyccnt_enable()
                elif s[1][0] == "r":
                    cyccnt_reset()
                elif s[1][0] == "d":
                    cyccnt_disable()
            gdb.write(prefix + "CYCCNT ({}): {}\n".format("ON" if cyccnt_active() else "OFF", self.cycles_str(cyccnt_read())))
        elif s[0] == "reset":
            if len(s) > 1:
                if s[1] == "cyccnt":
                    cyccnt_reset()
                    gdb.write(prefix + "CYCCNT reset\n")
                if s[1] == "counters":
                    cyccnt_reset()
                    gdb.write(prefix + "CYCCNT reset\n")
                else:
                    cyccnt_reset()
                    gdb.write(prefix + "CYCCNT reset\n")
            else:
                # Reset everything
                cyccnt_reset()
                gdb.write(prefix + "CYCCNT reset\n")
        elif s[0] == "configclk":
            if len(s) == 2:
                try:
                    self.clk = float(s[1])
                except:
                    show_help()
            else:
                show_help()
        else:
            # Try to figure out what stupid went on here
            gdb.write(args)
            show_help()

    def complete(self, text, word):
        form, s = gdb_util.get_format(gdb_util.lower_args(gdb_util.parse_args(text)))
        
        commands = ['help', 'configclk', 'reset', 'cyccnt']
        reset_commands = ['counters', 'cyccnt']
        cyccnt_commands = ['enable', 'reset', 'disable']

        if len(s) == 1:
            return filter(lambda x: x.startswith(s[0]), commands)

        if len(s) == 2:
            if s[0] == 'reset':
                return filter(lambda x: x.startswith(s[1]), reset_commands)
            if s[0] == 'cyccnt':
                return filter(lambda x: x.startswith(s[1]), cyccnt_commands)
    
    def cycles_str(self, cycles):
        if self.clk:
            return "{:d} cycles, {:.3es}\n".format(cycles, cycles * 1.0 / self.clk)
        else:
            return "{:d} cycles".format(cycles)
