#!/usr/bin/env python
"""
This file is part of PyCortexMDebug

PyCortexMDebug is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyCortexMDebug is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyCortexMDebug.  If not, see <http://www.gnu.org/licenses/>.
"""

import re
import gdb
import gdb_util

def DBGMCU_BASE(target):
    if re.search('stm32f0x', target) is not None:
        return 0x40000000 + 0x15800
    elif re.search('stm32f1x', target) is not None:
        return 0xE0000000 + 0x42000
    else:
        return False

DBGMCU_IDCODE = 0x0
DBGMCU_CR     = 0x4

DBGMCU_IDCODE_DEV_ID_SHIFT = 0
DBGMCU_IDCODE_DEV_ID_MASK = 0x00000fff
DBGMCU_IDCODE_REV_ID_SHIFT = 16
DBGMCU_IDCODE_REV_ID_MASK = 0xffff0000

DBGMCU_CR_SLEEP              = 1 << 0
DBGMCU_CR_STOP               = 1 << 1
DBGMCU_CR_STANDBY            = 1 << 2
DBGMCU_CR_TRACE_IOEN         = 1 << 5

DBGMCU_CR_TRACE_MODE_SHIFT   = 6
DBGMCU_CR_TRACE_MODE_MASK    = 0x000000C0

DBGMCU_CR_TRACE_MODE_ASYNC   = 0x0
DBGMCU_CR_TRACE_MODE_SYNC_1  = 0x1
DBGMCU_CR_TRACE_MODE_SYNC_2  = 0x2
DBGMCU_CR_TRACE_MODE_SYNC_4  = 0x3

DBGMCU_CR_IWDG_STOP          = 1 << 8
DBGMCU_CR_WWDG_STOP          = 1 << 9

DBGMCU_CR_TIM1_STOP          = 1 << 10
DBGMCU_CR_TIM2_STOP          = 1 << 11
DBGMCU_CR_TIM3_STOP          = 1 << 12
DBGMCU_CR_TIM4_STOP          = 1 << 13
DBGMCU_CR_CAN1_STOP          = 1 << 14
DBGMCU_CR_I2C1_SMBUS_TIMEOUT = 1 << 15
DBGMCU_CR_I2C2_SMBUS_TIMEOUT = 1 << 16
DBGMCU_CR_TIM8_STOP          = 1 << 17
DBGMCU_CR_TIM5_STOP          = 1 << 18
DBGMCU_CR_TIM6_STOP          = 1 << 19
DBGMCU_CR_TIM7_STOP          = 1 << 20
DBGMCU_CR_CAN2_STOP          = 1 << 21
DBGMCU_CR_TIM15_STOP         = 1 << 22
DBGMCU_CR_TIM16_STOP         = 1 << 23
DBGMCU_CR_TIM17_STOP         = 1 << 24
DBGMCU_CR_TIM12_STOP         = 1 << 25
DBGMCU_CR_TIM13_STOP         = 1 << 26
DBGMCU_CR_TIM14_STOP         = 1 << 27
DBGMCU_CR_TIM9_STOP          = 1 << 28
DBGMCU_CR_TIM10_STOP         = 1 << 29
DBGMCU_CR_TIM11_STOP         = 1 << 30

flags = {
    'sleep': DBGMCU_CR_SLEEP,
    'stop': DBGMCU_CR_STOP,
    'standby': DBGMCU_CR_STANDBY,
    #'trace_ioen': DBGMCU_CR_TRACE_IOEN,
    'iwdg_stop': DBGMCU_CR_IWDG_STOP,
    'wwdg_stop': DBGMCU_CR_WWDG_STOP,
    'tim1_stop': DBGMCU_CR_TIM1_STOP,
    'tim2_stop': DBGMCU_CR_TIM2_STOP,
    'tim3_stop': DBGMCU_CR_TIM3_STOP,
    'tim4_stop': DBGMCU_CR_TIM4_STOP,
    'can1_stop': DBGMCU_CR_CAN1_STOP,
    'i2c1_smbus_timeout': DBGMCU_CR_I2C1_SMBUS_TIMEOUT,
    'i2c2_smbus_timeout': DBGMCU_CR_I2C2_SMBUS_TIMEOUT,
    'tim8_stop': DBGMCU_CR_TIM8_STOP,
    'tim5_stop': DBGMCU_CR_TIM5_STOP,
    'tim6_stop': DBGMCU_CR_TIM6_STOP,
    'tim7_stop': DBGMCU_CR_TIM7_STOP,
    'can2_stop': DBGMCU_CR_CAN2_STOP,
    'tim15_stop': DBGMCU_CR_TIM15_STOP,
    'tim16_stop': DBGMCU_CR_TIM16_STOP,
    'tim17_stop': DBGMCU_CR_TIM17_STOP,
    'tim12_stop': DBGMCU_CR_TIM12_STOP,
    'tim13_stop': DBGMCU_CR_TIM13_STOP,
    'tim14_stop': DBGMCU_CR_TIM14_STOP,
    'tim9_stop': DBGMCU_CR_TIM9_STOP,
    'tim10_stop': DBGMCU_CR_TIM10_STOP,
    'tim11_stop': DBGMCU_CR_TIM11_STOP,
}

usage = """
dbgmcu idcode [dev_id, rev_id]:
    Show idcode

dbgmcu trace:
    Show trace state and mode
dbgmcu trace state:
    Show trace state
dbgmcu trace enable:
    Enable tracing
dbgmcu trace disable:
    Disable tracing
dbgmcu trace mode:
    Show trace mode
dbgmcu trace mode async|sync [1|2|4]:
    Change trace mode

dbgmcu flags:
    Show active debug flags
dbgmcu flags all:
    Show available debug flags
dbgmcu flags set [flags...]:
    Set debug flags
dbgmcu flags clear [flags...]:
    Clear debug flags

dbgmcu[/<format>] ...
    Specify format for numeric values
    d(default): decimal, x: hex, o: octal, b: binary
"""

def show_help():
    gdb.write(usage)

class DBGMCU(gdb.Command):
    DBGMCU_BASE = None
    
    def __init__(self):
        gdb.Command.__init__(self, "dbgmcu", gdb.COMMAND_DATA)

    def once(self):
        if self.DBGMCU_BASE is None:
            target = gdb.execute("monitor target current", True, True)
            self.DBGMCU_BASE = DBGMCU_BASE(target)
    
    def invoke(self, args, from_tty):
        f, s = gdb_util.get_format(gdb_util.lower_args(gdb_util.parse_args(args)))
        
        if len(s) == 0 or s[0] == '':
            s = ['h']
        
        for c in ['?', 'h', 'u']:
            if s[0].startswith(c):
                show_help()
                return

        self.once()

        if s[0].startswith('i'):
            self.dbgmcu_idcode(f, s[1:])
        elif s[0].startswith('t'):
            self.dbgmcu_trace(s[1:])
        elif s[0].startswith('f'):
            self.dbgmcu_flags(s[1:])

    def complete(self, text, word):
        f, s = gdb_util.get_format(gdb_util.lower_args(gdb_util.parse_args(text)))
        
        if len(s) == 1:
            return filter(lambda x: x.startswith(s[0]), ['help', 'idcode', 'flags', 'trace'])
        else:
            self.once()
            if s[0].startswith('i'):
                return filter(lambda x: x.startswith(s[-1]), ['device', 'revision'])
            elif s[0].startswith('f'):
                if len(s) == 2:
                    return filter(lambda x: x.startswith(s[1]), ['all', 'set', 'clear', 'reset'])
                else:
                    active = self.active_flags()
                    if s[1].startswith('s'):
                        return [flag for flag in flags if flag.startswith(s[-1]) and flag not in active]
                    elif s[1].startswith('c') or s[1].startswith('r'):
                        return [flag for flag in flags if flag.startswith(s[-1]) and flag in active]
            elif s[0].startswith('t'):
                if len(s) == 2:
                    return filter(lambda x: x.startswith(s[1]), ['enable', 'disable', 'mode'])
                elif s[1].startswith('m'):
                    if len(s) == 3:
                        return filter(lambda x: x.startswith(s[1]), ['async', 'sync'])
                    elif len(s) == 4 and s[2].startswith('s'):
                        return filter(lambda x: x.startswith(s[1]), ['1', '2', '4'])
        return []

    def dbgmcu_idcode(self, form, fields):
        idcode = gdb_util.reg_read(self.DBGMCU_BASE + DBGMCU_IDCODE)
        dev_id = (idcode & DBGMCU_IDCODE_DEV_ID_MASK) >> DBGMCU_IDCODE_DEV_ID_SHIFT
        dev_id = gdb_util.num_format(dev_id, form, 16)
        rev_id = (idcode & DBGMCU_IDCODE_REV_ID_MASK) >> DBGMCU_IDCODE_REV_ID_SHIFT
        rev_id = gdb_util.num_format(rev_id, form, 16)
        if len(fields) == 0:
            gdb.write("Dev. Id: {}\nRev. Id: {}\n".format(dev_id, rev_id))
        else:
            for field in fields:
                if field.startswith('d'):
                    gdb.write(dev_id + "\n")
                elif field.startswith('r'):
                    gdb.write(rev_id + "\n")
                else:
                    gdb.write("Unsupported field: {}...\n".format(field))

    def dbgmcu_trace_show(self, show_state = True, show_mode = True):
        val = gdb_util.reg_read(self.DBGMCU_BASE + DBGMCU_CR)
        state = val & DBGMCU_CR_TRACE_IOEN
        mode = (val & DBGMCU_CR_TRACE_MODE_MASK) >> DBGMCU_CR_TRACE_MODE_SHIFT
        async = mode == DBGMCU_CR_TRACE_MODE_ASYNC

        data = 0
        if mode == DBGMCU_CR_TRACE_MODE_SYNC_1:
            data = 1
        elif mode == DBGMCU_CR_TRACE_MODE_SYNC_2:
            data = 2
        elif mode == DBGMCU_CR_TRACE_MODE_SYNC_4:
            data = 4

        state_str = " enabled" if state else " disabled"
        mode_str = "async" if async else "sync data {}".format(data)
        
        gdb.write("trace" + (state_str if show_state else "") + (" mode " + mode_str if show_mode else "") + "\n")
    
    def dbgmcu_trace(self, args):
        if len(args) == 0:
            self.dbgmcu_trace_show()
        else:
            if args[0].startswith('s'): # show state
                self.dbgmcu_trace_show(True, False)
            elif args[0].startswith('e'): # enable
                gdb_util.reg_modify(self.DBGMCU_BASE + DBGMCU_CR, 0, DBGMCU_CR_TRACE_IOEN)
                gdb.write("trace enabled\n")
            elif args[0].startswith('d'): # disable
                gdb_util.reg_modify(self.DBGMCU_BASE + DBGMCU_CR, DBGMCU_CR_TRACE_IOEN, 0)
                gdb.write("trace disabled\n")
            elif args[0].startswith('m'):
                if len(args) == 1:
                    self.dbgmcu_trace_show(False, True)
                else:
                    mode = None
                    if args[1].startswith('a'):
                        mode = DBGMCU_CR_TRACE_MODE_ASYNC
                    elif args[1].startswith('s'):
                        if len(args) == 3:
                            if args[2] == '1':
                                mode = DBGMCU_CR_TRACE_MODE_SYNC_1
                            elif args[2] == '2':
                                mode = DBGMCU_CR_TRACE_MODE_SYNC_2
                            elif args[2] == '4':
                                mode = DBGMCU_CR_TRACE_MODE_SYNC_4
                            else:
                                gdb.write("Invalid sync trace data pins: {}...\n".format(args[2]))
                        else:
                            mode = DBGMCU_CR_TRACE_MODE_SYNC_1
                    else:
                        gdb.write("Invalid trace mode: {}...\n".format(args[1]))
                    if mode is not None:
                        gdb_util.reg_modify(self.DBGMCU_BASE + DBGMCU_CR,
                                            DBGMCU_CR_TRACE_MODE_MASK,
                                            mode << DBGMCU_CR_TRACE_MODE_SHIFT)
            else:
                gdb.write("Unsupported action: {}...\n".format(args[0]))

    def active_flags(self):
        val = gdb_util.reg_read(self.DBGMCU_BASE + DBGMCU_CR)
        return [flag for flag in flags if val & flags[flag]]
    
    def dbgmcu_flags(self, args):
        if len(args) == 0:
            val = gdb_util.reg_read(self.DBGMCU_BASE + DBGMCU_CR)
            gdb.write(" ".join(self.active_flags()) + "\n")
        else:
            if args[0].startswith('a') or args[0].startswith('l'):
                gdb.write(" ".join([flag for flag in flags]) + "\n")
            elif args[0].startswith('s') or args[0].startswith('c') or args[0].startswith('r'):
                mask = 0
                for flag in flags:
                    if flag in args[1:]:
                        mask |= flags[flag]
                gdb_util.reg_modify(self.DBGMCU_BASE + DBGMCU_CR,
                                    0 if args[0].startswith('s') else mask,
                                    mask if args[0].startswith('s') else 0)
            else:
                gdb.write("Unsupported action: {}...\n".format(args[0]))
