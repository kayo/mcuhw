#!/usr/bin/env python
"""
This file is part of PyCortexMDebug

PyCortexMDebug is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyCortexMDebug is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyCortexMDebug.  If not, see <http://www.gnu.org/licenses/>.
"""

import gdb
import re
import math

def reg_type(bits = 32):
    if bits == 8:
        return "unsigned char"
    elif bits == 16:
        return "unsigned short"
    elif bits == 32:
        return "unsigned int"
    elif bits == 64:
        return "unsigned long long"

def reg_read(addr, bits = 32):
    """ Read from memory and return an integer
    """
    return int(gdb.parse_and_eval("*({}*)0x{:x}".format(reg_type(bits), addr)))

def reg_write(addr, data, bits = 32):
    """ Write value to memory
    """
    return gdb.parse_and_eval("*({}*)0x{:x} = {:d}".format(reg_type(bits), addr, data))

def reg_modify(addr, clrbits, setbits, bits = 32):
    reg_write(addr, (reg_read(addr, bits) & ~clrbits) | setbits, bits)

def parse_args(args):
    return str(args).split(" ")

def lower_args(args):
    return map(lambda x: x.lower(), args)

def get_format(args):
    form = 'd'
    if args[0] and args[0][0] == '/':
        form = args[0][1:]
        if form not in ['d', 'a', 'x', 'o', 'b', 't']:
            gdb.write("Incorrect format {}\n".format(form))
            form = 'd'
        else:
            args = args[1:]
    return form, args

def num_format(value, form, bits = 32):
    """ Format a number based on a format character and bit-length
    """
    # get current gdb radix setting
    radix = int(re.search("\d+", gdb.execute("show output-radix", True, True)).group(0))

    # override it if asked to
    if form == 'x' or form == 'a': # For addresses, probably best in hex too
        radix = 16
    elif form == 'o':
        radix = 8
    elif form == 'b' or form == 't':
        radix = 2

    def fmt(p, l, f):
        return (p + "{:0" + str(int(math.ceil(bits/float(l)))) + f + "}").format(value)

    # format the output
    if radix == 16:
        return fmt('0x', 4, 'x')
    elif radix == 8:
        return fmt('0', 3, 'o')
    elif radix == 2:
        return fmt('0b', 1, 'b')
    else:
        # Decimal format
        return str(value)
