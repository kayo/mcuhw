import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), "lib"))

from svd_gdb import UseSVD
from dwt_gdb import DWT
from dbgmcu_gdb import DBGMCU

DWT()
UseSVD([os.path.join(os.path.dirname(__file__), "data")])
DBGMCU()
