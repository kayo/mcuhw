#include "mem.h"

int main(void) {
  uint16_t _ldr_size = _ldr_end - _ldr_start;
  uint16_t _cfg_size = _cfg_end - _cfg_start;
  uint16_t _ext_size = _ext_end - _ext_start;

  (void)_ldr_size;
  (void)_cfg_size;
  (void)_ext_size;

  for (;;);

  return 0;
}
