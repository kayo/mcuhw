#include "adc.h"

/*
 * use double-buffering conversion
 * to avoid simultaneous data access problems
 */

#define ADC_LENGTH (ADC_SAMPLES / 2)

static adc_channels_t sensor_data;

static void adc_convert(const adc_channels_t *data) {
  int sample;

  { /* We can get channels data by field name */
    uint32_t accum = 0;
    
    for (sample = 0; sample < ADC_LENGTH; sample++) {
      accum += data[sample].Vref;
    }
    
    sensor_data.Vref = accum / ADC_LENGTH;
  }

  { /* Also we can read channels data by index */
    uint32_t accum = 0;
    
    for (sample = 0; sample < ADC_LENGTH; sample++) {
      accum += data[sample]._[ADC_INDEX_TMCU];
    }
  
    sensor_data._[ADC_INDEX_TMCU] = accum / ADC_LENGTH;
  }

  { /* And of course we can iterate over all channels */
    int channel;
    
    /* Here we exclude two last channels which we converted above */
    for (channel = 0; channel < ADC_CHANNELS - 2; channel ++) {
      uint32_t accum = 0;
      
      for (sample = 0; sample < ADC_LENGTH; sample++) {
        accum += data[sample]._[channel];
      }
      
      sensor_data._[channel] = accum / ADC_LENGTH;
    }
  }
  
  /* do something with results of conversion */
}

void adc_dma_on_half(void) {
  adc_convert(&adc_dma_data[0]);
}

void adc_dma_on_full(void) {
  adc_convert(&adc_dma_data[ADC_LENGTH]);
}

int main(void) {
  mcu_modes_on_off(mcu_event_on_interrupt,
                   mcu_modes_notouch);

  irq_init();
  clk_init();
  pio_init();
  adc_init();

  mcu_modes_on_off(mcu_sleep_on_exit,
                   mcu_event_on_interrupt);

  for (;;) {
    mcu_wait_for_interrupt();
  }

  adc_done();
  pio_done();
  clk_done();
  irq_done();

  return 0;
}
