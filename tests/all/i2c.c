#include "i2c.h"

#define DS3231_I2C_ADDRESS 0x68
//#define DS3231_REG_TIMEVAL 0x00
#define DS3231_REG_TEMPVAL 0x11

enum {
  read_temp_addr,
  read_temp_data,
} state;

static void do_read_temp(void) {
  i2c_start();
  state = read_temp_addr;
}

volatile uint8_t temp = 0;

static void on_read_temp(uint8_t val) {
  temp = val;
}

void i2c_on_start(void) {
  switch (state) {
  case read_temp_addr:
    i2c_write(DS3231_I2C_ADDRESS);
    break;
  case read_temp_data:
    i2c_read(DS3231_I2C_ADDRESS);
    break;
  }
}

void i2c_on_addr(void) {
  switch (state) {
  case read_temp_addr:
    i2c_put(DS3231_REG_TEMPVAL);
    break;
  default:
    break;
  }
}

void i2c_rx_on_data(void) {
  switch (state) {
  case read_temp_data:
    on_read_temp(i2c_get());
    i2c_stop();
    break;
  default:
    break;
  }
}

void i2c_tx_on_done(void) {
  switch (state) {
  case read_temp_addr:
    i2c_start();
    state = read_temp_data;
  default:
    break;
  }
}

int main(void) {
  irq_init();
  clk_init();
  pio_init();
  i2c_init();

  /* Using interrupt driven flow control */
  //mcu_modes_on_off(mcu_sleep_on_exit,
  //                 mcu_modes_notouch);

  do_read_temp();

  for (;;) {
    mcu_wait_for_interrupt();
  }

  i2c_done();
  pio_done();
  clk_done();
  irq_done();

  return 0;
}
