#include "crc.h"

bool check_crc(const char *str, uint32_t crc, uint32_t req);

#define test_crc(str, crc) {                  \
    hw_crc32_reset();                         \
    volatile bool res =                       \
      check_crc(str,                          \
                hw_crc32_update(str,          \
                             sizeof(str)-1),  \
                crc);                         \
    (void)res;                                \
  }

bool check_crc(const char *str, uint32_t crc, uint32_t req) {
  (void)str;
  return crc == req;
}

int main(void) {
  clk_init();

  test_crc("123456789", 0xCBF43926);
  test_crc("12345678",  0x9AE0DAAF);
  test_crc("1234567",   0x5003699F);
  test_crc("123456",    0x0972D361);
  test_crc("12345",     0xCBF53A1C);
  test_crc("1234",      0x9BE3E0A3);
  test_crc("123",       0x884863D2);
  test_crc("12",        0x4F5344CD);
  test_crc("1",         0x83DCEFB7);

  for (; ; mcu_wait_for_event());

  clk_done();

  return 0;
}
