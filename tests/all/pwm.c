#include "pwm.h"

int main(void) {
  clk_init();
  pio_init();
  pwm_tim_init();

  pwm_tim_Umod_put(25 * PWM_TIM_TOP / 100);
  pwm_tim_Imod_put(50 * PWM_TIM_TOP / 100);

  for (;;) {}

  pwm_tim_done();
  pio_done();
  clk_done();

  return 0;
}
