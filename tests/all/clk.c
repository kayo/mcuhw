#include "clk.h"

void clk_on_tick(void) {
  // do something here
}

int main(void) {
  mcu_modes_on_off(mcu_event_on_interrupt,
                   mcu_modes_notouch);

  irq_init();
  clk_init();

  for (;;) {}

  clk_done();
  irq_done();

  return 0;
}
