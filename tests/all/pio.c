#include "pio.h"

void pio_P_BTN_on_edge(void) {
  // do something
}

int main(void) {
  clk_init();
  pio_init();

  bool btn_state = pio_P_BTN_get();
  bool rs485_dir = pio_RS485_DE_val();

  (void)btn_state;
  (void)rs485_dir;

  pio_ST_LED_set();
  pio_OL_LED_put(true);

  for (;;) {}

  pio_done();
  clk_done();

  return 0;
}
