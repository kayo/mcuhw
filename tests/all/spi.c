#include "spi.h"

/*
 * Serial Peripheral Interface loopback
 */

void spi_tx_dma_on_full(void) {
  /* stop transmitter */
  spi_tx_dma_off();
  
  int i;
  for (i = 0; i < spi_rx_dma_get_len(); i++) {
    if (spi_dma_data.rx[i] != spi_dma_data.tx[i]) {
      break;
    }
  }
}

int main(void) {
  irq_init();
  clk_init();
  pio_init();
  spi_init();

  /* Using interrupt driven flow control */
  //mcu_modes_on_off(mcu_sleep_on_exit,
  //                 mcu_modes_notouch);
  
  for (;;) {
    int i;
    for (i = 0; i < 256; i++) {
      spi_dma_data.tx[i] = i;
    }
    
    spi_tx_dma_set_len(256);
    spi_tx_dma_on();
    
    mcu_wait_for_interrupt();
  }

  spi_done();
  pio_done();
  clk_done();
  irq_done();

  return 0;
}
