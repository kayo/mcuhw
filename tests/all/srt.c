#include "srt.h"

/*
 * Serial rx/tx echo with rs485 phy support
 */

/* idle line handler */
void srt_rx_on_idle(void) {
  /* get length of received data */
  uint16_t rx_len = SRT_RX_SIZE - srt_rx_dma_get_len();

  if (rx_len > 0) {
    /* disable receiver */
    srt_rx_dma_off();

    /* set tx length */
    srt_tx_dma_set_len(rx_len);

#ifdef pio_rs485_re
    /* disable RS485 receiver */
    pio_rs485_re_off();
#endif

    /* enable RS485 driver */
    pio_rs485_de_on();

    /* start transmitter */
    srt_tx_dma_on();
  }
}

/* transmission complete handler */
void srt_tx_on_done(void) {
  /* disable transmission complete handler */
  srt_on_off(srt_no_touch, srt_tx_done);

  /* set the length of receiver buffer */
  srt_rx_dma_set_len(SRT_RX_SIZE);

  /* start receiver */
  srt_rx_dma_on();

  /* disable RS485 driver */
  pio_rs485_de_off();

#ifdef pio_rs485_re
  /* enable RS485 receiver */
  pio_rs485_re_on();
#endif
}

void srt_tx_dma_on_full(void) {
  /* stop transmitter */
  srt_tx_dma_off();

  /* enable transmission complete handler to determine when transmission actually ended */
  srt_on_off(srt_tx_done, srt_no_touch);
}

int main(void) {
  irq_init();
  clk_init();
  pio_init();
  srt_init();

  /* Using interrupt driven flow control */
  mcu_modes_on_off(mcu_sleep_on_exit,
                   mcu_modes_notouch);

  for (;;) {
    mcu_wait_for_interrupt();
  }

  srt_done();
  pio_done();
  clk_done();
  irq_done();

  return 0;
}
