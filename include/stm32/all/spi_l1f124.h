/** @addtogroup spi_defines

@author @htmlonly &copy; @endhtmlonly 2011 Fergus Noble <fergusnoble@gmail.com>

*/
/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2011 Fergus Noble <fergusnoble@gmail.com>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STM32_SPI_L1F124_H
#define STM32_SPI_L1F124_H
/**@{*/

/*
 * This file extends the common STM32 version with definitions only
 * applicable to the STM32L1/F1/2/4 series of devices.
 */

/* DFF: Data frame format */
/****************************************************************************/
/** @defgroup spi_dff SPI data frame format
@ingroup spi_defines

@{*/
#define SPI_CR1_DFF_8BIT        (0 << 11)
#define SPI_CR1_DFF_16BIT       (1 << 11)
/**@}*/
#define SPI_CR1_DFF             (1 << 11)

/**@}*/
#endif /* STM32_SPI_L1F124_H */
