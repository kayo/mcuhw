/** @addtogroup spi_defines
 */

/*
 * This file is part of the libopencm3 project.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STM32_SPI_F03_H
#define STM32_SPI_F03_H
/**@{*/

/*
 * This file extends the common stm32 version with defintions only
 * applicable to the STM32F0/F3 series of devices
 */

#define SPI_DR8(spi_base) MMIO8((spi_base) + 0x0c)

/* DFF: Data frame format */
/****************************************************************************/
/** @defgroup spi_dff SPI data frame format
 * @ingroup spi_defines
 *
 * @{*/

#define SPI_CR1_CRCL_8BIT   (0 << 11)
#define SPI_CR1_CRCL_16BIT  (1 << 11)
/**@}*/
#define SPI_CR1_CRCL        (1 << 11)

/* --- SPI_CR2 values ------------------------------------------------------ */

/* LDMA_TX: Last DMA transfer for transmission */
#define SPI_CR2_LDMA_TX     (1 << 14)

/* LDMA_RX: Last DMA transfer for reception */
#define SPI_CR2_LDMA_RX     (1 << 13)

/* FRXTH: FIFO reception threshold */
#define SPI_CR2_FRXTH       (1 << 12)

/* DS [3:0]: Data size */
#define SPI_CR2_DS_SHIFT    8
#define SPI_CR2_DS(x)       ((x) << SPI_CR2_DS_SHIFT)
#define SPI_CR2_DS_MASK     SPI_CR2_DS(0xF)

/* 0x0 - 0x2 NOT USED */
#define SPI_CR2_DS_4BIT     SPI_CR2_DS(0x3)
#define SPI_CR2_DS_5BIT     SPI_CR2_DS(0x4)
#define SPI_CR2_DS_6BIT     SPI_CR2_DS(0x5)
#define SPI_CR2_DS_7BIT     SPI_CR2_DS(0x6)
#define SPI_CR2_DS_8BIT     SPI_CR2_DS(0x7)
#define SPI_CR2_DS_9BIT     SPI_CR2_DS(0x8)
#define SPI_CR2_DS_10BIT    SPI_CR2_DS(0x9)
#define SPI_CR2_DS_11BIT    SPI_CR2_DS(0xA)
#define SPI_CR2_DS_12BIT    SPI_CR2_DS(0xB)
#define SPI_CR2_DS_13BIT    SPI_CR2_DS(0xC)
#define SPI_CR2_DS_14BIT    SPI_CR2_DS(0xD)
#define SPI_CR2_DS_15BIT    SPI_CR2_DS(0xE)
#define SPI_CR2_DS_16BIT    SPI_CR2_DS(0xF)

/* NSSP: NSS pulse management */
#define SPI_CR2_NSSP        (1 << 3)

/* --- SPI_SR values ------------------------------------------------------- */

/* FTLVL[1:0]: FIFO Transmission Level */
#define SPI_SR_FTLVL_FIFO_EMPTY   (0x0 << 11)
#define SPI_SR_FTLVL_QUARTER_FIFO (0x1 << 11)
#define SPI_SR_FTLVL_HALF_FIFO    (0x2 << 11)
#define SPI_SR_FTLVL_FIFO_FULL    (0x3 << 11)

/* FRLVL[1:0]: FIFO Reception Level */
#define SPI_SR_FRLVL_FIFO_EMPTY   (0x0 << 9)
#define SPI_SR_FRLVL_QUARTER_FIFO (0x1 << 9)
#define SPI_SR_FRLVL_HALF_FIFO    (0x2 << 9)
#define SPI_SR_FRLVL_FIFO_FULL    (0x3 << 9)

/**@}*/
#endif
