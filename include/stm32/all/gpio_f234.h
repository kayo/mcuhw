/** @addtogroup gpio_defines
 *
 * @author @htmlonly &copy; @endhtmlonly 2011
 * Fergus Noble <fergusnoble@gmail.com>
 * @author @htmlonly &copy; @endhtmlonly 2012
 * Ken Sarkies <ksarkies@internode.on.net>
 *
 */
/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2011 Fergus Noble <fergusnoble@gmail.com>
 * Copyright (C) 2012 Ken Sarkies <ksarkies@internode.on.net>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STM32_GPIO_F234_H
#define STM32_GPIO_F234_H
/**@{*/

/* GPIO port base addresses (for convenience) */
/** @defgroup gpio_port_id GPIO Port IDs
@ingroup gpio_defines

@{*/
#define GPIOA       GPIO_PORT_A_BASE
#define GPIOB       GPIO_PORT_B_BASE
#define GPIOC       GPIO_PORT_C_BASE
#define GPIOD       GPIO_PORT_D_BASE
#define GPIOE       GPIO_PORT_E_BASE
#define GPIOF       GPIO_PORT_F_BASE
/**@}*/

/* --- GPIO registers for STM32F2, STM32F3 and STM32F4 --------------------- */

/* Port mode register (GPIOx_MODER) */
#define GPIO_MODER(port)    MMIO32((port) + 0x00)

/* Port output type register (GPIOx_OTYPER) */
#define GPIO_OTYPER(port)   MMIO32((port) + 0x04)

/* Port output speed register (GPIOx_OSPEEDR) */
#define GPIO_OSPEEDR(port)  MMIO32((port) + 0x08)

/* Port pull-up/pull-down register (GPIOx_PUPDR) */
#define GPIO_PUPDR(port)    MMIO32((port) + 0x0c)

/* Port input data register (GPIOx_IDR) */
#define GPIO_IDR(port)      MMIO32((port) + 0x10)

/* Port output data register (GPIOx_ODR) */
#define GPIO_ODR(port)      MMIO32((port) + 0x14)

/* Port bit set/reset register (GPIOx_BSRR) */
#define GPIO_BSRR(port)     MMIO32((port) + 0x18)

/* Port configuration lock register (GPIOx_LCKR) */
#define GPIO_LCKR(port)     MMIO32((port) + 0x1c)

/* Alternate function low register (GPIOx_AFRL) */
#define GPIO_AFRL(port)     MMIO32((port) + 0x20)

/* Alternate function high register (GPIOx_AFRH) */
#define GPIO_AFRH(port)     MMIO32((port) + 0x24)

/* --- GPIOx_MODER values -------------------------------------------------- */

#define GPIO_MODE(n, mode)  ((mode) << (2 * (n)))
#define GPIO_MODE_MASK(n)   (0x3 << (2 * (n)))
/** @defgroup gpio_mode GPIO Pin Direction and Analog/Digital Mode
@ingroup gpio_defines
@{*/
#define GPIO_MODE_INPUT     0x0
#define GPIO_MODE_OUTPUT    0x1
#define GPIO_MODE_AF        0x2
#define GPIO_MODE_ANALOG    0x3
/**@}*/

/* --- GPIOx_OTYPER values ------------------------------------------------- */

#define GPIO_OTYPE(n, type) ((type) << (n))
#define GPIO_OTYPE_MASK(n)  (0x1 << (n))
/** @defgroup gpio_output_type GPIO Output Pin Driver Type
@ingroup gpio_defines
@list Push Pull
@list Open Drain
@{*/
#define GPIO_OTYPE_PP       0x0
#define GPIO_OTYPE_OD       0x1
/**@}*/

/* --- GPIOx_OSPEEDR values ------------------------------------------------ */

#define GPIO_OSPEED(n, speed) ((speed) << (2 * (n)))
#define GPIO_OSPEED_MASK(n)   (0x3 << (2 * (n)))
/** @defgroup gpio_speed GPIO Output Pin Speed
@ingroup gpio_defines
@{*/
#define GPIO_OSPEED_2MHZ    0x0
#define GPIO_OSPEED_25MHZ   0x1
#define GPIO_OSPEED_50MHZ   0x2
#define GPIO_OSPEED_100MHZ  0x3
/**@}*/

/* --- GPIOx_PUPDR values -------------------------------------------------- */

#define GPIO_PUPD(n, pupd)  ((pupd) << (2 * (n)))
#define GPIO_PUPD_MASK(n)   (0x3 << (2 * (n)))
/** @defgroup gpio_pup GPIO Output Pin Pullup
@ingroup gpio_defines
@{*/
#define GPIO_PUPD_NONE      0x0
#define GPIO_PUPD_PULLUP    0x1
#define GPIO_PUPD_PULLDOWN  0x2
/**@}*/

/* --- GPIOx_IDR values ---------------------------------------------------- */

/* GPIOx_IDR[15:0]: IDRy[15:0]: Port input data (y = 0..15) */

/* --- GPIOx_ODR values ---------------------------------------------------- */

/* GPIOx_ODR[15:0]: ODRy[15:0]: Port output data (y = 0..15) */

/* --- GPIOx_BSRR values --------------------------------------------------- */

/* GPIOx_BSRR[31:16]: BRy: Port x reset bit y (y = 0..15) */
/* GPIOx_BSRR[15:0]: BSy: Port x set bit y (y = 0..15) */
#define GPIO_BSRR_RESET(n) (1 << ((n) + 16))
#define GPIO_BSRR_SET(n) (1 << (n))

/* --- GPIOx_LCKR values --------------------------------------------------- */

#define GPIO_LCKK     (1 << 16)
/* GPIOx_LCKR[15:0]: LCKy: Port x lock bit y (y = 0..15) */

/* --- GPIOx_AFRL/H values ------------------------------------------------- */

/* Note: AFRL is used for bits 0..7, AFRH is used for 8..15 */
/* See datasheet table 6 (pg. 48) for alternate function mappings. */

#define GPIO_AFRL_AF(n, af)     ((af) << ((n) * 4))
#define GPIO_AFRL_AF_MASK(n)    GPIO_AFRL_AF(n, 0xf)

#define GPIO_AFRH_AF(n, af)     ((af) << (((n) - 8) * 4))
#define GPIO_AFRH_AF_MASK(n)    GPIO_AFRH_AF(n, 0xf)

/** @defgroup gpio_af_num Alternate Function Pin Selection
@ingroup gpio_defines
@{*/
#define GPIO_AF0      0x0
#define GPIO_AF1      0x1
#define GPIO_AF2      0x2
#define GPIO_AF3      0x3
#define GPIO_AF4      0x4
#define GPIO_AF5      0x5
#define GPIO_AF6      0x6
#define GPIO_AF7      0x7
#define GPIO_AF8      0x8
#define GPIO_AF9      0x9
#define GPIO_AF10     0xa
#define GPIO_AF11     0xb
#define GPIO_AF12     0xc
#define GPIO_AF13     0xd
#define GPIO_AF14     0xe
#define GPIO_AF15     0xf
/**@}*/

/* Note: EXTI source selection is now in the SYSCFG peripheral. */

/**@}*/
#endif /* STM32_GPIO_F234_H */
