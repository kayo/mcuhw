/** @addtogroup flash_defines
 *
 */
/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2010 Thomas Otto <tommi@viadmin.org>
 * Copyright (C) 2010 Mark Butler <mbutler@physics.otago.ac.nz>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * For details see:
 * PM0081 Programming manual: STM32F40xxx and STM32F41xxx Flash programming
 * September 2011, Doc ID 018520 Rev 1
 * https://github.com/libopencm3/libopencm3-archive/blob/master/st_micro/DM00023388.pdf
 */

#ifndef STM32_COMMON_FLASH_F01_H
#define STM32_COMMON_FLASH_F01_H
/**@{*/

/* --- Common FLASH registers ------------------------------------------------ */

#define FLASH_ACR     MMIO32(FLASH_MEM_INTERFACE_BASE + 0x00)
#define FLASH_OPTKEYR MMIO32(FLASH_MEM_INTERFACE_BASE + 0x08)

#define FLASH_OBR     MMIO32(FLASH_MEM_INTERFACE_BASE + 0x1C)
#define FLASH_WRPR    MMIO32(FLASH_MEM_INTERFACE_BASE + 0x20)

#define FLASH_BANK1 0

/* Only present in STM32F10x XL series */
#define FLASH_BANK2 1

#define FLASH_KEYR(BANK)    MMIO32(FLASH_MEM_INTERFACE_BASE + 0x04 + 0x40 * BANK)
#define FLASH_SR(BANK)      MMIO32(FLASH_MEM_INTERFACE_BASE + 0x0C + 0x40 * BANK)
#define FLASH_CR(BANK)      MMIO32(FLASH_MEM_INTERFACE_BASE + 0x10 + 0x40 * BANK)
#define FLASH_AR(BANK)      MMIO32(FLASH_MEM_INTERFACE_BASE + 0x14 + 0x40 * BANK)

#define FLASH_BANK(ADDR) (((uint32_t)(ADDR)) < FLASH_BASE + 0x00080000 ? FLASH_BANK1 : FLASH_BANK2)

/* --- FLASH_OPTION bytes ------------------------------------------------- */

#define FLASH_OPTION_BYTE(i) MMIO16(INFO_BASE + 0x0800 + ((i) * 2))

/* --- FLASH_ACR values ---------------------------------------------------- */

#define FLASH_ACR_LATENCY_SHIFT 0
#define FLASH_ACR_LATENCY       7

#define FLASH_ACR_PRFTBS  (1 << 5)
#define FLASH_ACR_PRFTBE  (1 << 4)

/* --- FLASH_SR values ----------------------------------------------------- */

#define FLASH_SR_EOP      (1 << 5)
#define FLASH_SR_WRPRTERR (1 << 4)
#define FLASH_SR_PGERR    (1 << 2)
#define FLASH_SR_BSY      (1 << 0)

/* --- FLASH_CR values ----------------------------------------------------- */

#define FLASH_CR_EOPIE    (1 << 12)
#define FLASH_CR_ERRIE    (1 << 10)
#define FLASH_CR_OPTWRE   (1 << 9)
#define FLASH_CR_LOCK     (1 << 7)
#define FLASH_CR_STRT     (1 << 6)
#define FLASH_CR_OPTER    (1 << 5)
#define FLASH_CR_OPTPG    (1 << 4)
#define FLASH_CR_MER      (1 << 2)
#define FLASH_CR_PER      (1 << 1)
#define FLASH_CR_PG       (1 << 0)

/* --- FLASH_OBR values ---------------------------------------------------- */

#define FLASH_OBR_RDPRT_SHIFT   1
#define FLASH_OBR_OPTERR        (1 << 0)

/* --- FLASH Keys -----------------------------------------------------------*/

#define FLASH_KEYR_KEY1     ((uint32_t)0x45670123)
#define FLASH_KEYR_KEY2     ((uint32_t)0xcdef89ab)

/**@}*/
#endif /* STM32F0_COMMON_FLASH_F01_H */
