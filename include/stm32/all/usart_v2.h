/** @addtogroup usart_defines

 @author @htmlonly &copy; @endhtmlonly 2016 Cem Basoglu <cem.basoglu@web.de>

 */

/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2016 Cem Basoglu <cem.basoglu@web.de>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STM32_USART_V2_H
#define STM32_USART_V2_H

/*****************************************************************************/
/* Register values                                                           */
/*****************************************************************************/

/* --- USART_RTOR values --------------------------------------------------- */

/* BLEN[7:0]: Block Length */
#define USART_RTOR_BLEN_SHIFT     24
#define USART_RTOR_BLEN_MASK      (0xFF << USART_RTOR_BLEN_SHIFT)
#define USART_RTOR_BLEN_VAL(x)    ((x) << USART_RTOR_BLEN_SHIFT)

/* RTO[23:0]: Receiver timeout value */
#define USART_RTOR_RTO_SHIFT      0
#define USART_RTOR_RTO_MASK       (0xFFFFF << USART_RTOR_RTO_SHIFT)
#define USART_RTOR_RTO_VAL(x)     ((x) << USART_RTOR_RTO_SHIFT)

#endif /* STM32_USART_V2_H */
