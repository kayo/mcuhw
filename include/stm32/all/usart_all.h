/** @addtogroup usart_defines
 *
 * @author @htmlonly &copy; @endhtmlonly 2009 Uwe Hermann <uwe@hermann-uwe.de>
 *
 */
/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2009 Uwe Hermann <uwe@hermann-uwe.de>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STM32_USART_ALL_H
#define STM32_USART_ALL_H
/**@{*/

/* --- Convenience macros -------------------------------------------------- */

/****************************************************************************/
/** @defgroup usart_reg_base USART register base addresses
@ingroup STM32F_usart_defines

@{*/
#define USART1        USART1_BASE
#define USART2        USART2_BASE
#define USART3        USART3_BASE
/**@}*/
#define UART4         UART4_BASE
#define UART5         UART5_BASE

/**@}*/
#endif /* STM32_USART_ALL_H */
