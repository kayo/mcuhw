/** @addtogroup crc_defines

@author @htmlonly &copy; @endhtmlonly 2010 Thomas Otto <tommi@viadmin.org>

*/

/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2010 Thomas Otto <tommi@viadmin.org>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

/* THIS FILE SHOULD NOT BE INCLUDED DIRECTLY, BUT ONLY VIA CRC.H
The order of header inclusion is important. crc.h includes the device
specific memorymap.h header before including this header file.*/

#ifndef STM32_CRC_ALL_H
#define STM32_CRC_ALL_H
/**@{*/

/*****************************************************************************/
/* Register definitions                                                      */
/*****************************************************************************/

/* Data register (CRC_DR) */
#define CRC_DR				MMIO32(CRC_BASE + 0x00)

/* Independent data register (CRC_IDR) */
#define CRC_IDR				MMIO32(CRC_BASE + 0x04)

/* Control register (CRC_CR) */
#define CRC_CR				MMIO32(CRC_BASE + 0x08)

/*****************************************************************************/
/* Register values                                                           */
/*****************************************************************************/

/* --- CRC_DR values ------------------------------------------------------- */

/* Bits [31:0]: Data register */

/* --- CRC_IDR values ------------------------------------------------------ */

/* Bits [31:8]: Reserved */

/* Bits [7:0]: General-purpose 8-bit data register bits */

/* --- CRC_CR values ------------------------------------------------------- */

/* Bits [31:1]: Reserved */

/* RESET bit */
#define CRC_CR_RESET			(1 << 0)

/*****************************************************************************/
/* API definitions                                                           */
/*****************************************************************************/

/*****************************************************************************/
/* API Functions                                                             */
/*****************************************************************************/

/**@}*/
#endif /* STM32_CRC_ALL_H */
