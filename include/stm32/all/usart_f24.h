/** @addtogroup usart_defines

@author @htmlonly &copy; @endhtmlonly 2011 Uwe Hermann <uwe@hermann-uwe.de>
@author @htmlonly &copy; @endhtmlonly 2011 Stephen Caudle <scaudle@doceme.com>

*/
/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2011 Fergus Noble <fergusnoble@gmail.com>
 * Copyright (C) 2011 Stephen Caudle <scaudle@doceme.com>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STM32_USART_F24_H
#define STM32_USART_F24_H

/* --- Convenience macros -------------------------------------------------- */

#define USART6        USART6_BASE
#define UART7         UART7_BASE
#define UART8         UART8_BASE

/* --- USART_CR1 values ---------------------------------------------------- */

/* OVER8: Oversampling mode */
#define USART_CR1_OVER8     (1 << 15)

/* --- USART_CR3 values ---------------------------------------------------- */

/* ONEBIT: One sample bit method enable */
#define USART_CR3_ONEBIT    (1 << 11)

#endif /* STM32_USART_F24_H */
