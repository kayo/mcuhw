/** @addtogroup timer_defines
 *
 * @author @htmlonly &copy; @endhtmlonly 2009 Piotr Esden-Tempski <piotr@esden.net>
 *
 */
/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2009 Piotr Esden-Tempski <piotr@esden.net>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STM32_TIM_H
#define STM32_TIM_H
/**@{*/

/* THIS FILE SHOULD NOT BE INCLUDED DIRECTLY, BUT ONLY VIA TIMER.H
The order of header inclusion is important. timer.h includes the device
specific memorymap.h header before including this header file.*/

/* --- Convenience macros -------------------------------------------------- */

/* Timer register base addresses (for convenience) */
/****************************************************************************/
/** @defgroup tim_reg_base Timer register base addresses
@ingroup timer_defines

@{*/
#define TIM1        TIM1_BASE
#define TIM2        TIM2_BASE
#define TIM3        TIM3_BASE
#if defined(TIM4_BASE)
#define TIM4        TIM4_BASE
#endif
#define TIM5        TIM5_BASE
#define TIM6        TIM6_BASE
#define TIM7        TIM7_BASE
#if defined(TIM8_BASE)
# define TIM8       TIM8_BASE
#endif
#if defined(TIM9_BASE)
# define TIM9       TIM9_BASE
#endif
#if defined(TIM10_BASE)
# define TIM10      TIM10_BASE
#endif
#if defined(TIM11_BASE)
# define TIM11      TIM11_BASE
#endif
#if defined(TIM12_BASE)
# define TIM12      TIM12_BASE
#endif
#if defined(TIM13_BASE)
# define TIM13      TIM13_BASE
#endif
#if defined(TIM14_BASE)
# define TIM14      TIM14_BASE
#endif
#if defined(TIM15_BASE)
# define TIM15      TIM15_BASE
#endif
#if defined(TIM16_BASE)
# define TIM16      TIM16_BASE
#endif
#if defined(TIM17_BASE)
# define TIM17      TIM17_BASE
#endif
/**@}*/

/* --- Timer registers ----------------------------------------------------- */

/* Control register 1 (TIMx_CR1) */
#define TIM_CR1(tim_base)     MMIO32((tim_base) + 0x00)

/* Control register 2 (TIMx_CR2) */
#define TIM_CR2(tim_base)     MMIO32((tim_base) + 0x04)

/* Slave mode control register (TIMx_SMCR) */
#define TIM_SMCR(tim_base)    MMIO32((tim_base) + 0x08)

/* DMA/Interrupt enable register (TIMx_DIER) */
#define TIM_DIER(tim_base)    MMIO32((tim_base) + 0x0C)

/* Status register (TIMx_SR) */
#define TIM_SR(tim_base)      MMIO32((tim_base) + 0x10)

/* Event generation register (TIMx_EGR) */
#define TIM_EGR(tim_base)     MMIO32((tim_base) + 0x14)

/* Capture/compare mode register 1 (TIMx_CCMR1) */
#define TIM_CCMR1(tim_base)   MMIO32((tim_base) + 0x18)

/* Capture/compare mode register 2 (TIMx_CCMR2) */
#define TIM_CCMR2(tim_base)   MMIO32((tim_base) + 0x1C)

/* Capture/compare enable register (TIMx_CCER) */
#define TIM_CCER(tim_base)    MMIO32((tim_base) + 0x20)

/* Counter (TIMx_CNT) */
#define TIM_CNT(tim_base)     MMIO32((tim_base) + 0x24)

/* Prescaler (TIMx_PSC) */
#define TIM_PSC(tim_base)     MMIO32((tim_base) + 0x28)

/* Auto-reload register (TIMx_ARR) */
#define TIM_ARR(tim_base)     MMIO32((tim_base) + 0x2C)

/* Repetition counter register (TIMx_RCR) */
#define TIM_RCR(tim_base)     MMIO32((tim_base) + 0x30)

/* Capture/compare register 1 (TIMx_CCR1) */
#define TIM_CCR1(tim_base)    MMIO32((tim_base) + 0x34)

/* Capture/compare register 2 (TIMx_CCR2) */
#define TIM_CCR2(tim_base)    MMIO32((tim_base) + 0x38)

/* Capture/compare register 3 (TIMx_CCR3) */
#define TIM_CCR3(tim_base)    MMIO32((tim_base) + 0x3C)

/* Capture/compare register 4 (TIMx_CCR4) */
#define TIM_CCR4(tim_base)    MMIO32((tim_base) + 0x40)

/* Break and dead-time register (TIMx_BDTR) */
#define TIM_BDTR(tim_base)    MMIO32((tim_base) + 0x44)

/* DMA control register (TIMx_DCR) */
#define TIM_DCR(tim_base)     MMIO32((tim_base) + 0x48)

/* DMA address for full transfer (TIMx_DMAR) */
#define TIM_DMAR(tim_base)    MMIO32((tim_base) + 0x4C)

/* --- TIMx_CR1 values ----------------------------------------------------- */

/****************************************************************************/
/** @defgroup tim_x_cr1_cdr TIMx_CR1 CKD[1:0] Clock Division Ratio
@ingroup timer_defines

@{*/
/* CKD[1:0]: Clock division */
#define TIM_CR1_CKD_SHIFT         8
#define TIM_CR1_CKD(x)            ((x) << TIM_CR1_CKD_SHIFT)
#define TIM_CR1_CKD_CK_INT        TIM_CR1_CKD(0x0)
#define TIM_CR1_CKD_CK_INT_MUL_1  TIM_CR1_CKD(0x0)
#define TIM_CR1_CKD_CK_INT_MUL_2  TIM_CR1_CKD(0x1)
#define TIM_CR1_CKD_CK_INT_MUL_4  TIM_CR1_CKD(0x2)
#define TIM_CR1_CKD_MASK          TIM_CR1_CKD(0x3)
/**@}*/

/* ARPE: Auto-reload preload enable */
#define TIM_CR1_ARPE      (1 << 7)

/* CMS[1:0]: Center-aligned mode selection */
/****************************************************************************/
/** @defgroup tim_x_cr1_cms TIMx_CR1 CMS[1:0]: Center-aligned Mode Selection
@ingroup timer_defines

@{*/
#define TIM_CR1_CMS_SHIFT       5
#define TIM_CR1_CMS(x)          ((x) << TIM_CR1_CMS_SHIFT)
#define TIM_CR1_CMS_EDGE        TIM_CR1_CMS(0x0)
#define TIM_CR1_CMS_CENTER_1    TIM_CR1_CMS(0x1)
#define TIM_CR1_CMS_CENTER_2    TIM_CR1_CMS(0x2)
#define TIM_CR1_CMS_CENTER_3    TIM_CR1_CMS(0x3)
#define TIM_CR1_CMS_CENTER_COMPARE_DOWN TIM_CR1_CMS(0x1)
#define TIM_CR1_CMS_CENTER_COMPARE_UP   TIM_CR1_CMS(0x2)
#define TIM_CR1_CMS_MASK        TIM_CR1_CMS(0x3)
/**@}*/

/* DIR: Direction */
/****************************************************************************/
/** @defgroup tim_x_cr1_dir TIMx_CR1 DIR: Direction
@ingroup timer_defines

@{*/
#define TIM_CR1_DIR_SHIFT   4
#define TIM_CR1_DIR(x)      ((x) << TIM_CR1_DIR_SHIFT)
#define TIM_CR1_DIR_UP      TIM_CR1_DIR(0)
#define TIM_CR1_DIR_DOWN    TIM_CR1_DIR(1)
/**@}*/

/* OPM: One pulse mode */
#define TIM_CR1_OPM     (1 << 3)

/* URS: Update request source */
#define TIM_CR1_URS     (1 << 2)

/* UDIS: Update disable */
#define TIM_CR1_UDIS    (1 << 1)

/* CEN: Counter enable */
#define TIM_CR1_CEN     (1 << 0)

/* --- TIMx_CR2 values ----------------------------------------------------- */

/****************************************************************************/
/** @defgroup tim_x_cr2_ois TIMx_CR2_OIS: Force Output Idle State Control Values
@ingroup timer_defines

@{*/
/* OIS4:*//** Output idle state 4 (OC4 output) */
#define TIM_CR2_OIS4      (1 << 14)

/* OIS3N:*//** Output idle state 3 (OC3N output) */
#define TIM_CR2_OIS3N     (1 << 13)

/* OIS3:*//** Output idle state 3 (OC3 output) */
#define TIM_CR2_OIS3      (1 << 12)

/* OIS2N:*//** Output idle state 2 (OC2N output) */
#define TIM_CR2_OIS2N     (1 << 11)

/* OIS2:*//** Output idle state 2 (OC2 output) */
#define TIM_CR2_OIS2      (1 << 10)

/* OIS1N:*//** Output idle state 1 (OC1N output) */
#define TIM_CR2_OIS1N     (1 << 9)

/* OIS1:*//** Output idle state 1 (OC1 output) */
#define TIM_CR2_OIS1      (1 << 8)
#define TIM_CR2_OIS_MASK  (0x7f << 8)
/**@}*/

/* TI1S: TI1 selection */
#define TIM_CR2_TI1S      (1 << 7)

/* MMS[2:0]: Master mode selection */
/****************************************************************************/
/** @defgroup tim_mastermode TIMx_CR2 MMS[6:4]: Master Mode Selection
@ingroup timer_defines

@{*/
#define TIM_CR2_MMS_SHIFT           4
#define TIM_CR2_MMS(x)              ((x) << TIM_CR2_MMS_SHIFT)
#define TIM_CR2_MMS_RESET           TIM_CR2_MMS(0x0)
#define TIM_CR2_MMS_ENABLE          TIM_CR2_MMS(0x1)
#define TIM_CR2_MMS_UPDATE          TIM_CR2_MMS(0x2)
#define TIM_CR2_MMS_COMPARE_PULSE   TIM_CR2_MMS(0x3)
#define TIM_CR2_MMS_COMPARE_OC1REF  TIM_CR2_MMS(0x4)
#define TIM_CR2_MMS_COMPARE_OC2REF  TIM_CR2_MMS(0x5)
#define TIM_CR2_MMS_COMPARE_OC3REF  TIM_CR2_MMS(0x6)
#define TIM_CR2_MMS_COMPARE_OC4REF  TIM_CR2_MMS(0x7)
#define TIM_CR2_MMS_MASK            TIM_CR2_MMS(0x7)
/**@}*/

/* CCDS: Capture/compare DMA selection */
#define TIM_CR2_CCDS      (1 << 3)

/* CCUS: Capture/compare control update selection */
#define TIM_CR2_CCUS      (1 << 2)

/* CCPC: Capture/compare preload control */
#define TIM_CR2_CCPC      (1 << 0)

/* --- TIMx_SMCR values ---------------------------------------------------- */

/* ETP: External trigger polarity */
#define TIM_SMCR_ETP      (1 << 15)

/* ECE: External clock enable */
#define TIM_SMCR_ECE      (1 << 14)

/* Generic trigger prescaler */
#define TIM_PS_OFF    0x0
#define TIM_PS_DIV1   0x0
#define TIM_PS_DIV2   0x1
#define TIM_PS_DIV4   0x2
#define TIM_PS_DIV8   0x3
#define TIM_PS_MASK   0x3

/* ETPS[1:0]: External trigger prescaler */
#define TIM_SMCR_ETPS_SHIFT       12
#define TIM_SMCR_ETPS(TIM_PS)     ((TIM_PS) << TIM_SMCR_ETPS_SHIFT)
#define TIM_SMCR_ETPS_MASK        TIM_SMCR_ETPS(TIM_PS_MASK)

/* Generic input filter values */
#define TIM_IF_OFF        0x0
#define TIM_IF_DTS1       0x0
#define TIM_IF_DTS1_DIV1  0x0
#define TIM_IF_CK_INT2    0x1
#define TIM_IF_CK_INT4    0x2
#define TIM_IF_CK_INT8    0x3
#define TIM_IF_DTS6_DIV2  0x4
#define TIM_IF_DTS3_DIV1  0x4
#define TIM_IF_DTS3       0x4
#define TIM_IF_DTS8_DIV2  0x5
#define TIM_IF_DTS4_DIV1  0x5
#define TIM_IF_DTS4       0x5
#define TIM_IF_DTS6_DIV4  0x6
#define TIM_IF_DTS3_DIV2  0x6
#define TIM_IF_DTS8_DIV4  0x7
#define TIM_IF_DTS2_DIV1  0x7
#define TIM_IF_DTS2       0x7
#define TIM_IF_DTS6_DIV8  0x8
#define TIM_IF_DTS3_DIV4  0x8
#define TIM_IF_DTS8_DIV8  0x9
#define TIM_IF_DTS5_DIV16 0xa
#define TIM_IF_DTS6_DIV16 0xb
#define TIM_IF_DTS3_DIV8  0xb
#define TIM_IF_DTS8_DIV16 0xc
#define TIM_IF_DTS4_DIV8  0xc
#define TIM_IF_DTS1_DIV2  0xc
#define TIM_IF_DTS5_DIV32 0xd
#define TIM_IF_DTS6_DIV32 0xe
#define TIM_IF_DTS3_DIV16 0xe
#define TIM_IF_DTS8_DIV32 0xf
#define TIM_IF_DTS4_DIV16 0xf
#define TIM_IF_DTS2_DIV8  0xf
#define TIM_IF_DTS1_DIV4  0xf
#define TIM_IF_MASK       0xf

/* ETF[3:0]: External trigger filter */
#define TIM_SMCR_ETF_SHIFT       8
#define TIM_SMCR_ETF(TIM_IF)     ((TIM_IF) << TIM_SMCR_ETF_SHIFT)
#define TIM_SMCR_ETF_MASK        TIM_SMCR_ETF(TIM_IF_MASK)

/* MSM: Master/slave mode */
#define TIM_SMCR_MSM        (1 << 7)

/* TS[2:0]: Trigger selection */
/** @defgroup tim_ts TS Trigger selection
@ingroup timer_defines

@{*/
#define TIM_SMCR_TS_SHIFT   4
#define TIM_SMCR_TS(x)      ((x) << TIM_SMCR_TS_SHIFT)
/** Internal Trigger 0 (ITR0) */
#define TIM_SMCR_TS_ITR0    TIM_SMCR_TS(0x0)
/** Internal Trigger 1 (ITR1) */
#define TIM_SMCR_TS_ITR1    TIM_SMCR_TS(0x1)
/** Internal Trigger 2 (ITR2) */
#define TIM_SMCR_TS_ITR2    TIM_SMCR_TS(0x2)
/** Internal Trigger 3 (ITR3) */
#define TIM_SMCR_TS_ITR3    TIM_SMCR_TS(0x3)
/** TI1 Edge Detector (TI1F_ED) */
#define TIM_SMCR_TS_TI1F_ED TIM_SMCR_TS(0x4)
/** Filtered Timer Input 1 (TI1FP1) */
#define TIM_SMCR_TS_TI1FP1  TIM_SMCR_TS(0x5)
/** Filtered Timer Input 2 (TI2FP2) */
#define TIM_SMCR_TS_TI2FP2  TIM_SMCR_TS(0x6)
/** External Trigger input (ETRF) */
#define TIM_SMCR_TS_ETRF    TIM_SMCR_TS(0x7)
#define TIM_SMCR_TS_MASK    TIM_SMCR_TS(0x7)
/**@}*/

/* SMS[2:0]: Slave mode selection */
/** @defgroup tim_sms SMS Slave mode selection
@ingroup timer_defines

@{*/
#define TIM_SMCR_SMS_SHIFT  0
#define TIM_SMCR_SMS(x)     ((x) << TIM_SMCR_SMS_SHIFT)
/** Slave mode disabled */
#define TIM_SMCR_SMS_OFF    TIM_SMCR_SMS(0x0)
/** Encoder mode 1 - Counter counts up/down on TI2FP2 edge depending on TI1FP1
level. */
#define TIM_SMCR_SMS_EM1    TIM_SMCR_SMS(0x1)
#define TIM_SMCR_SMS_ENCODER1 TIM_SMCR_SMS_EM1
/** Encoder mode 2 - Counter counts up/down on TI1FP1 edge depending on TI2FP2
level. */
#define TIM_SMCR_SMS_EM2    TIM_SMCR_SMS(0x2)
#define TIM_SMCR_SMS_ENCODER2 TIM_SMCR_SMS_EM2
/** Encoder mode 3 - Counter counts up/down on both TI1FP1 and TI2FP2 edges
depending on the level of the complementary input. */
#define TIM_SMCR_SMS_EM3    TIM_SMCR_SMS(0x3)
#define TIM_SMCR_SMS_ENCODER3 TIM_SMCR_SMS_EM3
/** Reset Mode - Rising edge of the selected trigger input (TRGI) reinitializes
 * the counter and generates an update of the registers.
 */
#define TIM_SMCR_SMS_RM     TIM_SMCR_SMS(0x4)
#define TIM_SMCR_SMS_RESET  TIM_SMCR_SMS_RM
/** Gated Mode - The counter clock is enabled when the trigger input (TRGI) is
 * high.
 */
#define TIM_SMCR_SMS_GM     TIM_SMCR_SMS(0x5)
#define TIM_SMCR_SMS_GATED  TIM_SMCR_SMS_GM
/**  Trigger Mode - The counter starts at a rising edge of the trigger TRGI. */
#define TIM_SMCR_SMS_TM     TIM_SMCR_SMS(0x6)
#define TIM_SMCR_SMS_TRIGGER TIM_SMCR_SMS_TM
/** External Clock Mode 1 - Rising edges of the selected trigger (TRGI) clock
 * the counter.
 */
#define TIM_SMCR_SMS_ECM1   TIM_SMCR_SMS(0x7)
#define TIM_SMCR_SMS_EXTERNAL_CLOCK TIM_SMCR_SMS_ECM1
#define TIM_SMCR_SMS_MASK   TIM_SMCR_SMS(0x7)
/**@}*/

/* --- TIMx_DIER values ---------------------------------------------------- */

/****************************************************************************/
/** @defgroup tim_irq_enable TIMx_DIER Timer DMA and Interrupt Enable Values
@ingroup timer_defines

@{*/
/* TDE:*//** Trigger DMA request enable */
#define TIM_DIER_TDE        (1 << 14)

/* COMDE:*//** COM DMA request enable */
#define TIM_DIER_COMDE      (1 << 13)

/* CC4DE:*//** Capture/Compare 4 DMA request enable */
#define TIM_DIER_CC4DE      (1 << 12)

/* CC3DE:*//** Capture/Compare 3 DMA request enable */
#define TIM_DIER_CC3DE      (1 << 11)

/* CC2DE:*//** Capture/Compare 2 DMA request enable */
#define TIM_DIER_CC2DE      (1 << 10)

/* CC1DE:*//** Capture/Compare 1 DMA request enable */
#define TIM_DIER_CC1DE      (1 << 9)

/* UDE*//**: Update DMA request enable */
#define TIM_DIER_UDE        (1 << 8)

/* BIE:*//** Break interrupt enable */
#define TIM_DIER_BIE        (1 << 7)

/* TIE:*//** Trigger interrupt enable */
#define TIM_DIER_TIE        (1 << 6)

/* COMIE:*//** COM interrupt enable */
#define TIM_DIER_COMIE      (1 << 5)

/* CC4IE:*//** Capture/compare 4 interrupt enable */
#define TIM_DIER_CC4IE      (1 << 4)

/* CC3IE:*//** Capture/compare 3 interrupt enable */
#define TIM_DIER_CC3IE      (1 << 3)

/* CC2IE:*//** Capture/compare 2 interrupt enable */
#define TIM_DIER_CC2IE      (1 << 2)

/* CC1IE:*//** Capture/compare 1 interrupt enable */
#define TIM_DIER_CC1IE      (1 << 1)

/* UIE:*//** Update interrupt enable */
#define TIM_DIER_UIE        (1 << 0)
/**@}*/

/* --- TIMx_SR values ------------------------------------------------------ */
/****************************************************************************/
/** @defgroup tim_sr_values TIMx_SR Timer Status Register Flags
@ingroup timer_defines

@{*/

/* CC4OF:*//** Capture/compare 4 overcapture flag */
#define TIM_SR_CC4OF      (1 << 12)

/* CC3OF:*//** Capture/compare 3 overcapture flag */
#define TIM_SR_CC3OF      (1 << 11)

/* CC2OF:*//** Capture/compare 2 overcapture flag */
#define TIM_SR_CC2OF      (1 << 10)

/* CC1OF:*//** Capture/compare 1 overcapture flag */
#define TIM_SR_CC1OF      (1 << 9)

/* BIF:*//** Break interrupt flag */
#define TIM_SR_BIF        (1 << 7)

/* TIF:*//** Trigger interrupt flag */
#define TIM_SR_TIF        (1 << 6)

/* COMIF:*//** COM interrupt flag */
#define TIM_SR_COMIF      (1 << 5)

/* CC4IF:*//** Capture/compare 4 interrupt flag */
#define TIM_SR_CC4IF      (1 << 4)

/* CC3IF:*//** Capture/compare 3 interrupt flag */
#define TIM_SR_CC3IF      (1 << 3)

/* CC2IF:*//** Capture/compare 2 interrupt flag */
#define TIM_SR_CC2IF      (1 << 2)

/* CC1IF:*//** Capture/compare 1 interrupt flag */
#define TIM_SR_CC1IF      (1 << 1)

/* UIF:*//** Update interrupt flag */
#define TIM_SR_UIF        (1 << 0)
/**@}*/

/* --- TIMx_EGR values ----------------------------------------------------- */

/****************************************************************************/
/** @defgroup tim_event_gen TIMx_EGR Timer Event Generator Values
@ingroup timer_defines

@{*/

/* BG:*//** Break generation */
#define TIM_EGR_BG        (1 << 7)

/* TG:*//** Trigger generation */
#define TIM_EGR_TG        (1 << 6)

/* COMG:*//** Capture/compare control update generation */
#define TIM_EGR_COMG      (1 << 5)

/* CC4G:*//** Capture/compare 4 generation */
#define TIM_EGR_CC4G      (1 << 4)

/* CC3G:*//** Capture/compare 3 generation */
#define TIM_EGR_CC3G      (1 << 3)

/* CC2G:*//** Capture/compare 2 generation */
#define TIM_EGR_CC2G      (1 << 2)

/* CC1G:*//** Capture/compare 1 generation */
#define TIM_EGR_CC1G      (1 << 1)

/* UG:*//** Update generation */
#define TIM_EGR_UG        (1 << 0)
/**@}*/

/* --- TIMx_CCMR1 values --------------------------------------------------- */

/* --- Output compare mode --- */

/* OC2CE: Output compare 2 clear enable */
#define TIM_CCMR1_OC2CE           (1 << 15)

/* Generic output compare mode values */
#define TIM_OCM_FROZEN          0x0
#define TIM_OCM_ACTIVE          0x1
#define TIM_OCM_INACTIVE        0x2
#define TIM_OCM_TOGGLE          0x3
#define TIM_OCM_FORCE_LOW       0x4
#define TIM_OCM_FORCE_INACTIVE  0x4
#define TIM_OCM_FORCE_HIGH      0x5
#define TIM_OCM_FORCE_ACTIVE    0x5
#define TIM_OCM_PWM1            0x6
#define TIM_OCM_UNDER_ACTIVE    0x6
#define TIM_OCM_OVER_INACTIVE   0x6
#define TIM_OCM_PWM2            0x7
#define TIM_OCM_UNDER_INACTIVE  0x7
#define TIM_OCM_OVER_ACTIVE     0x7
#define TIM_OCM_MASK            0x7

/* OC2M[2:0]: Output compare 2 mode */
#define TIM_CCMR1_OC2M_SHIFT      12
#define TIM_CCMR1_OC2M(TIM_OCM)   ((TIM_OCM) << TIM_CCMR1_OC2M_SHIFT)
#define TIM_CCMR1_OC2M_MASK       TIM_CCMR1_OC2M(TIM_OCM_MASK)

/* OC2PE: Output compare 2 preload enable */
#define TIM_CCMR1_OC2PE           (1 << 11)

/* OC2FE: Output compare 2 fast enable */
#define TIM_CCMR1_OC2FE           (1 << 10)

/* CC2S[1:0]: Capture/compare 2 selection */
/* Note: CC2S bits are writable only when the channel is OFF (CC2E = 0 in
 * TIMx_CCER). */
#define TIM_CCMR1_CC2S_SHIFT      8
#define TIM_CCMR1_CC2S(x)         ((x) << TIM_CCMR1_CC2S_SHIFT)
#define TIM_CCMR1_CC2S_OUT        TIM_CCMR1_CC2S(0x0)
#define TIM_CCMR1_CC2S_IN_TI2     TIM_CCMR1_CC2S(0x1)
#define TIM_CCMR1_CC2S_IN_TI1     TIM_CCMR1_CC2S(0x2)
#define TIM_CCMR1_CC2S_IN_TRC     TIM_CCMR1_CC2S(0x3)
#define TIM_CCMR1_CC2S_MASK       TIM_CCMR1_CC2S(0x3)

/* OC1CE: Output compare 1 clear enable */
#define TIM_CCMR1_OC1CE           (1 << 7)

/* OC1M[2:0]: Output compare 1 mode */
#define TIM_CCMR1_OC1M_SHIFT      4
#define TIM_CCMR1_OC1M(TIM_OCM)   ((TIM_OCM) << TIM_CCMR1_OC1M_SHIFT)
#define TIM_CCMR1_OC1M_MASK       TIM_CCMR1_OC1M(TIM_OCM_MASK)

/* OC1PE: Output compare 1 preload enable */
#define TIM_CCMR1_OC1PE           (1 << 3)

/* OC1FE: Output compare 1 fast enable */
#define TIM_CCMR1_OC1FE           (1 << 2)

/* CC1S[1:0]: Capture/compare 1 selection */
/* Note: CC2S bits are writable only when the channel is OFF (CC2E = 0 in
 * TIMx_CCER). */
#define TIM_CCMR1_CC1S_SHIFT      0
#define TIM_CCMR1_CC1S(x)         ((x) << TIM_CCMR1_CC1S_SHIFT)
#define TIM_CCMR1_CC1S_OUT        TIM_CCMR1_CC1S(0x0)
#define TIM_CCMR1_CC1S_IN_TI2     TIM_CCMR1_CC1S(0x2)
#define TIM_CCMR1_CC1S_IN_TI1     TIM_CCMR1_CC1S(0x1)
#define TIM_CCMR1_CC1S_IN_TRC     TIM_CCMR1_CC1S(0x3)
#define TIM_CCMR1_CC1S_MASK       TIM_CCMR1_CC1S(0x3)

/* --- Input capture mode --- */

/* IC2F[3:0]: Input capture 2 filter */
#define TIM_CCMR1_IC2F_SHIFT      12
#define TIM_CCMR1_IC2F(TIM_IF)    ((TIM_IF) << TIM_CCMR1_IC2F_SHIFT)
#define TIM_CCMR1_IC2F_MASK       TIM_CCMR1_IC2F(TIM_IF_MASK)

/* IC2PSC[1:0]: Input capture 2 prescaler */
#define TIM_CCMR1_IC2PSC_SHIFT    10
#define TIM_CCMR1_IC2PSC(TIM_PS)  ((TIM_PS) << TIM_CCMR1_IC2PSC_SHIFT)
#define TIM_CCMR1_IC2PSC_MASK     TIM_CCMR1_IC2PSC(TIM_PS_MASK)

/* IC1F[3:0]: Input capture 1 filter */
#define TIM_CCMR1_IC1F_SHIFT      4
#define TIM_CCMR1_IC1F(TIM_IF)    ((TIM_IF) << TIM_CCMR1_IC1F_SHIFT)
#define TIM_CCMR1_IC1F_MASK       TIM_CCMR1_IC1F(TIM_IF_MASK)

/* IC1PSC[1:0]: Input capture 1 prescaler */
#define TIM_CCMR1_IC1PSC_SHIFT    2
#define TIM_CCMR1_IC1PSC(TIM_PS)  ((TIM_PS) << TIM_CCMR1_IC1PSC_SHIFT)
#define TIM_CCMR1_IC1PSC_MASK     TIM_CCMR1_IC1PSC(TIM_PS_MASK)

/* --- TIMx_CCMR2 values --------------------------------------------------- */

/* --- Output compare mode --- */

/* OC4CE: Output compare 4 clear enable */
#define TIM_CCMR2_OC4CE           (1 << 15)

/* OC4M[2:0]: Output compare 4 mode */
#define TIM_CCMR2_OC4M_SHIFT      12
#define TIM_CCMR2_OC4M(TIM_OCM)   ((TIM_OCM) << TIM_CCMR2_OC4M_SHIFT)
#define TIM_CCMR2_OC4M_MASK       TIM_CCMR2_OC4M(TIM_OCM_MASK)

/* OC4PE: Output compare 4 preload enable */
#define TIM_CCMR2_OC4PE           (1 << 11)

/* OC4FE: Output compare 4 fast enable */
#define TIM_CCMR2_OC4FE           (1 << 10)

/* CC4S[1:0]: Capture/compare 4 selection */
/* Note: CC2S bits are writable only when the channel is OFF (CC2E = 0 in
 * TIMx_CCER). */
#define TIM_CCMR2_CC4S_SHIFT      8
#define TIM_CCMR2_CC4S(x)         ((x) << TIM_CCMR2_CC4S_SHIFT)
#define TIM_CCMR2_CC4S_OUT        TIM_CCMR2_CC4S(0x0)
#define TIM_CCMR2_CC4S_IN_TI4     TIM_CCMR2_CC4S(0x1)
#define TIM_CCMR2_CC4S_IN_TI3     TIM_CCMR2_CC4S(0x2)
#define TIM_CCMR2_CC4S_IN_TRC     TIM_CCMR2_CC4S(0x3)
#define TIM_CCMR2_CC4S_MASK       TIM_CCMR2_CC4S(0x3)

/* OC3CE: Output compare 3 clear enable */
#define TIM_CCMR2_OC3CE           (1 << 7)

/* OC3M[2:0]: Output compare 3 mode */
#define TIM_CCMR2_OC3M_SHIFT      4
#define TIM_CCMR2_OC3M(TIM_OCM)   ((TIM_OCM) << TIM_CCMR2_OC3M_SHIFT)
#define TIM_CCMR2_OC3M_MASK       TIM_CCMR2_OC3M(TIM_OCM_MASK)

/* OC3PE: Output compare 3 preload enable */
#define TIM_CCMR2_OC3PE           (1 << 3)

/* OC3FE: Output compare 3 fast enable */
#define TIM_CCMR2_OC3FE           (1 << 2)

/* CC3S[1:0]: Capture/compare 3 selection */
/* Note: CC2S bits are writable only when the channel is OFF (CC2E = 0 in
 * TIMx_CCER). */
#define TIM_CCMR2_CC3S_SHIFT      0
#define TIM_CCMR2_CC3S(x)         ((x) << TIM_CCMR2_CC3S_SHIFT)
#define TIM_CCMR2_CC3S_OUT        TIM_CCMR2_CC3S(0x0)
#define TIM_CCMR2_CC3S_IN_TI3     TIM_CCMR2_CC3S(0x1)
#define TIM_CCMR2_CC3S_IN_TI4     TIM_CCMR2_CC3S(0x2)
#define TIM_CCMR2_CC3S_IN_TRC     TIM_CCMR2_CC3S(0x3)
#define TIM_CCMR2_CC3S_MASK       TIM_CCMR2_CC3S(0x3)

/* --- Input capture mode --- */

/* IC4F[3:0]: Input capture 4 filter */
#define TIM_CCMR2_IC4F_SHIFT      12
#define TIM_CCMR2_IC4F(TIM_IF)    ((TIM_IF) << TIM_CCMR2_IC4F_SHIFT)
#define TIM_CCMR2_IC4F_MASK       TIM_CCMR2_IC4F(TIM_IF_MASK)

/* IC4PSC[1:0]: Input capture 4 prescaler */
#define TIM_CCMR2_IC4PSC_SHIFT    10
#define TIM_CCMR2_IC4PSC(TIM_PS)  ((TIM_PS) << TIM_CCMR2_IC4PSC_SHIFT)
#define TIM_CCMR2_IC4PSC_MASK     TIM_CCMR2_IC4PSC(TIM_PS_MASK)

/* IC3F[3:0]: Input capture 3 filter */
#define TIM_CCMR2_IC3F_SHIFT      4
#define TIM_CCMR2_IC3F(TIM_IF)    ((TIM_IF) << TIM_CCMR2_IC3F_SHIFT)
#define TIM_CCMR2_IC3F_MASK       TIM_CCMR2_IC3F(TIM_IF_MASK)

/* IC3PSC[1:0]: Input capture 3 prescaler */
#define TIM_CCMR2_IC3PSC_SHIFT    2
#define TIM_CCMR2_IC3PSC(TIM_PS)  ((TIM_PS) << TIM_CCMR2_IC3PSC_SHIFT)
#define TIM_CCMR2_IC3PSC_MASK     TIM_CCMR2_IC3PSC(TIM_PS_MASK)

/* --- TIMx_CCER values ---------------------------------------------------- */

/* CC4P: Capture/compare 4 output polarity */
#define TIM_CCER_CC4P       (1 << 13)

/* CC4E: Capture/compare 4 output enable */
#define TIM_CCER_CC4E       (1 << 12)

/* CC3NP: Capture/compare 3 complementary output polarity */
#define TIM_CCER_CC3NP      (1 << 11)

/* CC3NE: Capture/compare 3 complementary output enable */
#define TIM_CCER_CC3NE      (1 << 10)

/* CC3P: Capture/compare 3 output polarity */
#define TIM_CCER_CC3P       (1 << 9)

/* CC3E: Capture/compare 3 output enable */
#define TIM_CCER_CC3E       (1 << 8)

/* CC2NP: Capture/compare 2 complementary output polarity */
#define TIM_CCER_CC2NP      (1 << 7)

/* CC2NE: Capture/compare 2 complementary output enable */
#define TIM_CCER_CC2NE      (1 << 6)

/* CC2P: Capture/compare 2 output polarity */
#define TIM_CCER_CC2P       (1 << 5)

/* CC2E: Capture/compare 2 output enable */
#define TIM_CCER_CC2E       (1 << 4)

/* CC1NP: Capture/compare 1 complementary output polarity */
#define TIM_CCER_CC1NP      (1 << 3)

/* CC1NE: Capture/compare 1 complementary output enable */
#define TIM_CCER_CC1NE      (1 << 2)

/* CC1P: Capture/compare 1 output polarity */
#define TIM_CCER_CC1P       (1 << 1)

/* CC1E: Capture/compare 1 output enable */
#define TIM_CCER_CC1E       (1 << 0)

/* --- TIMx_CNT values ----------------------------------------------------- */

/* CNT[15:0]: Counter value */

/* --- TIMx_PSC values ----------------------------------------------------- */

/* PSC[15:0]: Prescaler value */

/* --- TIMx_ARR values ----------------------------------------------------- */

/* ARR[15:0]: Prescaler value */

/* --- TIMx_RCR values ----------------------------------------------------- */

/* REP[15:0]: Repetition counter value */

/* --- TIMx_CCR1 values ---------------------------------------------------- */

/* CCR1[15:0]: Capture/compare 1 value */

/* --- TIMx_CCR2 values ---------------------------------------------------- */

/* CCR2[15:0]: Capture/compare 2 value */

/* --- TIMx_CCR3 values ---------------------------------------------------- */

/* CCR3[15:0]: Capture/compare 3 value */

/* --- TIMx_CCR4 values ---------------------------------------------------- */

/* CCR4[15:0]: Capture/compare 4 value */

/* --- TIMx_BDTR values ---------------------------------------------------- */

/* MOE: Main output enable */
#define TIM_BDTR_MOE      (1 << 15)

/* AOE: Automatic output enable */
#define TIM_BDTR_AOE      (1 << 14)

/* BKP: Break polarity */
#define TIM_BDTR_BKP      (1 << 13)

/* BKE: Break enable */
#define TIM_BDTR_BKE      (1 << 12)

/* OSSR: Off-state selection of run mode */
#define TIM_BDTR_OSSR     (1 << 11)

/* OSSI: Off-state selection of idle mode */
#define TIM_BDTR_OSSI     (1 << 10)

/* LOCK[1:0]: Lock configuration */
/****************************************************************************/
/** @defgroup tim_lock TIM_BDTR_LOCK Timer Lock Values
@ingroup timer_defines

@{*/
#define TIM_BDTR_LOCK_SHIFT     8
#define TIM_BDTR_LOCK(x)        ((x) << TIM_BDTR_LOCK_SHIFT)
#define TIM_BDTR_LOCK_OFF       TIM_BDTR_LOCK(0x0)
#define TIM_BDTR_LOCK_LEVEL_1   TIM_BDTR_LOCK(0x1)
#define TIM_BDTR_LOCK_LEVEL_2   TIM_BDTR_LOCK(0x2)
#define TIM_BDTR_LOCK_LEVEL_3   TIM_BDTR_LOCK(0x3)
#define TIM_BDTR_LOCK_MASK      TIM_BDTR_LOCK(0x3)
/**@}*/

/* DTG[7:0]: Dead-time generator set-up */
#define TIM_BDTR_DTG_SHIFT      0
#define TIM_BDTR_DTG(x)         ((x) << TIM_BDTR_DTG_SHIFT)
#define TIM_BDTR_DTG_MASK       TIM_BDTR_DTG(0xff)

/* --- TIMx_DCR values ----------------------------------------------------- */

/* DBL[4:0]: DMA burst length */
#define TIM_DCR_DBL_SHIFT      8
#define TIM_DCR_DBL(x)         ((x) << TIM_DCR_DBL_SHIFT)
#define TIM_DCR_DBL_MASK       TIM_DCR_DBL(0x1f)
#define TIM_DCR_DBL_VAL(x)     TIM_DCR_DBL((x) - 1)

/* DBA[4:0]: DMA base address */
#define TIM_DCR_DBA_SHIFT      0
#define TIM_DCR_DBA(x)         ((x) << TIM_DCR_DBA_SHIFT)
#define TIM_DCR_DBA_MASK       TIM_DCR_DBA(0x1f)
#define TIM_DCR_DBA_REG(x)     TIM_DCR_DBA(&TIM_##x(TIM1_BASE) - &TIM_CR1(TIM1_BASE))

/* --- TIMx_DMAR values ---------------------------------------------------- */

/* DMAB[15:0]: DMA register for burst accesses */

/* --- TIMx convenience defines -------------------------------------------- */

/**@}*/

/* --- STM32 F2, F4 extras ------------------------------------------------- */

/*
 * TIM2 and TIM5 are now 32bit and the following registers are now 32-bit wide:
 * CNT, ARR, CCR1, CCR2, CCR3, CCR4
 */

/* Timer 2/5 option register (TIMx_OR) */
#define TIM_OR(tim_base)    MMIO32((tim_base) + 0x50)

/* --- TIM2_OR values ---------------------------------------------------- */

/* ITR1_RMP */
/****************************************************************************/
/** @defgroup tim2_opt_trigger_remap TIM2_OR Timer 2 Option Register Internal
Trigger 1 Remap

Only available in F2 and F4 series.
@ingroup timer_defines

@{*/
#define TIM2_OR_ITR1_RMP_SHIFT      10
#define TIM2_OR_ITR1_RMP(x)         ((x) << TIM2_OR_ITR1_RMP_SHIFT)
/** Internal Trigger 1 remapped to timer 8 trigger out */
#define TIM2_OR_ITR1_RMP_TIM8_TRGOU TIM2_OR_ITR1_RMP(0x0)
/** Internal Trigger 1 remapped to PTP trigger out */
#define TIM2_OR_ITR1_RMP_PTP        TIM2_OR_ITR1_RMP(0x1)
/** Internal Trigger 1 remapped to USB OTG FS SOF */
#define TIM2_OR_ITR1_RMP_OTG_FS_SOF TIM2_OR_ITR1_RMP(0x2)
/** Internal Trigger 1 remapped to USB OTG HS SOF */
#define TIM2_OR_ITR1_RMP_OTG_HS_SOF TIM2_OR_ITR1_RMP(0x3)
/**@}*/
#define TIM2_OR_ITR1_RMP_MASK       TIM2_OR_ITR1_RMP(0x3)

/* --- TIM5_OR values ---------------------------------------------------- */

/* ITR4_RMP */
/****************************************************************************/
/** @defgroup tim5_opt_trigger_remap TIM5_OR Timer 5 Option Register Internal Trigger 4 Remap

Only available in F2 and F4 series.
@ingroup timer_defines

@{*/
#define TIM5_OR_TI4_RMP_SHIFT   6
#define TIM5_OR_TI4_RMP(x)      ((x) << TIM5_OR_TI4_RMP_SHIFT)
/** Internal Trigger 4 remapped to GPIO (see reference manual) */
#define TIM5_OR_TI4_RMP_GPIO    TIM5_OR_TI4_RMP(0x0)
/** Internal Trigger 4 remapped to LSI internal clock */
#define TIM5_OR_TI4_RMP_LSI     TIM5_OR_TI4_RMP(0x1)
/** Internal Trigger 4 remapped to LSE internal clock */
#define TIM5_OR_TI4_RMP_LSE     TIM5_OR_TI4_RMP(0x2)
/** Internal Trigger 4 remapped to RTC output event */
#define TIM5_OR_TI4_RMP_RTC     TIM5_OR_TI4_RMP(0x3)
/**@}*/
#define TIM5_OR_TI4_RMP_MASK    TIM5_OR_TI4_RMP(0x3)

#endif /* STM32_TIM_H */
