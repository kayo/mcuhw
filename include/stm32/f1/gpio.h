/** @defgroup gpio_defines GPIO Defines

@brief <b>Defined Constants and Types for the STM32F1xx General Purpose I/O</b>

@ingroup STM32F1xx_defines

@version 1.0.0

@date 1 July 2012

LGPL License Terms @ref lgpl_license
 */
/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2009 Uwe Hermann <uwe@hermann-uwe.de>
 * Copyright (C) 2012 Piotr Esden-Tempski <piotr@esden.net>
 * Copyright (C) 2012 Ken Sarkies <ksarkies@internode.on.net>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

/**@{*/

#ifndef STM32F1_GPIO_H
#define STM32F1_GPIO_H

/* --- Convenience macros -------------------------------------------------- */

/* GPIO port base addresses (for convenience) */
/** @defgroup gpio_port_id GPIO Port IDs
@ingroup gpio_defines

@{*/
/* GPIO port base addresses (for convenience) */
#define GPIOA       GPIO_PORT_A_BASE
#define GPIOB       GPIO_PORT_B_BASE
#define GPIOC       GPIO_PORT_C_BASE
#define GPIOD       GPIO_PORT_D_BASE
#define GPIOE       GPIO_PORT_E_BASE
#define GPIOF       GPIO_PORT_F_BASE
#define GPIOG       GPIO_PORT_G_BASE
/**@}*/

/* --- GPIO registers ------------------------------------------------------ */

/* Port configuration register low (GPIOx_CRL) */
#define GPIO_CRL(port)      MMIO32((port) + 0x00)

/* Port configuration register low (GPIOx_CRH) */
#define GPIO_CRH(port)      MMIO32((port) + 0x04)

/* Port input data register (GPIOx_IDR) */
#define GPIO_IDR(port)      MMIO32((port) + 0x08)

/* Port output data register (GPIOx_ODR) */
#define GPIO_ODR(port)      MMIO32((port) + 0x0c)

/* Port bit set/reset register (GPIOx_BSRR) */
#define GPIO_BSRR(port)     MMIO32((port) + 0x10)

/* Port bit reset register (GPIOx_BRR) */
#define GPIO_BRR(port)      MMIO16((port) + 0x14)

/* Port configuration lock register (GPIOx_LCKR) */
#define GPIO_LCKR(port)     MMIO32((port) + 0x18)

/* --- GPIO_CRL/GPIO_CRH values -------------------------------------------- */

#define GPIO_CRL_MODE(n, mode) ((mode) << ((n) * 4))
#define GPIO_CRL_MODE_MASK(n) GPIO_CRL_MODE(n, 0x3)

#define GPIO_CRH_MODE(n, mode) ((mode) << (((n) - 8) * 4))
#define GPIO_CRH_MODE_MASK(n) GPIO_CRH_MODE(n, 0x3)

#define GPIO_CRL_CNF(n, cfg) ((cfg) << ((n) * 4 + 2))
#define GPIO_CRL_CNF_MASK(n) GPIO_CRL_CNF(n, 0x3)

#define GPIO_CRH_CNF(n, cfg) ((cfg) << (((n) - 8) * 4 + 2))
#define GPIO_CRH_CNF_MASK(n) GPIO_CRH_CNF(n, 0x3)

/** @defgroup gpio_cnf GPIO Pin Configuration
@ingroup gpio_defines
If mode specifies input, configuration can be
@li Analog input
@li Floating input
@li Pull up/down input

If mode specifies output, configuration can be
@li Digital push-pull
@li Digital open drain
@li Alternate function push-pull or analog output
@li Alternate function open drain or analog output
@{*/
/* CNF[1:0] values when MODE[1:0] is 00 (input mode) */
/** Analog Input */
#define GPIO_CNF_INPUT_ANALOG           0x00
/** Digital Input Floating */
#define GPIO_CNF_INPUT_FLOAT            0x01  /* Default */
/** Digital Input Pull Up and Down */
#define GPIO_CNF_INPUT_PULL_UPDOWN      0x02
/* CNF[1:0] values when MODE[1:0] is != 00 (one of the output modes) */
/** Digital Output Pushpull */
#define GPIO_CNF_OUTPUT_PUSHPULL        0x00
/** Digital Output Open Drain */
#define GPIO_CNF_OUTPUT_OPENDRAIN       0x01
/** Alternate Function Output Pushpull */
#define GPIO_CNF_OUTPUT_ALTFN_PUSHPULL  0x02
/** Alternate Function Output Open Drain */
#define GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN 0x03
/**@}*/

/* Pin mode (MODE[1:0]) values */
/** @defgroup gpio_mode GPIO Pin Mode
@ingroup gpio_defines
@li Input (default after reset)
@li Output mode at 10 MHz maximum speed
@li Output mode at 2 MHz maximum speed
@li Output mode at 50 MHz maximum speed
@{*/
#define GPIO_MODE_INPUT           0x00  /* Default */
#define GPIO_MODE_OUTPUT_10_MHZ   0x01
#define GPIO_MODE_OUTPUT_2_MHZ    0x02
#define GPIO_MODE_OUTPUT_50_MHZ   0x03
/**@}*/

/* --- GPIO_IDR values ----------------------------------------------------- */

/* GPIO_IDR[15:0]: IDRy[15:0]: Port input data (y = 0..15) */

/* --- GPIO_ODR values ----------------------------------------------------- */

/* GPIO_ODR[15:0]: ODRy[15:0]: Port output data (y = 0..15) */

/* --- GPIO_BSRR values ---------------------------------------------------- */

/* GPIO_BSRR[31:16]: BRy: Port x reset bit y (y = 0..15) */
/* GPIO_BSRR[15:0]: BSy: Port x set bit y (y = 0..15) */
#define GPIO_BSRR_RESET(n) (1 << ((n) + 16))
#define GPIO_BSRR_SET(n) (1 << (n))

/* --- GPIO_BRR values ----------------------------------------------------- */

/* GPIO_BRR[15:0]: BRy: Port x reset bit y (y = 0..15) */

/* --- AFIO registers ------------------------------------------------------ */

/* Event control register (AFIO_EVCR) */
#define AFIO_EVCR       MMIO32(AFIO_BASE + 0x00)

/* AF remap and debug I/O configuration register (AFIO_MAPR) */
#define AFIO_MAPR       MMIO32(AFIO_BASE + 0x04)

/* External interrupt configuration register [0..3] (AFIO_EXTICR[1..4])*/
#define AFIO_EXTICR(i)  MMIO32(AFIO_BASE + 0x08 + ((i) * 4))
#define AFIO_EXTICR1    AFIO_EXTICR(0)
#define AFIO_EXTICR2    AFIO_EXTICR(1)
#define AFIO_EXTICR3    AFIO_EXTICR(2)
#define AFIO_EXTICR4    AFIO_EXTICR(3)

/* AF remap and debug I/O configuration register (AFIO_MAPR) */
#define AFIO_MAPR2      MMIO32(AFIO_BASE + 0x1C)

/* --- AFIO_EVCR values ---------------------------------------------------- */

/* EVOE: Event output enable */
#define AFIO_EVCR_EVOE      (1 << 7)

/* PORT[2:0]: Port selection */
/** @defgroup afio_evcr_port EVENTOUT Port selection
@ingroup gpio_defines

@{*/
#define AFIO_EVCR_PORT_PA   (0x0 << 4)
#define AFIO_EVCR_PORT_PB   (0x1 << 4)
#define AFIO_EVCR_PORT_PC   (0x2 << 4)
#define AFIO_EVCR_PORT_PD   (0x3 << 4)
#define AFIO_EVCR_PORT_PE   (0x4 << 4)
/**@}*/

/* PIN[3:0]: Pin selection */
/** @defgroup afio_evcr_pin EVENTOUT Pin selection
@ingroup gpio_defines

@{*/
#define AFIO_EVCR_PIN_Px0   (0x0 << 0)
#define AFIO_EVCR_PIN_Px1   (0x1 << 0)
#define AFIO_EVCR_PIN_Px2   (0x2 << 0)
#define AFIO_EVCR_PIN_Px3   (0x3 << 0)
#define AFIO_EVCR_PIN_Px4   (0x4 << 0)
#define AFIO_EVCR_PIN_Px5   (0x5 << 0)
#define AFIO_EVCR_PIN_Px6   (0x6 << 0)
#define AFIO_EVCR_PIN_Px7   (0x7 << 0)
#define AFIO_EVCR_PIN_Px8   (0x8 << 0)
#define AFIO_EVCR_PIN_Px9   (0x9 << 0)
#define AFIO_EVCR_PIN_Px10  (0xA << 0)
#define AFIO_EVCR_PIN_Px11  (0xB << 0)
#define AFIO_EVCR_PIN_Px12  (0xC << 0)
#define AFIO_EVCR_PIN_Px13  (0xD << 0)
#define AFIO_EVCR_PIN_Px14  (0xE << 0)
#define AFIO_EVCR_PIN_Px15  (0xF << 0)
/**@}*/

/* --- AFIO_MAPR values ---------------------------------------------------- */

/* 31 reserved */

/** @defgroup afio_remap_cld Alternate Function Remap Controls for Connectivity
Line Devices only
@ingroup gpio_defines

@{*/
/* PTP_PPS_REMAP: */
/** Ethernet PTP PPS remapping (only connectivity line devices) */
#define AFIO_MAPR_PTP_PPS_REMAP   (1 << 30)

/* TIM2ITR1_IREMAP: */
/** TIM2 internal trigger 1 remapping (only connectivity line devices) */
#define AFIO_MAPR_TIM2ITR1_IREMAP (1 << 29)

/* SPI3_REMAP: */
/** SPI3/I2S3 remapping (only connectivity line devices) */
#define AFIO_MAPR_SPI3_REMAP      (1 << 28)

/* MII_REMAP: */
/** MII or RMII selection (only connectivity line devices) */
#define AFIO_MAPR_MII_RMII_SEL    (1 << 23)

/* CAN2_REMAP: */
/**  CAN2 I/O remapping (only connectivity line devices) */
#define AFIO_MAPR_CAN2_REMAP      (1 << 22)

/* ETH_REMAP: */
/**  Ethernet MAC I/O remapping (only connectivity line devices) */
#define AFIO_MAPR_ETH_REMAP       (1 << 21)

/**@}*/

/* 27 reserved */

/* SWJ_CFG[2:0]: Serial wire JTAG configuration */
/** @defgroup afio_swj_disable Serial Wire JTAG disables
@ingroup gpio_defines

@{*/
#define AFIO_MAPR_SWJ_CFG_SHIFT              24
#define AFIO_MAPR_SWJ_CFG_MASK               (0x7 << AFIO_MAPR_SWJ_CFG_SHIFT)
/** Full Serial Wire JTAG capability */
#define AFIO_MAPR_SWJ_CFG_FULL_SWJ           (0x0 << AFIO_MAPR_SWJ_CFG_SHIFT)
/** Full Serial Wire JTAG capability without JNTRST */
#define AFIO_MAPR_SWJ_CFG_FULL_SWJ_NO_JNTRST (0x1 << AFIO_MAPR_SWJ_CFG_SHIFT)
/** JTAG-DP disabled with SW-DP enabled */
#define AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON     (0x2 << AFIO_MAPR_SWJ_CFG_SHIFT)
/** JTAG-DP disabled and SW-DP disabled */
#define AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_OFF    (0x4 << AFIO_MAPR_SWJ_CFG_SHIFT)
/**@}*/

/** @defgroup afio_remap Alternate Function Remap Controls
@ingroup gpio_defines

@{*/
/* ADC2_ETRGREG_REMAP: */
/**
 * ADC2 external trigger regulator conversion remapping
 * (only low-, medium-, high- and XL-density devices)
 */
#define AFIO_MAPR_ADC2_ETRGREG_REMAP    (1 << 20)

/* ADC2_ETRGINJ_REMAP: */
/**
 * ADC2 external trigger injected conversion remapping
 * (only low-, medium-, high- and XL-density devices)
 */
#define AFIO_MAPR_ADC2_ETRGINJ_REMAP    (1 << 19)

/* ADC1_ETRGREG_REMAP: */
/**
 * ADC1 external trigger regulator conversion remapping
 * (only low-, medium-, high- and XL-density devices)
 */
#define AFIO_MAPR_ADC1_ETRGREG_REMAP    (1 << 18)

/* ADC1_ETRGINJ_REMAP: */
/**
 * ADC1 external trigger injected conversion remapping
 * (only low-, medium-, high- and XL-density devices)
 */
#define AFIO_MAPR_ADC1_ETRGINJ_REMAP    (1 << 17)

/* TIM5CH4_IREMAP: */
/** TIM5 channel 4 internal remap */
#define AFIO_MAPR_TIM5CH4_IREMAP  (1 << 16)

/* PD01_REMAP: */
/** Port D0/Port D1 mapping on OSC_IN/OSC_OUT */
#define AFIO_MAPR_PD01_REMAP      (1 << 15)

/* TIM4_REMAP: */
/** TIM4 remapping */
#define AFIO_MAPR_TIM4_REMAP      (1 << 12)

/* USART2_REMAP[1:0]: */
/** USART2 remapping */
#define AFIO_MAPR_USART2_REMAP    (1 << 3)

/* USART1_REMAP[1:0]: */
/** USART1 remapping */
#define AFIO_MAPR_USART1_REMAP    (1 << 2)

/* I2C1_REMAP[1:0]: */
/** I2C1 remapping */
#define AFIO_MAPR_I2C1_REMAP      (1 << 1)

/* SPI1_REMAP[1:0]: */
/** SPI1 remapping */
#define AFIO_MAPR_SPI1_REMAP      (1 << 0)
/**@}*/

/* CAN_REMAP[1:0]: CAN1 alternate function remapping */
/** @defgroup afio_remap_can1 Alternate Function Remap Controls for CAN 1
@ingroup gpio_defines

@{*/
#define AFIO_MAPR_CAN1_REMAP_SHIFT    13
#define AFIO_MAPR_CAN1_REMAP          (0x3 << AFIO_MAPR_CAN1_REMAP_SHIFT)
#define AFIO_MAPR_CAN1_REMAP_PORTA    (0x0 << AFIO_MAPR_CAN1_REMAP_SHIFT)
#define AFIO_MAPR_CAN1_REMAP_PORTB    (0x2 << AFIO_MAPR_CAN1_REMAP_SHIFT) /* Not 36pin pkg */
#define AFIO_MAPR_CAN1_REMAP_PORTD    (0x3 << AFIO_MAPR_CAN1_REMAP_SHIFT)
/**@}*/

/* TIM3_REMAP[1:0]: TIM3 remapping */
/** @defgroup afio_remap_tim3 Alternate Function Remap Controls for Timer 3
@ingroup gpio_defines

@{*/
#define AFIO_MAPR_TIM3_REMAP_SHIFT     10
#define AFIO_MAPR_TIM3_REMAP           (0x3 << AFIO_MAPR_TIM3_REMAP_SHIFT)
#define AFIO_MAPR_TIM3_REMAP_NO        (0x0 << AFIO_MAPR_TIM3_REMAP_SHIFT)
#define AFIO_MAPR_TIM3_REMAP_PARTIAL   (0x2 << AFIO_MAPR_TIM3_REMAP_SHIFT)
#define AFIO_MAPR_TIM3_REMAP_FULL      (0x3 << AFIO_MAPR_TIM3_REMAP_SHIFT)
/**@}*/

/* TIM2_REMAP[1:0]: TIM2 remapping */
/** @defgroup afio_remap_tim2 Alternate Function Remap Controls for Timer 2
@ingroup gpio_defines

@{*/
#define AFIO_MAPR_TIM2_REMAP_SHIFT     8
#define AFIO_MAPR_TIM2_REMAP           (0x3 << AFIO_MAPR_TIM2_REMAP_SHIFT)
#define AFIO_MAPR_TIM2_REMAP_NO        (0x0 << AFIO_MAPR_TIM2_REMAP_SHIFT)
#define AFIO_MAPR_TIM2_REMAP_PARTIAL1  (0x1 << AFIO_MAPR_TIM2_REMAP_SHIFT)
#define AFIO_MAPR_TIM2_REMAP_PARTIAL2  (0x2 << AFIO_MAPR_TIM2_REMAP_SHIFT)
#define AFIO_MAPR_TIM2_REMAP_FULL      (0x3 << AFIO_MAPR_TIM2_REMAP_SHIFT)
/**@}*/

/* TIM1_REMAP[1:0]: TIM1 remapping */
/** @defgroup afio_remap_tim1 Alternate Function Remap Controls for Timer 1
@ingroup gpio_defines

@{*/
#define AFIO_MAPR_TIM1_REMAP_SHIFT 6
#define AFIO_MAPR_TIM1_REMAP           (0x3 << AFIO_MAPR_TIM1_REMAP_SHIFT)
#define AFIO_MAPR_TIM1_REMAP_NO        (0x0 << AFIO_MAPR_TIM1_REMAP_SHIFT)
#define AFIO_MAPR_TIM1_REMAP_PARTIAL   (0x1 << AFIO_MAPR_TIM1_REMAP_SHIFT)
#define AFIO_MAPR_TIM1_REMAP_FULL      (0x3 << AFIO_MAPR_TIM1_REMAP_SHIFT)
/**@}*/

/* USART3_REMAP[1:0]: USART3 remapping */
/** @defgroup afio_remap_usart3 Alternate Function Remap Controls for USART 3
@ingroup gpio_defines

@{*/
#define AFIO_MAPR_USART3_REMAP_SHIFT   4
#define AFIO_MAPR_USART3_REMAP         (0x3 << AFIO_MAPR_USART3_REMAP_SHIFT)
#define AFIO_MAPR_USART3_REMAP_NO      (0x0 << AFIO_MAPR_USART3_REMAP_SHIFT)
#define AFIO_MAPR_USART3_REMAP_PARTIAL (0x1 << AFIO_MAPR_USART3_REMAP_SHIFT)
#define AFIO_MAPR_USART3_REMAP_FULL    (0x3 << AFIO_MAPR_USART3_REMAP_SHIFT)
/**@}*/

/** @defgroup afio_remap2 Alternate Function Remap Controls Secondary Set
@ingroup gpio_defines

@{*/
/** various remaps, dma/dac/timer triggers (HD only) */
#define AFIO_MAPR2_MISC_REMAP       (1 << 13)

/** TIM12_CH1 and TIM12_CH2 remapping (HD only) */
#define AFIO_MAPR2_TIM12_REMAP      (1 << 12)

/** TIM67_DAC_DMA remap to DMA1/DMA2 */
#define AFIO_MAPR2_TIM67_DAC_DMA_REMAP (1 << 11)

/* FSMC_NADV_DISCONNECT: */
/** The NADV is disconnected from its allocated pin */
#define AFIO_MAPR2_FSMC_NADV_DISCONNECT (1 << 10)

/* TIM14_REMAP: */
/**  TIM14 remapping */
#define AFIO_MAPR2_TIM14_REMAP      (1 << 9)

/* TIM13_REMAP: */
/**  TIM13 remapping */
#define AFIO_MAPR2_TIM13_REMAP      (1 << 8)

/* TIM11_REMAP: */
/**  TIM11 remapping */
#define AFIO_MAPR2_TIM11_REMAP      (1 << 7)

/* TIM10_REMAP: */
/**  TIM10 remapping */
#define AFIO_MAPR2_TIM10_REMAP      (1 << 6)

/* TIM9_REMAP: */
/**  TIM9 remapping */
#define AFIO_MAPR2_TIM9_REMAP       (1 << 5)

/** TIM1_DMA channel 1/2 remapping */
#define AFIO_MAPR2_TIM1_DMA_REMAP   (1 << 4)

/** CEC remapping (PB8 vs PB10) */
#define AFIO_MAPR2_CEC_REMAP        (1 << 3)

/** TIM17 remapping (PB9 vs PB7) */
#define AFIO_MAPR2_TIM17_REMAP      (1 << 2)

/** TIM16 remapping (PB8 vs PB6) */
#define AFIO_MAPR2_TIM16_REMAP      (1 << 1)

/** TIM15 remapping channels 1/2 */
#define AFIO_MAPR2_TIM15_REMAP      (1 << 0)
/**@}*/

/* --- AFIO_EXTICR values ------------------------------------------------- */

#define AFIO_EXTICR_SHIFT(n)   (4 * (n))
#define AFIO_EXTICR_VAL(n, v)  ((v) << AFIO_EXTICR_SHIFT(n))
#define AFIO_EXTICR_MSK(n)     AFIO_EXTICR_VAL(n, 0xf)

#define AFIO_EXTICR_GPIOA   0
#define AFIO_EXTICR_GPIOB   1
#define AFIO_EXTICR_GPIOC   2
#define AFIO_EXTICR_GPIOD   3
#define AFIO_EXTICR_GPIOE   4
#define AFIO_EXTICR_GPIOF   5

/** @defgroup afio_exti Alternate Function EXTI pin number
@ingroup gpio_defines

@{*/
#define AFIO_EXTI0                 0
#define AFIO_EXTI1                 1
#define AFIO_EXTI2                 2
#define AFIO_EXTI3                 3
#define AFIO_EXTI4                 4
#define AFIO_EXTI5                 5
#define AFIO_EXTI6                 6
#define AFIO_EXTI7                 7
#define AFIO_EXTI8                 8
#define AFIO_EXTI9                 9
#define AFIO_EXTI10               10
#define AFIO_EXTI11               11
#define AFIO_EXTI12               12
#define AFIO_EXTI13               13
#define AFIO_EXTI14               14
#define AFIO_EXTI15               15
/**@}*/

/**@}*/
#endif /* STM32F1_GPIO_H */
