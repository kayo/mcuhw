/** @defgroup rcc_defines RCC Defines
 *
 * @brief <b>Defined Constants and Types for the STM32F1xx Reset and Clock
 * Control</b>
 *
 * @ingroup STM32F1xx_defines
 *
 * @version 1.0.0
 *
 * @author @htmlonly &copy; @endhtmlonly 2009
 * Federico Ruiz-Ugalde \<memeruiz at gmail dot com\>
 * @author @htmlonly &copy; @endhtmlonly 2009
 * Uwe Hermann <uwe@hermann-uwe.de>
 *
 * @date 18 August 2012
 *
 * LGPL License Terms @ref lgpl_license
 *  */
/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2009 Uwe Hermann <uwe@hermann-uwe.de>
 * Copyright (C) 2009 Federico Ruiz-Ugalde <memeruiz at gmail dot com>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STM32F1_RCC_H
#define STM32F1_RCC_H
/**@{*/

/* Note: Regs/bits marked (**) only exist in "connectivity line" STM32s. */
/* Note: Regs/bits marked (XX) do NOT exist in "connectivity line" STM32s. */

/* --- RCC registers ------------------------------------------------------- */

#define RCC_CR          MMIO32(RCC_BASE + 0x00)
#define RCC_CFGR        MMIO32(RCC_BASE + 0x04)
#define RCC_CIR         MMIO32(RCC_BASE + 0x08)
#define RCC_APB2RSTR    MMIO32(RCC_BASE + 0x0c)
#define RCC_APB1RSTR    MMIO32(RCC_BASE + 0x10)
#define RCC_AHBENR      MMIO32(RCC_BASE + 0x14)
#define RCC_APB2ENR     MMIO32(RCC_BASE + 0x18)
#define RCC_APB1ENR     MMIO32(RCC_BASE + 0x1c)
#define RCC_BDCR        MMIO32(RCC_BASE + 0x20)
#define RCC_CSR         MMIO32(RCC_BASE + 0x24)
#define RCC_AHBRSTR     MMIO32(RCC_BASE + 0x28) /*(**)*/
#define RCC_CFGR2       MMIO32(RCC_BASE + 0x2c) /*(**)*/

/* --- RCC_CR values ------------------------------------------------------- */

#define RCC_CR_PLL3RDY      (1 << 29) /* (**) */
#define RCC_CR_PLL3ON       (1 << 28) /* (**) */
#define RCC_CR_PLL2RDY      (1 << 27) /* (**) */
#define RCC_CR_PLL2ON       (1 << 26) /* (**) */
#define RCC_CR_PLLRDY       (1 << 25)
#define RCC_CR_PLLON        (1 << 24)
#define RCC_CR_CSSON        (1 << 19)
#define RCC_CR_HSEBYP       (1 << 18)
#define RCC_CR_HSERDY       (1 << 17)
#define RCC_CR_HSEON        (1 << 16)
/* HSICAL: [15:8] */
/* HSITRIM: [7:3] */
#define RCC_CR_HSIRDY       (1 << 1)
#define RCC_CR_HSION        (1 << 0)

/* --- RCC_CFGR values ----------------------------------------------------- */

#define RCC_CFGR_OTGFSPRE     (1 << 22) /* Connectivity line */
#define RCC_CFGR_USBPRE       (1 << 22) /* LD,MD, HD, XL */

#define RCC_CFGR_PLLMUL_SHIFT 18
#define RCC_CFGR_PLLMUL       (0xF << RCC_CFGR_PLLMUL_SHIFT)

#define RCC_CFGR_PLLXTPRE     (1 << 17)
#define RCC_CFGR_PLLSRC_SHIFT 16
#define RCC_CFGR_PLLSRC       (1 << RCC_CFGR_PLLSRC_SHIFT)

#define RCC_CFGR_ADCPRE_SHIFT 14
#define RCC_CFGR_ADCPRE       (3 << RCC_CFGR_ADCPRE_SHIFT)

#define RCC_CFGR_PPRE2_SHIFT  11
#define RCC_CFGR_PPRE2        (7 << RCC_CFGR_PPRE2_SHIFT)

#define RCC_CFGR_PPRE1_SHIFT  8
#define RCC_CFGR_PPRE1        (7 << RCC_CFGR_PPRE1_SHIFT)

#define RCC_CFGR_HPRE_SHIFT   4
#define RCC_CFGR_HPRE         (0xF << RCC_CFGR_HPRE_SHIFT)

#define RCC_CFGR_SWS_SHIFT    2
#define RCC_CFGR_SWS          (3 << RCC_CFGR_SWS_SHIFT)

#define RCC_CFGR_SW_SHIFT     0
#define RCC_CFGR_SW           (3 << RCC_CFGR_SW_SHIFT)

/* MCO: Microcontroller clock output */
/** @defgroup rcc_cfgr_co RCC_CFGR Microcontroller Clock Output Source
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_CFGR_MCO_SHIFT     24
#define RCC_CFGR_MCO_MASK      0xf
#define RCC_CFGR_MCO_NOCLK     0x0
#define RCC_CFGR_MCO_SYSCLK    0x4
#define RCC_CFGR_MCO_HSI       0x5
#define RCC_CFGR_MCO_HSE       0x6
#define RCC_CFGR_MCO_PLL_DIV2  0x7
#define RCC_CFGR_MCO_PLL2      0x8 /* (**) */
#define RCC_CFGR_MCO_PLL3_DIV2 0x9 /* (**) */
#define RCC_CFGR_MCO_XT1       0xa /* (**) */
#define RCC_CFGR_MCO_PLL3      0xb /* (**) */
/**@}*/

/* USBPRE: USB prescaler (RCC_CFGR[22]) */
/** @defgroup rcc_cfgr_usbpre RCC_CFGR USB prescale Factors
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_CFGR_USBPRE_DIV1_5  0x0
#define RCC_CFGR_USBPRE_NODIV   0x1
#define RCC_CFGR_USBPRE_DIV1    0x1
/**@}*/

/* OTGFSPRE: USB OTG FS prescaler (RCC_CFGR[22]; only in conn. line STM32s) */
#define RCC_CFGR_USBPRE_DIV3  0x0
#define RCC_CFGR_USBPRE_DIV2  0x1

/* PLLMUL: PLL multiplication factor */
/** @defgroup rcc_cfgr_pmf RCC_CFGR PLL Multiplication Factor
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_CFGR_PLLMUL_MUL2   (0x0 << RCC_CFGR_PLLMUL_SHIFT) /* (XX) */
#define RCC_CFGR_PLLMUL_MUL3   (0x1 << RCC_CFGR_PLLMUL_SHIFT) /* (XX) */
#define RCC_CFGR_PLLMUL_MUL4   (0x2 << RCC_CFGR_PLLMUL_SHIFT)
#define RCC_CFGR_PLLMUL_MUL5   (0x3 << RCC_CFGR_PLLMUL_SHIFT)
#define RCC_CFGR_PLLMUL_MUL6   (0x4 << RCC_CFGR_PLLMUL_SHIFT)
#define RCC_CFGR_PLLMUL_MUL7   (0x5 << RCC_CFGR_PLLMUL_SHIFT)
#define RCC_CFGR_PLLMUL_MUL8   (0x6 << RCC_CFGR_PLLMUL_SHIFT)
#define RCC_CFGR_PLLMUL_MUL9   (0x7 << RCC_CFGR_PLLMUL_SHIFT)
#define RCC_CFGR_PLLMUL_MUL10  (0x8 << RCC_CFGR_PLLMUL_SHIFT) /* (XX) */
#define RCC_CFGR_PLLMUL_MUL11  (0x9 << RCC_CFGR_PLLMUL_SHIFT) /* (XX) */
#define RCC_CFGR_PLLMUL_MUL12  (0xa << RCC_CFGR_PLLMUL_SHIFT) /* (XX) */
#define RCC_CFGR_PLLMUL_MUL13  (0xb << RCC_CFGR_PLLMUL_SHIFT) /* (XX) */
#define RCC_CFGR_PLLMUL_MUL14  (0xc << RCC_CFGR_PLLMUL_SHIFT) /* (XX) */
#define RCC_CFGR_PLLMUL_MUL15  (0xd << RCC_CFGR_PLLMUL_SHIFT) /*(0xd << RCC_CFGR_PLLMUL_SHIFT): PLL x 15 */
#define RCC_CFGR_PLLMUL_MUL6_5 (0xd << RCC_CFGR_PLLMUL_SHIFT) /*(0xd << RCC_CFGR_PLLMUL_SHIFT): PLL x 6.5 for conn. line */
#define RCC_CFGR_PLLMUL_MUL16  (0xe << RCC_CFGR_PLLMUL_SHIFT) /* (XX) */
/* #define PLLMUL_PLL_CLK_MUL16   0xf */ /* (XX) */ /* Errata? 17? */
/**@}*/

/* TODO: conn. line differs. */
/* PLLXTPRE: HSE divider for PLL entry */
/** @defgroup rcc_cfgr_hsepre RCC_CFGR HSE Divider for PLL
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_CFGR_PLLXTPRE_HSE          (0x0 << 17)
#define RCC_CFGR_PLLXTPRE_HSE_DIV2     (0x1 << 17)
/**@}*/

/* PLLSRC: PLL entry clock source */
/** @defgroup rcc_cfgr_pcs RCC_CFGR PLL Clock Source
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_CFGR_PLLSRC_HSI_DIV2  (0x0 << RCC_CFGR_PLLSRC_SHIFT)
#define RCC_CFGR_PLLSRC_HSE       (0x1 << RCC_CFGR_PLLSRC_SHIFT)
#define RCC_CFGR_PLLSRC_PREDIV1   (0x1 << RCC_CFGR_PLLSRC_SHIFT) /* On conn. line */
/**@}*/

/* ADCPRE: ADC prescaler */
/****************************************************************************/
/** @defgroup rcc_cfgr_adcpre RCC ADC clock prescaler enable values
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_CFGR_ADCPRE_DIV2    (0x0 << RCC_CFGR_ADCPRE_SHIFT)
#define RCC_CFGR_ADCPRE_DIV4    (0x1 << RCC_CFGR_ADCPRE_SHIFT)
#define RCC_CFGR_ADCPRE_DIV6    (0x2 << RCC_CFGR_ADCPRE_SHIFT)
#define RCC_CFGR_ADCPRE_DIV8    (0x3 << RCC_CFGR_ADCPRE_SHIFT)
/**@}*/

/* PPRE2: APB high-speed prescaler (APB2) */
/** @defgroup rcc_cfgr_apb2pre RCC_CFGR APB2 prescale Factors
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_CFGR_PPRE2_NODIV   (0x0 << RCC_CFGR_PPRE2_SHIFT)
#define RCC_CFGR_PPRE2_DIV1    (0x0 << RCC_CFGR_PPRE2_SHIFT)
#define RCC_CFGR_PPRE2_DIV2    (0x4 << RCC_CFGR_PPRE2_SHIFT)
#define RCC_CFGR_PPRE2_DIV4    (0x5 << RCC_CFGR_PPRE2_SHIFT)
#define RCC_CFGR_PPRE2_DIV8    (0x6 << RCC_CFGR_PPRE2_SHIFT)
#define RCC_CFGR_PPRE2_DIV16   (0x7 << RCC_CFGR_PPRE2_SHIFT)
/**@}*/

/* PPRE1: APB low-speed prescaler (APB1) */
/** @defgroup rcc_cfgr_apb1pre RCC_CFGR APB1 prescale Factors
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_CFGR_PPRE1_NODIV   (0x0 << RCC_CFGR_PPRE1_SHIFT)
#define RCC_CFGR_PPRE1_DIV1    (0x0 << RCC_CFGR_PPRE1_SHIFT)
#define RCC_CFGR_PPRE1_DIV2    (0x4 << RCC_CFGR_PPRE1_SHIFT)
#define RCC_CFGR_PPRE1_DIV4    (0x5 << RCC_CFGR_PPRE1_SHIFT)
#define RCC_CFGR_PPRE1_DIV8    (0x6 << RCC_CFGR_PPRE1_SHIFT)
#define RCC_CFGR_PPRE1_DIV16   (0x7 << RCC_CFGR_PPRE1_SHIFT)
/**@}*/

/* HPRE: AHB prescaler */
/** @defgroup rcc_cfgr_ahbpre RCC_CFGR AHB prescale Factors
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_CFGR_HPRE_NODIV    (0x0 << RCC_CFGR_HPRE_SHIFT)
#define RCC_CFGR_HPRE_DIV1     (0x0 << RCC_CFGR_HPRE_SHIFT)
#define RCC_CFGR_HPRE_DIV2     (0x8 << RCC_CFGR_HPRE_SHIFT)
#define RCC_CFGR_HPRE_DIV4     (0x9 << RCC_CFGR_HPRE_SHIFT)
#define RCC_CFGR_HPRE_DIV8     (0xa << RCC_CFGR_HPRE_SHIFT)
#define RCC_CFGR_HPRE_DIV16    (0xb << RCC_CFGR_HPRE_SHIFT)
#define RCC_CFGR_HPRE_DIV64    (0xc << RCC_CFGR_HPRE_SHIFT)
#define RCC_CFGR_HPRE_DIV128   (0xd << RCC_CFGR_HPRE_SHIFT)
#define RCC_CFGR_HPRE_DIV256   (0xe << RCC_CFGR_HPRE_SHIFT)
#define RCC_CFGR_HPRE_DIV512   (0xf << RCC_CFGR_HPRE_SHIFT)
/**@}*/

/* SWS: System clock switch status */
#define RCC_CFGR_SWS_HSI   (0x0 << RCC_CFGR_SWS_SHIFT)
#define RCC_CFGR_SWS_HSE   (0x1 << RCC_CFGR_SWS_SHIFT)
#define RCC_CFGR_SWS_PLL   (0x2 << RCC_CFGR_SWS_SHIFT)

/* SW: System clock switch */
/** @defgroup rcc_cfgr_scs RCC_CFGR System Clock Selection
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_CFGR_SW_HSI    (0x0 << RCC_CFGR_SW_SHIFT)
#define RCC_CFGR_SW_HSE    (0x1 << RCC_CFGR_SW_SHIFT)
#define RCC_CFGR_SW_PLL    (0x2 << RCC_CFGR_SW_SHIFT)
/**@}*/

/* --- RCC_CIR values ------------------------------------------------------ */

/* Clock security system interrupt clear bit */
#define RCC_CIR_CSSC          (1 << 23)

/* OSC ready interrupt clear bits */
#define RCC_CIR_PLL3RDYC      (1 << 22) /* (**) */
#define RCC_CIR_PLL2RDYC      (1 << 21) /* (**) */
#define RCC_CIR_PLLRDYC       (1 << 20)
#define RCC_CIR_HSERDYC       (1 << 19)
#define RCC_CIR_HSIRDYC       (1 << 18)
#define RCC_CIR_LSERDYC       (1 << 17)
#define RCC_CIR_LSIRDYC       (1 << 16)

/* OSC ready interrupt enable bits */
#define RCC_CIR_PLL3RDYIE     (1 << 14) /* (**) */
#define RCC_CIR_PLL2RDYIE     (1 << 13) /* (**) */
#define RCC_CIR_PLLRDYIE      (1 << 12)
#define RCC_CIR_HSERDYIE      (1 << 11)
#define RCC_CIR_HSIRDYIE      (1 << 10)
#define RCC_CIR_LSERDYIE      (1 << 9)
#define RCC_CIR_LSIRDYIE      (1 << 8)

/* Clock security system interrupt flag bit */
#define RCC_CIR_CSSF          (1 << 7)

/* OSC ready interrupt flag bits */
#define RCC_CIR_PLL3RDYF      (1 << 6) /* (**) */
#define RCC_CIR_PLL2RDYF      (1 << 5) /* (**) */
#define RCC_CIR_PLLRDYF       (1 << 4)
#define RCC_CIR_HSERDYF       (1 << 3)
#define RCC_CIR_HSIRDYF       (1 << 2)
#define RCC_CIR_LSERDYF       (1 << 1)
#define RCC_CIR_LSIRDYF       (1 << 0)

/* --- RCC_APB2RSTR values ------------------------------------------------- */

/** @defgroup rcc_apb2rstr_rst RCC_APB2RSTR reset values
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_APB2RSTR_TIM17RST  (1 << 18)
#define RCC_APB2RSTR_TIM16RST  (1 << 17)
#define RCC_APB2RSTR_TIM15RST  (1 << 16)
#define RCC_APB2RSTR_ADC3RST   (1 << 15) /* (XX) */
#define RCC_APB2RSTR_USART1RST (1 << 14)
#define RCC_APB2RSTR_TIM8RST   (1 << 13) /* (XX) */
#define RCC_APB2RSTR_SPI1RST   (1 << 12)
#define RCC_APB2RSTR_TIM1RST   (1 << 11)
#define RCC_APB2RSTR_ADC2RST   (1 << 10)
#define RCC_APB2RSTR_ADC1RST   (1 << 9)
#define RCC_APB2RSTR_IOPGRST   (1 << 8)  /* (XX) */
#define RCC_APB2RSTR_IOPFRST   (1 << 7)  /* (XX) */
#define RCC_APB2RSTR_IOPERST   (1 << 6)
#define RCC_APB2RSTR_IOPDRST   (1 << 5)
#define RCC_APB2RSTR_IOPCRST   (1 << 4)
#define RCC_APB2RSTR_IOPBRST   (1 << 3)
#define RCC_APB2RSTR_IOPARST   (1 << 2)
#define RCC_APB2RSTR_AFIORST   (1 << 0)
/**@}*/

/* --- RCC_APB1RSTR values ------------------------------------------------- */

/** @defgroup rcc_apb1rstr_rst RCC_APB1RSTR reset values
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_APB1RSTR_DACRST    (1 << 29)
#define RCC_APB1RSTR_PWRRST    (1 << 28)
#define RCC_APB1RSTR_BKPRST    (1 << 27)
#define RCC_APB1RSTR_CAN2RST   (1 << 26) /* (**) */
#define RCC_APB1RSTR_CAN1RST   (1 << 25) /* (**) */
#define RCC_APB1RSTR_CANRST    (1 << 25) /* (XX) Alias for
                   CAN1RST */
#define RCC_APB1RSTR_USBRST    (1 << 23) /* (XX) */
#define RCC_APB1RSTR_I2C2RST   (1 << 22)
#define RCC_APB1RSTR_I2C1RST   (1 << 21)
#define RCC_APB1RSTR_UART5RST  (1 << 20)
#define RCC_APB1RSTR_UART4RST  (1 << 19)
#define RCC_APB1RSTR_USART3RST (1 << 18)
#define RCC_APB1RSTR_USART2RST (1 << 17)
#define RCC_APB1RSTR_SPI3RST   (1 << 15)
#define RCC_APB1RSTR_SPI2RST   (1 << 14)
#define RCC_APB1RSTR_WWDGRST   (1 << 11)
#define RCC_APB1RSTR_TIM7RST   (1 << 5)
#define RCC_APB1RSTR_TIM6RST   (1 << 4)
#define RCC_APB1RSTR_TIM5RST   (1 << 3)
#define RCC_APB1RSTR_TIM4RST   (1 << 2)
#define RCC_APB1RSTR_TIM3RST   (1 << 1)
#define RCC_APB1RSTR_TIM2RST   (1 << 0)
/**@}*/

/* --- RCC_AHBENR values --------------------------------------------------- */

/** @defgroup rcc_ahbenr_en RCC_AHBENR enable values
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_AHBENR_ETHMACENRX (1 << 16)
#define RCC_AHBENR_ETHMACENTX (1 << 15)
#define RCC_AHBENR_ETHMACEN   (1 << 14)
#define RCC_AHBENR_OTGFSEN    (1 << 12)
#define RCC_AHBENR_SDIOEN     (1 << 10)
#define RCC_AHBENR_FSMCEN     (1 << 8)
#define RCC_AHBENR_CRCEN      (1 << 6)
#define RCC_AHBENR_FLITFEN    (1 << 4)
#define RCC_AHBENR_SRAMEN     (1 << 2)
#define RCC_AHBENR_DMA2EN     (1 << 1)
#define RCC_AHBENR_DMA1EN     (1 << 0)
/**@}*/

/* --- RCC_APB2ENR values -------------------------------------------------- */

/** @defgroup rcc_apb2enr_en RCC_APB2ENR enable values
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_APB2ENR_TIM17EN   (1 << 17)
#define RCC_APB2ENR_TIM16EN   (1 << 16)
#define RCC_APB2ENR_TIM15EN   (1 << 16)
#define RCC_APB2ENR_ADC3EN    (1 << 15) /* (XX) */
#define RCC_APB2ENR_USART1EN  (1 << 14)
#define RCC_APB2ENR_TIM8EN    (1 << 13) /* (XX) */
#define RCC_APB2ENR_SPI1EN    (1 << 12)
#define RCC_APB2ENR_TIM1EN    (1 << 11)
#define RCC_APB2ENR_ADC2EN    (1 << 10)
#define RCC_APB2ENR_ADC1EN    (1 << 9)
#define RCC_APB2ENR_IOPGEN    (1 << 8)  /* (XX) */
#define RCC_APB2ENR_GPIOGEN   (1 << 8)  /* (XX) */
#define RCC_APB2ENR_IOPFEN    (1 << 7)  /* (XX) */
#define RCC_APB2ENR_GPIOFEN   (1 << 7)  /* (XX) */
#define RCC_APB2ENR_IOPEEN    (1 << 6)
#define RCC_APB2ENR_GPIOEEN   (1 << 6)
#define RCC_APB2ENR_IOPDEN    (1 << 5)
#define RCC_APB2ENR_GPIODEN   (1 << 5)
#define RCC_APB2ENR_IOPCEN    (1 << 4)
#define RCC_APB2ENR_GPIOCEN   (1 << 4)
#define RCC_APB2ENR_IOPBEN    (1 << 3)
#define RCC_APB2ENR_GPIOBEN   (1 << 3)
#define RCC_APB2ENR_IOPAEN    (1 << 2)
#define RCC_APB2ENR_GPIOAEN   (1 << 2)
#define RCC_APB2ENR_AFIOEN    (1 << 0)
/**@}*/

/* --- RCC_APB1ENR values -------------------------------------------------- */

/** @defgroup rcc_apb1enr_en RCC_APB1ENR enable values
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_APB1ENR_CECEN     (1 << 30)
#define RCC_APB1ENR_DACEN     (1 << 29)
#define RCC_APB1ENR_PWREN     (1 << 28)
#define RCC_APB1ENR_BKPEN     (1 << 27)
#define RCC_APB1ENR_CAN2EN    (1 << 26) /* (**) */
#define RCC_APB1ENR_CAN1EN    (1 << 25) /* (**) */
#define RCC_APB1ENR_CANEN     (1 << 25) /* (XX) Alias for CAN1EN */
#define RCC_APB1ENR_USBEN     (1 << 23) /* (XX) */
#define RCC_APB1ENR_I2C2EN    (1 << 22)
#define RCC_APB1ENR_I2C1EN    (1 << 21)
#define RCC_APB1ENR_UART5EN   (1 << 20)
#define RCC_APB1ENR_UART4EN   (1 << 19)
#define RCC_APB1ENR_USART3EN  (1 << 18)
#define RCC_APB1ENR_USART2EN  (1 << 17)
#define RCC_APB1ENR_SPI3EN    (1 << 15)
#define RCC_APB1ENR_SPI2EN    (1 << 14)
#define RCC_APB1ENR_WWDGEN    (1 << 11)
#define RCC_APB1ENR_TIM14EN   (1 << 8)
#define RCC_APB1ENR_TIM13EN   (1 << 7)
#define RCC_APB1ENR_TIM12EN   (1 << 6)
#define RCC_APB1ENR_TIM7EN    (1 << 5)
#define RCC_APB1ENR_TIM6EN    (1 << 4)
#define RCC_APB1ENR_TIM5EN    (1 << 3)
#define RCC_APB1ENR_TIM4EN    (1 << 2)
#define RCC_APB1ENR_TIM3EN    (1 << 1)
#define RCC_APB1ENR_TIM2EN    (1 << 0)
/**@}*/

/* --- RCC_BDCR values ----------------------------------------------------- */

#define RCC_BDCR_BDRST        (1 << 16)
#define RCC_BDCR_RTCEN        (1 << 15)
/* RCC_BDCR[9:8]: RTCSEL */
#define RCC_BDCR_RTCSEL_SHIFT 8
#define RCC_BDCR_RTCSEL       (3 << RCC_BDCR_RTCSEL_SHIFT)
#define RCC_BDCR_RTCSEL_NOCLK (0 << RCC_BDCR_RTCSEL_SHIFT)
#define RCC_BDCR_RTCSEL_LSE   (1 << RCC_BDCR_RTCSEL_SHIFT)
#define RCC_BDCR_RTCSEL_LSI   (2 << RCC_BDCR_RTCSEL_SHIFT)
#define RCC_BDCR_RTCSEL_HSE_DIV128 (3 << RCC_BDCR_RTCSEL_SHIFT)
#define RCC_BDCR_LSEBYP       (1 << 2)
#define RCC_BDCR_LSERDY       (1 << 1)
#define RCC_BDCR_LSEON        (1 << 0)

/* --- RCC_CSR values ------------------------------------------------------ */

#define RCC_CSR_LPWRRSTF      (1 << 31)
#define RCC_CSR_WWDGRSTF      (1 << 30)
#define RCC_CSR_IWDGRSTF      (1 << 29)
#define RCC_CSR_SFTRSTF       (1 << 28)
#define RCC_CSR_PORRSTF       (1 << 27)
#define RCC_CSR_PINRSTF       (1 << 26)
#define RCC_CSR_RMVF          (1 << 24)
#define RCC_CSR_RESET_FLAGS   (RCC_CSR_LPWRRSTF   \
                               | RCC_CSR_WWDGRSTF \
                               | RCC_CSR_IWDGRSTF \
                               | RCC_CSR_SFTRSTF  \
                               | RCC_CSR_PORRSTF  \
                               | RCC_CSR_PINRSTF)
#define RCC_CSR_LSIRDY        (1 << 1)
#define RCC_CSR_LSION         (1 << 0)

/* --- RCC_AHBRSTR values -------------------------------------------------- */

/** @defgroup rcc_ahbrstr_rst RCC_AHBRSTR reset values
@ingroup STM32F1xx_rcc_defines

@{*/
#define RCC_AHBRSTR_ETHMACRST     (1 << 14)
#define RCC_AHBRSTR_OTGFSRST      (1 << 12)
/**@}*/

/* --- RCC_CFGR2 values ---------------------------------------------------- */

/* I2S3SRC: I2S3 clock source */
#define RCC_CFGR2_I2S3SRC_SYSCLK       0x0
#define RCC_CFGR2_I2S3SRC_PLL3_VCO_CLK 0x1

/* I2S2SRC: I2S2 clock source */
#define RCC_CFGR2_I2S2SRC_SYSCLK       0x0
#define RCC_CFGR2_I2S2SRC_PLL3_VCO_CLK 0x1
#define RCC_CFGR2_I2S2SRC       (1 << 17)

/* PREDIV1SRC: PREDIV1 entry clock source */
#define RCC_CFGR2_PREDIV1SRC_HSE_CLK   0x0
#define RCC_CFGR2_PREDIV1SRC_PLL2_CLK  0x1
#define RCC_CFGR2_PREDIV1SRC    (1 << 16)

#define RCC_CFGR2_PLL3MUL_SHIFT 12
#define RCC_CFGR2_PLL3MUL       (0xF << RCC_CFGR2_PLL3MUL_SHIFT)

#define RCC_CFGR2_PLL2MUL_SHIFT 8
#define RCC_CFGR2_PLL2MUL       (0xF << RCC_CFGR2_PLL2MUL_SHIFT)

#define RCC_CFGR2_PREDIV2_SHIFT 4
#define RCC_CFGR2_PREDIV2       (0xF << RCC_CFGR2_PREDIV2_SHIFT)

#define RCC_CFGR2_PREDIV1_SHIFT 0
#define RCC_CFGR2_PREDIV1       (0xF << RCC_CFGR2_PREDIV1_SHIFT)

/* PLL3MUL: PLL3 multiplication factor */
#define RCC_CFGR2_PLL3MUL_PLL3_CLK_MUL8   0x6
#define RCC_CFGR2_PLL3MUL_PLL3_CLK_MUL9   0x7
#define RCC_CFGR2_PLL3MUL_PLL3_CLK_MUL10  0x8
#define RCC_CFGR2_PLL3MUL_PLL3_CLK_MUL11  0x9
#define RCC_CFGR2_PLL3MUL_PLL3_CLK_MUL12  0xa
#define RCC_CFGR2_PLL3MUL_PLL3_CLK_MUL13  0xb
#define RCC_CFGR2_PLL3MUL_PLL3_CLK_MUL14  0xc
#define RCC_CFGR2_PLL3MUL_PLL3_CLK_MUL16  0xe
#define RCC_CFGR2_PLL3MUL_PLL3_CLK_MUL20  0xf

/* PLL2MUL: PLL2 multiplication factor */
#define RCC_CFGR2_PLL2MUL_PLL2_CLK_MUL8   0x6
#define RCC_CFGR2_PLL2MUL_PLL2_CLK_MUL9   0x7
#define RCC_CFGR2_PLL2MUL_PLL2_CLK_MUL10  0x8
#define RCC_CFGR2_PLL2MUL_PLL2_CLK_MUL11  0x9
#define RCC_CFGR2_PLL2MUL_PLL2_CLK_MUL12  0xa
#define RCC_CFGR2_PLL2MUL_PLL2_CLK_MUL13  0xb
#define RCC_CFGR2_PLL2MUL_PLL2_CLK_MUL14  0xc
#define RCC_CFGR2_PLL2MUL_PLL2_CLK_MUL16  0xe
#define RCC_CFGR2_PLL2MUL_PLL2_CLK_MUL20  0xf

/* PREDIV: PREDIV1 division factor */
#define RCC_CFGR2_PREDIV1_NODIV    (0x0 << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV1     (0x0 << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV2     (0x1 << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV3     (0x2 << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV4     (0x3 << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV5     (0x4 << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV6     (0x5 << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV7     (0x6 << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV8     (0x7 << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV9     (0x8 << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV10    (0x9 << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV11    (0xa << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV12    (0xb << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV13    (0xc << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV14    (0xd << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV15    (0xe << RCC_CFGR2_PREDIV1_SHIFT)
#define RCC_CFGR2_PREDIV1_DIV16    (0xf << RCC_CFGR2_PREDIV1_SHIFT)

/* PREDIV2: PREDIV2 division factor */
#define RCC_CFGR2_PREDIV2_NODIV    (0x0 << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV1     (0x0 << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV2     (0x1 << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV3     (0x2 << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV4     (0x3 << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV5     (0x4 << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV6     (0x5 << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV7     (0x6 << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV8     (0x7 << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV9     (0x8 << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV10    (0x9 << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV11    (0xa << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV12    (0xb << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV13    (0xc << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV14    (0xd << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV15    (0xe << RCC_CFGR2_PREDIV2_SHIFT)
#define RCC_CFGR2_PREDIV2_DIV16    (0xf << RCC_CFGR2_PREDIV2_SHIFT)

/**@}*/
#endif /* STM32F1_RCC_H */
