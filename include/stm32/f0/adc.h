/** @defgroup adc_defines ADC Defines
 *
 * @brief <b>Defined Constants and Types for the STM32F0xx Analog to Digital
 * Converter</b>
 *
 * @ingroup STM32F0xx_defines
 *
 * @version 1.0.0
 *
 * @date 11 July 2013
 *
 * LGPL License Terms @ref lgpl_license
 */
/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2013 Frantisek Burian <BuFran@seznam.cz>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STM32F0_ADC_H
#define STM32F0_ADC_H
/**@{*/

#include <stm32/all/adc_v2.h>

/*****************************************************************************/
/* Module definitions                                                        */
/*****************************************************************************/

/** @defgroup adc_reg_base ADC register base addresses
 *  @ingroup adc_defines
 *
 *@{*/
#define ADC         ADC_BASE
#define ADC1        ADC_BASE /* for API compatibility */
/**@}*/

/*****************************************************************************/
/* Register values                                                           */
/*****************************************************************************/

/* ADC_CFGR1 Values ---------------------------------------------------------*/

#define ADC_CFGR1_AUTOOFF           (1 << 15)

/* ADC_CFGR2 Values ---------------------------------------------------------*/

#define ADC_CFGR2_CKMODE_SHIFT      30
#define ADC_CFGR2_CKMODE            (3 << ADC_CFGR2_CKMODE_SHIFT)
#define ADC_CFGR2_CKMODE_CK_ADC     (0 << ADC_CFGR2_CKMODE_SHIFT)
#define ADC_CFGR2_CKMODE_HSI14      (0 << ADC_CFGR2_CKMODE_SHIFT)
#define ADC_CFGR2_CKMODE_PCLK_DIV2  (1 << ADC_CFGR2_CKMODE_SHIFT)
#define ADC_CFGR2_CKMODE_APB_DIV2   (1 << ADC_CFGR2_CKMODE_SHIFT)
#define ADC_CFGR2_CKMODE_PCLK_DIV4  (2 << ADC_CFGR2_CKMODE_SHIFT)
#define ADC_CFGR2_CKMODE_APB_DIV4   (2 << ADC_CFGR2_CKMODE_SHIFT)

#define ADC_CFGR1_EXTSEL_TIM1_TRGO  ADC_CFGR1_EXTSEL_VAL(0)
#define ADC_CFGR1_EXTSEL_TIM1_CC4   ADC_CFGR1_EXTSEL_VAL(1)
#define ADC_CFGR1_EXTSEL_TIM3_TRGO  ADC_CFGR1_EXTSEL_VAL(3)
#define ADC_CFGR1_EXTSEL_TIM15_TRGO ADC_CFGR1_EXTSEL_VAL(4)

/* ADC_SMPR Values ----------------------------------------------------------*/

#define ADC_SMPR_SMP_SHIFT  0
#define ADC_SMPR_SMP        (7 << ADC_SMPR_SMP_SHIFT)
#define ADC_SMPR_SMP_1_5    (0 << ADC_SMPR_SMP_SHIFT)
#define ADC_SMPR_SMP_7_5    (1 << ADC_SMPR_SMP_SHIFT)
#define ADC_SMPR_SMP_13_5   (2 << ADC_SMPR_SMP_SHIFT)
#define ADC_SMPR_SMP_28_5   (3 << ADC_SMPR_SMP_SHIFT)
#define ADC_SMPR_SMP_41_5   (4 << ADC_SMPR_SMP_SHIFT)
#define ADC_SMPR_SMP_55_5   (5 << ADC_SMPR_SMP_SHIFT)
#define ADC_SMPR_SMP_71_5   (6 << ADC_SMPR_SMP_SHIFT)
#define ADC_SMPR_SMP_239_5  (7 << ADC_SMPR_SMP_SHIFT)

/** @defgroup adc_channel ADC Channel Numbers
 * @ingroup adc_defines
 *
 *@{*/
#define ADC_CHANNEL_TEMP  16
#define ADC_CHANNEL_VREF  17
#define ADC_CHANNEL_VBAT  18
/**@}*/

/** @defgroup adc_cal_regs ADC Calibration Registers
 * @ingroup adc_defines
 *
 *@{*/
#define ADC_CALR(VAL) MMIO16(INFO_BASE + (VAL))
#define ADC_CAL_TEMP_30C_AT_3V3  0x07b8
#define ADC_CAL_TEMP_110C_AT_3V3 0x07c2
/**@}*/

/**@}*/
#endif /* STM32F0_ADC_H */
