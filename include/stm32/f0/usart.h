/** @defgroup usart_defines USART Defines
 *
 * @brief <b>Defined Constants and Types for the STM32F0xx USART</b>
 *
 * @ingroup STM32F0xx_defines
 *
 * @version 1.0.0
 *
 * @date 2 July 2013
 *
 * LGPL License Terms @ref lgpl_license
 */

/*
 * This file is part of the libopencm3 project.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STM32F0_USART_H
#define STM32F0_USART_H

/*****************************************************************************/
/* Module definitions                                                        */
/*****************************************************************************/

#define USART4        USART4_BASE

/*****************************************************************************/
/* Register definitions                                                      */
/*****************************************************************************/

#define USART_CR1(usart_base)   MMIO32((usart_base) + 0x00)
#define USART_CR2(usart_base)   MMIO32((usart_base) + 0x04)
#define USART_CR3(usart_base)   MMIO32((usart_base) + 0x08)
#define USART_BRR(usart_base)   MMIO32((usart_base) + 0x0c)
#define USART_GTPR(usart_base)  MMIO32((usart_base) + 0x10)
#define USART_RTOR(usart_base)  MMIO32((usart_base) + 0x14)
#define USART_RQR(usart_base)   MMIO32((usart_base) + 0x18)
#define USART_ISR(usart_base)   MMIO32((usart_base) + 0x1c)
#define USART_ICR(usart_base)   MMIO32((usart_base) + 0x20)
#define USART_RDR(usart_base)   MMIO16((usart_base) + 0x24)
#define USART_TDR(usart_base)   MMIO16((usart_base) + 0x28)

/*****************************************************************************/
/* Register values                                                           */
/*****************************************************************************/

/* USART_CR1 Values ---------------------------------------------------------*/

#define USART_CR1_M1        (1 << 28) /* F07x */
#define USART_CR1_EOBIE     (1 << 27)
#define USART_CR1_RTOIE     (1 << 26)

#define USART_CR1_DEAT_SHIFT    21
#define USART_CR1_DEAT          (0x1F << USART_CR1_DEAT_SHIFT)
#define USART_CR1_DEAT_VAL(x)   ((x) << USART_CR1_DEAT_SHIFT)

#define USART_CR1_DEDT_SHIFT    16
#define USART_CR1_DEDT          (0x1F << USART_CR1_DEDT_SHIFT)
#define USART_CR1_DEDT_VAL(x)   ((x) << USART_CR1_DEDT_SHIFT)

#define USART_CR1_OVER8     (1 << 15)
#define USART_CR1_CMIE      (1 << 14)
#define USART_CR1_MME       (1 << 13)
#define USART_CR1_M         (1 << 12) /* Obsolete, please use M0 */
#define USART_CR1_M0        (1 << 12)
#define USART_CR1_WAKE      (1 << 11)
#define USART_CR1_PCE       (1 << 10)
#define USART_CR1_PS        (1 << 9)
#define USART_CR1_PEIE      (1 << 8)
#define USART_CR1_TXEIE     (1 << 7)
#define USART_CR1_TCIE      (1 << 6)
#define USART_CR1_RXNEIE    (1 << 5)
#define USART_CR1_IDLEIE    (1 << 4)
#define USART_CR1_TE        (1 << 3)
#define USART_CR1_RE        (1 << 2)
#define USART_CR1_UESM      (1 << 1)
#define USART_CR1_UE        (1 << 0)

/* USART_CR2 Values ---------------------------------------------------------*/

#define USART_CR2_ADD_SHIFT         24
#define USART_CR2_ADD(x)            ((x) << USART_CR2_ADD_SHIFT)
#define USART_CR2_ADD_MASK          USART_CR2_ADD(0xff)

#define USART_CR2_RTOEN             (1 << 23)

#define USART_CR2_ABRMOD_SHIFT        21
#define USART_CR2_ABRMOD(x)           ((x) << USART_CR2_ABRMOD_SHIFT)
#define USART_CR2_ABRMOD_START_BIT    USART_CR2_ABRMOD(0)
#define USART_CR2_ABRMOD_FALL_TO_FALL USART_CR2_ABRMOD(1)
#define USART_CR2_ABRMOD_FRAME_7F     USART_CR2_ABRMOD(2)
#define USART_CR2_ABRMOD_FRAME_55     USART_CR2_ABRMOD(3)
#define USART_CR2_ABRMOD_MASK         USART_CR2_ABRMOD(3)

#define USART_CR2_ABREN     (1 << 20)
#define USART_CR2_MSBFIRST  (1 << 19)
#define USART_CR2_DATAINV   (1 << 18)
#define USART_CR2_TXINV     (1 << 17)
#define USART_CR2_RXINV     (1 << 16)
#define USART_CR2_SWAP      (1 << 15)
#define USART_CR2_LINEN     (1 << 14)

#define USART_CR2_STOP_SHIFT    12
#define USART_CR2_STOP(x)       ((x) << USART_CR2_STOP_SHIFT)
#define USART_CR2_STOP_1        USART_CR2_STOP(0)
#define USART_CR2_STOP_2        USART_CR2_STOP(2)
#define USART_CR2_STOP_1_5      USART_CR2_STOP(3)
#define USART_CR2_STOP_MASK     USART_CR2_STOP(3)

#define USART_CR2_CLKEN     (1 << 11)
#define USART_CR2_CPOL      (1 << 10)
#define USART_CR2_CPHA      (1 << 9)
#define USART_CR2_LBCL      (1 << 8)
#define USART_CR2_LBIDE     (1 << 6)
#define USART_CR2_LBDL      (1 << 5)
#define USART_CR2_ADDM      (1 << 4) /* Obsolete, use ADDM7 */
#define USART_CR2_ADDM7     (1 << 4)

/* USART_CR3 Values ---------------------------------------------------------*/

#define USART_CR3_WUFIE     (1 << 22)

#define USART_CR3_WUS_SHIFT       20
#define USART_CR3_WUS             (3 << USART_CR3_WUS_SHIFT)
#define USART_CR3_WUS_ADDRMATCH   (0 << USART_CR3_WUS_SHIFT)
#define USART_CR3_WUS_STARTBIT    (2 << USART_CR3_WUS_SHIFT)
#define USART_CR3_WUS_RXNE        (3 << USART_CR3_WUS_SHIFT)

#define USART_CR3_SCARCNT_SHIFT   17
#define USART_CR3_SCARCNT         (7 << USART_CR3_SCARCNT_SHIFT)
#define USART_CR3_SCARCNT_DISABLE (0 << USART_CR3_SCARCNT_SHIFT)
#define USART_CR3_SCARCNT_VAL(x)  ((x) << USART_CR3_SCARCNT_SHIFT)

#define USART_CR3_DEP       (1 << 15)
#define USART_CR3_DEM       (1 << 14)
#define USART_CR3_DDRE      (1 << 13)
#define USART_CR3_OVRDIS    (1 << 12)
#define USART_CR3_ONEBIT    (1 << 11)
#define USART_CR3_CTSIE     (1 << 10)
#define USART_CR3_CTSE      (1 << 9)
#define USART_CR3_RTSE      (1 << 8)
#define USART_CR3_DMAT      (1 << 7)
#define USART_CR3_DMAR      (1 << 6)
#define USART_CR3_SCEN      (1 << 5)
#define USART_CR3_NACK      (1 << 4)
#define USART_CR3_HDSEL     (1 << 3)
#define USART_CR3_IRLP      (1 << 2)
#define USART_CR3_IREN      (1 << 1)
#define USART_CR3_EIE       (1 << 0)

/* USART_GTPR Values --------------------------------------------------------*/

#define USART_GTPR_GT_SHIFT     8
#define USART_GTPR_GT(x)        ((x) << USART_GTPR_GT_SHIFT)
#define USART_GTPR_GT_MASK      USART_GTPR_GT(0xff)

#define USART_GTPR_PSC_SHIFT    0
#define USART_GTPR_PSC(x)       ((x) << USART_GTPR_PSC_SHIFT)
#define USART_GTPR_PSC_MASK     USART_GTPR_PSC(0xff)

/* USART_RQR Values ---------------------------------------------------------*/

#define USART_RQR_TXFRQ     (1 << 4)
#define USART_RQR_RXFRQ     (1 << 3)
#define USART_RQR_MMRQ      (1 << 2)
#define USART_RQR_SBKRQ     (1 << 1)
#define USART_RQR_ABRRQ     (1 << 0)

/* USART_ISR Values ---------------------------------------------------------*/

#define USART_ISR_REACK     (1 << 22)
#define USART_ISR_TEACK     (1 << 21)
#define USART_ISR_WUF       (1 << 20)
#define USART_ISR_RWU       (1 << 19)
#define USART_ISR_SBKF      (1 << 18)
#define USART_ISR_CMF       (1 << 17)
#define USART_ISR_BUSY      (1 << 16)
#define USART_ISR_ABRF      (1 << 15)
#define USART_ISR_ABRE      (1 << 14)
#define USART_ISR_EOBF      (1 << 12)
#define USART_ISR_RTOF      (1 << 11)
#define USART_ISR_CTS       (1 << 10)
#define USART_ISR_CTSIF     (1 << 9)
#define USART_ISR_LBDF      (1 << 8)
#define USART_ISR_TXE       (1 << 7)
#define USART_ISR_TC        (1 << 6)
#define USART_ISR_RXNE      (1 << 5)
#define USART_ISR_IDLE      (1 << 4)
#define USART_ISR_ORE       (1 << 3)
#define USART_ISR_NF        (1 << 2)
#define USART_ISR_FE        (1 << 1)
#define USART_ISR_PE        (1 << 0)

/* USART_ICR Values ---------------------------------------------------------*/

#define USART_ICR_WUCF      (1 << 20)
#define USART_ICR_CMCF      (1 << 17)
#define USART_ICR_EOBCF     (1 << 12)
#define USART_ICR_RTOCF     (1 << 11)
#define USART_ICR_CTSCF     (1 << 9)
#define USART_ICR_LBDCF     (1 << 8)
#define USART_ICR_TCCF      (1 << 6)
#define USART_ICR_IDLECF    (1 << 4)
#define USART_ICR_ORECF     (1 << 3)
#define USART_ICR_NCF       (1 << 2)
#define USART_ICR_FECF      (1 << 1)
#define USART_ICR_PECF      (1 << 0)

#endif /* STM32F0_USART_H */
