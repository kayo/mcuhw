## Hardware layer for microcontrollers

The main goal of this project is providing lightweight hardware abstraction layer for bare metal embedded applications.

In order to reach that purpose we required to have a huge doze of metaprogramming here. In that solution used code generator, which produces low-level platform-specific code using hardware definition.

### Overview

The starting point for this project was a great library [libopencm3](http://libopencm3.org/). But optimized code isn't its strong side. Also it has some properties which obstruct portability of user's code.

I decided to use template processing to flexibly generate platform dependent code. I selected [GNU Autogen](https://www.gnu.org/software/autogen/) because it allows to use easy to read definition files and implements strong template language which designed especially for code generation task.

This project is at an early stage now, so many device drivers waiting to be implemented.

### Key features

* Flexible configuration
* Simple portability
* Highly optimized code

### Implemented drivers

* Platform interface (mcu)
* Interrupt handling (irq)
* Clock configuration (clk)
* Ports configuration (pio)
* Analog-to-digital convertor driver with DMA (adc)
* Serial Receiver-Transmitter (USART) driver (srt)
* Serial Peripharal Interface driver (spi)

### Supported platforms

* stm32f0
* stm32f1
* other platforms which supported by *libopencm3*

### How to use it

This is an example definition file `hardware.def`:

```c
autogen definitions mcuhw; // required for autogen

target = stm32f103rbt6; // required for determining hardware platform and capabilities

memory = { // memory configuration
  region = {
    // reserving 2 KBytes (2048 Bytes) for bootloader at beginning of flash memory
    id = ldr; bank = rom; size = 2, Kb; align = start;
  };
  region = {
    // reserving 1 KByte (1024 Bytes) for configuration area at end of flash memory
    id = cfg; mode = r; bank = rom; size = 1, Kb; align = end;
  };
};

hwdef = { // hardware definition group
  id = "system"; // optional group id which will be used as prefix for functions and definitions
    
  clk = { // clock configuration
    /*
      HSI HSI14 HSI48
      HSE:frequency(4..32MHz)
      LSI
      LSE:frequency(KHz)
      PLL:source(HSE|HSI/2),multiplier(2..16)[,prediv(1..16)]
      SYS:source(HSI|HSE|PLL)
      AHB:divider(SYS 1|2|4|8|16|64|128|256|512)
      APB:divider(AHB 1|2|4|8|16)
      STK:divider(1|8)
      RTC:source(LSE|LSI|HSE/32)
      ADC:source(HSI14|APB)[,divider(for APB 2|4)]
    */
    
    HSE = { // configure high speed external oscillator
      frequency = 8, MHz; // use 8 MHz crystal
    };
      
    CSS = on; // activate clock security system
    
    PLL = { // configure phase-locked loop unit
      source = HSE; // clocking from high-speed external oscillator
      multiplier = 7; // mupliply clock by 7
      divider = 1; // do not use pre-divider
    };
    
    SYS = { // configure system clock
      source = PLL; // clocking from phase-locked loop
    };

    AHB = { // configure AHB
      divider = 1; // no divide SYS clock
    };
    
    APB1 = { // configure APB1
      divider = 2; // divide AHB clock by 2
    };

    APB2 = { // configure APB2
      divider = 2; // divide AHB clock by 2
    };
    
    LSE = { // configure low-speed external oscillator
      frequency = 32768, Hz; // use 32.768 KHz crystal
    };
      
    RTC = { // configure realtime clock
      source = LSE; // clocking from low-speed oscillator
    };
    
    STK = { // run systick clock unit
      period = 10, mS; // configure 10mS period
      on-tick; // use tick event handler
    };
    
    ADC = { // clock ADC
      source  = APB/2; // clocking from APB/2
    };
    
    USART1; // clock USART
    
    PORTA; // clock GPIO ports
    PORTB;

    AFIO;  // clock alternative function i/o
  };

  pio = { // i/o ports configuration
    /*
      <port>[<pad>] = { <configuration> };

      id = <identifier>
      input = analog|float|pullup|pulldown
      output = analog|pushpull|opendrain
      speed = 2|10|50, MHz or low|medium|high
      active = high|low or true|false or 1|0
      initial = high|low ... or active|inactive
     */

    // analog input
    A[2] = { id = Ubat; input = analog; };
    
    // digital push-pull output, 2MHz speed, not inverted
    B[5] = { id = out_ctl; output = pushpull; speed = 2, MHz; active = high; };
    
    // digital open-drain output, medium speed, inverted, turned off on init
    B[6] = { id = ~status_led; output = opendrain; speed = medium; active = low; initial = inactive; };
    
    // RS485/UART ports
    // digital push-pull output, used as USART1 TX
    A[9] = { id = UART1_TX; output = pushpull; speed = 50, MHz; af = on; };
    // digital float input, used as USART1 RX
    A[10] = { id = UART1_RX; input = float; af = on; };
    
    // PWM push-pull output, 10MHz speed
    B[1] = { id = D_SUP; output = pushpull; speed = 10, MHz; af = on; };

    // digital pull-up input
    B[3] = { id = P_BTN; input = pullup; active = low; edge = both; on-edge; };
  };
};
```

This is an example C source file `firmware.c`:

```c
#include "hardware.h"

void system_clk_on_tick(void) {
  // do something
}

void system_pio_P_BTN_on_edge(void) {
  // do something
}

int main() {
  system_irq_init(); // initialize interrupt queries
  system_clk_init(); // initialize clock
  system_pio_init(); // initialize ports
  /* ... */

  size_t cfg_size = // get the size of configuration area
    (const uint8_t*)&_cfg_end
    - (const uint8_t*)&_cfg_start;
  
  bool button_state = system_pio_P_BTN_get(); // read button state
  system_pio_led_status_put(button_state); // write led status
  /* ... */

  system_pio_done(); // finalize ports
  system_clk_done(); // finalize clock
  system_irq_done(); // finalize interrupt queries
}
```

### How it works

After invoking AutoGen we will have three files:

* `hardware.h` which contains API definition
* `hardware.c` which contains API implementation, some initialization code, vector table and other boilerplate code (you don't need worry about it)
* `hardware.ld` which is a linker script

### Usage examples

Now you can look at sources into `tests/` directory to get a notion, how the hardware configuration can be defined and used.
