ocd.gdb_port ?= localhost:3333
ocd.iface ?= jlink
ocd.proto ?= swd
ocd.target ?= stm32f1x

OCD_BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))
OCD_EXTRA.stm32f0x := $(OCD_BASEPATH)stm32f0x.cfg

debug.GDBOPTS := -ex 'target remote $(ocd.gdb_port)'

OCD_COMMAND = $(Q)openocd \
  -c "source [find interface/$(ocd.iface).cfg]" \
  $(if $(OCD_TRANSPORT),-c "transport select $(ocd.proto)") \
  -c "source [find target/$(ocd.target).cfg]" \
  $(if $(OCD_EXTRA.$(ocd.target)),-c "source $(OCD_EXTRA.$(ocd.target))")

define FLASH_RULES
flash.img.$(1): $$($(1).BIN)
	$(OCD_COMMAND) -c "init" -c "program $$< verify reset" -c "shutdown"
endef

options:
	$(OCD_COMMAND) -c "init" -c "reset" -c "halt" -c "$(ocd.target) options_read 0" -c "shutdown"

protect:
	$(OCD_COMMAND) -c "init" -c "halt" -c "flash protect 0 0 last on" -c "reset run" -c "shutdown"

unprotect:
	$(OCD_COMMAND) -c "init" -c "halt" -c "flash protect 0 0 last off" -c "reset run" -c "shutdown"

restart:
	$(OCD_COMMAND) -c "init" -c "reset init" -c "reset run" -c "shutdown"

debug:
	$(OCD_COMMAND) -c "init" -c "reset init"

start.debug:
	$(OCD_COMMAND) -c "init" -c "reset init" &

stop.debug:
	$(Q)killall openocd

attach:
	$(OCD_COMMAND)
