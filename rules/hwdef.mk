hwdef.BASEPATH := $(subst $(dir $(abspath $(CURDIR)/xyz)),,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
hwdef.CDIRS := $(hwdef.BASEPATH)../include
hwdef.TDIRS := $(hwdef.BASEPATH)../src
hwdef.SVDDIR := $(hwdef.BASEPATH)../svd

# library, config path, file type -> file path
hwdef.GEN_P = gen/$(1)/hwd/$(notdir $(basename $(2))).$(3)
hwdef.AG = GUILE_AUTO_COMPILE=0 autogen $(addprefix -L,$(hwdef.TDIRS)) -T$(1) $(2)$(if $(3), > $(3))

# library, config, type
define HWD_GEN
hwdef.$(1).$(2).$(3) := $(call hwdef.GEN_P,$(1),$(2),$(3))
hwdef.$(1).$(2) += $$(hwdef.$(1).$(2).$(3))
$$(hwdef.$(1).$(2).$(3)): $(2)
	@echo TARGET $(1) HWDEF $(2) GEN $(3)
	$(Q)mkdir -p $$(dir $$@)
	$(Q)$$(call hwdef.AG,$(3),$$<,$$@)
endef

# library, config
define HWD_RULE
ifndef hwdef.$(1).$(2)
hwdef.$(1).$(2) :=
$(foreach t,h c ld,
$(call HWD_GEN,$(1),$(2),$(t)))
$(1).CDIRS += $(hwdef.CDIRS)
$(1).CDIRS += $$(dir $$(hwdef.$(1).$(2).h))
$(1).SRCS += $$(hwdef.$(1).$(2).c)
$(1).LDSCRIPT := $$(hwdef.$(1).$(2).ld)
$(1).GDBOPTS := \
  -ex 'source $(hwdef.SVDDIR)/gdb.py' \
  -ex 'svd-use $(shell $(call hwdef.AG,t,$(2)))' \
#  -ex 'dbgmcu flags set sleep stop standby'
build.hwdef: $$(hwdef.$(1).$(2))
$$($(1).SRCS): $$(hwdef.$(1).$(2).h)
clean: clean.hwdef.$(1).$(2)
clean.hwdef.$(1).$(2):
	@echo TARGET $(1) HWDEF $(2) CLEAN
	$(Q)rm -f $$(hwdef.$(1).$(2))
endif
endef

# library
define HWD_RULES
ifneq (,$($(1).HWDS))
$(foreach f,$($(1).HWDS),
$(call HWD_RULE,$(1),$(f)))
endif
endef
