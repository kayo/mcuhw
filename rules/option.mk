# library, config path -> c header path
OPT_P = gen/$(1)/opt/$(call FIXPATH,$(2)).h

# operand, arg1, arg2 -> arg1 operand arg2
option-concat = $(if $(and $(2),$(3)),$(2)$(1)$(3),$(or $(2),$(3)))

# find, rep, str -> fixed str
option-subst = $(subst $(strip)$(1),$(2),$(3))

# raw str -> quoted str
option-quote = $(call option-subst,	,\t,$(call option-subst, ,\s,$(1)))

# quoted str -> raw str
option-unquote = $(call option-subst,\t,	,$(call option-subst,\s, ,$(1)))

# raw string[, quotes(|d-"|s-')] -> shell string
# replace all newline characters with \n and wrap string to quotes
option-tostr = $(call option-tostr-$(2),$(1))
option-tostr-s = '$(call option-tostr-,$(1))'
option-tostr-d = "$(call option-tostr-,$(1))"
define option-tostr-
$(call option-subst,
,\n,$(subst $(option-double-quote),\$(option-double-quote),$(subst \n,\\\\n,$(1))))
endef

# first element(1), last element(-1), elements -> sub elements
option-range = $(call option-range-,$(1),$(if $(filter -%,$(2)),$(words $(call option-range,$(patsubst -%,%,$(2)),,$(3))),$(2)),$(3))
option-range- = $(wordlist $(or $(1),1),$(or $(2),$(words $(3))),$(3))

# elements -> unique elements
option-unique = $(if $(1),$(call option-unique-next,$(word 1,$(1)),$(call option-range,2,,$(1))))
# element, elements -> unique elements
option-unique-next = $(if $(filter $(1),$(2)),,$(1) )$(call option-unique,$(2))

# path -> config
option-load = $(shell git config --file '$(1)' --list | sed -e 's/ /\\s/g' -e 's/	/\\t/')

# config, group -> filtered config
option-filter = $(strip $(patsubst $(2).%,%,$(filter $(2).%,$(1))))

# config[, group] -> names
option-list = $(call option-unique,$(foreach n,$(subst --var-name-end--=,,$(filter %--var-name-end--=,$(subst =,--var-name-end--= ,$(call option-filter,$(1),$(or $(2),option))))),$(call option-subst, ,.,$(call option-range,1,-2,$(call option-subst,., ,$(n))))))

# config, option, field[, group] -> value
option-get = $(call option-get-,$(1),$(2),$(3),$(or $(4),option))
option-get- = $(call option-unquote,$(strip $(patsubst $(4).$(2).$(3)=%,%,$(filter $(4).$(2).$(3)=%,$(1)))))

# config, option, field[, group] -> exists
option-has = $(call option-has-,$(1),$(2),$(3),$(or $(4),option))
option-has- = $(strip $(filter $(4).$(2).$(3)=%,$(1)))

# config, option
option-value = $(strip $($(2)))

# a, b -> a === b
option-eq = $(call option-eq-,$(strip $(1)),$(strip $(2)))
option-eq- = $(and $(filter x$(1),x$(2)),$(filter x$(2),x$(1)))

option-basepath := $(subst $(dir $(abspath $(CURDIR)/xyz)),,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
include $(wildcard $(option-basepath)/option/*.mk)

# <style> <text>
option-pretty = $(option-style-$(1))$(2)\e[0m\e[39m\e[49m

option-style-type = \e[1m\e[31m
option-style-name = \e[0m\e[94m
option-style-value = \e[0m\e[92m
option-style-key = \e[1m\e[36m
option-style-generated = \e[95m
option-style-default = \e[2m\e[97m
option-style-custom = \e[93m
option-style-info = \e[37m

# config, option
option-info-type = $(call option-pretty,type,$(call option-get,$(1),$(2),type))

# config, option
option-info-name = $(call option-pretty,name,$(2))

# config, option
option-info-value = $(call option-pretty,value,$(call option-value,$(1),$(2)))

# config, option
option-flag = $(if $(call option-has,$(1),$(2),gen),generated,$(if $(call option-has,$(1),$(2),def),$(if $(call option-eq,$(call option-get,$(1),$(2),def),$($(2))),default,custom)))

# config, option
option-info-flag = $(call option-info-flag-,$(call option-flag,$(1),$(2)))
# flag
option-info-flag- = $(if $(1),$(call option-pretty,$(1),[$(1)]))

# config, option
option-info-key = $(call option-info-key-,$(call option-get,$(1),$(2),key))
# value
option-info-key- = $(if $(1),$(call option-pretty,key,\#$(1)\#))

# config, option
option-info-text = $(call option-pretty,info,$(call option-get,$(1),$(2),info))

# config, option
define option-info-short
$(call option-info-name,$(1),$(2)) = $(call option-info-value,$(1),$(2))
endef

# config, option
define option-info-long
$(call option-info-type,$(1),$(2)) $(call option-info-name,$(1),$(2)) = $(call option-info-value,$(1),$(2)) $(call option-info-flag,$(1),$(2)) $(call option-info-key,$(1),$(2))
endef

# config, option
define option-info-detail
$(call option-info-type,$(1),$(2)) $(call option-info-name,$(1),$(2)) = $(call option-info-value,$(1),$(2)) $(call option-info-flag,$(1),$(2)) $(call option-info-key,$(1),$(2))
  $(call option-info-text,$(1),$(2))
endef

# config, format
option-info = $(call option-info-,$(1),$(2),$(call option-list,$(1)))
# config, format, names
define option-info-
$(if $(3),$(call option-concat,
,$(call option-info-$(2),$(1),$(word 1,$(3))),$(call option-info-,$(1),$(2),$(call option-range,2,,$(3)))))
endef

# config -> c header file
#option-c-header = $(call option-c-defines,$(foreach n,$(call option-list,$(1)),$(if $(and $(call option-get,$(1),$(n),key),$(call option-value,$(1),$(n))),$(n))))
option-c-header = $(call option-c-defines,$(1),$(foreach n,$(call option-list,$(1)),$(if $(call option-get,$(1),$(n),key),$(n))))

# config, options -> c header
define option-c-defines
$(if $(2),$(call option-concat,
,$(call option-c-define,$(1),$(word 1,$(2))),$(call option-c-defines,$(1),$(call option-range,2,,$(2)))))
endef
# config, option -> c define
option-c-define = $(call option-put.$(or $(call option-get,$(1),$(2),type),raw),$(call option-get,$(1),$(2),key),$(call option-value,$(1),$(2)))

# key, value -> definition
define option-put.raw
$(if $(1),#define $(1)$(if $(2), $(2)))
endef

# config
define option-defines
$(foreach n,$(call option-list,$(1)),
$(call option-define,$(1),$(n)))
endef

# config, option
define option-define
$(2) ?= $(call option-get,$(1),$(2),def)
endef

.PHONY: show.option info.option

# library, config path, config
define OPT_APPLY
option.$(1).$(2).src := $(3)
$(call option-defines,$(3))
$(1).CINCS += $$(option.$(1).$(2))
$$(option.$(1).$(2)): $(2)
	@echo TARGET $(1) OPT $(2) GEN
	$(Q)mkdir -p $$(dir $$(option.$(1).$(2)))
	$(Q)printf $$(call option-tostr,$$(call option-c-header,$$(option.$(1).$(2).src)),d) > $$(option.$(1).$(2))
clean: clean.option.$(1).$(2)
clean.option.$(1).$(2):
	@echo TARGET $(1) OPT $(2) CLEAN
	$(Q)rm -f $$(option.$(1).$(2))
show.option: show.option.$(1).$(2)
show.option.$(1).$(2):
	$(Q)echo -e $$(call option-tostr,$$(call option-info,$$(option.$(1).$(2).src),short),s)
info.option: info.option.$(1).$(2)
info.option.$(1).$(2):
	$(Q)echo -e $$(call option-tostr,$$(call option-info,$$(option.$(1).$(2).src),detail),s)
endef

# library, config
define OPT_RULE
ifndef option.$(1).$(2)
option.$(1).$(2) := $(call OPT_P,$(1),$(2))
$(call OPT_APPLY,$(1),$(2),$(call option-load,$(2)))
endif
endef

# library
define OPT_RULES
ifneq (,$($(1).OPTS))
$(foreach f,$($(1).OPTS),
$(call OPT_RULE,$(1),$(f)))
endif
endef

option-double-quote = "
