debug.COPT = g
debug.CDBG = gdb3
debug.CDEFS += USE_DEBUG=1

release.COPT = s
release.CDBG = gdb3
release.CDEFS += USE_DEBUG=0
release.COPTS += omit-frame-pointer strict-aliasing

tyrant.CWARN = all extra shadow undef implicit-function-declaration redundant-decls missing-prototypes strict-prototypes no-pointer-sign pointer-arith
gentle.CWARN = error

native.LDOPTS += static
native.LDFLAGS += --static -nostartfiles

stalin.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))
stalin.INHERIT = $(stalin.mode) $(stalin.role) $(stalin.tune)
stalin.CSTD = gnu99
stalin.CDEFS = _GNU_SOURCE
stalin.COPTS = function-sections data-sections no-exceptions no-common
stalin.COPTS += $(if $(call option-true,$(stalin.lto)),lto)
stalin.COPTS += $(if $(call option-true,$(stalin.pgo)),profile-generate)
stalin.LDOPTS += -gc-sections

stalin.OPTS = $(stalin.BASEPATH)stalin.cf
TARGET.OPTS += stalin
