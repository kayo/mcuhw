option-wrap.ipv4 = $(1)

# key, value -> definition
define option-put.ipv4
$(call option-put.raw,$(1),$(call option-wrap.ipv4,$(subst .,$(strip ,),$(2))))
endef

option-wrap.mac = $(1)

# key, value -> definition
define option-put.mac
$(call option-put.raw,$(1),$(call option-wrap.mac,0x$(subst :,$(strip ,0x),$(2))))
endef
