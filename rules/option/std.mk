option-true = $(filter yes on true enable y 1,$(1))

# key, value -> definition
option-put.def = $(call option-put.raw,$(if $(call option-true,$(2)),$(1)))

# key, value -> definition
option-put.opt = $(call option-put.raw,$(1),$(if $(call option-true,$(2)),1,0))

# key, value -> definition
option-put.val = $(call option-put.raw,$(1),$(2))

# key, value -> definition
option-put.str = $(call option-put.raw,$(1),"$(2)")

option-wrap.ver = _($(1))

# key, value -> definition
define option-put.ver
$(call option-put.raw,$(1),$(call option-wrap.ver,$(subst .,$(strip ,),$(2))))
$(call option-put.raw,$(1)_NUM,{$(subst .,$(strip ,),$(2))})
$(call option-put.str,$(1)_STR,$(2))
endef

# key, value -> definition
define option-put.sym
$(call option-put.raw,$(1),$(2))
$(call option-put.str,$(1)_STR,$(2))
endef

# key, value -> definition
define option-put.set
$(call option-put.raw,$(1),$(subst $(strip) ,,$(patsubst %,_(%),$(2))))
endef
