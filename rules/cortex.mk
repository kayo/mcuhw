# presets for compiler, linker and debugger (rules.mk)
cortex-m.INHERIT := debug
cortex-m.CMACH := thumb
#cortex-m.SPECS ?= nosys nano
cortex-m.LDOPTS := -gc-sections static #-verbose
cortex-m.LDFLAGS := --static -nostartfiles -nodefaultlibs -nostdlib
cortex-m.LDFLAGS += -Wl,--whole-archive -lc_nano -lnosys -lgcc -Wl,--no-whole-archive -Wl,--allow-multiple-definition
cortex-m.DUMPOPTS := -b elf32-littlearm -m arm -M force-thumb

cortex-m0.INHERIT := cortex-m
cortex-m0.CMACH ?= cpu=cortex-m0 soft-float

cortex-m3.INHERIT := cortex-m
cortex-m3.CMACH ?= cpu=cortex-m3 soft-float fix-cortex-m3-ldrd

cortex-m4.INHERIT := cortex-m
cortex-m4.CMACH ?= cpu=cortex-m4 float-abi=hard fpu=fpv4-sp-d16

# target
cortex-from-target = $(strip $(foreach t,$(cortex.targets),$(if $(filter $(word 1,$(subst :, ,$(t))),$(subst _,,$(subst :,,$(subst /,,$(1))))),cortex-$(word 2,$(subst :, ,$(t))))))

cortex.targets = \
  stm32f0%:m0 \
  stm32f1%:m3 \
  stm32f2%:m3 \
  stm32f3%:m4 \
  stm32f4%:m4
