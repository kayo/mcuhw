DBG_BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))
DBG_LSUSB := $(shell lsusb)

ifndef debug.tool
  # try to detect debugging tool automatically
  ifneq ($(findstring SEGGER J-Link,$(DBG_LSUSB)),)
    debug.tool := ocd
  else
    ifneq ($(findstring Black Magic Debug Probe,$(DBG_LSUSB)),)
      debug.tool := bmp
    endif
  endif
endif

ifneq ($(filter ocd openocd OCD OPENOCD,$(debug.tool)),)
  include $(DBG_BASEPATH)openocd.mk
else
  ifneq ($(filter bmp blackmagic BMP BLACKMAGIC,$(debug.tool)),)
    include $(DBG_BASEPATH)blackmagic.mk
  endif
endif

debug.GDBOPTS += -ex 'attach 1'
debug.GDBOPTS += -ex 'set mem inaccessible-by-default off'

debug.tool:
	@echo debug.tool = $(debug.tool)
