bmp.gdb_port ?= /dev/ttyBmpGdb
bmp.proto ?= swdp
bmp.tpwr ?= off
bmp.srtt ?= off

BMP_BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

debug.GDBOPTS := -ex 'target extended-remote $(bmp.gdb_port)'
ifneq ($(filter on yes true enable y 1,$(bmp.tpwr)),)
  debug.GDBOPTS += -ex 'monitor tpwr enable' -ex 'shell sleep 1'
endif
debug.GDBOPTS += -ex 'monitor $(bmp.proto)_scan'
ifneq ($(filter on yes true enable y 1,$(bmp.srtt)),)
  debug.GDBOPTS += -ex 'monitor srtt_scan'
endif
